# Install script for directory: /Users/Enterprise/tracefreeios10andlower/FreeRDP/channels

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/audin/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/cliprdr/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/disp/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/drdynvc/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/drive/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/echo/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/parallel/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/rail/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/rdpdr/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/rdpei/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/rdpgfx/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/rdpsnd/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/serial/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/smartcard/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/tsmf/cmake_install.cmake")
  INCLUDE("/Users/Enterprise/tracefreeios10andlower/FreeRDP/channels/client/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

