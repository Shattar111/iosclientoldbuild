# Install script for directory: /Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/winpr" TYPE FILE FILES
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/asn1.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/bcrypt.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/bitstream.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/cmdline.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/collections.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/config.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/credentials.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/credui.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/crt.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/crypto.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/dsparse.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/endian.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/environment.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/error.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/file.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/handle.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/heap.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/ini.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/input.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/interlocked.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/io.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/library.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/locale.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/memory.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/midl.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/ndr.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/nt.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/ntlm.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/path.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/pipe.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/platform.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/pool.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/print.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/registry.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/rpc.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/sam.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/schannel.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/security.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/smartcard.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/spec.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/sspi.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/sspicli.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/stream.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/string.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/synch.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/sysinfo.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/tchar.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/thread.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/timezone.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/windows.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/winhttp.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/winpr.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/winsock.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/wlog.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/wnd.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/wtsapi.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/wtypes.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/winpr" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/winpr/include/winpr/tools" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

