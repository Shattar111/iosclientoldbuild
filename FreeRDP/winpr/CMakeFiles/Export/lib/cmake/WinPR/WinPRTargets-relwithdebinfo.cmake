#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "winpr-asn1" for configuration "RelWithDebInfo"
set_property(TARGET winpr-asn1 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-asn1 PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-asn1.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-asn1 )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-asn1 "${_IMPORT_PREFIX}/lib/libwinpr-asn1.a" )

# Import target "winpr-bcrypt" for configuration "RelWithDebInfo"
set_property(TARGET winpr-bcrypt APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-bcrypt PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS10.3.sdk/usr/lib/libz.dylib;winpr-crt;winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-bcrypt.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-bcrypt )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-bcrypt "${_IMPORT_PREFIX}/lib/libwinpr-bcrypt.a" )

# Import target "winpr-com" for configuration "RelWithDebInfo"
set_property(TARGET winpr-com APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-com PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-com.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-com )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-com "${_IMPORT_PREFIX}/lib/libwinpr-com.a" )

# Import target "winpr-credentials" for configuration "RelWithDebInfo"
set_property(TARGET winpr-credentials APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-credentials PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-credentials.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-credentials )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-credentials "${_IMPORT_PREFIX}/lib/libwinpr-credentials.a" )

# Import target "winpr-credui" for configuration "RelWithDebInfo"
set_property(TARGET winpr-credui APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-credui PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-credui.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-credui )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-credui "${_IMPORT_PREFIX}/lib/libwinpr-credui.a" )

# Import target "winpr-crt" for configuration "RelWithDebInfo"
set_property(TARGET winpr-crt APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-crt PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-crt.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-crt )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-crt "${_IMPORT_PREFIX}/lib/libwinpr-crt.a" )

# Import target "winpr-crypto" for configuration "RelWithDebInfo"
set_property(TARGET winpr-crypto APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-crypto PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-crypto.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-crypto )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-crypto "${_IMPORT_PREFIX}/lib/libwinpr-crypto.a" )

# Import target "winpr-dsparse" for configuration "RelWithDebInfo"
set_property(TARGET winpr-dsparse APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-dsparse PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-dsparse.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-dsparse )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-dsparse "${_IMPORT_PREFIX}/lib/libwinpr-dsparse.a" )

# Import target "winpr-environment" for configuration "RelWithDebInfo"
set_property(TARGET winpr-environment APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-environment PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-error"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-environment.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-environment )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-environment "${_IMPORT_PREFIX}/lib/libwinpr-environment.a" )

# Import target "winpr-error" for configuration "RelWithDebInfo"
set_property(TARGET winpr-error APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-error PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-nt"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-error.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-error )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-error "${_IMPORT_PREFIX}/lib/libwinpr-error.a" )

# Import target "winpr-file" for configuration "RelWithDebInfo"
set_property(TARGET winpr-file APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-file PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-handle;winpr-path;winpr-error;winpr-synch"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-file.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-file )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-file "${_IMPORT_PREFIX}/lib/libwinpr-file.a" )

# Import target "winpr-handle" for configuration "RelWithDebInfo"
set_property(TARGET winpr-handle APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-handle PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-handle.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-handle )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-handle "${_IMPORT_PREFIX}/lib/libwinpr-handle.a" )

# Import target "winpr-heap" for configuration "RelWithDebInfo"
set_property(TARGET winpr-heap APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-heap PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-heap.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-heap )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-heap "${_IMPORT_PREFIX}/lib/libwinpr-heap.a" )

# Import target "winpr-input" for configuration "RelWithDebInfo"
set_property(TARGET winpr-input APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-input PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-input.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-input )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-input "${_IMPORT_PREFIX}/lib/libwinpr-input.a" )

# Import target "winpr-interlocked" for configuration "RelWithDebInfo"
set_property(TARGET winpr-interlocked APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-interlocked PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-handle"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-interlocked.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-interlocked )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-interlocked "${_IMPORT_PREFIX}/lib/libwinpr-interlocked.a" )

# Import target "winpr-io" for configuration "RelWithDebInfo"
set_property(TARGET winpr-io APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-io PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-path"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-io.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-io )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-io "${_IMPORT_PREFIX}/lib/libwinpr-io.a" )

# Import target "winpr-library" for configuration "RelWithDebInfo"
set_property(TARGET winpr-library APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-library PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-library.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-library )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-library "${_IMPORT_PREFIX}/lib/libwinpr-library.a" )

# Import target "winpr-locale" for configuration "RelWithDebInfo"
set_property(TARGET winpr-locale APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-locale PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-locale.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-locale )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-locale "${_IMPORT_PREFIX}/lib/libwinpr-locale.a" )

# Import target "winpr-memory" for configuration "RelWithDebInfo"
set_property(TARGET winpr-memory APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-memory PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-memory.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-memory )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-memory "${_IMPORT_PREFIX}/lib/libwinpr-memory.a" )

# Import target "winpr-nt" for configuration "RelWithDebInfo"
set_property(TARGET winpr-nt APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-nt PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-nt.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-nt )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-nt "${_IMPORT_PREFIX}/lib/libwinpr-nt.a" )

# Import target "winpr-path" for configuration "RelWithDebInfo"
set_property(TARGET winpr-path APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-path PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-heap;winpr-environment"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-path.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-path )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-path "${_IMPORT_PREFIX}/lib/libwinpr-path.a" )

# Import target "winpr-pipe" for configuration "RelWithDebInfo"
set_property(TARGET winpr-pipe APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-pipe PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-synch;winpr-handle;winpr-file"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-pipe.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-pipe )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-pipe "${_IMPORT_PREFIX}/lib/libwinpr-pipe.a" )

# Import target "winpr-pool" for configuration "RelWithDebInfo"
set_property(TARGET winpr-pool APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-pool PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-thread;winpr-synch;winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-pool.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-pool )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-pool "${_IMPORT_PREFIX}/lib/libwinpr-pool.a" )

# Import target "winpr-registry" for configuration "RelWithDebInfo"
set_property(TARGET winpr-registry APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-registry PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-registry.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-registry )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-registry "${_IMPORT_PREFIX}/lib/libwinpr-registry.a" )

# Import target "winpr-rpc" for configuration "RelWithDebInfo"
set_property(TARGET winpr-rpc APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-rpc PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "/Users/Enterprise/tracefreeios10andlower/FreeRDP/external/openssl/lib/libssl.a;/Users/Enterprise/tracefreeios10andlower/FreeRDP/external/openssl/lib/libcrypto.a;/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS10.3.sdk/usr/lib/libz.dylib"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-rpc.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-rpc )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-rpc "${_IMPORT_PREFIX}/lib/libwinpr-rpc.a" )

# Import target "winpr-security" for configuration "RelWithDebInfo"
set_property(TARGET winpr-security APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-security PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-security.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-security )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-security "${_IMPORT_PREFIX}/lib/libwinpr-security.a" )

# Import target "winpr-smartcard" for configuration "RelWithDebInfo"
set_property(TARGET winpr-smartcard APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-smartcard PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-library;winpr-environment;winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-smartcard.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-smartcard )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-smartcard "${_IMPORT_PREFIX}/lib/libwinpr-smartcard.a" )

# Import target "winpr-sspi" for configuration "RelWithDebInfo"
set_property(TARGET winpr-sspi APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-sspi PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS10.3.sdk/usr/lib/libz.dylib;/Users/Enterprise/tracefreeios10andlower/FreeRDP/external/openssl/lib/libssl.a;/Users/Enterprise/tracefreeios10andlower/FreeRDP/external/openssl/lib/libcrypto.a;winpr-crt;winpr-sysinfo;winpr-registry;winpr-crypto;winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-sspi.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-sspi )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-sspi "${_IMPORT_PREFIX}/lib/libwinpr-sspi.a" )

# Import target "winpr-sspicli" for configuration "RelWithDebInfo"
set_property(TARGET winpr-sspicli APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-sspicli PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-sspicli.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-sspicli )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-sspicli "${_IMPORT_PREFIX}/lib/libwinpr-sspicli.a" )

# Import target "winpr-synch" for configuration "RelWithDebInfo"
set_property(TARGET winpr-synch APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-synch PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-handle;winpr-error;winpr-interlocked;winpr-thread;winpr-sysinfo"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-synch.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-synch )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-synch "${_IMPORT_PREFIX}/lib/libwinpr-synch.a" )

# Import target "winpr-sysinfo" for configuration "RelWithDebInfo"
set_property(TARGET winpr-sysinfo APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-sysinfo PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-sysinfo.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-sysinfo )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-sysinfo "${_IMPORT_PREFIX}/lib/libwinpr-sysinfo.a" )

# Import target "winpr-thread" for configuration "RelWithDebInfo"
set_property(TARGET winpr-thread APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-thread PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-path;winpr-handle"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-thread.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-thread )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-thread "${_IMPORT_PREFIX}/lib/libwinpr-thread.a" )

# Import target "winpr-timezone" for configuration "RelWithDebInfo"
set_property(TARGET winpr-timezone APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-timezone PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-timezone.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-timezone )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-timezone "${_IMPORT_PREFIX}/lib/libwinpr-timezone.a" )

# Import target "winpr-utils" for configuration "RelWithDebInfo"
set_property(TARGET winpr-utils APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-utils PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS10.3.sdk/usr/lib/libz.dylib;/Users/Enterprise/tracefreeios10andlower/FreeRDP/external/openssl/lib/libssl.a;/Users/Enterprise/tracefreeios10andlower/FreeRDP/external/openssl/lib/libcrypto.a;m;winpr-crt;winpr-file;winpr-path;winpr-synch;winpr-sysinfo"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-utils.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-utils )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-utils "${_IMPORT_PREFIX}/lib/libwinpr-utils.a" )

# Import target "winpr-winhttp" for configuration "RelWithDebInfo"
set_property(TARGET winpr-winhttp APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-winhttp PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-winhttp.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-winhttp )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-winhttp "${_IMPORT_PREFIX}/lib/libwinpr-winhttp.a" )

# Import target "winpr-winsock" for configuration "RelWithDebInfo"
set_property(TARGET winpr-winsock APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-winsock PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-winsock.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-winsock )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-winsock "${_IMPORT_PREFIX}/lib/libwinpr-winsock.a" )

# Import target "winpr-wnd" for configuration "RelWithDebInfo"
set_property(TARGET winpr-wnd APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-wnd PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-crt;winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-wnd.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-wnd )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-wnd "${_IMPORT_PREFIX}/lib/libwinpr-wnd.a" )

# Import target "winpr-wtsapi" for configuration "RelWithDebInfo"
set_property(TARGET winpr-wtsapi APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(winpr-wtsapi PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELWITHDEBINFO "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "winpr-nt;winpr-io;winpr-synch;winpr-file;winpr-error;winpr-library;winpr-environment;winpr-utils"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libwinpr-wtsapi.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS winpr-wtsapi )
list(APPEND _IMPORT_CHECK_FILES_FOR_winpr-wtsapi "${_IMPORT_PREFIX}/lib/libwinpr-wtsapi.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
