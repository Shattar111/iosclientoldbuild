# Install script for directory: /Users/Enterprise/tracefreeios10andlower/FreeRDP/include

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE FILE FILES
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/addin.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/altsec.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/api.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/client.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/constants.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/dvc.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/error.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/event.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/extension.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/freerdp.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/graphics.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/input.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/listener.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/message.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/metrics.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/peer.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/pointer.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/primary.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/primitives.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/rail.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/scancode.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/secondary.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/settings.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/svc.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/types.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/update.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/version.h"
    "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/window.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE FILE FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/version.h")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/cache" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/codec" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/crypto" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/gdi" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/locale" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/rail" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/utils" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/client" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/server" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/freerdp" TYPE DIRECTORY FILES "/Users/Enterprise/tracefreeios10andlower/FreeRDP/include/freerdp/channels" FILES_MATCHING REGEX "/[^/]*\\.h$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers")

