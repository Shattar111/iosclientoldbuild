#ifndef Sphere_iOS_ios_plugin_loader_h
#define Sphere_iOS_ios_plugin_loader_h

#include <freerdp/types.h>

void *sphere_load_channel_addin_entry(LPCSTR name, LPSTR subsystem, LPSTR type, DWORD flags);

#endif
