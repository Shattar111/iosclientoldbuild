#include "ios_plugin_loader.h"

#include <freerdp/addin.h>
#include <freerdp/dvc.h>

#include "audin_custom.h"
#include "audin_ios.h"

struct _STATIC_SUBSYSTEM_ENTRY
{
	const char* name;
	const char* type;
	const void* entry;
};
typedef struct _STATIC_SUBSYSTEM_ENTRY STATIC_SUBSYSTEM_ENTRY;

struct _STATIC_ADDIN_TABLE
{
	const char* name;
	const void* entry;
	const STATIC_SUBSYSTEM_ENTRY* table;
};
typedef struct _STATIC_ADDIN_TABLE STATIC_ADDIN_TABLE;

static const STATIC_SUBSYSTEM_ENTRY SPHERE_CLIENT_AUDIN_SUBSYSTEM_TABLE[] =
{
	{ "aq", NULL, audin_aq_subsystem_entry },
    { NULL, NULL, NULL }
};

static const STATIC_ADDIN_TABLE SPHERE_CLIENT_STATIC_ADDIN_TABLE[] =
{
	{ "audin", custom_audin_DVCPluginEntry, SPHERE_CLIENT_AUDIN_SUBSYSTEM_TABLE },
	{ NULL, NULL, NULL }
};

extern void* freerdp_channels_load_static_addin_entry(LPCSTR pszName, LPSTR pszSubsystem, LPSTR pszType, DWORD dwFlags);

void *sphere_load_channel_addin_entry(LPCSTR name, LPSTR subsystem, LPSTR type, DWORD flags) {
	int i, j;
    
    //printf("plugin lookup: %s : %s\n", name, subsystem);
    
	STATIC_SUBSYSTEM_ENTRY* subsystems;
    
	for (i = 0; SPHERE_CLIENT_STATIC_ADDIN_TABLE[i].name != NULL; i++) {
		if (strcmp(SPHERE_CLIENT_STATIC_ADDIN_TABLE[i].name, name) == 0) {
			if (subsystem != NULL) {
				subsystems = (STATIC_SUBSYSTEM_ENTRY *)SPHERE_CLIENT_STATIC_ADDIN_TABLE[i].table;
				for (j = 0; subsystems[j].name != NULL; j++) {
					if (strcmp(subsystems[j].name, subsystem) == 0) {
						if (type) {
							if (strcmp(subsystems[j].type, type) == 0)
								return (void*) subsystems[j].entry;
						} else {
							return (void*) subsystems[j].entry;
						}
					}
				}
			} else {
                void *retval = (void *)SPHERE_CLIENT_STATIC_ADDIN_TABLE[i].entry;
				return retval;
			}
		}
	}

    return freerdp_channels_load_static_addin_entry(name, subsystem, type, flags);
}
