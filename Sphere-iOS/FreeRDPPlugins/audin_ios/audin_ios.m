#import "audin_ios.h"

#include <freerdp/addin.h>
#include <freerdp/codec/dsp.h>
#include <freerdp/codec/audio.h>
#include <freerdp/channels/rdpsnd.h>

#import <AudioToolbox/AudioToolbox.h>

#include "audin_custom.h"

static const int AudioQueueBufferCount = 3;

typedef struct {
    IAudinDevice iface;
    
    UINT16 formatTag;
    UINT16 channels;
    UINT32 samplesPerSec;
    UINT16 blockAlign;
    UINT16 bitsPerSample;
    
    AudinReceive receive;
    void *userData;
    
    AudioStreamBasicDescription dataFormat;
    AudioQueueRef audioQueue;
    AudioQueueBufferRef buffers[AudioQueueBufferCount];
    UInt32 bufferSize;
    SInt64 currentPacket;
    bool isRunning;
    
} AudinIOSDevice;

#define LOG_FUNCTION() printf("%s\n", __PRETTY_FUNCTION__)

#define THIS(x) ((AudinIOSDevice *)(x))

static void HandleInputBuffer (void *aqData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer,
                               const AudioTimeStamp *inStartTime, UInt32 inNumPackets, const AudioStreamPacketDescription *inPacketDesc) {
    LOG_FUNCTION();
    AudinIOSDevice *device = THIS(aqData);
    if (device->isRunning) {
        device->receive(inBuffer->mAudioData, inBuffer->mAudioDataByteSize, device->userData);
        AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL);
    }
}
static void audin_ios_open(IAudinDevice* devplugin, AudinReceive receive, void* userData) {
    LOG_FUNCTION();
    AudinIOSDevice *device = THIS(devplugin);
    device->receive = receive;
    device->userData = userData;
    AudioStreamBasicDescription *dataFormat = &device->dataFormat;
    dataFormat->mFormatID = kAudioFormatLinearPCM;
    dataFormat->mSampleRate = device->samplesPerSec;
    dataFormat->mChannelsPerFrame = device->channels;
    dataFormat->mBitsPerChannel = device->bitsPerSample;
    dataFormat->mBytesPerPacket = dataFormat->mBytesPerFrame = device->channels * device->bitsPerSample / 8;
    dataFormat->mFramesPerPacket = 1;
    dataFormat->mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    OSStatus err = AudioQueueNewInput(&device->dataFormat, HandleInputBuffer, device, NULL, kCFRunLoopCommonModes, 0, &device->audioQueue);
    printf("%li", err);
    for (int i = 0; i < AudioQueueBufferCount; ++i) {
        AudioQueueAllocateBuffer(device->audioQueue, 8192, &device->buffers[i]);
        AudioQueueEnqueueBuffer(device->audioQueue, device->buffers[i], 0, NULL);
    }
    device->isRunning = true;
    AudioQueueStart(device->audioQueue, NULL);
}



static BOOL audin_ios_format_supported(IAudinDevice* devplugin, audinFormat* format) {
    LOG_FUNCTION();
    if (format->wFormatTag == WAVE_FORMAT_PCM &&
        format->nChannels == 2 &&
        format->nSamplesPerSec == 44100 &&
        format->wBitsPerSample == 16) {
        return TRUE;
    } else {
        return FALSE;
    }
}

static void audin_ios_set_format(IAudinDevice* devplugin, audinFormat* format, UINT32 FramesPerPacket) {
    LOG_FUNCTION();
    AudinIOSDevice *device = THIS(devplugin);
    device->formatTag = format->wFormatTag;
    device->channels = format->nChannels;
    device->samplesPerSec = format->nSamplesPerSec;
    device->blockAlign = format->nBlockAlign;
    device->bitsPerSample = format->wBitsPerSample;
}

static void audin_ios_close(IAudinDevice* devplugin) {
    LOG_FUNCTION();
    AudinIOSDevice *device = THIS(devplugin);
    AudioQueueStop(device->audioQueue, true);
    device->isRunning = false;
}

static void audin_ios_free(IAudinDevice* devplugin) {
    LOG_FUNCTION();
}


#ifdef STATIC_CHANNELS
#define freerdp_audin_client_subsystem_entry	ios_freerdp_audin_client_subsystem_entry
#endif

int audin_aq_subsystem_entry(PFREERDP_AUDIN_DEVICE_ENTRY_POINTS pEntryPoints)
{
//	ADDIN_ARGV* args = pEntryPoints->args;
	AudinIOSDevice* device = (AudinIOSDevice *)calloc(1, sizeof(AudinIOSDevice));
    
    device->iface.Open = audin_ios_open;
    device->iface.FormatSupported = audin_ios_format_supported;
    device->iface.SetFormat = audin_ios_set_format;
    device->iface.Close = audin_ios_close;
    device->iface.Free = audin_ios_free;
    
    pEntryPoints->pRegisterAudinDevice(pEntryPoints->plugin, (IAudinDevice *)device);
    
	return 0;
}
