/*******************************************************************************
 * $FileName: SPH_Constant.h$
 * $Date    : 2013.Nov.14$
 * $Revision: 1.0$
 * $Author  : Soknyra Te$
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  File is used to define constant object/variable.
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: 2013.Nov.14      Realwat Inc             Creation$
 *
 ******************************************************************************/

#ifndef FreeRDP_SPH_Constant_h
#define FreeRDP_SPH_Constant_h

// Define constant for commenting unused
#define SPH_IS_COMMENT (1)

// Define user bookmark keys
#define SPH_KEY_USERNAME       @"username"
#define SPH_KEY_PASSWORD       @"password"
#define SPH_KEY_HOSTNAME       @"hostname"
#define SPH_KEY_DOMAIN         @"domain"
#define SPH_KEY_SERVERHASHLIST @"serverhashlist"
#define SPH_KEY_SCREEN_HEIGHT  @"height"
#define SPH_KEY_SCREEN_WIDTH   @"width"
#define SPH_KEY_PORT           @"port"

// Define remote app
#define SPH_REMOTE_APP_NAME         @"IE"
#define SPH_SERVER_HASH_KEY_DEFAULT @"F36374DB7BA78EE15D465D1D1DADCB91$"

// __REALWAT__ : BEGIN : 2013/Dec/02 : Define string
#define SPH_STR_TITLE_NETWORK_ERROR @"Internet connection not available"
#define SPH_STR_MSG_NETWORK_ERROR   @"Please check your internet connection and try again."

#define SPH_STR_TITLE_SERVER1_ERROR @"Invalid Server Address"
#define SPH_STR_MSG_SERVER_ERROR   @"The Server Address provided is unable to connect. Please contact your System Administrator."

#define SPH_STR_TITLE_SERVER_ERROR @"App Launch Error"

#define SPH_STR_TITLE_SESSION_TIMEOUT @"Session timed out"
#define SPH_STR_MSG_LOGIN_AGAIN       @"Please login again."
#define SPH_STR_TITLE_NO_USERNAME_ACCESS @"App Launch Error"
#define SPH_STR_MSG_NO_USERNAME_ACCESS       @"You cannot login with this account."

// __REWLWAT__ : END

//__REALWAT__ : BEGIN: 05/Dec/2013: PTS: #GC-5001
#define SPH_DEFAULTS_REMEMBER_USERNAME         @"rememberUsername"
#define SPH_DEFAULTS_REMEMBER_PASSWORD         @"rememberPassword"
#define SPH_DEFAULTS_REMEMBER_IPADDRESS        @"rememberipaddress"

#define SPH_DEFAULTS_REMEMBER_STATUS           @"remember"
//__REALWAT__ : END

//__REALWAT__ : BEGIN: 06/Dec/2013: PTS: #GC-5022
#define SPH_HELP_URL_IPAD            @"https://gwsupport.sphere3d.com/apps/Glassware.php" //@"http://www.sphere3d.com/preprod/Glassware.php"
#define SPH_HELP_URL_IPHONE            @"https://gwsupport.sphere3d.com/apps/Glasswareiphone.php" //@"http://www.sphere3d.com/preprod/Glassware.php"

//__REALWAT__ : END

// 2013/Dec/10 : BEGIN : Fixed PTS:GC-19
#define SPH_STR_REGISTER_FIELD_ALL     @"Please complete all required fields and try again."
#define SPH_STR_REGISTER_FAILED        @"Registration failed, Please try again."
#define SPH_STR_REGISTER_USER_EXIST    @"Email/Username already in use"
#define SPH_STR_REGISTER_INVALID_EMAIL @"Please enter a valid email address."
#define SPH_STR_REGISTER_REGISTRATION_CLOSED @"Registration closed, please try again later."
// END

//__REALWAT__ : BEGIN: 26/Dec/2013: PTS: #GC-5036
#define SPH_UPLOAD_IMG_MAX_SIZE            (3)
#define SPH_UPLOAD_IMG_MAX_RESOLUTION      (1200)
#define SPH_UPLOAD_ALERT_TAG               (1000)
#define SPH_UPLOAD_ALERT_TITLE             @"Upload Image"
#define SPH_UPLOAD_ALERT_MSG_SIZE          @"Could not upload, image is too large"
#define SPH_UPLOAD_ALERT_MSG_RESOLUTION    @"Could not upload, image is too large"
//__REALWAT__ : END

//__REALWAT__ : BEGIN: 17/Jan/2014: PTS: #ISC-5045
#define NCD_PING_TIME                       (20) // 50 sec.
#define NCD_GREEN_NETINDICATOR              (50)
#define NCD_YELLOW_NETINDICATOR             (100)
#define NCD_RED_NETINDICATOR                (100)

#define NCD_DEFAULTS_PING_TIME              @"Pingtime"
#define NCD_DEFAULTS_GREEN_NETINDICATOR     @"GreenIndicator"
#define NCD_DEFAULTS_YELLOW_NETINDICATOR    @"YellowIndicator"
#define NCD_DEFAULTS_RED_NETINDICATOR       @"RedIndicator"

#define SPH_NCD_ALERT_TAG                   (2000)

//__REALWAT__ : END

// __REALWAT__ : BEGIN : 2014.Jan.24 : PTS:ISC-5043

#define SPH_PURCHASE_PRODUCT_ID_DEMO    @"com.realwat.businesscanvas.demo"
#define SPH_PURCHASE_PRODUCT_ID_MONTHLY @"com.realwat.businesscanvas.monthly02"
#define SPH_PURCHASE_PRODUCT_ID_YEARLY  @"com.realwat.businesscanvas.yearly02"

#define SPH_PURCHASE_DESC_DEFAULT    @"Corel® WordPerfect® Office X7 – Home & Student Edition lets you easily create documents, spreadsheets, presentations and more. Work with over 60 file types, including PDFs and the latest Microsoft Office formats. Choose from hundreds of free templates and extras to make your projects easier. The new Mail Merge Wizard will save you time when creating letters, envelopes and labels, and with Roxio Secure Burn you can create your own CDs, DVDs or Blu-ray™ discs. Plus, for users on the go, there’s the new WordPerfect app for iPad. Get everything you need in an office suite for a competitive price."

#define SPH_PURCHASE_IMG_DEFAULT     @"WPO.png"

#define SPH_PURCHASE_TITLE_DEMO      @"Demo"
#define SPH_PURCHASE_DESC_DEMO		 SPH_PURCHASE_DESC_DEFAULT
#define SPH_PURCHASE_PRICE_DEMO      @"Demo"
#define SPH_PURCHASE_IMG_DEMO        SPH_PURCHASE_IMG_DEFAULT
#define SPH_PURCAHSE_BGCORLOR_DEMO   @"bg_purchase_1.png"

#define SPH_PURCHASE_TITLE_MONTHLY    @"Monthly Package"
#define SPH_PURCHASE_DESC_MONTHLY     SPH_PURCHASE_DESC_DEFAULT
#define SPH_PURCHASE_PRICE_MONTHLY    @"4.99$"
#define SPH_PURCHASE_IMG_MONTHLY      SPH_PURCHASE_IMG_DEFAULT
#define SPH_PURCAHSE_BGCORLOR_MONTHLY @"bg_purchase_3.png"

#define SPH_PURCHASE_TITLE_YEARLY    @"Yearly Package"
#define SPH_PURCHASE_DESC_YEARLY     SPH_PURCHASE_DESC_DEFAULT
#define SPH_PURCHASE_PRICE_YEARLY    @"49.99$"
#define SPH_PURCHASE_IMG_YEARLY      SPH_PURCHASE_IMG_DEFAULT
#define SPH_PURCAHSE_BGCORLOR_YEARLY @"bg_purchase_4.png"

// __REALWAT__ : END : PTS:ISC-5043
// __REALWAT__ : BEGIN : 29/Jan/2014: PTS: #ISC-5051
#define SPH_SETTING_NETWORK_PING_STATUS     @"NetworkPingStatus"
#define SPH_NETWORK_PING_DARK_STAUS         @"dark"
// __REALWAT__ : END : 29/Jan/2014

// __REALWAT__ : BEGIN : 2014.Feb.03

// __REALWAT__ : END

// __REALWAT__ : BEGIN : 07/Feb/2014
#define SPH_SERVER_API_URL_AUTHENTICATION   @"preprod.auth.frostcat.com"
#define SPH_SERVER_API_URL_REGISTRATION     @"http://preprod.web.frostcat.com/register"
#define SPH_SERVER_API_URL_DROPBOX          @"http://preprod.web.frostcat.com/install.php"
// __REALWAT__ : END : 07/Feb/2014
#endif
