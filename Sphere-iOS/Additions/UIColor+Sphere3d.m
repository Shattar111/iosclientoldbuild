//
//  UIColor+Sphere3d.m
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-21.
//
//

#import "UIColor+Sphere3d.h"

@implementation UIColor (Sphere3d)

+ (UIColor *)sphere3dControlsTintColor
{
    return [[[UIColor alloc] initWithRed:113/255.0f green:163/255.0f blue:206/255.0f alpha:1.0f] autorelease];
}
@end
