#ifndef FreeRDP_SPH_ErrorCode_h
#define FreeRDP_SPH_ErrorCode_h

typedef enum eSPH_ErrorCode
{
	SPH_SUCCESS               = 0,
	SPH_ERROR                 = 1,
	SPH_ERROR_PARAMETER       = SPH_ERROR + 1,
	SPH_ERROR_OBJECT_NULL     = SPH_ERROR + 2,
	SPH_ERROR_INVALID_USER    = SPH_ERROR + 3,
	SPH_ERROR_INTERNET_CONN   = SPH_ERROR + 4,
    SPH_ERROR_APP_LAUNCH       = SPH_ERROR + 5,
    // VANYUTH : BEGIN : 05/02/2014
    SPH_ERROR_INVALID_TYPE    = SPH_ERROR + 6,
    // VANYUTH : END : 05/02/2014

}tSPH_ErrorCode;

// Define log
#define SPH_LOG_CONSOL(__ERROR_CODE__) NSLog(@"%s [Line %d] [ErrorCode : %d]", \
								__PRETTY_FUNCTION__, __LINE__, __ERROR_CODE__ )

#endif
