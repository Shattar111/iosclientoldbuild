//
//  NSError+Sphere3d.m
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-22.
//
//

#import "NSError+Sphere3d.h"

@implementation NSError (Sphere3d)

+ (NSError*)sphere3dUnableToSubmitFormData
{
    return [NSError errorWithDomain:@"Sphere3dAPI" code:0 userInfo:@{@"message": @"Unable to submit form data.\nPlease check your internet connection."}];
}

+ (NSError*)sphere3dUnableToReadServerResponse
{
    return [NSError errorWithDomain:@"Sphere3dAPI" code:0 userInfo:@{@"message": @"Unable to read server response.\nTry again later."}];
}

@end
