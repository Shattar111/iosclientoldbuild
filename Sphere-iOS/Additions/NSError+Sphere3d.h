//
//  NSError+Sphere3d.h
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-22.
//
//

#import <Foundation/Foundation.h>

@interface NSError (Sphere3d)

+ (NSError*)sphere3dUnableToSubmitFormData;
+ (NSError*)sphere3dUnableToReadServerResponse;

@end
