//
//  UIColor+Sphere3d.h
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-21.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (Sphere3d)

+ (UIColor *)sphere3dControlsTintColor;

@end
