#import <UIKit/UIKit.h>

@interface MyApplication : UIApplication

@property (nonatomic) BOOL hasBluetoothKeyboard;

+ (MyApplication *)sharedMyApplication;

@end
