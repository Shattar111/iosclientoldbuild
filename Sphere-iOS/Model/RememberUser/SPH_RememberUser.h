/*
 cSPH_RememberUser.h
 
 Glassware Connect
 
 Created by Sphere 3D 2015
 
 This class is used to manage remember me info:
 */
 
 
#import <Foundation/Foundation.h>
#import "SPH_ErrorCode.h"


@interface cSPH_RememberUser : NSObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *ipaddress;

@property (nonatomic, strong) NSNumber *rememberStatus;

- (tSPH_ErrorCode)loadMememberMeInfo;
- (tSPH_ErrorCode)saveInfo;
- (tSPH_ErrorCode)resetInfo;

@end
