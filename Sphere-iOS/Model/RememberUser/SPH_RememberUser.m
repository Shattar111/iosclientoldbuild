/*******************************************************************************
 * $FileName: $
 * $Date    : $
 * $Revision: 1.0$
 * $Author  : RealWat $
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D $
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  This is the implemementation of class cSPH_RememberUser
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: #date#          #name#              Creation$
 *
 ******************************************************************************/
#import "SPH_RememberUser.h"
#import "SPH_Constant.h"

@implementation cSPH_RememberUser

/*****************************************************************************
 ; loadMememberMeInfo
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	This function is used to load memember me information from user defaults
 ;
 ; RETURNS
 ;	SPH_ERROR_OBJECT_NULL
 ;  SPH_SUCCESS
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;      NONE
 ;  OUTPUT:
 ;      NONE
 ;
 ; IMPLEMENTATION
 ;   Load user preference from user default
 ;   Get remember info from user default
 ;   Set default value if there is not remember status save
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	__Description__
 ; 2013-12-05        RealWat Inc         GC-5001      Creation
 ******************************************************************************/
- (tSPH_ErrorCode)loadMememberMeInfo
{
    // load user preference from user default
    NSUserDefaults *lpDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( nil == lpDefaults )
    {
        SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
        
        return ( SPH_ERROR_OBJECT_NULL );
    }
    
    // get remember info from user default
    self.rememberStatus = [lpDefaults valueForKey:
                                                  SPH_DEFAULTS_REMEMBER_STATUS];
    self.username = [lpDefaults valueForKey:SPH_DEFAULTS_REMEMBER_USERNAME];
    self.password = [lpDefaults valueForKey:SPH_DEFAULTS_REMEMBER_PASSWORD];
    self.ipaddress = [lpDefaults valueForKey:SPH_DEFAULTS_REMEMBER_IPADDRESS];

    // if there is not remember status save set default value
    if ( nil == self.rememberStatus )
    {
        self.rememberStatus = @(NO);
    }
    
    return (SPH_SUCCESS);
}

/*****************************************************************************
 ; saveInfo
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to save memember me information to user default
 ;
 ; RETURNS
 ;	NONE
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;      NONE
 ;  OUTPUT:
 ;      NONE
 ;
 ; IMPLEMENTATION
 ;    load user preference from user default
 ;    set value to user defaults
 ;    save remember info to user default
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	   __Description__
 ; 2013-12-05        RealWat Inc        GC-5001           Creation
 ******************************************************************************/
- (tSPH_ErrorCode)saveInfo
{
    // load user preference from user default
    NSUserDefaults *lpDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( nil == lpDefaults )
    {
        SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
        
        return ( SPH_ERROR_OBJECT_NULL );
    }
    
    // set value to user defaults
    [lpDefaults setObject:self.rememberStatus
                   forKey:SPH_DEFAULTS_REMEMBER_STATUS];
    [lpDefaults setObject:self.username
                   forKey:SPH_DEFAULTS_REMEMBER_USERNAME];
    [lpDefaults setObject:self.password
                   forKey:SPH_DEFAULTS_REMEMBER_PASSWORD];
    [lpDefaults setObject:self.ipaddress
                   forKey:SPH_DEFAULTS_REMEMBER_IPADDRESS];

    // save remember info to user default
    [lpDefaults synchronize];
    
    
    return ( SPH_SUCCESS );
}

/*****************************************************************************
 ; resetInfo
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to reset memember me information to user default
 ;
 ; RETURNS
 ;	SPH_ERROR_OBJECT_NULL
 ;  SPH_SUCCESS
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;      NONE
 ;  OUTPUT:
 ;      NONE
 ;
 ; IMPLEMENTATION
 ;    load user preference from user default
 ;    remove memember info value from user defaults
 ;    save remember info to user default
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	   __Description__
 ; 2013-12-05        RealWat Inc        GC-5001           Creation
 ******************************************************************************/
- (tSPH_ErrorCode)resetInfo
{
    // load user preference from user default
    NSUserDefaults *lpDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( nil == lpDefaults )
    {
        SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
        
        return ( SPH_ERROR_OBJECT_NULL );
    }
    
    // remove memember info value from user defaults
    [lpDefaults removeObjectForKey:SPH_DEFAULTS_REMEMBER_STATUS];
    [lpDefaults removeObjectForKey:SPH_DEFAULTS_REMEMBER_PASSWORD];
    [lpDefaults removeObjectForKey:SPH_DEFAULTS_REMEMBER_USERNAME];
    [lpDefaults removeObjectForKey:SPH_DEFAULTS_REMEMBER_IPADDRESS];

    // save remember info to user default
    [lpDefaults synchronize];
    
    return ( SPH_SUCCESS );
}

@end
