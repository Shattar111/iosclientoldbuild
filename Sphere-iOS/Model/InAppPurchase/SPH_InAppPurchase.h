/*******************************************************************************
 * $FileName: SPH_InAppPurchase.h$
 * $Date    : 2013-12-13$
 * $Revision: 1.0$
 * $Author  : RealWat Inc $
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D $
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  Clase declaration for In-App purchase API.
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: 2013-12-13       RealWat Inc             Creation$
 *
 ******************************************************************************/

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol SPH_InAppPurchaseDelegate <NSObject>

- (int) onPurchasing;
- (int) onPurchaseFailed;
- (int) onPurchaseCompleted;
- (int) onPurchaseNoProductAvailable;

@end

@interface SPH_InAppPurchase : NSObject <SKProductsRequestDelegate,
                                         SKPaymentTransactionObserver>
{
	NSString   *m_strPurchaseId;
}

@property (nonatomic, retain) id<SPH_InAppPurchaseDelegate> delegate;

- (int)startPurchase:(NSString*)inProductId;

@end
