/*******************************************************************************
 * $FileName: SPH_InAppPurchase.m$
 * $Date    : 2013-12-13$
 * $Revision: 1.0$
 * $Author  : RealWat Inc $
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D $
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  Clase implementation for In-App purchase API.
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: 2013-12-13       RealWat Inc             Creation$
 *
 ******************************************************************************/

#import "SPH_InAppPurchase.h"
#import <CommonCrypto/CommonCrypto.h>

#import "SPH_ErrorCode.h"
#import "SPH_Constant.h"

@implementation SPH_InAppPurchase


/*******************************************************************************
 ; startPurchase
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;   This function used to startPurchase
 ;
 ;
 ; RETURNS
 ;  NA
 ;
 ; INTERFACE NOTES
 ;  INPUT : NSString (inProductId)
 ;  OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  -check you can make payment
 ;                  - request with productIdentifiers
 ;
 ; HISTORY
 ; _date_     :    _authorId_  :      _PTS_         : _Description_
 ; 2013.Dec.13      Realwat Inc        GC-5032          Creation$
 ******************************************************************************/
- (int)startPurchase:(NSString*)inProductId
{
    
    if ( TRUE != [SKPaymentQueue canMakePayments])
	{
        NSLog(@"Parental-controls are enabled");
        
        UIAlertView *lpFailureAlert = [[UIAlertView alloc]
                                       initWithTitle :@"No Product:"
                                       message: @"No Product Available."
                                       delegate : self cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
        [lpFailureAlert show];
        [lpFailureAlert release];
		return (1);
    }
	
	m_strPurchaseId = inProductId;
	
	[[SKPaymentQueue defaultQueue] addTransactionObserver:self];
	
	SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
										  initWithProductIdentifiers:
					   [NSSet setWithObjects:SPH_PURCHASE_PRODUCT_ID_MONTHLY,
											 SPH_PURCHASE_PRODUCT_ID_YEARLY,
						                     nil]];
	productsRequest.delegate = self;
	[productsRequest start];
	[productsRequest release];
	productsRequest = nil;
    
	return (0);
}

/*******************************************************************************
 ; productsRequest
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;              This  is degate when request completed it response
 ;
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTES
 ;          INPUT : NSKProductRequest (request)
 ;                  SKProdctResponse (response)
 ;          OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  -check response number of products
 ;                  - SKPayment : start payment with ] id
 ;                  - check add observer hasAddObserver
 ;                  - add protocol TransationObserver : it check Transation completed
 ;                  - SKPaymentQueue : used to communcate with app store .
 ;                  when payment is added to Queue and store kit transation
 ;                  request to app store.Store kit show dialog box autherize
 ;                  payment .when it completed Transation it return to
 ;                  application's observer
 ;
 ;
 ; HISTORY
 ; _date_       :     _authorId_     :     _PTS_       :   _Description_
 ; 2013.Dec.13         Realwat Inc         GC-5032            Creation$
 ******************************************************************************/

- (void)productsRequest:(SKProductsRequest *)request
	 didReceiveResponse:(SKProductsResponse *)response
{
    
	BOOL       lIsValidProduct = FALSE;
    
    for ( NSString *invalidProductId in response.invalidProductIdentifiers)
	{
        NSLog(@"Problem in iTunes connect configuration for product: %@" , invalidProductId);
	}
    
	for ( SKProduct *lpProduct in response.products )
	{
		if ( TRUE == [lpProduct.productIdentifier isEqualToString:m_strPurchaseId] )
		{
			lIsValidProduct = TRUE;
			
			SKPayment *lpPayment = [SKPayment paymentWithProduct:lpProduct];

			if ( nil == lpPayment )
			{
				if ( nil != _delegate )
				{
					[_delegate onPurchaseFailed];
				}
				return;
			}
		
			[[SKPaymentQueue defaultQueue] addPayment:lpPayment];
		
			break;
		}
	}
	
    if ( FALSE == lIsValidProduct )
	{
    
		NSLog(@"No products available");
	
		if ( nil != _delegate )
		{
			[_delegate onPurchaseNoProductAvailable];
		}
		
    }
}

/*******************************************************************************
 ; request
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;              This  is degate  call when request error
 ;
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTES
 ;          INPUT : SKRequest (request )
 ;                  NSError (error)
 ;          OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  - call cancelPurchase
 ;
 ;
 ; HISTORY
 ; _date_       :   _authorId_    :     _PTS_     :     _Description_
 ; 2013.Dec.13       Realwat Inc       GC-5032            Creation$
 ******************************************************************************/

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"error");
	if ( nil != _delegate )
	{
		[_delegate onPurchaseFailed];
	}
}

/*******************************************************************************
 ; paymentQueue
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;              This  is degate check transation
 ;
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTES
 ;          INPUT : SKPaymentQueue (queue )
 ;                  NSArray (transactions)
 ;          OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  -check state of transation
 ;
 ;
 ; HISTORY
 ; _date_      :   _authorId_    :   _PTS_    :   _Description_
 ; 2013.Dec.13      Realwat Inc     GC-5032         Creation$
 ******************************************************************************/
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    
    for (SKPaymentTransaction *lpTransaction in transactions)
	{
        SKPayment *lpPayment = [lpTransaction payment];
        
        if([lpPayment.productIdentifier isEqualToString:m_strPurchaseId])
			{
            NSLog(@"%@ = payement queue payment.productIdentifier",lpPayment.productIdentifier);
            
            switch (lpTransaction.transactionState)
				{
					case SKPaymentTransactionStatePurchasing:
					{
						NSLog(@"purchasing");
						if ( nil != _delegate )
						{
							[_delegate onPurchasing];
						}
                    }
                    break;
					
					case SKPaymentTransactionStatePurchased:
					{
						NSLog(@"completeTransaction");
					
						[self completeTransaction:lpTransaction];
                    }
                    break;
					
					case SKPaymentTransactionStateFailed:
					{
						NSLog(@"failedTransaction");
						
						if ( nil != _delegate )
						{
							[_delegate onPurchaseFailed];
						}
					
						[self failedTransaction:lpTransaction];
                    }
                    break;
					case SKPaymentTransactionStateRestored:
					{
						NSLog(@"restoreTransaction");
						[self restoreTransaction:lpTransaction];
                    }
					default:
                    break;
				}
			}
		}
}

/*******************************************************************************
 ; recordTransaction
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;              This function used to record transationdsss
 ;
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTES
 ;          INPUT : SKPaymentTransaction (pTransation )
 ;          OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  -recordTransation
 ;
 ;
 ; HISTORY
 ; _date_      :    _authorId_     :   _PTS_       : _Description_
 ; 2013.Dec.13       Realwat Inc       GC-5032        Creation$
 ******************************************************************************/

- (void)recordTransaction:(SKPaymentTransaction *)pTransaction
{
    NSLog(@"inside the recordTransaction");
	
	if ( nil == pTransaction )
	{
		SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
		return;
	}
	
	NSDateFormatter *lpFormatter = [[NSDateFormatter alloc] init];
	[lpFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
	
	NSString *lpStringFromDate = [lpFormatter stringFromDate:pTransaction.transactionDate];
	
	[lpFormatter release];
		
	NSLog(@"Purchase Date: %@",lpStringFromDate);
	
	NSString *lpReceiptStr = [[[NSString alloc] initWithData:pTransaction.transactionReceipt encoding:NSUTF8StringEncoding] autorelease];
	
	NSLog(@"Receipts : %@", lpReceiptStr);
    
}

-(tSPH_ErrorCode) storeTransactionReceipt:(SKPaymentTransaction*)pTranscation
{
	if ( nil == pTranscation )
	{
		SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
		return (SPH_ERROR_PARAMETER);
	}
	
	NSUserDefaults *lpStorage = [NSUserDefaults standardUserDefaults];
	
	if ( nil == lpStorage )
	{
		SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
		return (SPH_ERROR_OBJECT_NULL);
	}
	
	NSData  *lpReciept = pTranscation.transactionReceipt;
	
	__unused NSArray *lpSavedReceipt = [lpStorage arrayForKey:@"receipts"];
	
	if ( nil == lpReciept )
	{
		[lpStorage setObject:@"" forKey:@"receipts"];
	}
	else
	{
		[lpStorage setObject:lpReciept forKey:@"receipts"];
	}
	
	return (SPH_SUCCESS);
}
/*******************************************************************************
 ; completeTransaction
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;              This function used whem completeTransation
 ;
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTESdd
 ;          INPUT : SKPaymentTransaction (pTransation )
 ;          OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  -recordTransation
 ;                  - provide content
 ;                  -
 ;
 ;
 ; HISTORY
 ; _date_      :    _authorId_     :   _PTS_       : _Description_
 ; 2013.Dec.13       Realwat Inc       GC-5032        Creation$
 ******************************************************************************/
- (int) completeTransaction: (SKPaymentTransaction *)pTransaction

{
	
	if ( nil == pTransaction )
	{
		return (1);
	}
	
    [self recordTransaction: pTransaction];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: pTransaction];
	
	if ( nil != _delegate )
	{
		[_delegate onPurchaseCompleted];
	}
    
	return (0);
}
/*******************************************************************************
 ; restoreTransaction
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;              This function used to restore transationdsss
 ;
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTESdd
 ;          INPUT : SKPaymentTransaction (pTransation )
 ;          OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  -restore transation
 ;
 ;
 ; HISTORY
 ; _date_      :    _authorId_     :   _PTS_       : _Description_
 ; 2013.Dec.13       Realwat Inc       GC-5032        Creation$
 ******************************************************************************/
- (void) restoreTransaction: (SKPaymentTransaction *)pTransaction
{
    [[SKPaymentQueue defaultQueue] finishTransaction: pTransaction];
}
/*******************************************************************************
 ; failedTransaction
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;              This function used when transation failed
 ;
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTESdd
 ;          INPUT : SKPaymentTransaction (pTransation )
 ;          OUTPUT: NA
 ;
 ; IMPLEMENTATION
 ;                  - check kind of error transation
 ;
 ;
 ; HISTORY
 ; _date_      :    _authorId_     :   _PTS_       : _Description_
 ; 2013.Dec.13       Realwat Inc       GC-5032        Creation$
 ******************************************************************************/
- (void) failedTransaction: (SKPaymentTransaction *)pTransaction
{
    
	
    if (pTransaction.error.code != SKErrorPaymentCancelled)
	{
        if(pTransaction.error.code == SKErrorUnknown)
		{
            
            NSLog(@"Unknown Error (%d), product: %@", (int)pTransaction.error.code,
                  pTransaction.payment.productIdentifier);
            
		}
        
        if(pTransaction.error.code == SKErrorClientInvalid)
		{
            NSLog(@"Client invalid (%d), product: %@", (int)pTransaction.error.code,
                  pTransaction.payment.productIdentifier);
            
			
		}
        
        if(pTransaction.error.code == SKErrorPaymentInvalid)
		{
            NSLog(@"Payment invalid (%d), product: %@", (int)pTransaction.error.code,
                  pTransaction.payment.productIdentifier);
            
		}
        
        if(pTransaction.error.code == SKErrorPaymentNotAllowed)
		{
            NSLog(@"Payment not allowed (%d), product: %@", (int)pTransaction.error.code,
                  pTransaction.payment.productIdentifier);
            
        }
		
		UIAlertView *lpFailureAlert = [[UIAlertView alloc]
									   initWithTitle :@"In-App-Purchase Error:"
									   
									   message: @"There was an error "
									   "purchasing this item please try again."
									   
									   delegate : self cancelButtonTitle:@"OK"
									   otherButtonTitles:nil];
		[lpFailureAlert show];
		[lpFailureAlert release];
        
	}//if
    
    [[SKPaymentQueue defaultQueue] finishTransaction: pTransaction];
    
}

/*******************************************************************************
 ; hashedValueForAccountName
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;		Custom method to calculate the SHA-256 hash using Common Crypto
 ;
 ; RETURNS
 ;          NA
 ;
 ; INTERFACE NOTESdd
 ;
 ;
 ; IMPLEMENTATION
 ; 
 ;
 ; HISTORY
 ; _date_      :    _authorId_     :   _PTS_       : _Description_
 ; 2013.Dec.13       Realwat Inc       GC-5032        Creation$
 ******************************************************************************/
- (NSString *)hashedValueForAccountName:(NSString*)userAccountName
{
    const int HASH_SIZE = 32;
    unsigned char hashedChars[HASH_SIZE];
    const char *accountName = [userAccountName UTF8String];
    size_t accountNameLen = strlen(accountName);
	
    // Confirm that the length of the user name is small enough
    // to be recast when calling the hash function.
    if (accountNameLen > UINT32_MAX)
	{
        NSLog(@"Account name too long to hash: %@", userAccountName);
        return nil;
    }
    
	CC_SHA256(accountName, (CC_LONG)accountNameLen, hashedChars);
	
    // Convert the array of bytes into a string showing its hex representation.
    NSMutableString *userAccountHash = [[NSMutableString alloc] init];
    for (int i = 0; i < HASH_SIZE; i++)
	{
        // Add a dash every four bytes, for readability.
        if (i != 0 && i%4 == 0)
		{
            [userAccountHash appendString:@"-"];
        }
    
		[userAccountHash appendFormat:@"%02x", hashedChars[i]];
    }
	
    return userAccountHash;
}

@end
