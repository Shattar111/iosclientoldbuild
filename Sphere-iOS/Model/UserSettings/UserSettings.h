/*
 UserSettings.h
 
 Glassware Connect
 
 Created by Sphere 3D 2015
 
 UserSettings handles all the setting of the Glassware app (saving user defaults).
 */



#import <Foundation/Foundation.h>

@interface UserSettings : NSObject

@property (nonatomic, strong) NSNumber *sessionDuration;
@property (nonatomic, strong) NSNumber *mousePointerSize;
@property (nonatomic, strong) NSNumber *keyboardFunctionKeys;
@property (nonatomic, strong) NSNumber *keyboardFunctionEnableSwitch;
@property (nonatomic, strong) NSNumber *toolbarSize; //toolbar111;
@property (nonatomic, strong) NSNumber *propsizeval;
@property (nonatomic, strong) NSNumber *sessionScrollSpeed;
@property (nonatomic, strong) NSNumber *sessionPageSpeed;

//__REALWAT__ : BEGIN: 17/Jan/2014: PTS: #ISC-5045
@property (nonatomic, strong) NSNumber *pingTimeInterval;
@property (nonatomic, strong) NSNumber *greenIndicator;
@property (nonatomic, strong) NSNumber *yellowIndicator;
@property (nonatomic, strong) NSNumber *redIndicatior;
//__REALWAT__ : END

+ (instancetype)loadUserSettings;

- (void)save;

@end
