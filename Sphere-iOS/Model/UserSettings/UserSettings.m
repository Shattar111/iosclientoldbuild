//
//  UserSettings.m
//  IE on Demand
//
//  Created by Xtreme Labs on 2013-10-15.
//
//

#import "UserSettings.h"
#import "TouchPointerView.h"

// __REALWAT__ : BEGIN : 2014/Jan/17 : PTS: #ISC-5045
#import "SPH_Constant.h"
// __REALWAT__ : END

#define DEFAULTS_KEYBOARD_FUNCTION_KEYS @"KeyboardFunctionKeys"
#define DEFAULTS_MOUSE_POINTER_SIZE     @"MousePointerSize"
#define DEFAULTS_SESSION_DURATION       @"SessionDuration"
#define DEFAULTS_TOOLBAR                @"Toolbar"
#define DEFAULTS_FUNCTIONTOOL           @"FunctionToolbar"
#define DEFAULTS_PROPERTY_BAR           @"PropertyBar"
#define DEFAULTS_SCROLL_SPREED          @"ScrollSpeed"
#define DEFAULTS_PAGE_SCOLL_SPREED      @"Pagespeed"

@implementation UserSettings

+ (instancetype)loadUserSettings {
    UserSettings *settings = [[[UserSettings alloc] init] autorelease];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    settings.propsizeval = [defaults valueForKey:DEFAULTS_PROPERTY_BAR];
    settings.toolbarSize = [defaults valueForKey:DEFAULTS_TOOLBAR];
    settings.mousePointerSize = [defaults valueForKey:DEFAULTS_MOUSE_POINTER_SIZE];
    settings.sessionDuration = [defaults valueForKey:DEFAULTS_SESSION_DURATION];
    settings.sessionScrollSpeed = [defaults valueForKey:DEFAULTS_SCROLL_SPREED];
    settings.sessionPageSpeed = [defaults valueForKey:DEFAULTS_PAGE_SCOLL_SPREED];
    settings.keyboardFunctionKeys = [defaults valueForKey:DEFAULTS_KEYBOARD_FUNCTION_KEYS];
    settings.keyboardFunctionEnableSwitch = [defaults valueForKey:DEFAULTS_FUNCTIONTOOL];

    //__REALWAT__ : BEGIN: 17/Jan/2014: PTS: #ISC-5045
    settings.pingTimeInterval = [defaults valueForKey:NCD_DEFAULTS_PING_TIME];
    settings.greenIndicator   = [defaults valueForKey:NCD_DEFAULTS_GREEN_NETINDICATOR];
    settings.yellowIndicator  = [defaults valueForKey:NCD_DEFAULTS_YELLOW_NETINDICATOR];
    settings.redIndicatior    = [defaults valueForKey:NCD_DEFAULTS_RED_NETINDICATOR];
    //__REALWAT__ : END
    
    // if there is not settings saved return default values
    if (nil == settings.propsizeval) settings.propsizeval = @(0);
    if (nil == settings.toolbarSize) settings.toolbarSize = @(0);
    if (nil == settings.mousePointerSize) settings.mousePointerSize = @(S3MousePointerSizeSmall);

    if (nil == settings.sessionDuration) settings.sessionDuration = @(15);
    if (nil == settings.sessionScrollSpeed) settings.sessionScrollSpeed = @(25);
    if (nil == settings.keyboardFunctionKeys) settings.keyboardFunctionKeys = @(YES);
    if (nil == settings.keyboardFunctionEnableSwitch) settings.keyboardFunctionEnableSwitch = @(YES);

    
    //__REALWAT__ : BEGIN: 17/Jan/2014: PTS: #ISC-5045
    if (nil == settings.pingTimeInterval)
    {
        settings.pingTimeInterval = @(NCD_PING_TIME);
    }
    if (nil == settings.greenIndicator)
    {
        settings.greenIndicator = @(NCD_GREEN_NETINDICATOR);
    }
    if (nil == settings.yellowIndicator)
    {
        settings.yellowIndicator = @(NCD_YELLOW_NETINDICATOR);
    }
    if (nil == settings.redIndicatior)
    {
        settings.redIndicatior = @(NCD_RED_NETINDICATOR);
    }
    
    //__REALWAT__ : END
    
    return settings;
}

- (void)save {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:self.propsizeval forKey:DEFAULTS_PROPERTY_BAR];
    [defaults setObject:self.toolbarSize forKey:DEFAULTS_TOOLBAR];
    [defaults setObject:self.mousePointerSize forKey:DEFAULTS_MOUSE_POINTER_SIZE];
    [defaults setObject:self.sessionDuration forKey:DEFAULTS_SESSION_DURATION];
    [defaults setObject:self.sessionScrollSpeed forKey:DEFAULTS_SCROLL_SPREED];
    [defaults setObject:self.sessionPageSpeed forKey:DEFAULTS_PAGE_SCOLL_SPREED];
    [defaults setBool:[self.keyboardFunctionKeys boolValue] forKey:DEFAULTS_KEYBOARD_FUNCTION_KEYS];
    [defaults setBool:[self.keyboardFunctionEnableSwitch boolValue] forKey:DEFAULTS_FUNCTIONTOOL];

    //__REALWAT__ : BEGIN: 17/Jan/2014: PTS: #ISC-5045
    
    [defaults setObject:self.pingTimeInterval
                 forKey:NCD_DEFAULTS_PING_TIME];
    [defaults setObject:self.greenIndicator
                 forKey:NCD_DEFAULTS_GREEN_NETINDICATOR];
    [defaults setObject:self.yellowIndicator
                 forKey:NCD_DEFAULTS_YELLOW_NETINDICATOR];
    [defaults setObject:self.redIndicatior
                 forKey:NCD_DEFAULTS_RED_NETINDICATOR];
    //__REALWAT__ : END

    [defaults synchronize];
}

@end
