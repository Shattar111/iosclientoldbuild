//
//  RegistrationData.h
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-22.
//
//

#import <Foundation/Foundation.h>

@interface RegistrationData : NSObject
@property (copy, nonatomic) NSString* name;
@property (copy, nonatomic) NSString* occupation;
@property (copy, nonatomic) NSString* country;
@property (copy, nonatomic) NSString* username;
@property (copy, nonatomic) NSString* email;

- (NSDictionary*)toDictionary;
- (NSString*)postParams;

@end
