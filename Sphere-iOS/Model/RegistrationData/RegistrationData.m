//
//  RegistrationData.m
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-22.
//
//

#import "RegistrationData.h"

@implementation RegistrationData

- (NSDictionary*)toDictionary {
    return @{
     @"name": self.name,
     @"occupation" : self.occupation,
     @"country" : self.country,
     @"username" : self.username,
     @"email" : self.email
     };
}

- (NSString*)postParams {
    NSDictionary *dict = [self toDictionary];
    NSArray *keys = [[self toDictionary] allKeys];
    NSMutableString *paramList = [NSMutableString string];

    for(NSString *key in keys)
    {
        [paramList appendString:[NSString stringWithFormat:@"%@=%@&", key, dict[key]]];
    }
    
    return paramList;
}

@end