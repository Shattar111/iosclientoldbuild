/*
 UserSettings.h
 
 Glassware Connect
 
 Created by Sphere 3D 2015
 
 UserSession manages username/password/hostname.
 */


#import "UserSession.h"

// __REALWAT__ : BEGIN : 2013/Nov/21 : Include file
#import "SPH_Constant.h"
// __REALWAT__ : END

static NSString* kUsernameKey = SPH_KEY_USERNAME;
static NSString* kPasswordKey = SPH_KEY_PASSWORD;
static NSString* kipaddressKey = SPH_KEY_HOSTNAME;

static NSString* kServerHashListKey = SPH_KEY_SERVERHASHLIST;

@implementation UserSession

+ (instancetype)loadUserSession
{
    UserSession *session = [[[UserSession alloc] init] autorelease];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    session.username = [prefs objectForKey:kUsernameKey];   //username and password
    session.password = [prefs objectForKey:kPasswordKey];
    session.ipaddress = [prefs objectForKey:kipaddressKey];

    session.serverHashList = [prefs objectForKey:kServerHashListKey];
    return session;
}

- (void)save
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:self.username forKey:kUsernameKey];
    [prefs setValue:self.password forKey:kPasswordKey];
    [prefs setValue:self.ipaddress forKey:kipaddressKey];

    [prefs setValue:self.serverHashList forKey:kServerHashListKey];
    [prefs synchronize];
}

- (void)removeUserSession {
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:kUsernameKey];
    [prefs removeObjectForKey:kPasswordKey];
    [prefs removeObjectForKey:kipaddressKey];

    [prefs removeObjectForKey:kServerHashListKey];
    [prefs synchronize];
}

@end
