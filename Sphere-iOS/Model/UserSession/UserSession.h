/*
 UserSession.h
 
 Glassware Connect
 
 Created by Sphere 3D 2015
 
 UserSession manages username/password/hostname.
 */


#import <Foundation/Foundation.h>


@interface UserSession : NSObject

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *ipaddress;

@property (nonatomic, copy) NSString *serverHashList;

+ (instancetype)loadUserSession;
- (void)save;
- (void)removeUserSession;

@end
