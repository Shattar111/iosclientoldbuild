/*******************************************************************************
 * $FileName: SPH_PurchaseInfo.m$
 * $Date    : 2014-Jan-24$
 * $Revision: 1.0$
 * $Author  : RealWat Inc $
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D $
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  Clase implementation for Purchase info.
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: 2014-Jan-24       RealWat Inc             Creation$
 *
 ******************************************************************************/

#import "SPH_PurchaseInfo.h"

@implementation SPH_PurchaseInfo

@end
