/*******************************************************************************
 * $FileName: SPH_PurchaseInfo.h$
 * $Date    : 2014-Jan-24$
 * $Revision: 1.0$
 * $Author  : RealWat Inc $
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D $
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  Clase declaration for Purchase info.
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: 2014-Jan-24       RealWat Inc             Creation$
 *
 ******************************************************************************/

#import <Foundation/Foundation.h>

/*****************************************************************************
 ; eSPH_PurchaseType
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Define purchase type for the In-App purchase process.
 ;  - Demo type
 ;  - Weekly type
 ;  - Monthly type
 ;  - and Yearly type.
 ;
 ; RETURNS
 ;	NA
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;      NONE
 ;  OUTPUT:
 ;      NONE
 ;
 ; IMPLEMENTATION
 ;   NA
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	__Description__
 ; 2014-Jan-24        RealWat Inc         GC-5001      Creation
 ******************************************************************************/
typedef enum eSPH_PurchaseType
{
	SPH_PURCHASE_TYPE_DEMO    = 0, // DEMO PRODUCT
	SPH_PURCHASE_TYPE_WEEKLY  = 1, // WEEKLY SUBCRIPTION
	SPH_PURCHASE_TYPE_MONTHLY = 2, // MONTHLY SUBCRIPTION
	SPH_PURCHASE_TYPE_YEARLY  = 3  // YEARLY SUBSCRIPTION
	
}tSPH_PurchaseType;

@interface SPH_PurchaseInfo : NSObject

@property (nonatomic, assign) tSPH_PurchaseType   purchaseType;
@property (nonatomic, retain) NSString            * imageName;
@property (nonatomic, retain) NSString            * title;
@property (nonatomic, retain) NSString            * description;
@property (nonatomic, retain) NSString            * price;
@property (nonatomic, retain) NSString            * productId;

@end
