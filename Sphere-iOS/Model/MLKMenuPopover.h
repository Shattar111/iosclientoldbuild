/*
MLKMenuPopover.h
 
 Glassware Connect
 
 Created by Sphere 3D 2015
 
 MLKMenuPopover is used to store/list IP history.
 */

#import <UIKit/UIKit.h>

@class MLKMenuPopover;

@protocol MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex;
- (void)menuPopover:(MLKMenuPopover *)menuPopover DeletedMenuItemAtIndex:(NSInteger)selectedIndex;
- (void)menuPopover:(MLKMenuPopover *)menuPopover OrderedMenuItems:(NSMutableArray*)OrdmenuItems;


@end

@interface MLKMenuPopover : UIView <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,assign) id<MLKMenuPopoverDelegate> menuPopoverDelegate;

- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)menuItems;
- (void)showInView:(UIView *)view;
- (void)dismissMenuPopover;
- (void)layoutUIForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

@end
