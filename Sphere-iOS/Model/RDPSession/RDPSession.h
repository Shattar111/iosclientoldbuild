/*
 RDP Session object 
 
 Copyright 2013 Thincast Technologies GmbH, Authors: Martin Fleisz, Dorian Johnson
 
 This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. 
 If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#include <freerdp/freerdp.h>

//__REALWAT__:BEGIN: 26/12/2013: PTS: GC-5038
#import "SPH_ErrorCode.h"
//__REALWAT__:END

// forward declaration
@class RDPSession;
@class ComputerBookmark;
@class ConnectionParams;

// notification handler for session disconnect
extern NSString* TSXSessionDidDisconnectNotification;
extern NSString* TSXSessionDidFailToConnectNotification;

// protocol for session notifications
@protocol RDPSessionDelegate <NSObject>
@optional
- (void)session:(RDPSession*)session didFailToConnect:(int)reason;
- (void)sessionWillConnect:(RDPSession*)session;
- (void)sessionDidConnect:(RDPSession*)session;
- (void)sessionWillDisconnect:(RDPSession*)session;
- (void)sessionDidDisconnect:(RDPSession*)session;
- (void)sessionBitmapContextWillChange:(RDPSession*)session;
- (void)sessionBitmapContextDidChange:(RDPSession*)session;
- (void)session:(RDPSession*)session needsRedrawInRect:(CGRect)rect;
- (CGSize)sizeForFitScreenForSession:(RDPSession*)session;

- (void)session:(RDPSession*)session requestsAuthenticationWithParams:(NSMutableDictionary*)params;
- (void)session:(RDPSession*)session verifyCertificateWithParams:(NSMutableDictionary*)params;

//__REALWAT__: BEGIN : 20/12/2013: PTS: GC-45
- (void)sessionDidStartUploadingImage:(RDPSession*)session;
- (void)sessionDidFinishedUploadingImage:(RDPSession*)session;
-(void)sessionDidUploadExceedImage:(RDPSession *)session
                           message:(NSString *)lpMsg;

//__REALWAT__: END

@end

//__REALWAT__: BEGIN : PTS: GC-45

#define BI_RGB      0x00000000
#define BI_JPEG     0x00000004
#define BI_PNG      0x00000005
#define EKSIZE      16

#pragma pack(push,2)

// This structure contains information about the type, size, and layout of a file that containing a
// device-independent bitmap (DIB).
typedef struct tagBITMAPFILEHEADER
{
    WORD bfType;
    DWORD bfSize;
    WORD bfReserved1;
    WORD bfReserved2;
    DWORD bfOffBits;
}
BITMAPFILEHEADER;

// This structure contains information about the dimensions and color format of a device-independent bitmap (DIB).
typedef struct tagBITMAPINFOHEADER
{
    DWORD biSize;
    LONG biWidth;
    LONG biHeight;
    WORD biPlanes;
    WORD biBitCount;
    DWORD biCompression;
    DWORD biSizeImage;
    LONG biXPelsPerMeter;
    LONG biYPelsPerMeter;
    DWORD biClrUsed;
    DWORD biClrImportant;
}
BITMAPINFOHEADER;

#pragma pack(pop)

//__REALWAT__: END

// rdp session
@interface RDPSession : NSObject 
{
@private
	freerdp* _freerdp;

    ComputerBookmark* _bookmark;
    
	ConnectionParams* _params;
	
	NSObject<RDPSessionDelegate>* _delegate;
    
    NSCondition* _ui_request_completed;
    
    NSString* _name;
    
    // flag if the session is suspended
    BOOL _suspended;
    
	// flag that specifies whether the RDP toolbar is visible
	BOOL _toolbar_visible;
    
    //__REALWAT__: BEGIN : PTS: GC-45
    
    // clipboard redirect
    UIPasteboard    * pasteboard_rd; /* for reading from clipboard */
	int               pasteboard_changecount;
	int               pasteboard_format;
	UIPasteboard    * pasteboard_wr; /* for writing to clipboard */
    NSTimer         * pasteboard_timer;
    
    //__REALWAT__: END
}

@property (readonly) ConnectionParams* params;
@property (readonly) ComputerBookmark* bookmark;
@property (assign) id <RDPSessionDelegate> delegate;
@property (assign) BOOL toolbarVisible;
@property (readonly) CGContextRef bitmapContext;
@property (readonly) NSCondition* uiRequestCompleted;


//__REALWAT__: BEGIN : PTS: GC-45
@property (assign)UIPasteboard* pasteboard_rd;
@property (assign)UIPasteboard* pasteboard_wr;
@property (assign)int           pasteboard_format;

//__REALWAT__: END

// initialize a new session with the given bookmark
- (id)initWithBookmark:(ComputerBookmark*)bookmark;

#pragma mark - session control functions

// connect the session
-(void)connect;

// disconnect session
-(void)disconnect;

// suspends the session
-(void)suspend;

// resumes a previously suspended session
-(void)resume;

// returns YES if the session is started
-(BOOL)isSuspended;

// send input event to the server
-(void)sendInputEvent:(NSDictionary*)event;

// session needs a refresh of its view
- (void)setNeedsDisplayInRectAsValue:(NSValue*)rect_value;

// get a small session screenshot
- (UIImage*)getScreenshotWithSize:(CGSize)size;

// returns the session's current paramters
- (rdpSettings*)getSessionParams;

// returns the session's name (usually the label of the bookmark the session was created with)
- (NSString*)sessionName;

//__REALWAT__: BEGIN: 16/12/2013: GC-45

- (void) onPasteboardTimerFired :(NSTimer*) timer;

- (NSData *)imageAsBMP32:(UIImage *)image includeHeader:(BOOL)flag;

- (void) startProgressUploadingImage;
- (void) stopProgressUploadingImage;
//__REALWAT__: END

//__REALWAT__: BEGIN: 16/12/2013: GC-5038
-(tSPH_ErrorCode)checkExceedImageSize:(UIImage*)pImage
                               output:(BOOL *)pStatus;

-(tSPH_ErrorCode)checkExceedImageResolution:(UIImage*)pImage
                                     output:(UIImage**)pOutImage;

//__REALWAT__: END
@end
