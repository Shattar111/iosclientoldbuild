/*
 RDP Session object 
 
 Copyright 2013 Thincast Technologies GmbH, Authors: Martin Fleisz, Dorian Johnson
 
 This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. 
 If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#import "ios_freerdp.h"
#import "ios_freerdp_ui.h"
#import "ios_freerdp_events.h"

#import "freerdp/client/cliprdr.h"
#import "SPH_Constant.h"

#import "Utils.h"
#import "RDPSession.h"
#import "TSXTypes.h"
#import "Bookmark.h"
#import "ConnectionParams.h"

NSString* TSXSessionDidDisconnectNotification = @"TSXSessionDidDisconnect";
NSString* TSXSessionDidFailToConnectNotification = @"TSXSessionDidFailToConnect";

@interface RDPSession (Private)
- (void)runSession;
- (void)runSessionFinished:(NSNumber*)result;
- (mfInfo*)mfi;

// The connection thread calls these on the main thread.
- (void)sessionWillConnect;
- (void)sessionDidConnect;
- (void)sessionDidDisconnect;
- (void)sessionDidFailToConnect:(int)reason;
- (void)sessionBitmapContextWillChange;
- (void)sessionBitmapContextDidChange;
@end

@implementation RDPSession

@synthesize delegate=_delegate, params=_params, toolbarVisible = _toolbar_visible, uiRequestCompleted = _ui_request_completed, bookmark = _bookmark;

//__REALWAT__:BEGIN: 13/12/2013: PTS: GC-45
@synthesize pasteboard_rd       = pasteboard_rd;
@synthesize pasteboard_wr       = pasteboard_wr;
@synthesize pasteboard_format   = pasteboard_format;
//__REALWAT__:END

+ (void)initialize
{
    ios_init_freerdp();
}

// Designated initializer.
- (id)initWithBookmark:(ComputerBookmark *)bookmark
{
	if (!(self = [super init]))
		return nil;
	
	if (!bookmark)
		[NSException raise:NSInvalidArgumentException format:@"%s: params may not be nil.", __func__];
	
    _bookmark = [bookmark retain];
	_params = [[bookmark params] copy];
    _name = [[bookmark label] retain];
    _delegate = nil;	
    _toolbar_visible = YES;
	_freerdp = ios_freerdp_new();
    rdpSettings* settings = _freerdp->settings;	
    _ui_request_completed = [[NSCondition alloc] init];
    
    BOOL connected_via_3g = ![bookmark conntectedViaWLAN];
    
	// Screen Size is set on connect (we need a valid delegate in case the user choose an automatic screen size)
    
	// Other simple numeric settings
	if ([_params hasValueForKey:@"colors"])
		settings->ColorDepth = [_params intForKey:@"colors" with3GEnabled:connected_via_3g];
	
	if ([_params hasValueForKey:@"port"])
		settings->ServerPort = [_params intForKey:@"port"];
	
	if ([_params boolForKey:@"console"])
		settings->ConsoleSession = 1;

	// connection info	
    settings->ServerHostname = strdup([_params UTF8StringForKey:@"hostname"]);
	
	// String settings
	if ([[_params StringForKey:@"username"] length])
		settings->Username = strdup([_params UTF8StringForKey:@"username"]);
    
	if ([[_params StringForKey:@"password"] length])
		settings->Password = strdup([_params UTF8StringForKey:@"password"]);
	
	if ([[_params StringForKey:@"domain"] length])
		settings->Domain = strdup([_params UTF8StringForKey:@"domain"]);
    
	settings->ShellWorkingDirectory = strdup([_params UTF8StringForKey:@"working_directory"]);
	settings->AlternateShell = strdup([_params UTF8StringForKey:@"remote_program"]);
    
    //redirected clipboard
    settings->RedirectClipboard = TRUE;
    
	// RemoteFX
	if ([_params boolForKey:@"perf_remotefx" with3GEnabled:connected_via_3g])
	{
		settings->RemoteFxCodec = TRUE;
		settings->FastPathOutput = TRUE;
		settings->ColorDepth = 32;
		settings->LargePointerFlag = TRUE;
        settings->FrameMarkerCommandEnabled = TRUE;
        settings->FrameAcknowledge = 10;
	}
	else
	{
		// enable NSCodec if remotefx is not used
		settings->NSCodec = TRUE;
	}

	settings->BitmapCacheV3Enabled = TRUE;

	// Performance flags
    settings->DisableWallpaper = ![_params boolForKey:@"perf_show_desktop" with3GEnabled:connected_via_3g];
    settings->DisableFullWindowDrag = ![_params boolForKey:@"perf_window_dragging" with3GEnabled:connected_via_3g];
    settings->DisableMenuAnims = ![_params boolForKey:@"perf_menu_animation" with3GEnabled:connected_via_3g];
    settings->DisableThemes = ![_params boolForKey:@"perf_windows_themes" with3GEnabled:connected_via_3g];
    settings->AllowFontSmoothing = [_params boolForKey:@"perf_font_smoothing" with3GEnabled:connected_via_3g];
    settings->AllowDesktopComposition = [_params boolForKey:@"perf_desktop_composition" with3GEnabled:connected_via_3g];
    
	settings->PerformanceFlags = PERF_FLAG_NONE;
	if (settings->DisableWallpaper)
        settings->PerformanceFlags |= PERF_DISABLE_WALLPAPER;        
	if (settings->DisableFullWindowDrag)
        settings->PerformanceFlags |= PERF_DISABLE_FULLWINDOWDRAG;
	if (settings->DisableMenuAnims)
		settings->PerformanceFlags |= PERF_DISABLE_MENUANIMATIONS;
	if (settings->DisableThemes)
        settings->PerformanceFlags |= PERF_DISABLE_THEMING;
	if (settings->AllowFontSmoothing)
		settings->PerformanceFlags |= PERF_ENABLE_FONT_SMOOTHING;
    if (settings->AllowDesktopComposition)
        settings->PerformanceFlags |= PERF_ENABLE_DESKTOP_COMPOSITION;
		
	if ([_params hasValueForKey:@"width"])
		settings->DesktopWidth = [_params intForKey:@"width"];
	if ([_params hasValueForKey:@"height"])
		settings->DesktopHeight = [_params intForKey:@"height"];
	
    // security
    switch ([_params intForKey:@"security"]) 
    {
        case TSXProtocolSecurityNLA:
            settings->RdpSecurity = FALSE;
            settings->TlsSecurity = FALSE;
            settings->NlaSecurity = TRUE;
            settings->ExtSecurity = FALSE;
            break;

        case TSXProtocolSecurityTLS:
            settings->RdpSecurity = FALSE;
            settings->TlsSecurity = TRUE;
            settings->NlaSecurity = FALSE;
            settings->ExtSecurity = FALSE;
            break;
            
        case TSXProtocolSecurityRDP:
            settings->RdpSecurity = TRUE;
            settings->TlsSecurity = FALSE;
            settings->NlaSecurity = FALSE;
            settings->ExtSecurity = FALSE;
            settings->DisableEncryption = TRUE;
            settings->EncryptionMethods = ENCRYPTION_METHOD_40BIT | ENCRYPTION_METHOD_128BIT | ENCRYPTION_METHOD_FIPS;
            settings->EncryptionLevel = ENCRYPTION_LEVEL_CLIENT_COMPATIBLE;
            break;

        default:
            break;
    }
    
    // ts gateway settings
    if ([_params boolForKey:@"enable_tsg_settings"])
    {
        settings->GatewayHostname = strdup([_params UTF8StringForKey:@"tsg_hostname"]);
        settings->GatewayPort = [_params intForKey:@"tsg_port"];
        settings->GatewayUsername = strdup([_params UTF8StringForKey:@"tsg_username"]);
        settings->GatewayPassword = strdup([_params UTF8StringForKey:@"tsg_password"]);
        settings->GatewayDomain = strdup([_params UTF8StringForKey:@"tsg_domain"]);
        settings->GatewayUsageMethod = TSC_PROXY_MODE_DIRECT;
        settings->GatewayEnabled = TRUE;
        settings->GatewayUseSameCredentials = FALSE;
    }
    
	// Remote keyboard layout
	settings->KeyboardLayout = 0x409;
    
	// Audio settings
    settings->AudioPlayback = FALSE;
    settings->AudioCapture = FALSE;
	
	// __REALWAT__ : BEGIN : 2013/Nov/27 : Added
	// Redirect clipboard settings
	settings->RedirectClipboard = TRUE;
	// __REALWAT__ : END
	
    
	[self mfi]->session = self;
	return self;
}

- (void)dealloc
{
	[self setDelegate:nil];
    [_bookmark release];
    [_name release];
	[_params release];
    [_ui_request_completed release];
    
	ios_freerdp_free(_freerdp);
	
	[super dealloc];
}

- (CGContextRef)bitmapContext
{
    return [self mfi]->bitmap_context;
}

#pragma mark -
#pragma mark Connecting and disconnecting

- (void)connect
{    
	// Set Screen Size to automatic if widht or height are still 0
    rdpSettings* settings = _freerdp->settings;	
	if (settings->DesktopWidth == 0 || settings->DesktopHeight == 0)
	{
        CGSize size = CGSizeZero;        
        if ([[self delegate] respondsToSelector:@selector(sizeForFitScreenForSession:)])
            size = [[self delegate] sizeForFitScreenForSession:self];
        
        if (!CGSizeEqualToSize(CGSizeZero, size))
        {
            [_params setInt:size.width forKey:@"width"];
            [_params setInt:size.height forKey:@"height"];
            settings->DesktopWidth = size.width;
            settings->DesktopHeight = size.height;
        }
	}
    
    // TODO: This is a hack to ensure connections to RDVH with 16bpp don't have an odd screen resolution width
    //       Otherwise this could result in screen corruption ..
    if (settings->ColorDepth <= 16)
        settings->DesktopWidth &= (~1);
    
	[self performSelectorInBackground:@selector(runSession) withObject:nil];
}

- (void)disconnect
{
	mfInfo* mfi = [self mfi];
	
    ios_events_send(mfi, [NSDictionary dictionaryWithObject:@"disconnect" forKey:@"type"]);

	if (mfi->connection_state == TSXConnectionConnecting)
	{
		mfi->unwanted = YES;
		[self sessionDidDisconnect];
		return;
	}	
}

- (TSXConnectionState)connectionState
{
	return [self mfi]->connection_state;
}

// suspends the session
-(void)suspend
{
    if(!_suspended)
    {
        _suspended = YES;
//        instance->update->SuppressOutput(instance->context, 0, NULL);
    }
}

// resumes a previously suspended session
-(void)resume
{
    if(_suspended)
    {
/*        RECTANGLE_16 rec;
        rec.left = 0;
        rec.top = 0;
        rec.right = instance->settings->width;
        rec.bottom = instance->settings->height;
*/        
        _suspended = NO;
//        instance->update->SuppressOutput(instance->context, 1, &rec);
//        [delegate sessionScreenSettingsChanged:self];        
    }
}

// returns YES if the session is started
-(BOOL)isSuspended
{
    return _suspended;
}

#pragma mark -
#pragma mark Input events

- (void)sendInputEvent:(NSDictionary*)eventDescriptor
{
	if ([self mfi]->connection_state == TSXConnectionConnected)
		ios_events_send([self mfi], eventDescriptor);
}

#pragma mark -
#pragma mark Server events (main thread)

- (void)setNeedsDisplayInRectAsValue:(NSValue*)rect_value
{
	if ([[self delegate] respondsToSelector:@selector(session:needsRedrawInRect:)])
		[[self delegate] session:self needsRedrawInRect:[rect_value CGRectValue]];
} 


#pragma mark -
#pragma mark interface functions

- (UIImage*)getScreenshotWithSize:(CGSize)size
{
    NSAssert([self mfi]->bitmap_context != nil, @"Screenshot requested while having no valid RDP drawing context");
    
	CGImageRef cgImage = CGBitmapContextCreateImage([self mfi]->bitmap_context);
	UIGraphicsBeginImageContext(size);
	
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0, size.height);
	CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, size.width, size.height), cgImage);
	
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();

	UIGraphicsEndImageContext();
	CGImageRelease(cgImage);	
	
	return viewImage;    
}

- (rdpSettings*)getSessionParams
{
    return _freerdp->settings;
}

- (NSString*)sessionName
{
    return _name;
}

//__REALWAT__: BEGIN: 13/12/2013: GC-45
/*****************************************************************************
 ; onPasteboardTimerFired
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to handle pasteboard timer event
 ;
 ; RETURNS
 ;	NONE
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;
 ;  OUTPUT:
 ;
 ;
 ; IMPLEMENTATION
 ;
 ;
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	  __Description__
 ; 13/12/2013        Realwat Inc         GC-45           Creation
 ******************************************************************************/
- (void) onPasteboardTimerFired :(NSTimer*) timer
{
    
    int      i = 0;
    NSArray *lpPasteImages;
    NSArray *lpPasteStrings;
    
    i = (int) [pasteboard_rd changeCount];
    
    if (i != pasteboard_changecount)
    {
        pasteboard_changecount = i;
        
        lpPasteImages = [pasteboard_rd images];
        
        if ( 0 < lpPasteImages )
        {
            cliprdr_send_supported_format_list(_freerdp, CB_FORMAT_DIB);
        }
        else
        {
            lpPasteStrings = [pasteboard_rd strings];
            
            if ( 0 < lpPasteStrings.count )
            {
                cliprdr_send_supported_format_list(_freerdp,
                                                   CB_FORMAT_UNICODETEXT);
            }
        }
    }
    
}


-(NSData *)imageAsBMP32:(UIImage *)image includeHeader:(BOOL)flag
{
    // Convert to raw format and determine height and width.
    CGImageRef imgRef      = [image CGImage];
    CGFloat    height      = CGImageGetHeight(imgRef);
    CGFloat    width       = CGImageGetWidth(imgRef);
    
    // Set Bitmpat (BMP) parameters and allocate memory.
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(height * width * bytesPerPixel);
    
    // Create context, draw/convert image and release.
    CGContextRef context = CGBitmapContextCreate(rawData, width, height, bitsPerComponent, bytesPerRow,
                                                 colorSpace, kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imgRef);
    CGContextRelease(context);
    
    // Allocate Bitmap headers and set values.
    BITMAPFILEHEADER bfh;
    BITMAPINFOHEADER bih;
    
    memcpy((char *)&bfh.bfType, "BM", 2);
    bfh.bfSize = (DWORD)(sizeof(bfh) + sizeof(bih) + bytesPerPixel * height * width);
    bfh.bfReserved1 = 0;
    bfh.bfReserved2 = 0;
    bfh.bfOffBits = sizeof(bfh) + sizeof(bih);
    
    bih.biSize = sizeof(bih);
    bih.biWidth = (LONG)width;
    bih.biHeight = (LONG)height;
    bih.biPlanes = 1;
    bih.biBitCount = 32;
    bih.biCompression = BI_RGB; // uncompressed 24-bit RGB
    bih.biSizeImage = 0;        // can be zero for BI_RGB bitmaps
    bih.biXPelsPerMeter = 3780; // 96dpi equivalent/usually ignored
    bih.biYPelsPerMeter = 3780;
    bih.biClrUsed = 0;
    bih.biClrImportant = 0;
    
    // Write image to NSData
    DWORD bmpSize = bfh.bfSize;
    if (!flag) bmpSize = (DWORD)(sizeof(bih) * bytesPerPixel * height * width);
    NSMutableData *data = [NSMutableData dataWithCapacity:bmpSize];
    
    if (flag) [data appendBytes:&bfh length:sizeof(bfh)];
    [data appendBytes:&bih length:sizeof(bih)];
    
    int byteIndex = (height - 1) * bytesPerRow;
    while (byteIndex >= 0) {
        [data appendBytes:&rawData[byteIndex] length:bytesPerRow];
        byteIndex -= bytesPerRow;
    }
    
    // Release memory and return image.
    free(rawData);
    return (NSData *)data;
}
// __REALWAT__ : BEGIN : 05/02/2014
/*****************************************************************************
 ; checkExceedImageResolution
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to exceed resolution of upload image
 ;
 ; RETURNS
 ;	SPH_ERROR_PARAMETER : this code is returned when parameter is invalid
 ;  SPH_SUCCESS         : this return code occur when no error
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;      pImage  : image to be upload to server
 ;  OUTPUT:
 ;      pStatus : excedd status of image
 ;
 ; IMPLEMENTATION
 ;    validate parameter
 ;    Convert to raw format and determine height and width
 ;    check execeed resolution
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	    __Description__
 ; 30/12/2013        Realwat Inc         GC-5038           Creation
 ******************************************************************************/
-(tSPH_ErrorCode)checkExceedImageResolution:(UIImage*)pImage
                                     output:(UIImage **)pOutImage
{
    CGImageRef lpImgRef     = nil;
    CGFloat    lHeight      = 0;
    CGFloat    lWidth       = 0;
    CGSize     lSize        = CGSizeMake(0.0, 0.0);
    tSPH_ErrorCode lReturn  = SPH_ERROR;
    
    // validate parameter
    if ( nil == pImage )
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return ( SPH_ERROR_PARAMETER );
    }
    if (nil == pOutImage)
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return ( SPH_ERROR_PARAMETER );
    }
    
    // Convert to raw format and determine height and width.
    lpImgRef     = [pImage CGImage];
    lHeight      = CGImageGetHeight(lpImgRef);
    lWidth       = CGImageGetWidth(lpImgRef);
    
    // check execeed resolution
    if ( (SPH_UPLOAD_IMG_MAX_RESOLUTION > lWidth ) &&
         (SPH_UPLOAD_IMG_MAX_RESOLUTION > lHeight ) )
    {
        *pOutImage = pImage;
        return (SPH_SUCCESS);
    }
    lReturn = [self calculateNewImageSize:pImage NewSize:&lSize];
    if (SPH_SUCCESS != lReturn)
    {
        SPH_LOG_CONSOL(lReturn);
        return ( lReturn );
    }
    lReturn = [self getNewImage:pOutImage OldImage:pImage scaledToSize:lSize];
    if (SPH_SUCCESS != lReturn)
    {
        SPH_LOG_CONSOL(lReturn);
        return ( lReturn );
    }
    
    return ( SPH_SUCCESS );
}

-(tSPH_ErrorCode)calculateNewImageSize:(UIImage*)pImg
                               NewSize:(CGSize*)pSize
{
    CGFloat         lImgHeight      = 0;
    CGFloat         lImgWidth       = 0;
    tSPH_ErrorCode  lReturn         = SPH_ERROR;
    CGImageRef      lImgRef         = nil;
    CGFloat         lAspRatio       = 0;
    CGSize          lSize           = CGSizeMake(0.0, 0.0);
    if (nil == pImg)
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return SPH_ERROR_PARAMETER;
    }
    lImgRef = [pImg CGImage];
    if (nil == lImgRef)
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return SPH_ERROR_PARAMETER;
    }
    lImgHeight = CGImageGetHeight(lImgRef);
    lImgWidth = CGImageGetWidth(lImgRef);
    if ((SPH_UPLOAD_IMG_MAX_RESOLUTION > lImgWidth) &&
        (SPH_UPLOAD_IMG_MAX_RESOLUTION > lImgHeight))
    {
        return SPH_SUCCESS;
    }
    lSize.width = lImgWidth;
    lSize.height = lImgHeight;
    lReturn = [self calculateAspectRatioImage:lSize AspectRatio:&lAspRatio];
  //  NSLog(@"Aspect Ratio size = %f",lAspRatio);
    if (SPH_SUCCESS != lReturn)
    {
        SPH_LOG_CONSOL(lReturn);
        return lReturn;
    }
    lImgWidth = SPH_UPLOAD_IMG_MAX_RESOLUTION;
    lImgHeight = lImgWidth * lAspRatio;
    lSize.width = lImgWidth;
    lSize.height = lImgHeight;
    *pSize = lSize;
    
    return SPH_SUCCESS;
}
-(tSPH_ErrorCode)calculateAspectRatioImage:(CGSize)sizeImage
                               AspectRatio:(CGFloat*)pAspRatio
{
    if (nil == pAspRatio)
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return SPH_ERROR_PARAMETER;
    }
    CGFloat    lHeight      = 0;
    CGFloat    lWidth       = 0;
    lWidth = sizeImage.width;
    lHeight = sizeImage.height;
    *pAspRatio = lHeight / lWidth;
    
    return SPH_SUCCESS;
}
- (tSPH_ErrorCode)getNewImage:(UIImage **)pNewimage
                     OldImage:(UIImage*)pImage
                 scaledToSize:(CGSize)newSize
{
    if (nil == pNewimage || nil == pImage)
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return SPH_ERROR_PARAMETER;
    }
   // NSLog(@"New size image width = %f, heigth = %f",newSize.width,newSize.height);
    UIImage *lpNewImg = nil;
    UIGraphicsBeginImageContext(newSize);
    [pImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    lpNewImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    *pNewimage = lpNewImg;
    return SPH_SUCCESS;
}

// __REALWAT__ : END : 05/02/2014

/*****************************************************************************
 ; checkExceedImageSize
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to exceed size of upload image
 ;
 ; RETURNS
 ;	SPH_ERROR_PARAMETER : this code is returned when parameter is invalid
 ;  SPH_SUCCESS         : this return code occur when no error
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;      pImage  : image to be upload to server
 ;  OUTPUT:
 ;      pStatus : excedd status of image
 ;
 ; IMPLEMENTATION
 ;    validate parameter
 ;    get image data
 ;    check if size of image is bigger than maximum image size
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	    __Description__
 ; 26/12/2013        Realwat Inc         GC-5038           Creation
 ******************************************************************************/
-(tSPH_ErrorCode)checkExceedImageSize:(UIImage*)pImage
                               output:(BOOL *)pStatus
{
    BOOL       lStatus     = NO;
    NSData    *lpImageData = nil;
    NSInteger  lSize       = 0;
    
    // validate parameter
    if ( nil == pImage )
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return ( SPH_ERROR_PARAMETER );
    }
    
    // get image data
    lpImageData = UIImageJPEGRepresentation(pImage, 1);//(pImage);
    
    lSize = 1024 * ( 1024 * SPH_UPLOAD_IMG_MAX_SIZE );
    
    // check if size of image is bigger than maximum image size
    if ( lSize < lpImageData.length)
    {
        lStatus = YES;
    }
    
   // NSLog(@"limit: %d, size: %d", lSize, lpImageData.length);
    
    *pStatus = lStatus;

    return ( SPH_SUCCESS );
}

/*****************************************************************************
 ; showExceedImageSizeAlert
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to display alert of exceed image size
 ;
 ; RETURNS
 ;  SPH_SUCCESS         : this return code occur when no error
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;      lpMsg  : string of message
 ;  OUTPUT:
 ;      NONE
 ;
 ; IMPLEMENTATION
 ;    tell delegate session did uploading with exceed image
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	    __Description__
 ; 26/12/2013        Realwat Inc         GC-5038           Creation
 ******************************************************************************/
-(tSPH_ErrorCode)showExceedImageSizeAlert: (NSString *)lpMsg
{
    BOOL       lStatus     = NO;
    
    // validate parameter
    if ( nil == lpMsg )
    {
        SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
        return ( SPH_ERROR_PARAMETER);
    }
    
    lStatus = [[self delegate] respondsToSelector:
                            @selector(sessionDidUploadExceedImage:message:)];
     
     // tell delegate session did uploading with exceed image
     if (YES == lStatus)
     {
         [[self delegate] sessionDidUploadExceedImage:self
                                              message:lpMsg];
     }
    
    return ( SPH_SUCCESS );
}
/*****************************************************************************
 ; startProgressUploadingImage
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to start progress of upload image
 ;
 ; RETURNS
 ;	NONE
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;
 ;  OUTPUT:
 ;
 ;
 ; IMPLEMENTATION
 ;      start progress animation of upload image within 2 seconds
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	    __Description__
 ; 20/12/2013        Realwat Inc         GC-5036           Creation
 ******************************************************************************/
- (void) startProgressUploadingImage
{
    //NSLog(@"start upload");
    
    // start progress animation of upload image within 2 seconds
    [self performSelector:@selector(startProgressUpload)
               withObject:nil
               afterDelay:0.2];
}

/*****************************************************************************
 ; startProgressUpload
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to start progress of upload image
 ;
 ; RETURNS
 ;	NONE
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;
 ;  OUTPUT:
 ;
 ;
 ; IMPLEMENTATION
 ;    tell delegate session did start uploading image
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	    __Description__
 ; 20/12/2013        Realwat Inc         GC-5036           Creation
 ******************************************************************************/
-(void)startProgressUpload
{
    BOOL  lStatus = NO;
    
    lStatus = [[self delegate] respondsToSelector:
                                    @selector(sessionDidStartUploadingImage:)];
    
    // tell delegate session did start uploading image
    if (YES == lStatus)
    {
        [[self delegate] sessionDidStartUploadingImage:self];
    }
}
/*****************************************************************************
 ; stopProgressUploadingImage
 ;----------------------------------------------------------------------------
 ; DESCRIPTION
 ;	Function is used to start progress of upload image
 ;
 ; RETURNS
 ;	NONE
 ;
 ; INTERFACE NOTES
 ;  INPUT:
 ;
 ;  OUTPUT:
 ;
 ;
 ; IMPLEMENTATION
 ;    tell delegate session did stop uploading image
 ;
 ; HISTORY
 ; __date__		:	__authorId__	:	__PTS__  :	    __Description__
 ; 20/12/2013        Realwat Inc         GC-5036           Creation
 ******************************************************************************/
-(void)stopProgressUploadingImage
{
    BOOL  lStatus = NO;
    
    lStatus = [[self delegate] respondsToSelector:
                                @selector(sessionDidFinishedUploadingImage:)];
    
    // tell delegate session did stop uploading image
    if (YES == lStatus)
    {
        [[self delegate] sessionDidFinishedUploadingImage:self];
    }
}
//__REALWAT__: END

@end

#pragma mark -
@implementation RDPSession (Private)

- (mfInfo*)mfi
{
	return MFI_FROM_INSTANCE(_freerdp);
}

// Blocks until rdp session finishes.
- (void)runSession
{
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];	

    // Run the session
    [self performSelectorOnMainThread:@selector(sessionWillConnect) withObject:nil waitUntilDone:YES];
    int result_code = ios_run_freerdp(_freerdp);
    [self mfi]->connection_state = TSXConnectionDisconnected;
    [self performSelectorOnMainThread:@selector(runSessionFinished:) withObject:[NSNumber numberWithInt:result_code] waitUntilDone:YES];

    [pool release];
}

// Main thread.
- (void)runSessionFinished:(NSNumber*)result
{
	int result_code = [result intValue];
	
	switch (result_code)
	{
		case MF_EXIT_CONN_CANCELED:
			[self sessionDidDisconnect];
			break;
		case MF_EXIT_LOGON_TIMEOUT:
		case MF_EXIT_CONN_FAILED:
			[self sessionDidFailToConnect:result_code];
			break;
		case MF_EXIT_SUCCESS:
		default:
			[self sessionDidDisconnect];	
            break;
	}			 
}

#pragma mark -
#pragma mark Session management (main thread)

- (void)sessionWillConnect
{	
	if ([[self delegate] respondsToSelector:@selector(sessionWillConnect:)])
		[[self delegate] sessionWillConnect:self];
}

- (void)sessionDidConnect
{
    //__REALWAT__: BEGIN: PTS: GC-45
    
    pasteboard_changecount = (int) [pasteboard_rd changeCount];

    pasteboard_rd    = [UIPasteboard generalPasteboard];
    
    pasteboard_wr    = [UIPasteboard generalPasteboard];
    
    pasteboard_timer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                        target:self
                                    selector:@selector(onPasteboardTimerFired:)
                                                      userInfo:nil
                                                       repeats:YES];
    //__REALWAT__: END
    
	if ([[self delegate] respondsToSelector:@selector(sessionDidConnect:)])	
		[[self delegate] sessionDidConnect:self];	
}

- (void)sessionDidFailToConnect:(int)reason
{	
    [[NSNotificationCenter defaultCenter] postNotificationName:TSXSessionDidFailToConnectNotification object:self];

	if ([[self delegate] respondsToSelector:@selector(session:didFailToConnect:)])
		[[self delegate] session:self didFailToConnect:reason];
}

- (void)sessionDidDisconnect
{	
    [[NSNotificationCenter defaultCenter] postNotificationName:TSXSessionDidDisconnectNotification object:self];
	
    if ([[self delegate] respondsToSelector:@selector(sessionDidDisconnect:)])	
		[[self delegate] sessionDidDisconnect:self];
}

- (void)sessionBitmapContextWillChange
{
	if ([[self delegate] respondsToSelector:@selector(sessionBitmapContextWillChange:)])	
		[[self delegate] sessionBitmapContextWillChange:self];
}

- (void)sessionBitmapContextDidChange
{
	if ([[self delegate] respondsToSelector:@selector(sessionBitmapContextDidChange:)])
		[[self delegate] sessionBitmapContextDidChange:self];
}

- (void)sessionRequestsAuthenticationWithParams:(NSMutableDictionary*)params
{
	if ([[self delegate] respondsToSelector:@selector(session:requestsAuthenticationWithParams:)])
		[[self delegate] session:self requestsAuthenticationWithParams:params];    
}

- (void)sessionVerifyCertificateWithParams:(NSMutableDictionary*)params
{
	if ([[self delegate] respondsToSelector:@selector(session:verifyCertificateWithParams:)])
		[[self delegate] session:self verifyCertificateWithParams:params];    
}

@end
