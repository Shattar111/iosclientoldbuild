//

//  Sphere3DAPI.m

//  IE on Demand

//

//  Created by Xtreme Labs on 2013-10-10.

//

//

#import "Sphere3DAPI.h"
#import "UserSession.h"
#import "RegistrationData.h"
#import "NSError+Sphere3d.h"


static NSString *kRegistrationURL = SPH_SERVER_API_URL_REGISTRATION;
static NSString *kRemoteAppName = @"empty";


typedef enum ExpectedResponse {
    
    ExpectedResponseNothing, //no response expected
    ExpectedResponseReady, //"Ready" string
    ExpectedResponseLogin, //login response, see docs
    ExpectedResponseAuthenticate, //re-authentication response, see docs
    ExpectedResponseAppLaunch //{"success":true} or {"success":false,"message":"error message"}
    
} ExpectedResponse;


@interface Sphere3DAPI () <NSStreamDelegate>

@property (nonatomic, copy) LoginCompletionBlock loginCompletionBlock;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *ipaddress;
@property (nonatomic) BOOL restoringFromSession;
@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;
@property (nonatomic, copy) ReadyToLaunchRemoteBlock readyToLaunchRemoteBlock;

//protocol state
@property (nonatomic) ExpectedResponse expectedResponse;
@property (nonatomic) BOOL connectedToAuthServer;
@property (nonatomic) BOOL connectingToAuthServer;
@property (nonatomic) NSInteger authServerIndex;
@property (nonatomic) NSString *IpSuccess;
@property (nonatomic) BOOL connectedToAuthServer2;
@property (nonatomic) BOOL connectingToAuthServer2;
@property (nonatomic) NSInteger authServerIndex2;
@property (nonatomic, retain) NSData *pendingData;

//TODO: Create request queue
@property (nonatomic, retain) NSString *scheduledAppLaunch;

@end


@implementation Sphere3DAPI


+ (instancetype)sharedInstance {
    
    static Sphere3DAPI *instance = nil;
    if (nil == instance) {
        instance = [[Sphere3DAPI alloc] init];
    }
    return instance;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        self.restoringFromSession = NO;
    }
    
    return self;
    
}


- (void)dealloc {
    
    Block_release(_loginCompletionBlock);
    Block_release(_readyToLaunchRemoteBlock);
    [_username release];
    [_password release];
    [_ipaddress release];
    [_inputStream release];
    [_outputStream release];
    
    [super dealloc];
    
}



#pragma mark - API Calls

- (void)registerUserWithRegistrationData:(RegistrationData*)registrationData completionBlock:(RegistrationCompletionBlock)complete {
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kRegistrationURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[registrationData postParams] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    RegistrationCompletionBlock callback = complete;
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSError *responseError = nil;
        // NSLog(@"Server returned: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        NSError *error;
        NSDictionary *responseDict  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        RegistrationResponse responseCode = [responseDict[@"successCode"] intValue];
        
        if (nil != connectionError) {
            
            responseError = [NSError sphere3dUnableToSubmitFormData];
            callback(responseCode, responseError);
            return;
        }
        
        if (nil != error)
        {
            responseError = [NSError sphere3dUnableToSubmitFormData];
            callback(responseCode, responseError);
            return;
        }
        
        if (RegistrationResponseSuccess != responseCode) {
            
            responseError = [NSError errorWithDomain:@"Sphere3dAPI" code:0 userInfo:@{@"message": responseDict[@"message"]}];
        }
        
        callback(responseCode, responseError);
        
    }];
    
}



-(void)loginWithUsername:(NSString*)username
             andPassword:(NSString *)password
     withCompletionBlock:(LoginCompletionBlock)complete
withReadyToLaunchRemoteBlock:(ReadyToLaunchRemoteBlock)ready
      andSelectedAppName:(NSString*) selectedAppName
{
    
    self.restoringFromSession = NO;
    self.loginCompletionBlock = complete;
    self.readyToLaunchRemoteBlock = ready;
    
    self.username = username;
    self.password = password;
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    [self sendLoginRequest:selectedAppName];
}

- (void)connectTo2:(NSString *)address port:(UInt32)port requireSSL:(BOOL)requireSSL {
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)address, port, &readStream, &writeStream);
    NSAssert(readStream && writeStream, @"Can't create sockets to connect to auth server");
    //toll-free bridge
    self.inputStream = (NSInputStream *)readStream;
    self.outputStream = (NSOutputStream *)writeStream;
    
    if (requireSSL) {
        [self.inputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
        [self.outputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
        NSDictionary *settings = @{(id)kCFStreamSSLValidatesCertificateChain : @NO};
        CFReadStreamSetProperty(readStream, kCFStreamPropertySSLSettings, settings);
        CFWriteStreamSetProperty(writeStream, kCFStreamPropertySSLSettings, settings);
    }
    
    //CF memory management
    CFRelease(readStream);
    CFRelease(writeStream);
    
    self.inputStream.delegate = self;
    self.outputStream.delegate = self;
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.inputStream open];
    [self.outputStream open];
}


- (void)connectTo:(NSString *)address port:(UInt32)port requireSSL:(BOOL)requireSSL {
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSObject * object = [prefs objectForKey:@"ipaddressjson"];
    
    if(object != nil){
        
        NSString* ipadressjson = @"192.168.0.173";
        CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)ipadressjson, 9990, &readStream, &writeStream);
        
        NSAssert(readStream && writeStream, @"Can't create sockets to connect to auth server");
        
        self.inputStream = (NSInputStream *)readStream;
        self.outputStream = (NSOutputStream *)writeStream;
        
        NSLog(@"ip json ");
    }
    else
    {
         NSArray *ipAddressArray = [NSArray arrayWithObjects:@"74.205.50.146", @"198.61.142.189", @"146.20.32.78", @"ambrsc.cloudapp.net", nil];
        
        uint32_t rnd = arc4random_uniform([ipAddressArray count]);
        
        NSString *randomObject = [ipAddressArray objectAtIndex:rnd];
        
        NSLog(@"ipaddress----%@",randomObject);

        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        [prefs setObject:randomObject forKey:@"SelectedIpAddress"];
        [prefs synchronize];
        
        CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)randomObject, 9990, &readStream, &writeStream);
        
        NSAssert(readStream && writeStream, @"Can't create sockets to connect to auth server");
        
        self.inputStream = (NSInputStream *)readStream;
        self.outputStream = (NSOutputStream *)writeStream;
        
        NSLog(@"ip that user entered");
    }
    
    if (requireSSL) {
        
        [self.inputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
        [self.outputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
        
        NSDictionary *settings = @{(id)kCFStreamSSLValidatesCertificateChain : @NO};
        
        CFReadStreamSetProperty(readStream, kCFStreamPropertySSLSettings, settings);
        CFWriteStreamSetProperty(writeStream, kCFStreamPropertySSLSettings, settings);
    }
    //CF memory management
    
    CFRelease(readStream);
    CFRelease(writeStream);
    
    self.inputStream.delegate = self;
    self.outputStream.delegate = self;
    
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [self.inputStream open];
    [self.outputStream open];
}



- (void)disconnect {
    
    self.connectedToAuthServer = NO;
    self.connectingToAuthServer = NO;
    
    self.authServerIndex = 0;
    self.expectedResponse = ExpectedResponseNothing;
    
    [self.inputStream close];
    [self.outputStream close];
}



- (void)sendRequestJSON:(NSDictionary *)dict {
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:NULL];
    [self sendData:requestData];
}



- (NSString *)serverHash {
    
    static NSString *const defaultServerHash = @"F36374DB7BA78EE15D465D1D1DADCB91";
    NSString *savedServerHash = [[NSUserDefaults standardUserDefaults] stringForKey:@"ServerHash"];
    
    if (!savedServerHash) {
        
        [self setServerHash:defaultServerHash];
        return defaultServerHash;
    }
    
    return savedServerHash;
}



- (void)setServerHash:(NSString *)serverHash {
    
    [[NSUserDefaults standardUserDefaults] setObject:serverHash forKey:@"ServerHash"];
}



- (NSArray *)authServers {
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* IPAdressEnteredSaved = @"146.20.32.78";
    NSLog(@"ipaddress----%@",IPAdressEnteredSaved);
    
    NSArray *defaultAuthServers = @[@{@"ip":IPAdressEnteredSaved,@"port":@9990}];
    NSArray *authServers = [[NSUserDefaults standardUserDefaults] arrayForKey:@"AuthServers2"];
    
    if (!authServers) {
        
        [self setAuthServers:defaultAuthServers];
        return defaultAuthServers;
    }
    
    return authServers;
}



- (void)setAuthServers:(NSArray *)authServers {
    
    [[NSUserDefaults standardUserDefaults] setObject:authServers forKey:@"AuthServers2"];
}

- (void)sendLoginRequest2:(NSString*) appName {
    
    //NSString * defaultServerHash = [self serverHash];
    if (appName==nil){
        appName = kRemoteAppName;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* AccountCode = [prefs objectForKey:@"TraceFreeAccountCode"];
    
    if ([AccountCode length] == 0) {
        
        NSLog(@"connect tcp method if code is null");
        
        NSDictionary *requestTraceFree = @{@"request" : @"templogin", @"tempcode" : appName, @"accountcode" : @""};
        
        self.launchedApp = appName;
        self.authServerIndex = 0;
        
        self.connectingToAuthServer = YES;
        [self connectToNextAuthServer];
        [self sendRequestJSON:requestTraceFree];
        
        self.expectedResponse = ExpectedResponseLogin;
    }
    else
    {
        
        NSLog(@"connect tcp method if code has data then.....");
        
        NSDictionary *requestTraceFree = @{@"request" : @"templogin", @"tempcode" : appName, @"accountcode" : AccountCode};
        
        self.launchedApp = appName;
        self.authServerIndex = 0;
        
        self.connectingToAuthServer = YES;
        [self connectToNextAuthServer];
        [self sendRequestJSON:requestTraceFree];
        
        self.expectedResponse = ExpectedResponseLogin;
    }
    
    /*
     if([AccountCode length] > 0)
     {
     // do something
     }
     */
}

- (void)sendLoginRequest:(NSString*) appName {
    
    //NSString * defaultServerHash = [self serverHash];
    if (appName==nil){
        appName = kRemoteAppName;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* AccountCode = [prefs objectForKey:@"TraceFreeAccountCode"];

    if ([AccountCode length] == 0) {
        
        NSLog(@"connect tcp method if code is null");

        NSDictionary *requestTraceFree = @{@"request" : @"templogin", @"tempcode" : appName, @"accountcode" : @""};
        
        self.launchedApp = appName;
        self.authServerIndex = 0;
        
        self.connectingToAuthServer = YES;
        [self connectToNextAuthServer];
        [self sendRequestJSON:requestTraceFree];
        
        self.expectedResponse = ExpectedResponseLogin;
    }
    else
    {
        
        NSLog(@"connect tcp method if code has data then.....");

        NSDictionary *requestTraceFree = @{@"request" : @"templogin", @"tempcode" : appName, @"accountcode" : AccountCode};
        
        self.launchedApp = appName;
        self.authServerIndex = 0;
        
        self.connectingToAuthServer = YES;
        [self connectToNextAuthServer];
        [self sendRequestJSON:requestTraceFree];
        
        self.expectedResponse = ExpectedResponseLogin;
    }
 
 /*
    if([AccountCode length] > 0)
    {
        // do something
    }
   */
}



- (void)connectToNextAuthServer {
    NSLog(@"this method is hit");
    NSArray *authServers = [self authServers];
    NSDictionary *server = authServers[self.authServerIndex];
    NSString *ip = server[@"ip"];
    UInt32 port = [server[@"port"] integerValue];
    
    [self connectTo:ip port:port requireSSL:YES];
}

- (void)connectToNextAuthServer2 {
    
    
    NSArray *authServers123 = [[NSUserDefaults standardUserDefaults] arrayForKey:@"AuthServers2"];
    //NSLog(@"array 123:%@", authServers123);
    
    NSArray *authServers = [self authServers];
    
    // NSLog(@"saved server:%@", authServers);
    
    //int arrayLength = authServers123.count;
    
    //NSLog(@"count:%i", arrayLength);
    
    
    //int arrayLength2 = _authServerIndex;
    
    //NSLog(@"count2:%i", arrayLength2);
    
    if (self.authServerIndex > [authServers123 count]) {
        [self disconnect];
        [self notifyErrorCode:SPH_ERROR_INTERNET_CONN message:@"Network Error."];
        
    } else {
        
        if (!self.connectedToAuthServer) {
            
            @try
            {
                NSDictionary *server2 = authServers[self.authServerIndex];
                
                
                NSString *ip = server2[@"ip"];
                UInt32 port = [server2[@"port"] integerValue];
                [self connectTo2:ip port:port requireSSL:YES];
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                
                NSString *ipaddressaved = ip;
                
                [prefs setObject:ipaddressaved forKey:@"ipaddressaved"];
                [prefs synchronize];
                
                self.authServerIndex++;
                //int arrayLength3 = _authServerIndex;
                
                //NSLog(@"count3:%i", arrayLength3);
            }
            @catch (NSException *exception)
            {
                //NSLog(@"%@ ",exception.name);
                //NSLog(@"Reason: %@ ",exception.reason);
                [self disconnect];
                
                [self notifyErrorCode:SPH_ERROR_INTERNET_CONN message:@"Network Error."];
                
            }
            @finally
            {
                
            }
        }
    }
    
}


- (void)sendRestoreSessionRequest {
    
    NSDictionary *requestDictionary = @{@"request" : @"authenticate", @"username" : self.username, @"password" : self.password};
    
    if (!self.connectedToAuthServer && !self.connectingToAuthServer) {
        
        [self connectToNextAuthServer];
    }
    
    [self sendRequestJSON:requestDictionary];
    
    self.expectedResponse = ExpectedResponseAuthenticate;
}



- (void)sendRunDropboxRequest {
    
    [self sendRunApplicationRequest:@"Dropbox"];
}



- (void)sendRunApplicationRequest:(NSString *)appName {
    
    if (self.connectedToAuthServer) {
        
        NSDictionary *requestDictionary = @{@"request":@"launch", @"app":appName};
        
        [self sendRequestJSON:requestDictionary];
        
        self.expectedResponse = ExpectedResponseAppLaunch;
        
    } else {
        
        [self sendRestoreSessionRequest];
        
        self.scheduledAppLaunch = appName;
    }
}



- (void)sendData:(NSData *)data {
    
    NSMutableData *dataToSend = [NSMutableData dataWithData:data];
    [dataToSend appendData:[self requestSeparatorData]];
    
    if (!self.connectedToAuthServer) {
        
        self.pendingData = data;
        
    } else {
        
        [self.outputStream write:[dataToSend bytes] maxLength:[dataToSend length]];
    }
}



- (NSData *)requestSeparatorData {
    
    return [NSData dataWithBytes:"$" length:1];
}



- (void)logout {
    
    UserSession *user = [UserSession loadUserSession];
    [user removeUserSession];
    
    [self disconnect];
}



#pragma mark - NSStreamDelegate



- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent

{
    //  NSLog(@"Got event:%d from stream:%@", streamEvent, theStream);
    switch (streamEvent)
    {
        case NSStreamEventOpenCompleted:
            
            //NSLog(@"Socket opened");
            
            self.connectingToAuthServer = NO;
            self.connectedToAuthServer = YES;
            
            break;
            
        case NSStreamEventHasSpaceAvailable:
            
            // NSLog(@"Socket has space available");
            
            if (self.pendingData) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self sendData:self.pendingData];
                    
                    self.pendingData = nil;
                    
                });
                
            }
            
            break;
            
        case NSStreamEventHasBytesAvailable:
            
            if ([self.inputStream hasBytesAvailable]) {
                
                uint8_t buf[4096];
                
                NSUInteger length = [self.inputStream read:buf maxLength:sizeof(buf)];
                NSData *response = [NSData dataWithBytes:buf length:length];
                
                [self parseRawResponseData:response];
                
                NSString *receivedString = [[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] autorelease];
                NSLog(@"Received: %@", receivedString);
                
            }
            
            break;
            
        case NSStreamEventErrorOccurred:
            // NSLog(@"Stream error reached");
            
            if (self.connectingToAuthServer) {
                
                if (theStream == self.inputStream)
                {
                    //NSLog(@"connecting error");
                    NSString *app = @"empty";
                    [self sendLoginRequest2:app];
                }
            } else
            {
                [self disconnect];
                [self notifyErrorCode:SPH_ERROR_INTERNET_CONN message:@"Network Error."];
            }
            break;
            
            
        case NSStreamEventEndEncountered:
            
            //NSLog(@"Stream end reached");
            
            [self disconnect];
            
            break;
            
        default:
            
            // do nothing
            break;
    }
}



- (void)parseRawResponseData:(NSData *)responseData {
    
    //TODO: Implement correct behaviour for multiple-frame responses
    
    //Convert data to string, split it by '$' sign, convert components back to data
    
    NSString *responseString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSArray *items = [responseString componentsSeparatedByString:@"$"];
    
    for (NSString *response in items) {
        
        if ([response length] > 0) {
            
            NSData *responseData = [response dataUsingEncoding:NSUTF8StringEncoding];
            [self parseResponseData:responseData];
        }
    }
}









- (void)parseResponseData:(NSData *)responseData {
    
    NSError *error = nil;
    NSDictionary *parsedResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    
    if (error) {
        
        NSString *responseString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        [self processStringResponse:responseString];
        
    } else {
        
        [self processJSONResponse:parsedResponse];
    }
}



- (void)processStringResponse:(NSString *)response {
    
    if ( [response rangeOfString:@"Ready"].location != NSNotFound) {
        
        if (self.readyToLaunchRemoteBlock) {
            
            self.readyToLaunchRemoteBlock();
            
        }
        
        self.expectedResponse = ExpectedResponseReady;
        
       // [self ipAddressSavingPlist: self.IpSuccess];
        
    }
    else
    {
    }
    
}



- (void)processJSONResponse:(NSDictionary *)response {
    
    switch (self.expectedResponse) {
            
        case ExpectedResponseLogin:
            
            //   return [self processLoginResponse:response];
            
        case ExpectedResponseAuthenticate:
            
            //  return [self processAuthenticateResponse:response];
            
        case ExpectedResponseAppLaunch:
            
            // return [self processAppLaunchResponse:response];
            
        default:
            
            return [self processAuthenticateResponse:response];
            
    }
    
}



- (void)processLoginResponse:(NSDictionary *)response {
    
    if (response[@"serverhash"]) {
        
        [self updateAuthServersFromLoginResponse:response];
    }
    
    self.accountInfo = response[@"account"];
    
    if ([self checkResponseReadyToConnect:response]) {
        
        [self connectToRDPFromLoginResponse:response];
        
        //Wait for ready response to launch RDP session
        
        self.expectedResponse = ExpectedResponseReady;
        
    } else {
        
        self.expectedResponse = ExpectedResponseNothing;
    }
}



- (void)processAuthenticateResponse:(NSDictionary *)response {
    
     NSLog(@"connectmethod:%@", response);
    
    self.expectedResponse = ExpectedResponseNothing;
    
    //BOOL valid = [response[@"valid"] boolValue];
    
    // NSLog(@"server response: %@", response);
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"rdpint"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loginexit"];
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ipaddressjson"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    
    NSString *serverport1234 = response[@"port"];
    NSString* tempq = [prefs objectForKey:@"ipaddressaved"];
    
    if ([response objectForKey:@"message"]) {
        
        NSString *message = response[@"message"];
        
        // NSLog(@"server:%@", message);
        
        [self notifyErrorCode:SPH_ERROR_APP_LAUNCH message:message];
        
        /*
         
         SPH_ERROR_PARAMETER       = SPH_ERROR + 1,
         
         SPH_ERROR_OBJECT_NULL     = SPH_ERROR + 2,
         
         SPH_ERROR_INVALID_USER    = SPH_ERROR + 3,
         
         SPH_ERROR_INTERNET_CONN   = SPH_ERROR + 4,
         
         SPH_ERROR_APP_LAUNCH
         
         */
        
    }
    
    else{
        
        if ([response[@"valid"] boolValue]) {
            NSLog(@"method1");

            NSArray *message = response[@"authservers"];
            
            if (!message || !message.count)
            {
                //  NSLog(@"hasnoservers");
                //remove authsever list comes back empty delete the saved servers
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AuthServers2"];
            }
            else
            {
                
                //NSLog(@"save these servers server:%@", message);
                
                [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"AuthServers2"];
                
                //saved auth servers if there is any.....
                
                /*
                 NSArray *authServers = [[NSUserDefaults standardUserDefaults] arrayForKey:@"AuthServers2"];
                 NSLog(@"savedddddddd:%@", authServers);
                 */
            }
             NSString *AccountCodeSave3 = response[@"accountcode"];
            
            NSLog(@"accountcodesentfromserver:%@", AccountCodeSave3);

            NSString* IPAdressEnteredSaved = [prefs objectForKey:@"SelectedIpAddress"];
            NSLog(@"ipaddress----%@",IPAdressEnteredSaved);
          
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSString* AccountCode = [prefs objectForKey:@"TraceFreeAccountCode"];
            
            if ([AccountCode length] == 0)
            {
                NSLog(@"if code is null add to memory");

                NSString *AccountCodeSave = response[@"accountcode"];
                
                [prefs setObject:AccountCodeSave forKey:@"TraceFreeAccountCode"];
                [prefs synchronize];
                
            }
            else
            {
                NSLog(@"code has data");

                NSString *AccountCodeSave2 = response[@"accountcode"];

                if ([AccountCode isEqualToString:AccountCodeSave2]) {
                    NSLog(@"compared strings when connecting in");

                }
                else{
                    NSLog(@"compared strings if not equal save new code");
                    [prefs setObject:AccountCodeSave2 forKey:@"TraceFreeAccountCode"];
                    [prefs synchronize];
                }
    
            }
            
            
            
            //iff accountcode  has data then compare, if same account thats fine, if new account code, save new one
            
            
            NSString *tracefreeuser = response[@"user"];
            //NSLog(@"user:%@", tracefreeuser);

            [prefs setObject:tracefreeuser forKey:@"TraceFreeUser"];
            [prefs synchronize];
            
            NSString *tracefreepass = response[@"pass"];
           // NSLog(@"pass:%@", tracefreepass);

            [prefs setObject:tracefreepass forKey:@"TraceFreePass"];
            [prefs synchronize];
            
            NSString *tracefreeport = response[@"port"];
            //NSLog(@"port:%@", tracefreeport);

            
            NSString *servertoconnectto = response[@"server"];
            NSDictionary *server = servertoconnectto;
            NSString *serverip = server[@"ip"];
            NSString *serverport = server[@"port"];
            
            if (![response objectForKey:@"server"])
            {
                
                ComputerBookmark *bookmark = [[[ComputerBookmark alloc] initWithBaseDefaultParameters] autorelease];
                
                NSObject * object = [prefs objectForKey:@"ipaddressjson"];
                if(object != nil){
                    
                    tempq = [prefs objectForKey:@"ipaddressjson"];
                }
                
                ConnectionParams *connectionParams = [bookmark params];
                [connectionParams setValue:tracefreeuser forKey:SPH_KEY_USERNAME];
                [connectionParams setValue:tracefreepass forKey:SPH_KEY_PASSWORD];
                [connectionParams setValue:IPAdressEnteredSaved forKey:SPH_KEY_HOSTNAME];
                
                self.connectedIP = tempq ;
                
                if (!serverport1234) {
                    [connectionParams setValue:@"3389" forKey:SPH_KEY_PORT];
                } else {
                    [connectionParams setValue:tracefreeport forKey:SPH_KEY_PORT];
                }
                
                [connectionParams setValue:@"" forKey:SPH_KEY_DOMAIN];
                [connectionParams setValue:response[@"serverhash"] forKey:SPH_KEY_SERVERHASHLIST];
                
                [self saveLoggedInUser];
                
                // [self ipAddressSavingPlist:tempq];
                
                self.IpSuccess = tempq;
                
                /*****************************************************************************/
                /* this code is for the sound rediction and possibly the microphone.. */
                
                // sound rediction
                if ([response[@"audio"] boolValue])
                {
                    //NSLog(@"AUDIO IS ON ");
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    
                    NSString *ServerAudioEnableDisable = @"True";  //or false depending on the app/and whats comming backfrom tcp stream.
                    
                    [prefs setObject:ServerAudioEnableDisable forKey:@"ServerAudioEnableDisable"];
                    [prefs synchronize];
                    
                    
                }
                else if(![response[@"audio"] boolValue])
                {
                    //NSLog(@"AUDIO IS OFF ");
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    
                    NSString *ServerAudioEnableDisable = @"False";  //or false depending on the app/and whats comming backfrom tcp stream.
                    
                    [prefs setObject:ServerAudioEnableDisable forKey:@"ServerAudioEnableDisable"];
                    [prefs synchronize];
                    
                    
                }
                
                /******************************************************************************/
                
                if (self.loginCompletionBlock) {
                    
                    self.loginCompletionBlock(bookmark, nil);
                }
                
                if (self.scheduledAppLaunch) {
                    
                    [self sendRunApplicationRequest:self.scheduledAppLaunch];
                    
                    self.scheduledAppLaunch = nil;
                }
            }
            else
            {
                ComputerBookmark *bookmark = [[[ComputerBookmark alloc] initWithBaseDefaultParameters] autorelease];
                
                ConnectionParams *connectionParams = [bookmark params];
                [connectionParams setValue:_username forKey:SPH_KEY_USERNAME];
                [connectionParams setValue:_password forKey:SPH_KEY_PASSWORD];
                [connectionParams setValue:serverip forKey:SPH_KEY_HOSTNAME];
                
                self.connectedIP = serverip ;
                
                [connectionParams setValue:serverport forKey:SPH_KEY_PORT];
                [connectionParams setValue:@"" forKey:SPH_KEY_DOMAIN];
                [connectionParams setValue:response[@"serverhash"] forKey:SPH_KEY_SERVERHASHLIST];
                
                [self saveLoggedInUser];
                
                //[self ipAddressSavingPlist:serverip];
                
                self.IpSuccess = serverip;
                /*****************************************************************************/
                /* this code is for the sound rediction and possibly the microphone.. */
                
                // sound rediction
                if ([response[@"audio"] boolValue])
                {
                    // NSLog(@"AUDIO IS ON ");
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    
                    NSString *ServerAudioEnableDisable = @"True";  //or false depending on the app/and whats comming backfrom tcp stream.
                    
                    [prefs setObject:ServerAudioEnableDisable forKey:@"ServerAudioEnableDisable"];
                    [prefs synchronize];
                }
                else if(![response[@"audio"] boolValue] || [NSNull null])
                {
                    // NSLog(@"AUDIO IS OFF ");
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    
                    NSString *ServerAudioEnableDisable = @"False";  //or false depending on the app/and whats comming backfrom tcp stream.
                    
                    [prefs setObject:ServerAudioEnableDisable forKey:@"ServerAudioEnableDisable"];
                    [prefs synchronize];
                }
                
                /******************************************************************************/
                
                if (self.loginCompletionBlock) {
                    
                    self.loginCompletionBlock(bookmark, nil);
                }
                
                if (self.scheduledAppLaunch) {
                    
                    [self sendRunApplicationRequest:self.scheduledAppLaunch];
                    
                    self.scheduledAppLaunch = nil;
                }
                
                NSString* readyyhack = [prefs objectForKey:@"readyhack"];
                
                if ( [readyyhack rangeOfString:@"Ready"].location != NSNotFound) {
                    
                    NSString *readyhack = @"Ready";
                    NSData   *datahack = [readyhack dataUsingEncoding:NSUTF8StringEncoding];
                    [self parseRawResponseData:datahack];
                }
            }
        }
        else if (![response[@"valid"] boolValue]){
            
            //NSLog(@"wrongpass");
            
            NSString *message = response[@"message"];
            
            [self notifyErrorCode:SPH_ERROR_INVALID_USER message:message];
        }
    }
}


- (void)ipAddressSavingPlist:(NSString*)ipData {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ipAddress.plist"]; //3
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) //4
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"ipAddress" ofType:@"plist"]; //5
        
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    
    NSMutableArray *arrData = [[NSMutableArray alloc] initWithContentsOfFile: path];
    
    //here add elements to data file and write data to file
    
    NSMutableArray *arrChkData = [[NSMutableArray alloc] initWithContentsOfFile:path];
    if (![arrChkData containsObject:ipData]) {
        [arrData addObject:ipData];
        
        [arrData writeToFile: path atomically:YES];
    }
    if (arrData == nil || [arrData count] == 0) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ipaddresshack"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        NSString *ipaddresshack = @"iphack";
        
        [prefs setObject:ipaddresshack forKey:@"ipaddresshack"];
        
        [prefs synchronize];
    }
}



-(void)ready1234

{
    if (self.readyToLaunchRemoteBlock) {
        
        self.readyToLaunchRemoteBlock();
    }
}

- (void)processAppLaunchResponse:(NSDictionary *)response {
    
    BOOL success = [response[@"success"] boolValue];
    
    if (success) {
        
        //NSLog(@"App launch succeeded");
        
    } else {
        
        //NSLog(@"App launch error: %@", response[@"message"]);
    }
    
    self.expectedResponse = ExpectedResponseNothing;
}



- (BOOL)checkResponseReadyToConnect:(NSDictionary *)response {
    
    NSString *message = response[@"message"];
    
    if (![response[@"valid"] boolValue]) {
        
        [self notifyErrorCode:SPH_ERROR_INVALID_USER message:message];
        
        return NO;
        
    } else if (![response[@"connect"] boolValue]) {
        
        [self notifyErrorCode:SPH_ERROR_APP_LAUNCH message:message];
        
        return NO;
        
    } else {
        
        return YES;
        
    }
    
}



- (void)notifyErrorCode:(NSInteger)code message:(NSString *)message {
    
    NSError *error = [NSError errorWithDomain:@"Sphere3dAPI" code:code userInfo:@{@"message": message}];
    
    if (self.loginCompletionBlock) {
        
        self.loginCompletionBlock(nil, error);
    }
}



- (void)notifyErrorMessage:(NSString *)message {
    
    [self notifyErrorCode:SPH_ERROR message:message];
}



- (void)updateAuthServersFromLoginResponse:(NSDictionary *)response {
    
    [self setAuthServers:response[@"authservers"]];
    
    [self setServerHash:response[@"serverhash"]];
}



- (void)connectToRDPFromLoginResponse:(NSDictionary *)response {
    
    ComputerBookmark *bookmark = [[[ComputerBookmark alloc] initWithBaseDefaultParameters] autorelease];
    
    NSDictionary *server = response[@"server"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* tempq = [prefs objectForKey:@"ipaddressaved"];
    
    //NSLog(@"Server settings: %@", server);
    
    NSAssert(server, @"Can't get RDP server settings");
    
    ConnectionParams *connectionParams = [bookmark params];
    
    [connectionParams setValue:_username forKey:SPH_KEY_USERNAME];
    [connectionParams setValue:_password forKey:SPH_KEY_PASSWORD];
    [connectionParams setValue:tempq forKey:SPH_KEY_HOSTNAME];
    
    self.connectedIP = server[@"ip"];
    
    [connectionParams setValue:server[@"port"] forKey:SPH_KEY_PORT];
    [connectionParams setValue:server[@"domain"] forKey:SPH_KEY_DOMAIN];
    [connectionParams setValue:response[@"serverhash"] forKey:SPH_KEY_SERVERHASHLIST];
    
    [self saveLoggedInUser];
    
    if (self.loginCompletionBlock) {
        
        self.loginCompletionBlock(bookmark, nil);
    }
}



- (BOOL)hasSavedSession {
    
    return NO;
    
    //    UserSession *session = [UserSession loadUserSession];
    
    //    return session && session.username && session.password;
    
}



- (void)restoreSavedSession:(LoginCompletionBlock)complete

withReadyToLaunchRemoteBlock:(ReadyToLaunchRemoteBlock)ready

{
    self.restoringFromSession = NO;
    self.loginCompletionBlock = complete;
    self.readyToLaunchRemoteBlock = ready;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* tempq = [prefs objectForKey:@"pass1"];
    NSString* tempq1 = [prefs objectForKey:@"user1"];
    
    
    self.username = tempq1;
    self.password = tempq;
    
    [self sendLoginRequest];
}



- (void)saveLoggedInUser {
    
    UserSession *user = [[[UserSession alloc] init] autorelease];
    
    user.username = self.username;
    user.password = self.password;
    user.ipaddress = self.ipaddress;
    
    [user save];
    
}





@end
