//
//  Sphere3DAPI.h
//  IE on Demand
//
//  Created by Xtreme Labs on 2013-10-10.
//
//

#import <Foundation/Foundation.h>

#import "SPH_ErrorCode.h"
#import "SPH_Constant.h"
#import "Bookmark.h"

typedef NS_OPTIONS(NSInteger, RegistrationResponse) {
    RegistrationResponseFailed = -1,
    RegistrationResponseFieldMissing = 0,
    RegistrationResponseSuccess = 1,
    RegistrationResponseEmailOrUsernameTaken = 2,
    RegistrationResponseRegistrationsClosed = 3
};

typedef void (^LoginCompletionBlock)(ComputerBookmark *connectionSettings, NSError*error);
typedef void (^RegistrationCompletionBlock)(RegistrationResponse code, NSError *error);
// __REALWAT__ : BEGIN : 2013/Nov/28 : Fixed PTS:SPH-53
typedef void (^ReadyToLaunchRemoteBlock)();
// __REALWAT__ : END

@class RegistrationData;

@interface Sphere3DAPI : NSObject 

@property (nonatomic, retain) NSString *connectedIP;
@property (nonatomic, retain) NSDictionary *accountInfo;

+ (instancetype)sharedInstance;
- (void)registerUserWithRegistrationData:(RegistrationData*)registrationData
						 completionBlock:(RegistrationCompletionBlock)complete;

- (void)loginWithUsername:(NSString*)username
			  andPassword:(NSString *)password
	  withCompletionBlock:(LoginCompletionBlock)complete
withReadyToLaunchRemoteBlock:(ReadyToLaunchRemoteBlock)ready
andSelectedAppName:(NSString*) selectedAppName;

- (void)logout;

- (BOOL)hasSavedSession;

- (void)restoreSavedSession:(LoginCompletionBlock)complete
withReadyToLaunchRemoteBlock:(ReadyToLaunchRemoteBlock)ready;

- (void)sendRunDropboxRequest;

//Hack for issue #4
@property (nonatomic, retain) NSString *launchedApp;

@end
