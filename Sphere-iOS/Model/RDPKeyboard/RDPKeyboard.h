/*
 RDP Keyboard helper 

RDPKeyboard.h
 
 Glassware Connect
 
 Created by Sphere 3D 2015
 
 RDPKeyboard maps the iOS keystroke to windows keys.
 */


#import <Foundation/Foundation.h>
#import "RDPSession.h"

@class RDPKeyboard;


@protocol RDPKeyboardDelegate <NSObject>
@optional
- (void)modifiersChangedForKeyboard:(RDPKeyboard*)keyboard;
@end


@interface RDPKeyboard : NSObject {

    RDPSession* _session;
    
	int _virtual_key_map[256];
    int _unicode_map[256];
    NSDictionary* _special_keys;

    NSObject<RDPKeyboardDelegate>* _delegate;
    
	BOOL _ctrl_pressed;
    BOOL _alt_pressed;
    BOOL _shift_pressed;
    BOOL _win_pressed;
}

@property (assign) id <RDPKeyboardDelegate> delegate;
@property (readonly) BOOL ctrlPressed;
@property (readonly) BOOL altPressed;
@property (readonly) BOOL shiftPressed;
@property (readonly) BOOL winPressed;

// returns a keyboard instance
+ (RDPKeyboard*)getSharedRDPKeyboard;

// init the keyboard and assign the given rdp session and delegate
- (void)initWithSession:(RDPSession*)session delegate:(NSObject<RDPKeyboardDelegate>*)delegate;

// called to reset any pending key states (i.e. pressed modifier keys)
- (void)reset;

// sends the given unicode character to the server
- (void)sendUnicode:(int)character;

// send a key stroke event using the given virtual key code
- (void)sendVirtualKeyCode:(int)keyCode;

// __REALWAT__ : BEGIN : 2013/Nov/18 : Added alt tab combine key

// send alt, shift and tab keys
- (void)sendVirtualKeyAltShiftTab;
-(void)largertoolbar;
- (void)largerproptoolbar;

// toggle delete key, returns true if pressed, otherwise false
- (void)toggleDelKey;

// toggle tab key, returns true if pressed, otherwise false
- (void)toggleTabKey;

// send ctl and c keys
- (void)sendVirtualKeyUndo;

// send ctl and c keys
- (void)sendVirtualKeyCopy;

// sent ctl and v keys
- (void)sendVirtualKeyPaste;

// __REALWAT__ : END

// toggle ctrl key, returns true if pressed, otherwise false
- (void)toggleCtrlKey;

// toggle alt key, returns true if pressed, otherwise false
- (void)toggleAltKey;

// toggle shift key, returns true if pressed, otherwise false
- (void)toggleShiftKey;

// toggle windows key, returns true if pressed, otherwise false
- (void)toggleWinKey;

// send key strokes
- (void)sendEnterKeyStroke;
- (void)sendEscapeKeyStroke;
- (void)sendBackspaceKeyStroke;

- (void)sendRightArrowKeyUp:(BOOL)up;
- (void)sendLeftArrowKeyUp:(BOOL)up;
- (void)sendUpArrowKeyUp:(BOOL)up;
- (void)sendDownArrowKeyUp:(BOOL)up;
- (void)sendDeleteKeyUp:(BOOL)up;
- (void)sendTabKeyUp:(BOOL)up;
- (void)sendCtrlKeyUp:(BOOL)up;
- (void)sendAltKeyUp:(BOOL)up;

- (void)sendVirtualKeyF1Plus;
@end
