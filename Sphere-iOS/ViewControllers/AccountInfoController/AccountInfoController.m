#import "AccountInfoController.h"

#import "Sphere3DAPI.h"

@interface AccountInfoController () <UITableViewDataSource>

@property (nonatomic, retain) NSDictionary *accountInfo;

@end

@implementation AccountInfoController

- (void)dealloc {
    [_accountInfo release];
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.accountInfo = [[Sphere3DAPI sharedInstance] accountInfo];
}

- (IBAction)done {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)textForCellAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return @"Account type";
        case 1:
            return @"Subscription days left";
        case 2:
            return @"Subscription ends";
        default:
            return nil;
    }
}

- (NSString *)detailForCellAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return [self accountType];
        case 1:
            return [self daysLeft];
        case 2:
            return [self subscriptionEnd];
        default:
            return nil;
    }
}

- (NSString *)accountType {
    NSInteger accountType = [self.accountInfo[@"accounttype"] integerValue];
    switch (accountType) {
        case 0:
            return @"Trial";
        case 1:
            return @"Subscriber";
        case 2:
            return @"Unlimited";
        default:
            return nil;
    }
}

- (NSString *)daysLeft {
    return [self.accountInfo[@"subscriptiondays"] stringValue];
}

- (NSString *)subscriptionEnd {
    NSTimeInterval subscriptionEndStamp = [self.accountInfo[@"subscriptionend"] doubleValue];
    NSDate *subscriptionEndDate = [NSDate dateWithTimeIntervalSince1970:subscriptionEndStamp];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    return [formatter stringFromDate:subscriptionEndDate];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const cellID = @"AccountInfoCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID] autorelease];
    }
    NSInteger index = indexPath.row;
    [cell.textLabel setText:[self textForCellAtIndex:index]];
    [cell.detailTextLabel setText:[self detailForCellAtIndex:index]];
    return cell;
}


@end
