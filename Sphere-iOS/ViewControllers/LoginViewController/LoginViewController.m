//
//  LoginViewController.m
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  This code is apart of the Login GUI for Glassware Connect. This is where ther user enters in the loging credentials to a glassware appliance.
//
//  The Loginview controller mostly consists of:
//
//  Login gui
//  Login credential input and saving
//  pinging ip
//  network configutation testing
//  display prevouis successful connected glassware appliance ip addresses
//  Main screen when user logs off the appliaction
//  Iphone and Ipad detection for screen size, features etc..

#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "SPH_RememberUser.h"
#import "Reachability.h"
//#import "SimplePingHelper.h"
#import "UIDeviceHardware.h"
#import "appSettingsBackgrounds.h"
#import "MyApplication.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Security/Security.h>
#import "UIView+RNActivityView.h"
#include <arpa/inet.h>
#import <WebKit/WebKit.h>
#define iOS9 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define iOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])

static NSString *kInvalidCredentialsMessage = @"Invalid username and/or password. Please try again.";
static NSString *kEmptyCredentialsMessage = @"*Please make sure URL field is filled";
typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);

//UIView *view6;
UIView *dimView;
//UITextField *LiceneseField;
@interface LoginViewController ()

@property (retain, nonatomic) IBOutlet UIView *ContainerView;
@property (nonatomic, assign) IBOutlet UITextField *usernameTextField;
@property (nonatomic, assign) IBOutlet UITextField *passwordTextField;
@property (nonatomic, assign) IBOutlet UITextField *ipaddressTextField;
@property (nonatomic, assign) IBOutlet UILabel *warningLabel;
@property (nonatomic, assign) IBOutlet UIButton *rememberMeButtons;
@property (retain, nonatomic) IBOutlet UIImageView *imgIPStatus;
@property (retain, nonatomic) IBOutlet UIView *lineView;
@property (nonatomic, assign) IBOutlet UIButton *ConnectButton;
@property (nonatomic, assign) IBOutlet UILabel *RememberMeLabel;
@property (nonatomic, assign) IBOutlet UILabel *URLLabel;
@property (retain, nonatomic) IBOutlet UIView *UrlView;


@property (nonatomic) BOOL rememberMeSelected;
@property (nonatomic, retain) NSError *loginError;
@property (nonatomic, retain) NSString *lastUsername;
@property (nonatomic) BOOL subscribedToKeyboardNotifications;
@property (nonatomic, assign) CGRect originalFrame;
@property (retain, nonatomic) NSMutableData *receivedData;

@property (strong, nonatomic) UIView *LicenseView;
@property (strong, nonatomic) UIView *WebviewLicenseID;
@property (nonatomic, assign) UITextField *LiceneseIDField;
@property (nonatomic, assign) UILabel *MessageLabel1;
@property (nonatomic, assign) UILabel *MessageLabel2;
@property (nonatomic, assign) UILabel *MessageLabel3;
@property (nonatomic, assign) UILabel *MessageLabel4;
@property (nonatomic, assign) UILabel *MessageLabel5;
@property (nonatomic, assign) UILabel *MessageLabelInternet;

@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSMutableArray *menuItems;
@property (retain, nonatomic) IBOutlet UIButton *btnDropDown;
@property (retain, nonatomic) IBOutlet UILabel *labelversion;
@property (retain, nonatomic) IBOutlet UILabel *lblSignIn;
@property (nonatomic) BOOL bluetoothKeyboardTest;

@property (nonatomic) BOOL hasBeacon;

@end

@implementation CALayer (Enhancement)

-(void)setTextViewBorderColor:(UIColor*)mycolor
{
    self.borderColor = mycolor.CGColor;
}
@end
@import AssetsLibrary;
@implementation LoginViewController

+ (instancetype)loginViewController
{
    return [LoginViewController loginViewControllerWithLoginError:nil username:nil];
}

+ (instancetype)loginViewControllerWithLoginError:(NSError*)error username:(NSString *)username
{
    NSString* nibName = nil;
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    if (IS_IPAD)
        nibName = @"LoginViewController";
    else if (IS_IPHONE)
    {
        if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"])
            nibName = @"LoginViewControlleriPhone4";
        else
            nibName = @"LoginViewControlleriPhone";
    }
    [h release];
    LoginViewController *Controller = [[[LoginViewController alloc] initWithNibName:nibName bundle:nil] autorelease];
    Controller.loginError = error;
    Controller.lastUsername = username;
    return Controller;
}

- (void)dealloc
{
    [_loginError release];
    [_lastUsername release];
    [_imgIPStatus release];
    [_btnDropDown release];
    [_lineView release];
    [_UrlView release];
    [_ContainerView release];
    [_lblSignIn release];
    [_backgroundImage release];
    
    [super dealloc];
}

#pragma mark - View life cycle

-(void)LicenseKeyview
{
    //create uiview here,,, center it then addd text fiel labels text and buttons
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(80, 150, 160, 160)];
    view.backgroundColor = [UIColor greenColor];
    view.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    
    [self.view addSubview:view];
}


-(void)HiddenOutlets
{
    self.ContainerView.hidden = YES;
    self.RememberMeLabel.hidden = YES;
    self.URLLabel.hidden = YES;
    self.labelversion.hidden = YES;
    self.rememberMeButtons.hidden = YES;
    self.UrlView.hidden = YES;
    self.lineView.hidden = YES;
    self.ConnectButton.hidden = YES;
}

-(void)SendtoHttpPost
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //saved data
    NSString* SavedLicenseID = [prefs objectForKey:@"LicenseID"];
    NSString* SavedUrlfromUser = [prefs objectForKey:@"user1"];
    // NSLog(@"leicense id string----%@",SavedLicenseID);
    // NSLog(@"url string----%@",SavedUrlfromUser);
    
    //url post
    NSString *postURL = [NSString stringWithFormat:@"url=%@&key=%@",SavedUrlfromUser,SavedLicenseID];
    
    //  NSLog(@"POST string----%@",postURL);
    
    NSData *postData = [postURL dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:@"https://api.pinkbyte.net/launch/set"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(conn) {
        //NSLog(@"Connection Successful");
    } else {
        //  NSLog(@"Connection could not be made");
    }
    
}

// This method is used to receive the data which we get using post method.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    //  NSLog(@"Recieving Data...");
    [self.receivedData appendData:data];
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
    NSString* UrlCode = [json objectForKey:@"code"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:UrlCode forKey:@"URLCode"];
    [prefs synchronize];
}

// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //NSLog(@"error...");
}

// This method is used to process the data after connection has made successfully.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"did finish...");
}

-(void)LicenceAuthCheck
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString* SavedLicenseID = [prefs objectForKey:@"LicenseID"];
    
    NSString* AuthcodeKey = [prefs objectForKey:@"AuthcodeLicenseKey"];
    
    NSString *deviceString = @"IPAD-";
    
    NSString* udidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSString *udidFinal = [deviceString stringByAppendingString:udidString];
    
    NSString *FieldAppID = @"Hy1iQMgGX";
    
    NSString *UrlStringtoData=[[NSString alloc]initWithFormat:@"https://license.pinkbyte.net/v1/validate?key=%@&user=%@&auth=%@&app=%@",SavedLicenseID,udidFinal,AuthcodeKey,FieldAppID];
    NSLog(@"string space----%@",UrlStringtoData);
    
    
    
    NSString *encodedUrl = [UrlStringtoData stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSURL *EncrpytedUrl = [[NSURL alloc] initWithString:encodedUrl];
    NSData *jsonData= [NSData dataWithContentsOfURL:EncrpytedUrl];
    NSError *error = nil;
    // NSLog(@"data----%@",str);
    
    NSDictionary *dataDictionary= [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    // [[NSUserDefaults standardUserDefaults] setObject:dataDictionary forKey:@"dictionaryKey"];
    
    NSLog(@"data----%@",dataDictionary);
    
    NSString * debug = [dataDictionary objectForKey:@"debug"];
    NSString * showrequest = [dataDictionary objectForKey:@"showrequest"];
    NSString * valid = [dataDictionary objectForKey:@"valid"];
    NSString * authCode = [dataDictionary objectForKey:@"auth"];
    NSString * message = [dataDictionary objectForKey:@"message"];
    
    
    
    if([valid intValue] == 0) //if valid is 0 or false
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LicenseID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.LiceneseIDField.text = @"";
        
        self.MessageLabel4.hidden = NO;
        
        [self.MessageLabel4 setText:@"Expired License, enter valid license"];
        
        [self LicenseIDView2];
        
        self.ContainerView.hidden = YES;
        self.RememberMeLabel.hidden = YES;
        self.URLLabel.hidden = YES;
        self.labelversion.hidden = YES;
        self.rememberMeButtons.hidden = YES;
        self.UrlView.hidden = YES;
        self.lineView.hidden = YES;
        self.ConnectButton.hidden = YES;
        
        /*
         if([showrequest intValue] == 0) //showrequst is 0
         {
         [self.label4 setText:message];
         self.label4.hidden = NO;
         self.label6.hidden = YES;
         
         // [self LicenseIDView2];
         
         }
         else // show request is true, show error
         {
         
         NSString *mess = message;
         
         NSString *mess2 = @"License is valid, but it has already been used on another computer.";
         
         if ([mess isEqualToString:mess2])
         {
         [self.label4 setText:@"Valid License, but in used another session."];
         self.label4.hidden = NO;
         self.label6.hidden = YES;
         }
         //  [self LicenseIDView2];
         
         }
         */
    }
    else{ //valid is 1 or true
        
        self.ContainerView.hidden = NO;
        self.RememberMeLabel.hidden = NO;
        self.URLLabel.hidden = NO;
        self.labelversion.hidden = NO;
        self.rememberMeButtons.hidden = NO;
        self.UrlView.hidden = NO;
        self.lineView.hidden = NO;
        self.ConnectButton.hidden = NO;
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        NSString * Licnesehack = @"1";
        [prefs setObject:Licnesehack forKey:@"Licencekeyhack"];
        [prefs synchronize];
    }
    if ([authCode length] == 0) { //wont ever hit this
        
        NSLog(@"code is null");
    }
    else //if auth code has data
    {
        if ([AuthcodeKey isEqualToString:authCode]) //wont hit this ever
        {
            
        }
        else{ //auth code is different add new auth code to default
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            [prefs setObject:authCode forKey:@"AuthcodeLicenseKey"];
            [prefs synchronize];
        }
    }
}
- (void)viewDidLoad
{
    
    self.usernameTextField.text = @"google.com";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString* valuesaved = [prefs objectForKey:@"Licencekeyhack"];
    if([valuesaved intValue] == 0)  //if lincese hack is  0 pop up view
    {
        [self HiddenOutlets];
        [self LicenseIDView];
    }
    else // license hack is 1, this has the auth code and user stored to run the license key authentication
    {
        [self LicenceAuthCheck];
    }
    
    _hasBeacon = NO;
    
    [_labelversion setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    _labelversion.font = [UIFont fontWithName:@"Roboto-Bold" size:12.0f];
    [_labelversion setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    //[_labelversion setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    // [_labelversion setNumberOfLines:1];//Set number of lines in label.
    
    NSString * Version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    _labelversion.text = [NSString stringWithFormat:@"Ver.%@", Version];
    //_labelversion.adjustsFontSizeToFitWidth = YES;
    
    NSString* IpaddressHack = [prefs objectForKey:@"ipaddresshack"];
    
    if ([IpaddressHack isEqualToString:@"iphack"])
    {
        self.btnDropDown.hidden = NO;
    }
    else
    {
        self.btnDropDown.hidden = YES;
    }
    NSObject * object = [prefs objectForKey:@"applaunchermessage"];
    
    if(object != nil)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"applaunchermessage"];
    }
    /*
     //xml parser
     NSURL *url=[[NSURL alloc]initWithString:@"http://192.168.0.174/LNHexNumberpad.xib"];
     NSData  *DaTa=[[NSData alloc]initWithContentsOfURL:url];
     NSString *myString = [[NSString alloc] initWithData:DaTa encoding:NSUTF8StringEncoding];
     
     // NSLog(@"data----%@",myString);
     NSXMLParser  *Xmlpaeser=[[NSXMLParser alloc]initWithData:DaTa];
     Xmlpaeser.delegate=self;
     [Xmlpaeser parse];
     [Xmlpaeser release];
     
     NSString *filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"sample%d.xml", myString]];
     
     */
    //Detect Device rotation
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    [super viewDidLoad];
    
    if (self.loginError)
    {
        self.warningLabel.text = kInvalidCredentialsMessage;
        self.warningLabel.hidden = (nil == self.loginError);
    }
    if (self.lastUsername)
    {
        self.usernameTextField.text = self.lastUsername;
    }
    cSPH_RememberUser *rememberedUser = [[[cSPH_RememberUser alloc] init] autorelease];
    
    if (!rememberedUser)
    {
        SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
        return;
    }
    tSPH_ErrorCode errorCode = [rememberedUser loadMememberMeInfo];
    
    if ( SPH_SUCCESS != errorCode)
    {
        SPH_LOG_CONSOL(errorCode);
        return;
    }
    self.rememberMeSelected = [rememberedUser.rememberStatus boolValue];
    if (self.rememberMeSelected)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString* SavedUsernameFromLogin = [prefs objectForKey:@"user1"];
        
        self.usernameTextField.text     = SavedUsernameFromLogin;
        
        self.rememberMeButtons.selected = YES;
    }
    
    self.usernameTextField.delegate= self;
    
    _originalFrame = self.view.frame;
    NSString* LoginExitHack = [prefs objectForKey:@"loginexit"];
    
    if([LoginExitHack intValue] == 4)
    {
        [self loginClick:nil];
    }
    else
    {
    }
    
    _lblSignIn.text = [NSString stringWithFormat:@"Sign In to %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishWithBeacon:)
                                                 name:@"BeaconNotification"
                                               object:nil];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* BackgroundImageSetting = [prefs objectForKey:@"BackgroundImageSetting"];
    
    if (BackgroundImageSetting != nil) {
        _backgroundImage.image = [UIImage imageNamed:BackgroundImageSetting];
        
    }
    [_backgroundImage setNeedsDisplay];
    
    
    if (BackgroundImageSetting == nil) {
        NSString* BackgroundCameraRollimage = [prefs objectForKey:@"BackgroundCameraRollimage"];
        
        if (BackgroundCameraRollimage != nil) {
            
            NSURL *url = [NSURL URLWithString:[BackgroundCameraRollimage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@"camrollurl %@",url);
            
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library assetForURL:url resultBlock:^(ALAsset *asset)
             {
                 UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0.5 orientation:UIImageOrientationUp];
                 
                 _backgroundImage.image = copyOfOriginalImage;
             }
                    failureBlock:^(NSError *error)
             {
                 // error handling
                 NSLog(@"failure-----");
             }];
            
        }
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = NO;
    
    [super viewWillDisappear:animated];
}


/*
 - (void)subscribeKeyboardNotifications {
 if (!self.subscribedToKeyboardNotifications) {
 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide) name:UIKeyboardDidHideNotification object:nil];
 self.subscribedToKeyboardNotifications = YES;
 }
 }
 
 - (void)unsubscribeKeyboardNotifications {
 if (self.subscribedToKeyboardNotifications) {
 [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
 [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
 self.subscribedToKeyboardNotifications = NO;
 }
 }
 
 -(void)keyboardDidShow
 {
 
 UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication]
 statusBarOrientation];
 
 if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
 [UIView animateWithDuration:0.15 animations:^{
 self.view.frame = CGRectMake(-150,0,768,1024);
 }];
 }
 if (interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
 [UIView animateWithDuration:0.15 animations:^{
 self.view.frame = CGRectMake(150,0,768,1024);
 }];
 }
 }
 
 -(void)keyboardDidHide
 {
 [UIView animateWithDuration:0.25 animations:^{
 self.view.frame = CGRectMake(0,0,768,1024);
 } completion:^(BOOL finished) {
 }];
 }
 */

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.bluetoothKeyboardTest = NO;
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = NO;
    // Move view when keyboard shows up
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication]
                                                   statusBarOrientation];
    //[UIView beginAnimations:nil context:NULL];
    //NSLog(@"frame: %@", NSStringFromCGRect(self.view.frame));
    
    [self.imgIPStatus setImage:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    if(iOS9)
    {
        
        UITextInputAssistantItem* Blockshorcuttooolbaruser = [_usernameTextField inputAssistantItem];
        Blockshorcuttooolbaruser.leadingBarButtonGroups = @[];
        Blockshorcuttooolbaruser.trailingBarButtonGroups = @[];
        
        UITextInputAssistantItem* Blockshorcuttooolbarpass = [_passwordTextField inputAssistantItem];
        Blockshorcuttooolbarpass.leadingBarButtonGroups = @[];
        Blockshorcuttooolbarpass.trailingBarButtonGroups = @[];
        
        UITextInputAssistantItem* Blockshorcuttooolbarip = [_ipaddressTextField inputAssistantItem];
        Blockshorcuttooolbarip.leadingBarButtonGroups = @[];
        Blockshorcuttooolbarip.trailingBarButtonGroups = @[];
        
    }
    
    
}
- (void)keyboardDidShow: (NSNotification *) notif{
    self.bluetoothKeyboardTest = NO;
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = NO;
    
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication]
                                                   statusBarOrientation];
    
    if (interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             self.view.frame = (iOS8 ? CGRectMake(0,-150,self.view.frame.size.width,self.view.frame.size.height) : CGRectMake(150,0,self.view.frame.size.width,self.view.frame.size.height));
                         }];
    }
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             self.view.frame = (iOS8 ? CGRectMake(0,-150,self.view.frame.size.width,self.view.frame.size.height) :
                                                CGRectMake(-150,0,self.view.frame.size.width,self.view.frame.size.height));
                         }];
    }
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    self.bluetoothKeyboardTest = YES;
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = YES;
    CGRect returnframe =self.view.frame;
    if (iOS8)
        returnframe.origin.y = 0;
    else
        returnframe.origin.x = 0;
    
    [UIView animateWithDuration:0.3f animations:^{self.view.frame = returnframe;}];
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.ipaddressTextField)
    {
        //[SimplePingHelper ping:[self.ipaddressTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] target:self sel:@selector(pingResult:)];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if (isBackSpace == -8) {
        NSLog(@"Backspace was pressed");
        self.MessageLabel5.hidden = YES;
        self.MessageLabel1.hidden = YES;
    }
    //    struct sockaddr_in callAddress;
    //    callAddress.sin_len = sizeof(callAddress);
    //    callAddress.sin_family = AF_INET;
    //    callAddress.sin_port = htons(24);
    //    callAddress.sin_addr.s_addr = inet_addr("12.1.12.1");
    
    
    //    if (textField == self.ipaddressTextField) {
    //        NSRegularExpression *ValidIpAddressRegex = [NSRegularExpression
    //                                                    regularExpressionWithPattern:@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
    //                                      options:0
    //                                      error:nil];
    //
    //        NSRegularExpression *ValidHostnameRegex = [NSRegularExpression
    //                                                   regularExpressionWithPattern:@"^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9]))*$"
    //                                      options:0
    //                                      error:nil];
    //
    //
    //
    //        NSUInteger IPFormatMatches = [ValidIpAddressRegex numberOfMatchesInString:self.ipaddressTextField.text options:0 range:NSMakeRange(0, [self.ipaddressTextField.text length])];
    //
    //        NSUInteger HostFormatMatches = [ValidHostnameRegex numberOfMatchesInString:self.ipaddressTextField.text options:0 range:NSMakeRange(0, [self.ipaddressTextField.text length])];
    //
    //
    //        Reachability* reachability = [[Reachability reachabilityWithHostName: self.ipaddressTextField.text] retain];
    //        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
    //
    //              if (netStatus && (IPFormatMatches || HostFormatMatches))
    //            [self.imgIPStatus setImage:[UIImage imageNamed:@"Correct"]];
    //        else
    //            [self.imgIPStatus setImage:[UIImage imageNamed:@"Wrong"]];
    //    }
    //
    if (textField == self.usernameTextField)
    {
        //if( [string isEqualToString:@" "] )
        //  return NO;
    }
    if (textField == self.passwordTextField)
    {
        // if( [string isEqualToString:@" "] )
        //   return NO;
    }
    if (textField == self.ipaddressTextField)
    {
        // [SimplePingHelper ping:[self.ipaddressTextField.text stringByReplacingCharactersInRange:range withString:string] target:self sel:@selector(pingResult:)];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _usernameTextField)
    {
        [_passwordTextField becomeFirstResponder];
    }
    else if (textField == _passwordTextField)
    {
        [_ipaddressTextField becomeFirstResponder];
    }
    
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)pingResult:(NSNumber*)success
{
    //NSLog(@"suc:%@", success);
    [self.imgIPStatus setImage:nil];
    if (success.boolValue){
        [self.imgIPStatus setImage:[UIImage imageNamed:@"Correct"]];
        _warningLabel.hidden = YES;
        _warningLabel.text = @"";
    }
    else{
        [self.imgIPStatus setImage:[UIImage imageNamed:@"Wrong"]];
        _warningLabel.hidden = NO;
        _warningLabel.text = @"Validating...";
    }
}

#pragma mark - IBActions
-(void) AppLauncher
{
    [self loginClick:nil];
}

-(void)LoadingActivityView
{
    [self.view showActivityView];
    [self.view hideActivityViewWithAfterDelay:0.5];
}

- (IBAction)ShowSettings:(id)sender {
    
    [self LoadingActivityView];
    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"AppSettingsStoryboard" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"appSettingsBackgrounds"];
    
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:viewController animated:YES completion:nil];
    
    //Add notification to observe any background image if it's changed....
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishWithBackgroundSelect:)
                                                 name:@"NotifyBackgroundImageSetting"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishWithBackgroundCameraRollSelect:)
                                                 name:@"CameraRollBackgroundImage"
                                               object:nil];
    
}
-(void)LicenseIDView2
{
    self.LicenseView = [[UIView alloc] initWithFrame:CGRectMake(550,380, 550, 380)];
    self.LicenseView.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:240/255.0 alpha:1];
    self.LicenseView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    
    self.LicenseView.center = CGPointMake(self.view.frame.size.width  / 2,
                                          self.view.frame.size.height / 2);
    
    UIView *TopbarView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 550, 50)];
    TopbarView.backgroundColor = [UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1];
    
    UILabel *LicenseTitle=[[UILabel alloc]initWithFrame:CGRectMake(195, 4, 150,50)];//Set frame of label in your viewcontroller.
    [LicenseTitle setText:@"License Key"];//Set text in label.
    [LicenseTitle setTextColor:[UIColor whiteColor]];
    [LicenseTitle setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [LicenseTitle setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [LicenseTitle setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [LicenseTitle setFont:[UIFont boldSystemFontOfSize:16]];
    
    UILabel *LicenseLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 85, 150,50)];//Set frame of label in your viewcontroller.
    [LicenseLabel setText:@"License Key :"];//Set text in label.
    [LicenseLabel setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [LicenseLabel setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [LicenseLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [LicenseLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [LicenseLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:22]];
    
    UILabel *StatusLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 160, 120,50)];//Set frame of label in your viewcontroller.
    [StatusLabel setText:@"Status :"];//Set text in label.
    [StatusLabel setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [StatusLabel setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [StatusLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [StatusLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [StatusLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:22]];
    
    self.MessageLabel1=[[UILabel alloc]initWithFrame:CGRectMake(33, 160, 500,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel1 setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel1 setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel1 setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel1 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel1 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel1.hidden = YES;
    
    self.MessageLabel3=[[UILabel alloc]initWithFrame:CGRectMake(147, 160, 180,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel3 setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel3 setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel3 setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel3 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel3 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel3.hidden = YES;
    
    self.MessageLabel4 =[[UILabel alloc]initWithFrame:CGRectMake(50, 160, 500,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel4  setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel4  setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel4  setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel4 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel4 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel4.hidden = YES;
    
    self.MessageLabel5 =[[UILabel alloc]initWithFrame:CGRectMake(0, 160, 500,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel5  setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel5  setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel5  setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel5 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel5 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel5.hidden = YES;
    
    self.MessageLabel2=[[UILabel alloc]initWithFrame:CGRectMake(120,140, 300,20)];//Set frame of label in your viewcontroller.
    [self.MessageLabel2 setText:@"*Please fill in the License key field"];//Set text in label.
    [self.MessageLabel2 setTextColor:[UIColor colorWithRed:237/255.0 green:41/255.0 blue:57/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel2 setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel2 setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel2 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
    self.MessageLabel2.hidden = YES;
    
    self.MessageLabelInternet=[[UILabel alloc]initWithFrame:CGRectMake(60,143, 512,20)];//Set frame of label in your viewcontroller.
    [self.MessageLabelInternet setText:@"*Please check your internet connection and try again."];//Set text in label.
    [self.MessageLabelInternet setTextColor:[UIColor colorWithRed:237/255.0 green:41/255.0 blue:57/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabelInternet setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabelInternet setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabelInternet setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabelInternet setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
    self.MessageLabelInternet.hidden = YES;
    
    UIView*  ViewLine = [[UIView alloc] initWithFrame:CGRectMake(93,218, 380, 3)];
    ViewLine.backgroundColor = [UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1];
    
    self.LiceneseIDField = [[UITextField alloc] initWithFrame:CGRectMake(170, 80, 290,60)];
    self.LiceneseIDField.borderStyle = UITextBorderStyleLine;
    self.LiceneseIDField.placeholder = @"    License Key*";
    self.LiceneseIDField.backgroundColor = [UIColor whiteColor];
    self.LiceneseIDField.delegate= self;
    
    [self inputtextdata];
    
    UIButton *DoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [DoneButton addTarget:self
                   action:@selector(LicenseViewRemove)
         forControlEvents:UIControlEventTouchUpInside];
    [DoneButton setTitle:@"Done" forState:UIControlStateNormal];
    DoneButton.frame = CGRectMake(475, 10, 90, 35.0);
    DoneButton.showsTouchWhenHighlighted = YES;
    
    self.ContainerView.hidden = YES;
    self.RememberMeLabel.hidden = YES;
    self.URLLabel.hidden = YES;
    self.labelversion.hidden = YES;
    self.rememberMeButtons.hidden = YES;
    self.UrlView.hidden = YES;
    self.lineView.hidden = YES;
    self.ConnectButton.hidden = YES;
    
    UIButton *OKButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [OKButton addTarget:self
                 action:@selector(LicenseViewFirst)
       forControlEvents:UIControlEventTouchUpInside];
    [OKButton setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];
    [OKButton setTitle:@"OK" forState:UIControlStateNormal];
    OKButton.frame = CGRectMake(115, 250, 160, 65);
    OKButton.showsTouchWhenHighlighted = YES;
    
    UIButton *RequestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [RequestButton addTarget:self
                      action:@selector(RequestLicense)
            forControlEvents:UIControlEventTouchUpInside];
    [RequestButton setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];
    [RequestButton setTitle:@"Request License" forState:UIControlStateNormal];
    RequestButton.frame = CGRectMake(300, 250, 160, 65);
    RequestButton.showsTouchWhenHighlighted = YES;
    
    [TopbarView addSubview:LicenseTitle];
    
    [self.LicenseView addSubview:ViewLine];
    [self.LicenseView addSubview:TopbarView];
    [self.LicenseView addSubview:LicenseLabel];
    [self.LicenseView addSubview:StatusLabel];
    
    [self.LicenseView addSubview:self.MessageLabel1];
    [self.LicenseView addSubview:self.MessageLabel2];
    [self.LicenseView addSubview:self.MessageLabel3];
    [self.LicenseView addSubview:self.MessageLabel4];
    [self.LicenseView addSubview:self.MessageLabel5];
    [self.LicenseView addSubview:self.MessageLabelInternet];
    
    [self.LicenseView addSubview:OKButton];
    [self.LicenseView addSubview:RequestButton];
    [self.LicenseView addSubview:self.LiceneseIDField];
    
    [self.view addSubview:self.LicenseView];
    
}


-(void)LicenseIDView
{
    self.LicenseView = [[UIView alloc] initWithFrame:CGRectMake(550,380, 550, 380)];
    self.LicenseView.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:240/255.0 alpha:1];
    self.LicenseView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    
    self.LicenseView.center = CGPointMake(self.view.frame.size.width  / 2,
                                          self.view.frame.size.height / 2);
    
    UIView *TopbarView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 550, 50)];
    TopbarView.backgroundColor = [UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1];
    
    UILabel *LicenseTitle=[[UILabel alloc]initWithFrame:CGRectMake(195, 4, 150,50)];//Set frame of label in your viewcontroller.
    [LicenseTitle setText:@"License Key"];//Set text in label.
    [LicenseTitle setTextColor:[UIColor whiteColor]];
    [LicenseTitle setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [LicenseTitle setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [LicenseTitle setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [LicenseTitle setFont:[UIFont boldSystemFontOfSize:16]];
    
    UILabel *LicenseLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 85, 150,50)];//Set frame of label in your viewcontroller.
    [LicenseLabel setText:@"License Key :"];//Set text in label.
    [LicenseLabel setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [LicenseLabel setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [LicenseLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [LicenseLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [LicenseLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:22]];
    
    UILabel *StatusLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 160, 120,50)];//Set frame of label in your viewcontroller.
    [StatusLabel setText:@"Status :"];//Set text in label.
    [StatusLabel setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [StatusLabel setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [StatusLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [StatusLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [StatusLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:22]];
    
    self.MessageLabel1=[[UILabel alloc]initWithFrame:CGRectMake(33, 160, 500,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel1 setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel1 setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel1 setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel1 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel1 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel1.hidden = YES;
    
    self.MessageLabel3=[[UILabel alloc]initWithFrame:CGRectMake(147, 160, 180,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel3 setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel3 setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel3 setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel3 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel3 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel3.hidden = YES;
    
    self.MessageLabel4 =[[UILabel alloc]initWithFrame:CGRectMake(50, 160, 500,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel4  setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel4  setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel4  setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel4 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel4 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel4.hidden = YES;
    
    self.MessageLabel5 =[[UILabel alloc]initWithFrame:CGRectMake(0, 160, 500,50)];//Set frame of label in your viewcontroller.
    [self.MessageLabel5  setTextColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel5  setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel5  setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel5 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel5 setFont:[UIFont fontWithName:@"Roboto" size:17]];
    self.MessageLabel5.hidden = YES;
    
    self.MessageLabel2=[[UILabel alloc]initWithFrame:CGRectMake(120,140, 300,20)];//Set frame of label in your viewcontroller.
    [self.MessageLabel2 setText:@"*Please fill in the License key field"];//Set text in label.
    [self.MessageLabel2 setTextColor:[UIColor colorWithRed:237/255.0 green:41/255.0 blue:57/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabel2 setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabel2 setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabel2 setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabel2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
    self.MessageLabel2.hidden = YES;
    
    self.MessageLabelInternet=[[UILabel alloc]initWithFrame:CGRectMake(60,143, 512,20)];//Set frame of label in your viewcontroller.
    [self.MessageLabelInternet setText:@"*Please check your internet connection and try again."];//Set text in label.
    [self.MessageLabelInternet setTextColor:[UIColor colorWithRed:237/255.0 green:41/255.0 blue:57/255.0 alpha:1]];//Set text color in label.
    [self.MessageLabelInternet setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [self.MessageLabelInternet setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [self.MessageLabelInternet setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [self.MessageLabelInternet setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
    self.MessageLabelInternet.hidden = YES;
    
    UIView*  ViewLine = [[UIView alloc] initWithFrame:CGRectMake(93,218, 380, 3)];
    ViewLine.backgroundColor = [UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1];
    
    self.LiceneseIDField = [[UITextField alloc] initWithFrame:CGRectMake(170, 80, 290,60)];
    self.LiceneseIDField.borderStyle = UITextBorderStyleLine;
    self.LiceneseIDField.placeholder = @"    License Key*";
    self.LiceneseIDField.backgroundColor = [UIColor whiteColor];
    self.LiceneseIDField.delegate= self;
    
    [self inputtextdata];
    
    UIButton *DoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [DoneButton addTarget:self
                   action:@selector(LicenseViewRemove)
         forControlEvents:UIControlEventTouchUpInside];
    [DoneButton setTitle:@"Done" forState:UIControlStateNormal];
    DoneButton.frame = CGRectMake(475, 10, 90, 35.0);
    DoneButton.showsTouchWhenHighlighted = YES;
    
    self.ContainerView.hidden = YES;
    self.RememberMeLabel.hidden = YES;
    self.URLLabel.hidden = YES;
    self.labelversion.hidden = YES;
    self.rememberMeButtons.hidden = YES;
    self.UrlView.hidden = YES;
    self.lineView.hidden = YES;
    self.ConnectButton.hidden = YES;
    
    UIButton *OKButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [OKButton addTarget:self
                 action:@selector(LicenseViewFirst)
       forControlEvents:UIControlEventTouchUpInside];
    [OKButton setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];
    [OKButton setTitle:@"OK" forState:UIControlStateNormal];
    OKButton.frame = CGRectMake(115, 250, 160, 65);
    OKButton.showsTouchWhenHighlighted = YES;
    
    UIButton *RequestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [RequestButton addTarget:self
                      action:@selector(RequestLicense)
            forControlEvents:UIControlEventTouchUpInside];
    [RequestButton setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1]];
    [RequestButton setTitle:@"Request License" forState:UIControlStateNormal];
    RequestButton.frame = CGRectMake(300, 250, 160, 65);
    RequestButton.showsTouchWhenHighlighted = YES;
    
    [TopbarView addSubview:LicenseTitle];
    
    [self.LicenseView addSubview:ViewLine];
    [self.LicenseView addSubview:TopbarView];
    [self.LicenseView addSubview:LicenseLabel];
    [self.LicenseView addSubview:StatusLabel];
    
    [self.LicenseView addSubview:self.MessageLabel1];
    [self.LicenseView addSubview:self.MessageLabel2];
    [self.LicenseView addSubview:self.MessageLabel3];
    [self.LicenseView addSubview:self.MessageLabel4];
    [self.LicenseView addSubview:self.MessageLabel5];
    [self.LicenseView addSubview:self.MessageLabelInternet];
    [self.LicenseView addSubview:OKButton];
    [self.LicenseView addSubview:RequestButton];
    
    [self.LicenseView addSubview:self.LiceneseIDField];
    [self.view addSubview:self.LicenseView];
    
}
-(void)inputtextdata
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString* userLicenseID = [prefs objectForKey:@"LicenseID"];
    
    if ([userLicenseID length] == 0) {
        self.LiceneseIDField.text = @"";
    }
    else
    {
        self.LiceneseIDField.text = userLicenseID;
    }
    
}
-(void)RequestLicense
{
    self.WebviewLicenseID = [[UIView alloc] initWithFrame:CGRectMake(400,280, 700,500)];
    self.WebviewLicenseID.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:240/255.0 alpha:1];
    self.WebviewLicenseID.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    
    self.WebviewLicenseID.center = CGPointMake(self.view.frame.size.width  / 2,
                                               self.view.frame.size.height / 2);
    
    UIView*  view3 = [[UIView alloc] initWithFrame:CGRectMake(0,0, 700, 50)];
    view3.backgroundColor = [UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1];
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(285, 4, 150,50)];//Set frame of label in your viewcontroller.
    [label setText:@"License Key"];//Set text in label.
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [label setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [label setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(webViewRemove)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    button.frame = CGRectMake(620, 10, 90, 35.0);
    
    button.showsTouchWhenHighlighted = YES;
    
    /* webview without zoom or and interaction
     UIWebView *webview=[[UIWebView alloc]initWithFrame:CGRectMake(0, 50, 700,500)];
     NSString *url=@"https://www.pinkbyte.com/request";
     NSURL *nsurl=[NSURL URLWithString:url];
     NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
     [webview loadRequest:nsrequest];
     [self.view addSubview:webview];
     webview.scrollView.delegate = self;
     webview.scrollView.minimumZoomScale = 1.0;
     webview.scrollView.maximumZoomScale = 1.0;
     webview.scrollView.scrollEnabled = NO;
     webview.scrollView.bounces = NO;
     */
    
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 50, 700,500) configuration:theConfiguration];
    webView.navigationDelegate = self;
    NSURL *nsurl=[NSURL URLWithString:@"https://www.pinkbyte.com/request"];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webView loadRequest:nsrequest];
    
    UIScrollView *scrollView = (UIScrollView *)[webView.subviews objectAtIndex:0];
    scrollView.delegate = self;
    webView.scrollView.delegate = self;
    webView.scrollView.delegate = nil;
    webView.scrollView.maximumZoomScale = 1.0;
    webView.scrollView.minimumZoomScale = 1.0;
    webView.contentMode = UIViewContentModeScaleAspectFit;
    
    [view3 addSubview:label];
    
    [self.WebviewLicenseID addSubview:view3];
    [self.WebviewLicenseID addSubview:button];
    [self.WebviewLicenseID addSubview:webView];
    
    [self.view addSubview:self.WebviewLicenseID];
    
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}
-(void)webViewRemove
{
    [UIView animateWithDuration:0.3
                          delay:0.3
                        options: UIViewAnimationOptionTransitionCurlDown
                     animations:^{
                         self.WebviewLicenseID.alpha = 0;
                     }completion:^(BOOL finished){
                         [self.WebviewLicenseID removeFromSuperview];
                         // [dimView removeFromSuperview];
                     }];
}
-(void)LicenseViewRemove
{
    [UIView animateWithDuration:0.3
                          delay:0.3
                        options: UIViewAnimationOptionTransitionCurlDown
                     animations:^{
                         self.LicenseView.alpha = 0;
                         //dimView.alpha = 0;
                         
                     }completion:^(BOOL finished){
                         [self.LicenseView removeFromSuperview];
                         // [dimView removeFromSuperview];
                     }];
}
-(void)LicenseViewFirst
{
    NSString *TextData = self.LiceneseIDField.text;
    
    BOOL isEmptyCredentials = 0 == [TextData length];
    //if has no text,
    if (isEmptyCredentials)
    {
        self.MessageLabel2.hidden = NO;
        self.MessageLabelInternet.hidden = YES;
        
        return;
    }
    else // if text
    {
        if ([self hasInternet])
        {
            [self LicenseProcess];
        }
        else
        {
            self.MessageLabelInternet.hidden = NO;
            self.MessageLabel2.hidden = YES;
            self.MessageLabel5.hidden = YES;
        }
        return;
    }
}

-(void)LicenseProcess
{
    self.MessageLabel2.hidden = YES;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* AuthcodeKey = [prefs objectForKey:@"AuthcodeLicenseKey"];
    
    if ([AuthcodeKey length] == 0) { // if has no authkey in userdefaults stored
        
        NSLog(@"AuthcodeKey has no data");
        
        NSString *TextData = self.LiceneseIDField.text;
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        [prefs setObject:TextData forKey:@"LicenseID"];
        [prefs synchronize];
        
        NSString *deviceString = @"IPAD-";
        NSString* udidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        NSString *udidFinal = [deviceString stringByAppendingString:udidString];
        
        NSString *FieldAppID = @"Hy1iQMgGX";
        NSString *firstTimeAuth = @"";
        
        NSString *UrlStringtoData=[[NSString alloc]initWithFormat:@"https://license.pinkbyte.net/v1/validate?key=%@&user=%@&auth=%@&app=%@",TextData,udidFinal,firstTimeAuth,FieldAppID];
        NSLog(@"string space----%@",UrlStringtoData);
        
        NSString *encodedUrl = [UrlStringtoData stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSURL *EncrpytedUrl = [[NSURL alloc] initWithString:encodedUrl];
        NSData *jsonData= [NSData dataWithContentsOfURL:EncrpytedUrl];
        NSError *error = nil;
        
        NSDictionary *dataDictionary= [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        
        NSLog(@"data----%@",dataDictionary);
        
        NSString * debug = [dataDictionary objectForKey:@"debug"];
        NSString * showrequest = [dataDictionary objectForKey:@"showrequest"];
        NSString * valid = [dataDictionary objectForKey:@"valid"];
        NSString * authCode = [dataDictionary objectForKey:@"auth"];
        NSString * message = [dataDictionary objectForKey:@"message"];
        
        if([valid intValue] == 0) //if valid is false
        {
            [self.MessageLabel5 setText:message];
            self.MessageLabel5.hidden = NO;
            self.MessageLabel1.hidden = YES;
            self.MessageLabelInternet.hidden = YES;
            self.MessageLabel4.hidden = YES;
            
            if([showrequest intValue] == 0) //showrequst is 0
            {
                [self.MessageLabel5 setText:message];
                self.MessageLabel5.hidden = NO;
                self.MessageLabel3.hidden = YES;
            }
            else // show request is true, show error
            {
                NSString *mess = message;
                NSString *mess2 = @"License is valid, but it has already been used on another computer.";
                
                if ([mess isEqualToString:mess2])
                {
                    [self.MessageLabel1 setText:@"Valid License, currently in use."];
                    self.MessageLabel1.hidden = NO;
                    self.MessageLabel3.hidden = YES;
                    self.MessageLabel5.hidden = YES;
                }
            }
        }
        else{  //if valid is true
            
            self.MessageLabelInternet.hidden = YES;
            self.ContainerView.hidden = NO;
            self.RememberMeLabel.hidden = NO;
            self.URLLabel.hidden = NO;
            self.labelversion.hidden = NO;
            self.rememberMeButtons.hidden = NO;
            self.UrlView.hidden = NO;
            self.lineView.hidden = NO;
            self.ConnectButton.hidden = NO;
            
            [self LicenseViewRemove];
            
            [self.MessageLabel3 setText:@"License is Valid"];
            self.MessageLabel3.hidden = NO;
            self.MessageLabel1.hidden = YES;
            self.MessageLabel5.hidden = YES;
            self.MessageLabel4.hidden = YES;
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSString * Licnesehack = @"1";
            [prefs setObject:Licnesehack forKey:@"Licencekeyhack"];
            [prefs synchronize];
            
            if ([authCode length] == 0) { //if authcode userdefalt is = 0
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                
                [prefs setObject:authCode forKey:@"AuthcodeLicenseKey"];
                [prefs synchronize];
            }
            else //if authcode is different.
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                
                [prefs setObject:authCode forKey:@"AuthcodeLicenseKey"];
                [prefs synchronize];
            }
        }
    }
    else //if authcode has data
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        NSString *TextData = self.LiceneseIDField.text;
        [prefs setObject:TextData forKey:@"LicenseID"];
        [prefs synchronize];
        
        NSString *deviceString = @"IPAD-";
        
        NSString* udidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        NSString *udidFinal = [deviceString stringByAppendingString:udidString];
        
        NSString *FieldAppID = @"Hy1iQMgGX";
        NSString *SecondTimeAuth = AuthcodeKey;
        
        NSString *UrlStringtoData=[[NSString alloc]initWithFormat:@"https://license.pinkbyte.net/v1/validate?key=%@&user=%@&auth=%@&app=%@",TextData,udidFinal,SecondTimeAuth,FieldAppID];
        NSLog(@"string space----%@",UrlStringtoData);
        
        
        NSString *encodedUrl = [UrlStringtoData stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSURL *EncrpytedUrl = [[NSURL alloc] initWithString:encodedUrl];
        NSData *jsonData= [NSData dataWithContentsOfURL:EncrpytedUrl];
        NSError *error = nil;
        
        NSDictionary *dataDictionary= [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        
        //NSLog(@"data----%@",dataDictionary);
        
        NSString * debug = [dataDictionary objectForKey:@"debug"];
        NSString * showrequest = [dataDictionary objectForKey:@"showrequest"];
        NSString * valid = [dataDictionary objectForKey:@"valid"];
        NSString * authCode = [dataDictionary objectForKey:@"auth"];
        NSString * message = [dataDictionary objectForKey:@"message"];
        
        if([valid intValue] == 0) //if valid is 0 or false
        {
            if([showrequest intValue] == 0) //showrequst is 0
            {
                [self.MessageLabel1 setText:message];
                self.MessageLabel1.hidden = NO;
                self.MessageLabel3.hidden = YES;
            }
            else // show request is true, show error
            {
                NSString *mess = message;
                NSString *mess2 = @"License is valid, but it has already been used on another computer.";
                
                if ([mess isEqualToString:mess2])
                {
                    [self.MessageLabel1 setText:@"Valid License, currently in use."];
                    self.MessageLabel1.hidden = NO;
                    self.MessageLabel3.hidden = YES;
                }
            }
        }
        else{ //valid is 1 or true
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSString * Licnesehack = @"1";
            [prefs setObject:Licnesehack forKey:@"Licencekeyhack"];
            [prefs synchronize];
            
            [self.MessageLabel3 setText:@"License is Valid"];
            self.MessageLabel3.hidden = NO;
            self.MessageLabel1.hidden = YES;
        }
        
        NSString* AuthcodeKey = [prefs objectForKey:@"AuthcodeLicenseKey"];
        
        if ([authCode length] == 0) { //wont ever hit this
            NSLog(@"code is null");
        }
        else //if auth code has data
        {
            if ([AuthcodeKey isEqualToString:authCode]) //wont hit this ever
            {
                
            }
            else{ //auth code is different add new auth code to default
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                
                [prefs setObject:authCode forKey:@"AuthcodeLicenseKey"];
                [prefs synchronize];
            }
        }
    }
}

-(void)didFinishWithBackgroundCameraRollSelect:(NSNotification*)notification
{
    
    NSString *selectedImage = notification.userInfo[@"SelectedCamerarollImage"] ;
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@", selectedImage];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"camrollurl %@",url);
    
    switch ([[UIApplication sharedApplication] statusBarOrientation]) {
        case UIInterfaceOrientationLandscapeLeft:
            [self CameraLandscapeLeft:selectedImage];
        case UIInterfaceOrientationLandscapeRight:
            [self CameraLandscapeRight:selectedImage];
        case UIInterfaceOrientationPortrait:
            [self CameraPortrait:selectedImage];
        case UIInterfaceOrientationPortraitUpsideDown:
            
        default:
            break;
    }
    
    
    
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    
    [Def setObject:urlString forKey:@"BackgroundCameraRollimage"];
    
    
}
-(void)CameraPortrait:(NSString*)StringForCamroll
{
    NSString *urlString = [NSString stringWithFormat:@"%@", StringForCamroll];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"camrollurl %@",url);
    
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:url resultBlock:^(ALAsset *asset)
     {
         UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0 orientation:UIImageOrientationUp];
         
         _backgroundImage.image = copyOfOriginalImage;
     }
            failureBlock:^(NSError *error)
     {
         // error handling
         NSLog(@"failure-----");
     }];
    
}
-(void)CameraLandscapeLeft:(NSString*)StringForCamroll
{
    NSString *urlString = [NSString stringWithFormat:@"%@", StringForCamroll];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"camrollurl %@",url);
    
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:url resultBlock:^(ALAsset *asset)
     {
         UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0 orientation:UIImageOrientationLeft];
         
         _backgroundImage.image = copyOfOriginalImage;
     }
            failureBlock:^(NSError *error)
     {
         // error handling
         NSLog(@"failure-----");
     }];
    
}
-(void)CameraLandscapeRight:(NSString*)StringForCamroll
{
    NSString *urlString = [NSString stringWithFormat:@"%@", StringForCamroll];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"camrollurl %@",url);
    
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:url resultBlock:^(ALAsset *asset)
     {
         UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0 orientation:UIImageOrientationRight];
         
         _backgroundImage.image = copyOfOriginalImage;
     }
            failureBlock:^(NSError *error)
     {
         // error handling
         NSLog(@"failure-----");
     }];
    
}
-(void)didFinishWithBackgroundSelect:(NSNotification*)notification{
    
    NSString *selectedImage = notification.userInfo[@"selectedImage"] ;
    _backgroundImage.image = [UIImage imageNamed:selectedImage];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotifyBackgroundImageSetting" object:self];
}

-(void)didFinishWithBeacon:(NSNotification*)notification{
    NSString *username = notification.userInfo[@"Username"];
    NSString *password  =  notification.userInfo[@"Password"];
    NSString *ipaddress = notification.userInfo[@"IPAddress"];
    
    self.usernameTextField.text = username;
    self.passwordTextField.text = password;
    self.ipaddressTextField.text = ipaddress;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconNotification" object:self];
    
    [self performSelector:@selector(LoginHack) withObject:self afterDelay:0];
    self.rememberMeButtons.selected = YES;
    self.rememberMeSelected = YES;
    _hasBeacon = YES;
    
}
-(void)LoginHack
{
    [self loginClick:self];
}

-(void)doA{
    
    _warningLabel.hidden = NO;
    _warningLabel.text = @"Validating, Please wait...";
}
- (IBAction)loginClick:(id)sender
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* Beaconappstring = @"empty";
    [prefs setObject:Beaconappstring forKey:@"Beaconappstring2"];
    [prefs synchronize];
    
    if ([self hasInternet])
    {
        NSString *username = self.usernameTextField.text;
        NSString *password = self.passwordTextField.text;
        NSString *ipaddress =self.ipaddressTextField.text;
        username = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (NSNotFound != [username rangeOfString:@"admin"].location || NSNotFound != [username rangeOfString:@"glass"].location || NSNotFound != [username rangeOfString:@"staller"].location || NSNotFound != [username rangeOfString:@"etwork"].location)
        {
            [self showUserNotAllowaccessAlert];
            return;
        }
        else
        {
            NSString *userstring= username;
            NSString *passstring= password;
            
            NSRange usernamespaced = [userstring rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
            NSRange passwordspaced = [passstring rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if(usernamespaced.location != NSNotFound || passwordspaced.location != NSNotFound)
            {
                NSString* replaceduser = [userstring stringByReplacingOccurrencesOfString:@" " withString:@"-"];
                NSString* replacedpassword = [passstring stringByReplacingOccurrencesOfString:@" " withString:@"-"];
                NSLog(@"string space----%@",replaceduser);
                NSLog(@"string space----%@",replacedpassword);
                
                BOOL isEmptyCredentials = 0 == [replaceduser length];
                if (isEmptyCredentials)
                {
                    self.warningLabel.text = kEmptyCredentialsMessage;
                    self.warningLabel.hidden = NO;
                    return;
                }
                self.warningLabel.hidden = YES;
                cSPH_RememberUser *rememberedUser = [[[cSPH_RememberUser alloc] init] autorelease];
                
                if (!rememberedUser)
                {
                    SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
                    return;
                }
                if (self.rememberMeSelected)
                {
                    rememberedUser.username = username;
                    rememberedUser.password = password;
                    rememberedUser.ipaddress = ipaddress;
                    rememberedUser.rememberStatus = @YES;
                    [rememberedUser saveInfo];
                }
                else
                {
                    [rememberedUser resetInfo];
                }
                
                NSString *userstring2= @"test";
                NSString *passstring2= @"test";
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:username forKey:@"user1"];
                [prefs setObject:password forKey:@"pass1"];
                [prefs synchronize];
                
                [self SendtoHttpPost];
                
                // Notify on next runloop iteration to prevent possible controller deallocation
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate loginViewController:self loginWithUsername:userstring2 password:passstring2 HasBeacon:_hasBeacon];
                });
            }
            else{
                
                BOOL isEmptyCredentials = 0 == [username length];
                if (isEmptyCredentials)
                {
                    self.warningLabel.text = kEmptyCredentialsMessage;
                    self.warningLabel.hidden = NO;
                    return;
                }
                self.warningLabel.hidden = YES;
                cSPH_RememberUser *rememberedUser = [[[cSPH_RememberUser alloc] init] autorelease];
                
                if (!rememberedUser)
                {
                    SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
                    return;
                }
                if (self.rememberMeSelected)
                {
                    rememberedUser.username = username;
                    rememberedUser.password = password;
                    rememberedUser.ipaddress = ipaddress;
                    rememberedUser.rememberStatus = @YES;
                    [rememberedUser saveInfo];
                }
                else
                {
                    [rememberedUser resetInfo];
                }
                NSString *UrlStringtoData=[[NSString alloc]initWithFormat:@"http://%@/client/validate.php?username=%@&password=%@", ipaddress,username,password];
                NSString *encodedUrl = [UrlStringtoData stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
                NSURL *EncrpytedUrl = [[NSURL alloc] initWithString:encodedUrl];
                NSData *jsonData= [NSData dataWithContentsOfURL:EncrpytedUrl];
                NSError *error = nil;
                // NSLog(@"data----%@",str);
                if(jsonData != nil)
                {
                    NSDictionary *dataDictionary= [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
                    [[NSUserDefaults standardUserDefaults] setObject:dataDictionary forKey:@"dictionaryKey"];
                    if (error != nil)
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error loading data from the server" message:[NSString stringWithFormat:@"Error in loading data, error desc.: %@", error] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSString *IpAddressSaved = ipaddress;
                    [prefs setObject:IpAddressSaved forKey:@"ipaddressaved"];
                    [prefs setObject:username forKey:@"user1"];
                    [prefs setObject:password forKey:@"pass1"];
                    [prefs synchronize];
                    
                    // Notify on next runloop iteration to prevent possible controller deallocation
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate loginViewController:self loginWithUsername:username password:password HasBeacon:_hasBeacon];
                    });
                    
                }
                else
                {
                    [self showServerErrorAlert];
                    return;
                }
            }
            
        }
    }
    else
    {
        [self showNetworkErrorAlert];
        return;
    }
    
    
    
}

- (void) loadTheApps:(NSDictionary*)jsonData
{
    NSString * Clientmode = [jsonData objectForKey:@"clientmode"];
    NSString * Status = [jsonData objectForKey:@"status"];
    NSString * Message = [jsonData objectForKey:@"message"];
    if([Clientmode intValue] == 1)
    {
        if([Status intValue] == 1)
        {
            NSLog(@"Success loigin to app screen");
            //khaleds code for the images...
        }
        else
        {
            //error message.. if status is 0 {"clientmode":"1","status":0,"message":"Invalid username and\/or password."}
            NSString * message = [jsonData objectForKey:@"message"];
            NSLog(@"Received: %@", message);
        }
    }
    else
    {
        // use the app app launcher  brandon isnt done this yet...{"clientmode":"0","status":1,"message":"","launchapp":"AppLauncher"}
    }
}

- (IBAction)register
{
    RegistrationViewController *controller = [RegistrationViewController registrationViewController];
    controller.delegate = self;
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)forgotPassword
{
    RegistrationViewController *controller = [RegistrationViewController registrationViewController];
    controller.forgotPassword = YES;
    controller.delegate = self;
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:controller animated:YES completion:nil];
}


- (IBAction)rememberMe
{
    self.rememberMeButtons.selected = self.rememberMeSelected = !self.rememberMeSelected;
}

-(BOOL) shouldAutorotate
{
    BOOL _shouldRotate;
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    
    if ([[Def objectForKey:@"AutoRotationLock"] isEqualToString:@"Locked"])
        _shouldRotate = NO;
    else
        _shouldRotate = YES;
    
    
    if(IS_IPAD)
    {
        return _shouldRotate;
    }
    else
    {
        return NO;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    if(IS_IPAD)
    {
        return UIInterfaceOrientationMaskAll;
    }
    
    else
    {
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (void) orientationChanged
{
    //[self.menuPopover dismissMenuPopover];
    //Using convertPoint to get the relative position inside a parent UIView
    CGPoint aPosViewA = [_btnDropDown.superview convertPoint:_btnDropDown.center toView:self.view];
    self.menuPopover.frame = CGRectMake(aPosViewA.x+15, aPosViewA.y+10, 220, 210);
}



#pragma mark - Check the internet connection
-(BOOL) hasInternet
{
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [reach currentReachabilityStatus];
    if (internetStatus == NotReachable){
        //[self showNetworkErrorAlert];
        return NO;
    }
    return YES;
}

#pragma mark - RegistrationViewControllerDelegate
- (void)registrationViewControllerDidFinished
{
    //warning Move it to presenting controller?
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Dropdown IPs
- (NSMutableArray*)ipAddressLoadPlist {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ipAddress.plist"]; //3
    NSMutableArray *arrData = [[NSMutableArray alloc] initWithContentsOfFile:path];
    if (arrData == nil || [arrData count] == 0)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ipaddresshack"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.btnDropDown.hidden = NO;
    }
    return arrData;
}

- (IBAction)DropdownIPaction:(id)sender
{
    self.menuItems = [self ipAddressLoadPlist];
    // Hide already showing popover
    [self.menuPopover dismissMenuPopover];
    //Using convertPoint to get the relative position inside a parent UIView
    CGPoint aPosViewA = [_btnDropDown.superview convertPoint:_btnDropDown.center toView:self.view];
    self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(aPosViewA.x+15, aPosViewA.y+10, 220, 210) menuItems:self.menuItems];
    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.view];
}

#pragma mark MLKMenuPopoverDelegate
- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    _ipaddressTextField.text = [self.menuItems objectAtIndex:selectedIndex];
    //[SimplePingHelper ping:[self.ipaddressTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] target:self sel:@selector(pingResult:)];
}

-(void)menuPopover:(MLKMenuPopover *)menuPopover DeletedMenuItemAtIndex:(NSInteger)selectedIndex
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ipAddress.plist"]; //3
    NSMutableArray *arrData = [[NSMutableArray alloc] initWithContentsOfFile:path];
    self.menuItems = arrData;
    [self.menuItems removeObjectAtIndex:selectedIndex];
    [arrData writeToFile: path atomically:YES];
    [self.menuPopover dismissMenuPopover];
    if (arrData == nil || [arrData count] == 0)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ipaddresshack"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.btnDropDown.hidden = YES;
    }
}

-(void)menuPopover:(MLKMenuPopover *)menuPopover OrderedMenuItems:(NSMutableArray*)OrdmenuItems
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ipAddress.plist"]; //3
    NSMutableArray *arrData = [[NSMutableArray alloc] initWithContentsOfFile:path];
    arrData = OrdmenuItems;
    [arrData writeToFile: path atomically:YES];
}
@end
