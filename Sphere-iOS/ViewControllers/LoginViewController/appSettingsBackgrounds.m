//
//  appSettingsBackgrounds.m
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2016-06-14.
//
//
#import "LoginViewController.h"

#import "appSettingsBackgrounds.h"
#import "AppDelegate.h"

@interface bgObjectX : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *Background;
@property (nonatomic, strong) NSString *LaunchImage;
@end
@implementation bgObjectX
@synthesize title, Background, LaunchImage;
@end

@interface appSettingsBackgrounds ()
@property (retain, nonatomic) IBOutlet UITableView *tvBackgrounds;
@property(nonatomic,strong) NSMutableArray *arrJSON;
@property (nonatomic, strong) NSString *selectedImageName;
@property (retain, nonatomic) IBOutlet UISwitch *swLock;
@property (retain, nonatomic) IBOutlet UISwitch *swBeaconMonitoring;
@end

UIImagePickerController *CameraRollPicker;

@implementation appSettingsBackgrounds

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self testJsonIsValid];
    NSLog(@"%lu",  (unsigned long)[_arrJSON count]);
    
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    if ([[Def objectForKey:@"AutoRotationLock"] isEqualToString:@"Locked"]) {
        [_swLock setOn:YES];
    } else{
        [_swLock setOn:NO];
        
    }
    if ([[Def objectForKey:@"beacon"] isEqualToString:@"beaconidon"]) {
        [_swBeaconMonitoring setOn:YES];
    } else{
        [_swBeaconMonitoring setOn:NO];
        
    }
}

- (void)testJsonIsValid
{
    _arrJSON = [[NSMutableArray array] init];
    
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"bgJSON" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filepath];
    NSError *error = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingMutableContainers
                                                               error:&error];
    if( !error )
    {
        NSDictionary *JsonDataDictionary = [jsonData objectForKey:@"BackgroundList"];
        for (NSDictionary * dataDict in JsonDataDictionary)
        {
            bgObjectX *bg = [[bgObjectX alloc] init];
            bg.title = [dataDict objectForKey:@"title"];
            bg.Background = [dataDict objectForKey:@"Background"];
            bg.LaunchImage = [dataDict objectForKey:@"LaunchImage"];
            
            [_arrJSON addObject:bg];
            [_arrJSON retain];
        }
    }
    else {
        NSLog(@"%@", [error description]);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrJSON count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    
    bgObjectX *bg = _arrJSON[indexPath.row];
    cell.textLabel.text = bg.title;
    cell.imageView.image = [UIImage imageNamed:bg.Background];
    UIImage *image = cell.imageView.image;
    UIGraphicsBeginImageContext(CGSizeMake(100,100));
    // draw scaled image into thumbnail context
    
    [image drawInRect:CGRectMake(5, 5, 100, 100)]; //
    UIImage *newThumbnail = UIGraphicsGetImageFromCurrentImageContext();
    // pop the context
    UIGraphicsEndImageContext();
    if(newThumbnail == nil)
    {
        NSLog(@"could not scale image");
        cell.imageView.image = image;
    }
    else
    {
        cell.imageView.image = newThumbnail;
    }
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"Select a Background";
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    bgObjectX *bg = _arrJSON[indexPath.row];
    _selectedImageName = bg.Background;
    
    if (_selectedImageName != nil){
        NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
        [Def setObject:_selectedImageName forKey:@"BackgroundImageSetting"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotifyBackgroundImageSetting"
                                                        object:self
                                                      userInfo:@{@"selectedImage": _selectedImageName}];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath isEqual:((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject])]){
        //end of loading
        [self HighlightLastSelectedImage];
        
    }
}

- (void)HighlightLastSelectedImage
{
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    NSString *savedBGImage = [Def objectForKey:@"BackgroundImageSetting"];
    
    
    NSArray *arrBG = [_arrJSON valueForKey:@"Background"];
    int i = 0;
    for (id tempArrObject in arrBG) {
    //    NSLog(@"Single element: %@", tempArrObject);
        
        if ([savedBGImage isEqualToString: tempArrObject]) {
            NSIndexPath* idx = [NSIndexPath indexPathForRow:i inSection:0];
            [_tvBackgrounds selectRowAtIndexPath:idx animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            [self tableView:_tvBackgrounds didSelectRowAtIndexPath:idx];
        }
        
        i++;
    }
}

#pragma mark CameraRoll into background image

- (IBAction)btnDone:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)CameraRollBackgroundImage:(id)sender {
    
    CameraRollPicker = [[UIImagePickerController alloc] init];
    CameraRollPicker.delegate = self;
   // CameraRollPicker.allowsEditing = YES;
    CameraRollPicker.modalPresentationStyle = UIModalPresentationCurrentContext;

    CameraRollPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    [self presentViewController:CameraRollPicker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey: @"UIImagePickerControllerReferenceURL"];
   // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
   // _backgroundImage.image = chosenImage;
   // NSLog(@"Dictionary: %@", [info description]);
   // NSLog(@"data----%@",mediaType);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CameraRollBackgroundImage"
                                                        object:self
                                                      userInfo:@{@"SelectedCamerarollImage": mediaType}];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"BackgroundImageSetting"];

    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)swLockOrientation:(id)sender {
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    
    if ([sender isOn]) {
        [Def setObject:@"Locked" forKey:@"AutoRotationLock"];
        
    } else {
        [Def setObject:@"unLocked" forKey:@"AutoRotationLock"];
    }
    [Def synchronize];
    
}
- (IBAction)swBeaconMonitoringMethod:(id)sender {
    //Check if there are any beacons selected in plist file (at scaning beacons)
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString* path = [appDelegate GetPlistFilePath];
    NSArray *plist = [NSMutableArray arrayWithContentsOfFile: path];
    
    if ([plist count] <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No beacons selected!"
                                                        message:@"Please select nearby beacons from 'iBeacons' Tab."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        
        [alert show];
        [sender setOn:NO animated:YES];

    }
    else  //If there is selected beacons
    {
        NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
        
        if ([sender isOn]) {
            NSLog(@"locked");
            [Def setObject:@"Locked" forKey:@"MontioringLock"];
            
        } else {
            NSLog(@"unlocked");
            
            [Def setObject:@"unLocked" forKey:@"MontioringLock"];
            [Def setObject:@"beaconidoff" forKey:@"beacon"];
            
        }
        [Def synchronize];
        
    }
}
- (void)dealloc {
    [_tvBackgrounds release];
    [_swLock release];
    [_swBeaconMonitoring release];
   // [CameraRollPicker release];
    [super dealloc];
}
@end
