//
//  SignInButton.m
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2016-02-23.
//
//

#import "SignInButton.h"

@implementation SignInButton
- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = [UIColor colorWithWhite:0.1f alpha:0.1f];
    }
    else {
        self.backgroundColor = [UIColor clearColor];
    }
}
@end
