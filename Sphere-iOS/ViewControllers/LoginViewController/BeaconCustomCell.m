//
//  BeaconCustomCell.m
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2016-06-22.
//
//

#import "BeaconCustomCell.h"

@implementation BeaconCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
