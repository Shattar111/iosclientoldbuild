//
//  appSettingsBackgrounds.h
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2016-06-14.
//
//

#import <UIKit/UIKit.h>


@interface appSettingsBackgrounds : UIViewController<UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate>


@end

