//
//  LoginViewControllerDelegate.h
//  IE on Demand
//
//  Created by Xtreme Labs on 2013-10-11.
//
//

#ifndef IE_on_Demand_LoginViewControllerDelegate_h
#define IE_on_Demand_LoginViewControllerDelegate_h

@protocol LoginViewControllerDelegate <NSObject>
@required
- (void)loginViewController:(id)controller loginWithUsername:(NSString*)username password:(NSString*)password HasBeacon:(BOOL) HasBeacon;


@end

#endif
