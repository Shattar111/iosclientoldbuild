//
//  appSettingsiBeacon.m
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2016-06-14.
//
//

#import "appSettingsiBeacon.h"
#import "BeaconCustomCell.h"
#import "LoginViewController.h"
#import "AppDelegate.h"

@interface appSettingsiBeacon ()
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *beaconsArray;
@property (nonatomic, strong) ESTBeaconManager *beaconManager;
@property (nonatomic, strong) CLBeaconRegion *region;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segScanning;
@property (nonatomic, strong) NSString *beacontest;

@end

@implementation appSettingsiBeacon

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Nearby Beacons";
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([BeaconCustomCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([BeaconCustomCell class])];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /*
     * Starts looking for Estimote beacons.
     */
    
    /* We specify it using only the ESTIMOTE_PROXIMITY_UUID because we want to discover all
     * hardware beacons with Estimote's proximty UUID.
     */
    self.region = [[CLBeaconRegion alloc] initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID
                                                     identifier:@"Sphere3DRegion"];
    
    self.beaconManager = [ESTBeaconManager new];
    self.beaconManager.delegate = self;
    
    
    [self startRangingBeacons];
}
-(void)startRangingBeacons
{
    if ([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        [self.beaconManager requestAlwaysAuthorization];
    }
    else if([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Access Denied"
                                                        message:[NSString stringWithFormat:@"You have denied access to location services. Change this in app settings. Goto Settings > Privacy > Location Serivces > %@" , [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        
        [alert show];
    }
    else if([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusRestricted)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Not Available"
                                                        message:@"You have no access to location services."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        
        [alert show];
    }
    
    if([ESTBeaconManager authorizationStatus] == kCLAuthorizationStatusAuthorized){
        
        [self.beaconManager startRangingBeaconsInRegion:self.region];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    /*
     *Stops ranging after exiting the view.
     */
    [self.beaconManager stopRangingBeaconsInRegion:self.region];
}

- (IBAction)btnDone:(id)sender {
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        
    //Reset beacon regioning
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate stopBeaconRegioning];
    [appDelegate initializeBeacon];

    
}

#pragma mark - ESTBeaconManager delegate

- (void)beaconManager:(id)manager didChangAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status != kCLAuthorizationStatusNotDetermined && status != kCLAuthorizationStatusDenied )
    {
        [self.beaconManager startRangingBeaconsInRegion:self.region];
    }
}

- (void)beaconManager:(id)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
    UIAlertView* errorView = [[UIAlertView alloc] initWithTitle:@"Ranging error"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    [errorView show];
}
- (void)beaconManager:(id)manager monitoringDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
    UIAlertView* errorView = [[UIAlertView alloc] initWithTitle:@"Monitoring error"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    [errorView show];
}

- (void)beaconManager:(id)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    self.beaconsArray = beacons;
    
    [self.tableView reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.beaconsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BeaconCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([BeaconCustomCell class]) forIndexPath:indexPath];
    
    /*
     * Fill the table with beacon data.
     */
    
    id beacon = [self.beaconsArray objectAtIndex:indexPath.row];
    
    if ([beacon isKindOfClass:[CLBeacon class]])
    {
        CLBeacon *cBeacon = (CLBeacon *)beacon;
        
        cell.lblMajor.text = cBeacon.major.stringValue;
        cell.lblMinor.text = cBeacon.minor.stringValue;
        
        if (cBeacon.accuracy < 0.0) {
            cell.lblDistance.text = @"Unknown";
            cell.lblDistance.textColor = [UIColor redColor];
        } else {
            cell.lblDistance.text = [NSString stringWithFormat:@"%.2f m" , cBeacon.accuracy];
            cell.lblDistance.textColor = [UIColor blackColor];
        }
        cell.lblUUID.text = cBeacon.proximityUUID.UUIDString;
    }
    
    //Put switch view into the table Cell
    UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
    cell.accessoryView = switchView;
    [switchView setOn:NO animated:NO];
    
    switchView.tag = indexPath.row;
    
    //Assign action into the switch
    [switchView addTarget:self action:@selector(updateSwitchAtIndexPath:) forControlEvents:UIControlEventValueChanged];
    
    
    //Set Disable all switchs by default
    [self EnableDisableSwitchBtn:NO];
    
    //Set switch status to ON if already selected
    [switchView setOn:[self SetSwitchStatus:cell.lblUUID.text Major:cell.lblMajor.text Minor:cell.lblMinor.text]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)updateSwitchAtIndexPath:(id)sender{ //Once switch button is tapped set its status and save/remove to plist file
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    BeaconCustomCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    UISwitch *switchView = (UISwitch *)sender;
    
    if ([switchView isOn]) {
        [switchView setOn:YES animated:YES];
        
        NSLog(@"%d", [sender tag]);
        
        switch(switchView.tag){
            case 0:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidon";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit1");
                
                
            }
                break;
            case 1:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidon";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit1");
                
                
                
            }
                break;
            case 2:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidon";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit1");
                
                
                
            }
                break;
            case 3:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidon";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit1");
                
            }
                break;
                // ...
            default:
                break;
        }
        
        [self BeaconSavingPlist:cell.lblUUID.text
                          Major:cell.lblMajor.text
                          Minor:cell.lblMinor.text];
    } else {
        [switchView setOn:NO animated:YES];
        
        NSLog(@"%d", [sender tag]);
        
        switch(switchView.tag){
            case 0:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidoff";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit");
            }
                break;
            case 1:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidoff";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit2");
            }
                break;
            case 2:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidoff";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit2");
            }
                break;
            case 3:
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *scanbeaconstring = @"beaconidoff";
                [prefs setObject:scanbeaconstring forKey:@"beacon"];
                [prefs synchronize];
                
                NSLog(@"hit2");
            }
                break;
                // ...
            default:
                break;
        }
        
        
        
        [self BeaconRemovePlist:cell.lblUUID.text
                          Major:cell.lblMajor.text
                          Minor:cell.lblMinor.text];
    }
}

- (IBAction)Scanning:(UISegmentedControl *)sender {  // START/STOP scanning button
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.beaconManager startRangingBeaconsInRegion:self.region];
            [self EnableDisableSwitchBtn:NO];
            break;
        case 1:
            [self.beaconManager stopRangingBeaconsInRegion:self.region];
            [self EnableDisableSwitchBtn:YES];
            break;
        default:
            break;
    }
}

- (void) EnableDisableSwitchBtn:(BOOL)YesNo {     //Sets switch button to Enable/Disable during scanning
    for (UIView *view in self.tableView.subviews){
        for (id subview in view.subviews){
            if ([subview isKindOfClass:[UITableViewCell class]]){
                UITableViewCell *cell = subview;
                UISwitch *switchView = (UISwitch *)cell.accessoryView;
                if (YesNo)
                    [switchView setEnabled:YES];
                else
                    [switchView setEnabled:NO];
            }
        }
    }
}

-(BOOL)SetSwitchStatus:(NSString*)uuid Major:(NSString*)major Minor:(NSString*)minor{ //Check if the beacon is exist in the plist file and set switch button ON
    
    NSString* path = [self GetPlistFilePath];
    
    NSArray *newContent = [[NSArray alloc]initWithContentsOfFile:path];
    
    for (int i=0; i < newContent.count; i++) {
        NSDictionary *dictOfArray = [newContent objectAtIndex:i];
        
        if ([[dictOfArray objectForKey:@"UUID"] isEqualToString:uuid] && [[dictOfArray objectForKey:@"Major"] isEqualToString:major] && [[dictOfArray objectForKey:@"Minor"] isEqualToString:minor]) {
            
            return YES;
        }
    }
    return NO;
}

- (void)BeaconSavingPlist:(NSString*)UUid Major:(NSString*)major Minor:(NSString*)minor { //Save selected beacon to plist file when switch is ON
    
    NSString* path = [self GetPlistFilePath];
    
    //Make Array of dictionary for beacon details
    NSDictionary *plistDict = [[NSDictionary alloc]
                               initWithObjects: [NSArray arrayWithObjects: UUid, minor, major, nil] forKeys:[NSArray arrayWithObjects: @"UUID", @"Minor",@"Major", nil]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) //Check if the plist file already exists
    {   //Append the array of dictionary to plist
        NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:path];
        [array addObject:plistDict];
        [array writeToFile:path atomically:YES];
    }
    else
    {
        NSArray *array = [NSArray arrayWithObject:plistDict];
        [array writeToFile:path atomically:YES];
    }
}

- (void)BeaconRemovePlist:(NSString*)UUid Major:(NSString*)major Minor:(NSString*)minor {  //Remove selected beacon from plist file when switch is OFF
    
    NSString* path = [self GetPlistFilePath];
    
    NSMutableArray *plist = [NSMutableArray arrayWithContentsOfFile: path];
    
    //Loop through all the beacons in plist to find the match
    for (NSDictionary *item in plist) {
        //Compare for selected beacon and remove it from plist
        if ([[item objectForKey:@"UUID"] isEqualToString:UUid] && [[item objectForKey:@"Major"] isEqualToString:major] && [[item objectForKey:@"Minor"] isEqualToString:minor])
            
            [plist removeObject:item];
        
        [plist writeToFile:path atomically:YES];
        break;
    }
}

-(NSString*)GetPlistFilePath{ //Get the path of the plist file in the document structure of the device
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"BeaconList.plist"]; //3
    
    return path;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_tableView release];
    [_segScanning release];
    [super dealloc];
}
@end

