//
//  BeaconCustomCell.h
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2016-06-22.
//
//

#import <UIKit/UIKit.h>

@interface BeaconCustomCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblMajor;
@property (retain, nonatomic) IBOutlet UILabel *lblMinor;
@property (retain, nonatomic) IBOutlet UILabel *lblUUID;
@property (retain, nonatomic) IBOutlet UILabel *lblDistance;
@end
