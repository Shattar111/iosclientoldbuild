//
//  LoginViewController.m
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  This code is apart of the Login GUI for Glassware Connect. This is where ther user enters in the loging credentials to a glassware appliance.
//
//  The Loginview controller mostly consists of:
//
//  Login gui
//  Login credential input and saving
//  pinging ip
//  network configutation testing
//  display prevouis successful connected glassware appliance ip addresses
//  Main screen when user logs off the appliaction
//  Iphone and Ipad detection for screen size, features etc..

#import "CustomViewController.h"
#import "LoginViewControllerDelegate.h"
#import "RegistrationViewControllerDelegate.h"
#import "MLKMenuPopover.h"
#import <Security/Security.h>


@interface LoginViewController : CustomViewController <RegistrationViewControllerDelegate, UITextFieldDelegate,MLKMenuPopoverDelegate,UIImagePickerControllerDelegate,UIWebViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) id<LoginViewControllerDelegate, NSObject> delegate;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;

+ (instancetype)loginViewController;
+ (instancetype)loginViewControllerWithLoginError:(NSError*)error username:(NSString*)username ;

- (void) AppLauncher;

@end

