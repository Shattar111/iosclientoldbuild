//
//  BouncingBallsView.h
//  Gravity
//
//  Created by Khaled Murshed on 2014-11-21.
//  Copyright (c) 2014 Sphere3D. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BouncingBallsView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;

@end
