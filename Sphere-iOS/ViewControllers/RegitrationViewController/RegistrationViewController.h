//
//  RegistrationViewController.h
//  IE on Demand
//
//  Created by Xtreme Labs on 2013-10-16.
//
//

#import <UIKit/UIKit.h>
#import "RegistrationViewControllerDelegate.h"

@interface RegistrationViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) id<RegistrationViewControllerDelegate, NSObject> delegate;

@property (nonatomic, strong) IBOutlet UITextField *nameTextField;
@property (nonatomic, strong) IBOutlet UITextField *occupationTextField;
@property (nonatomic, strong) IBOutlet UITextField *countryTextField;
@property (nonatomic, strong) IBOutlet UITextField *usernameTextField;
@property (nonatomic, strong) IBOutlet UITextField *emailTextField;

@property (nonatomic, strong) IBOutlet UIButton *registerButton;
// __REALWAT__ : BEGIN : 2013/Dec/10 : Fixed PTS:GC-19
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
// __REALWAT__ : END
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *registeringActivityIndicator;
@property (nonatomic, strong) IBOutlet UILabel *errorMessage;
@property (nonatomic, strong) IBOutlet UIButton *Fourtydaybutton;

@property (nonatomic, strong) IBOutlet UIView *preregisterView;
@property (nonatomic, strong) IBOutlet UIView *confirmationView;

@property (nonatomic) BOOL forgotPassword;

+ (instancetype)registrationViewController;

- (IBAction)cancel:(id)sender;
- (IBAction)registration:(id)sender;
- (IBAction)FourtyFivedaytrail:(id)sender;

- (IBAction)goToMail:(id)sender;

@end

