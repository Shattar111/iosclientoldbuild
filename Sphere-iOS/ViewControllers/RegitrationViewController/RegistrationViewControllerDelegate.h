//
//  RegistrationViewControllerDelegate.h
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-16.
//
//

#ifndef IE_on_Demand_RegistrationViewControllerDelegate_h
#define IE_on_Demand_RegistrationViewControllerDelegate_h

@protocol RegistrationViewControllerDelegate <NSObject>
@required
- (void)registrationViewControllerDidFinished;

@end


#endif
