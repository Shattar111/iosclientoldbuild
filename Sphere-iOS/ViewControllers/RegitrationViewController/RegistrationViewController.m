//
//  RegistrationViewController.m
//  IE on Demand
//
//  Created by Xtreme Labs on 2013-10-16.
//
//

#import "RegistrationViewController.h"
#import "RegistrationData.h"

#import "Sphere3DAPI.h"

#import "SPH_InAppViewController.h"

@interface RegistrationViewController ()
@property (nonatomic, assign, getter = isAgreedToTerms) BOOL agreedToTerms;
@end

@implementation RegistrationViewController

+ (instancetype)registrationViewController {
    return [[[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil] autorelease];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.agreedToTerms = NO;
    }
    return self;
}

#pragma mark - View life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.confirmationView.frame = CGRectMake(self.view.frame.size.width, 0, self.confirmationView.frame.size.width, self.confirmationView.frame.size.height);
    [self.view addSubview:self.confirmationView];
    
    [self.registeringActivityIndicator stopAnimating];
    [self FourtyFivedaytrail:nil];
}

#pragma mark - IBActions

- (IBAction)cancel:(id)sender
{
	
    self.errorMessage.text = @"";
    if (nil != self.delegate) {
        [self.delegate registrationViewControllerDidFinished];
    }
	
}
- (IBAction)FourtyFivedaytrail:(id)sender;
{
    UIToolbar *lotoolbar = [[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 700, 42)] autorelease];
    
    [lotoolbar setTintColor:[UIColor blackColor]];
    
    UIBarButtonItem *button=[[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done)] autorelease];
    
    lotoolbar.items = @[button];
    
    [self.view addSubview:lotoolbar];
    
    
    CGRect webFrame = CGRectMake(0.0, 41, 600, 600);
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:webFrame];
    webView.opaque = NO;
    
    webView.scalesPageToFit = YES;
    
    NSString *urlAddress = nil;

    if (self.forgotPassword) {
        urlAddress = @"http://wpforipad.com/forgotpassword.php";
    } else {
        urlAddress = @"http://preprod.web.frostcat.com/register";
    }
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [webView loadRequest:requestObj];
    
    [self.view addSubview:webView];
    
    [webView release];

    lotoolbar.translatesAutoresizingMaskIntoConstraints = NO;
    webView.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *views = @{@"t":lotoolbar, @"w":webView};
    NSArray *hToolbarConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[t]-0-|" options:0 metrics:nil views:views];
    NSArray *hWebViewConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[w]-0-|" options:0 metrics:nil views:views];
    NSArray *vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[t(41)]-0-[w]-0-|" options:0 metrics:nil views:views];
    [self.view addConstraints:hToolbarConstraints];
    [self.view addConstraints:hWebViewConstraints];
    [self.view addConstraints:vConstraints];
}

- (IBAction)registration:(id)sender
{
	
    self.errorMessage.text = @"";
    if([self isAnyFieldEmpty])
    {
        //UIAlertView *alert = [UIAlertView incompleteRegistrationFieldsAlert];
        //[alert show];
		
		// __REALWAT__ : BEGIN : 2013/Dec/10 : Change to contant string
        self.errorMessage.text = SPH_STR_REGISTER_FIELD_ALL;
		// __REALWAT__ : END
	
        return;
    }
    
    if(![self validateEmailWithString:self.emailTextField.text])
    {
        //UIAlertView *alert = [UIAlertView invalidRegistrationFieldsAlert];
        //[alert show];
		
		// __REALWAT__ : BEGIN : 2013/Dec/10 : Change to contant string
        self.errorMessage.text = SPH_STR_REGISTER_INVALID_EMAIL;
		// __REALWAT__ : END
	
        return;
    }
    
    RegistrationData *registrationData = [[RegistrationData alloc] init];
    registrationData.name = self.nameTextField.text;
    registrationData.occupation = self.occupationTextField.text;
    registrationData.country = self.countryTextField.text;
    registrationData.username = self.usernameTextField.text;
    registrationData.email = self.emailTextField.text;
    
    [self.registeringActivityIndicator startAnimating];
    self.registerButton.enabled = NO;
    
	// __REALWAT__ : BEGIN : 2013/Dec/10 : Fixed PTS:GC-19
	self.cancelButton.enabled = NO;
	
	[[Sphere3DAPI sharedInstance] registerUserWithRegistrationData:registrationData
                 completionBlock:^(RegistrationResponse code, NSError *error)
	{
		[self.registeringActivityIndicator stopAnimating];
		self.registerButton.enabled = YES;
		self.cancelButton.enabled   = YES;
		//NSLog(@"%d", code);
	switch (code)
		{
		case RegistrationResponseFailed:
			{
				self.errorMessage.text = SPH_STR_REGISTER_FAILED;
			}
			break;
		case RegistrationResponseFieldMissing:
			{
				self.errorMessage.text = SPH_STR_REGISTER_FIELD_ALL;
			}
			break;
		case RegistrationResponseSuccess:
			{
				[self showRegistrationCompleteView];
			}
			break;
		case RegistrationResponseEmailOrUsernameTaken:
			{
				self.errorMessage.text = SPH_STR_REGISTER_USER_EXIST;
			}
			break;
		case RegistrationResponseRegistrationsClosed:
			{
				self.errorMessage.text = SPH_STR_REGISTER_REGISTRATION_CLOSED;
			}
			break;
		default:
			self.errorMessage.text = SPH_STR_REGISTER_FAILED;
			break;
	}
	}];
	// __REALWAT__ : END

}

-(void)done

{
    
    //  self.errorMessage.text = @"";
    
    if (nil != self.delegate) {
        
        [self.delegate registrationViewControllerDidFinished];
        
    }
    
}
//TODO: TR/NM - test launch mail on iPad device
- (IBAction)goToMail:(id)sender {
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString:@"mailto:"]];
}

#pragma  mark - UITextFieldDelegate

//TODO: email field is not resigning keyboard with return key pressed
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.nameTextField) {
        [self.occupationTextField becomeFirstResponder];
    } else if (textField == self.occupationTextField) {
        [self.countryTextField becomeFirstResponder];
    } else if (textField == self.countryTextField) {
        [self.usernameTextField becomeFirstResponder];
    } else if (textField == self.usernameTextField) {
        [self.emailTextField becomeFirstResponder];
    } else {
       [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - Private

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)isAnyFieldEmpty
{
    return (0 == [self.nameTextField.text length] ||
            0 == [self.occupationTextField.text length] ||
            0 == [self.countryTextField.text length] ||
            0 == [self.usernameTextField.text length] ||
            0 == [self.emailTextField.text length]);
}
- (void)showRegistrationCompleteView {
    [UIView animateWithDuration:0.5f
                     animations:^{
                         self.confirmationView.frame = self.preregisterView.frame;
                         self.preregisterView.frame = CGRectMake(-self.view.frame.size.width, 0, self.preregisterView.frame.size.width, self.preregisterView.frame.size.height);
                     }
     ];
	
	SPH_InAppViewController *lpInAppView = [[[SPH_InAppViewController alloc] initWithNibName:@"SPH_InAppViewController" bundle:nil] autorelease];
	
	lpInAppView.modalPresentationStyle = UIModalPresentationFormSheet;
	
	[self presentViewController:lpInAppView animated:YES completion:nil];
}

@end
