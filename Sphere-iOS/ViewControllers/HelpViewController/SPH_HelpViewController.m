#import "SPH_HelpViewController.h"

#import "SPH_Constant.h"


#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])

@interface SPH_HelpViewController ()

@property (nonatomic, strong) IBOutlet UIWebView *helpView;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end

@implementation SPH_HelpViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _helpView.delegate = self;
    if(IS_IPAD)
    {
    NSURL *url = [NSURL URLWithString:SPH_HELP_URL_IPAD];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                             cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:10.0];
        [self.helpView loadRequest:request];

    }
    else
    {
        
        NSURL *url = [NSURL URLWithString:SPH_HELP_URL_IPHONE];
        NSURLRequest *request = [NSURLRequest requestWithURL:url
                                                 cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:10.0];
        [self.helpView loadRequest:request];
        
 
        
    }

    self.title = @"Quick Help Guide";
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    _activityView.hidden = YES;
}

- (void)dealloc {
    [_activityView release];
    [super dealloc];
}
@end
