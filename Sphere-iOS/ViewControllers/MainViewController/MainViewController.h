//
//  MainViewController.m
//
//  Created by Sphere 3D 2015
//  This module handles the GUI of Validating credentials, Connecting to cloud, and Ready to launch.
//  Animates the graphics according to the current stage of validation.
//  On successful user validation it loads the JSON data from the server.
//  Creates the Collection View of JSON apps.
//  Finally, its responsible for loading the RDP Session GUI.
//  Handles both iPhone and iPad screen sizes


#import "CustomViewController.h"
@interface AppsObject : NSObject
@property (nonatomic, strong) NSString *connectName;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *iconPath;
@property (nonatomic, strong) NSString *serverip;
@end

@interface MainViewController : CustomViewController

+ (instancetype)mainViewController;
-(void)restoreSavedSession;

@end
