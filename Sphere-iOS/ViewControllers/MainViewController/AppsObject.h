//
//  AppsObject.h
//  AppScreen
//
//  Created by Khaled Murshed on 2015-05-22.
//  Copyright (c) 2015 Sphere3D. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppsObject : NSObject
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *iconPath;


@end
