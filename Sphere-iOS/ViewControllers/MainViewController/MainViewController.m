//
//  MainViewController.m
//  IE on Demand
//
//  Created by Sphere 3D 2015
//  This module handles the GUI of Validating credentials, Connecting to cloud, and Ready to launch.
//  Animates the graphics according to the current stage of validation.
//  On successful user validation it loads the JSON data from the server.
//  Creates the Collection View of JSON apps.
//  Finally, its responsible for loading the RDP Session GUI.
//  Handles both iPhone and iPad screen sizes

#import "MainViewController.h"
#import "ProgressDotsView.h"
#import "LoadingView.h"
#import "SPH_ErrorCode.h"
#import "SPH_RememberUser.h"
#import "Utils.h"
#import "Sphere3DAPI.h"
#import "Bookmark.h"
#import "UserSession.h"
#import "RDPSession.h"
#import "AMTumblrHud.h"
#import "AlertViewController.h"
#import "LoginViewController.h"
#import "RDPSessionViewController.h"
#import "AppCells.h"
#import "UIDeviceHardware.h"
#import "SCLAlertView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Reachability.h"

#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])

static NSInteger RDCSESSION_VIEWCTRL_DELAY = 3.0f;

typedef NS_ENUM(NSInteger, LoadingStage) {
    LoadingStageValidatingCredentials,
    LoadingStageConnectingToCloud,
    LoadingStageReadyToLaunch
};
@implementation AppsObject
@synthesize connectName, displayName, iconPath, serverip;
@end
@import AssetsLibrary;

@interface MainViewController () <LoginViewControllerDelegate, SPH_RDPSessionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic, assign) BOOL hasRestoredSession;
@property (nonatomic, assign) LoadingStage currentLoadingStage;
@property (nonatomic, strong) NSTimer *sessionTimer2;
@property (strong, nonatomic) IBOutlet ProgressDotsView *leftProgressDotView;
@property (strong, nonatomic) IBOutlet ProgressDotsView *rightProgressDotView;
@property (strong, nonatomic) IBOutlet ProgressDotsView *topProgressDotView;
@property (strong, nonatomic) IBOutlet ProgressDotsView *bottomProgressDotView;

@property (strong, nonatomic) IBOutlet LoadingView *validatingCredView;
@property (strong, nonatomic) IBOutlet LoadingView *connectingToCloudView;
@property (strong, nonatomic) IBOutlet LoadingView *readyToLaunchView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *validatingViewVerticalConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *validatingViewHorizontalConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *readyToLaunchVerticalConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *readyToLaunchHorizontalConstraint;

@property (retain, nonatomic) IBOutlet NSLayoutConstraint *topDotViewVerticalConstraints;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *bottomDotViewVerticalConstrint;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *leftDotViewVerticalConstraints;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *rightDotViewVerticalConstrint;


@property (nonatomic, retain) RDPSessionViewController *sessionViewController;
@property (nonatomic) NSString *IpSuccess;
@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *password;
@property (nonatomic) BOOL sessionExpired;

@property (nonatomic, strong) AMTumblrHud *tumblrHUD;

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, retain) UICollectionView *appCollectionView;
@property (nonatomic, strong) NSMutableArray *arrApps;
@property (nonatomic, strong) UIButton *exitButton;
@property (nonatomic, strong) UIButton *MicButton;
@property (strong, nonatomic) UIView *InternetConnView;

@property (retain, nonatomic) ComputerBookmark *pB;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;
-(void)restoreSavedSession;
@end

@implementation MainViewController
UICollectionView *_collectionView;
UIActivityIndicatorView *spinner;
UIView *contentView;

+ (instancetype)mainViewController
{
    return [[[self alloc] init] autorelease];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hasRestoredSession = NO;
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
}

#pragma mark - View life cycle
-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* BackgroundImageSetting = [prefs objectForKey:@"BackgroundImageSetting"];
    
    if (BackgroundImageSetting != nil) {
        _backgroundImage.image = [UIImage imageNamed:BackgroundImageSetting];
        
    }
    [_backgroundImage setNeedsDisplay];
    
    
    if (BackgroundImageSetting == nil) {
        NSString* BackgroundCameraRollimage = [prefs objectForKey:@"BackgroundCameraRollimage"];
        
        if (BackgroundCameraRollimage != nil) {
            
            NSURL *url = [NSURL URLWithString:[BackgroundCameraRollimage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@"camrollurl %@",url);
            
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library assetForURL:url resultBlock:^(ALAsset *asset)
             {
                 UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0.5 orientation:UIImageOrientationUp];
                 
                 _backgroundImage.image = copyOfOriginalImage;
             }
                    failureBlock:^(NSError *error)
             {
                 // error handling
                 NSLog(@"failure-----");
             }];
            
        }
    }
}
-(BOOL) hasInternet
{
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [reach currentReachabilityStatus];
    if (internetStatus == NotReachable){
        //[self showNetworkErrorAlert];
        return NO;
    }
    return YES;
}

- (void)viewDidLoad
{
 
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    // Set default values for most NSUserDefaults
    [[NSUserDefaults standardUserDefaults] registerDefaults:
     [NSDictionary dictionaryWithContentsOfFile:
      [[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist"]]];
    
    // Set the accecpting certificate during making connection to the remote server without
    // to alert message to user.
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"security.accept_certificates"];
    
    // init global settings
    SetSwapMouseButtonsFlag([[NSUserDefaults standardUserDefaults] boolForKey:@"ui.swap_mouse_buttons"]);
    SetInvertScrollingFlag([[NSUserDefaults standardUserDefaults] boolForKey:@"ui.invert_scrolling"]);
    
    if (!self.hasRestoredSession)
    {
        self.hasRestoredSession = YES;
        cSPH_RememberUser *rememberedUser = [[[cSPH_RememberUser alloc] init] autorelease];
        [rememberedUser loadMememberMeInfo];
        BOOL rememberStatus = [rememberedUser.rememberStatus boolValue];
        
        if ([[Sphere3DAPI sharedInstance] hasSavedSession] && rememberStatus) {
            [self restoreSavedSession];
        } else {
            
            
            if ([self hasInternet])
            {
                  [self presentLoginViewController];
            }
            else
            {
                UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
                
                if ([[h platformString] isEqualToString:@"iPad Mini (GSM)"])
                {
                    self.InternetConnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 1024)];
                    self.InternetConnView.backgroundColor = [UIColor colorWithRed:83/255.0 green:128/255.0 blue:195/255.0 alpha:1];
                    self.InternetConnView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
                    
                    self.InternetConnView.center = CGPointMake(self.view.frame.size.width  / 2, self.view.frame.size.height / 2);
                    
                    UIImageView *imageview = [[UIImageView alloc]
                                              initWithFrame:CGRectMake(300, 300,80, 80)];
                    [imageview setImage:[UIImage imageNamed:@"modal_warning_icon.png"]];
                    [imageview setContentMode:UIViewContentModeScaleAspectFit];
                    imageview.center = CGPointMake(520, 300);
                    
                    //[imageview release];
                    
                    UILabel *AlertmessageTitle=[[UILabel alloc]initWithFrame:CGRectMake(300,300, 500,100)];//Set frame of label in your viewcontroller.
                    [AlertmessageTitle setText:@"Internet connection not available"];//Set text in label.
                    [AlertmessageTitle setTextColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1]];//Set text color in label.
                    [AlertmessageTitle setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
                    [AlertmessageTitle setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
                    [AlertmessageTitle setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
                    [AlertmessageTitle setFont:[UIFont fontWithName:@"Roboto-Bold" size:30]];
                    AlertmessageTitle.center = CGPointMake(520, 400);
                    
                    
                    UILabel *Alertmessage=[[UILabel alloc]initWithFrame:CGRectMake(300,300, 532,105)];//Set frame of label in your viewcontroller.
                    [Alertmessage setText:@"Please check your internet connection and try again."];//Set text in label.
                    [Alertmessage setTextColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1]];//Set text color in label.
                    [Alertmessage setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
                    [Alertmessage setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
                    [Alertmessage setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
                    [Alertmessage setFont:[UIFont fontWithName:@"Roboto" size:23]];
                    Alertmessage.center = CGPointMake(515, 480);
                    
                    // self.MessageLabel2.hidden = YES;
                    
                    UIButton *RequestButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    [RequestButton addTarget:self
                                      action:@selector(CheckInternetMethod)
                            forControlEvents:UIControlEventTouchUpInside];
                    [RequestButton setBackgroundColor:[UIColor colorWithRed:41/255.0 green:171/255.0 blue:226/255.0 alpha:1]];
                    [RequestButton setTitle:@"Try Again" forState:UIControlStateNormal];
                    RequestButton.frame = CGRectMake(300, 600, 330, 65);
                    RequestButton.showsTouchWhenHighlighted = YES;
                    RequestButton.center = CGPointMake(515, 600);
                    RequestButton.titleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:20];
                    
                    [self.InternetConnView addSubview:imageview];
                    [self.InternetConnView addSubview:AlertmessageTitle];
                    [self.InternetConnView addSubview:Alertmessage];
                    
                    [self.InternetConnView addSubview:RequestButton];
                    
                    [self.view addSubview: self.InternetConnView];
                    // [self.InternetConnView release];
                    
                }
                else{
            self.InternetConnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 1024)];
            self.InternetConnView.backgroundColor = [UIColor colorWithRed:83/255.0 green:128/255.0 blue:195/255.0 alpha:1];
            self.InternetConnView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
                
            self.InternetConnView.center = CGPointMake(self.view.frame.size.width  / 2, self.view.frame.size.height / 2);
              
                UIImageView *imageview = [[UIImageView alloc]
                                          initWithFrame:CGRectMake(300, 300,80, 80)];
                [imageview setImage:[UIImage imageNamed:@"modal_warning_icon.png"]];
                [imageview setContentMode:UIViewContentModeScaleAspectFit];
                imageview.center = CGPointMake(self.view.frame.size.width  / 2, 300);

                //[imageview release];
                
                UILabel *AlertmessageTitle=[[UILabel alloc]initWithFrame:CGRectMake(300,300, 500,100)];//Set frame of label in your viewcontroller.
                [AlertmessageTitle setText:@"Internet connection not available"];//Set text in label.
                [AlertmessageTitle setTextColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1]];//Set text color in label.
                [AlertmessageTitle setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
                [AlertmessageTitle setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
                [AlertmessageTitle setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
                [AlertmessageTitle setFont:[UIFont fontWithName:@"Roboto-Bold" size:30]];
                AlertmessageTitle.center = CGPointMake(self.view.frame.size.width  / 2, 400);

                
                UILabel *Alertmessage=[[UILabel alloc]initWithFrame:CGRectMake(300,300, 532,105)];//Set frame of label in your viewcontroller.
                [Alertmessage setText:@"Please check your internet connection and try again."];//Set text in label.
                [Alertmessage setTextColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1]];//Set text color in label.
                [Alertmessage setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
                [Alertmessage setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
                [Alertmessage setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
                [Alertmessage setFont:[UIFont fontWithName:@"Roboto" size:23]];
                Alertmessage.center = CGPointMake(self.view.frame.size.width  / 2, 480);
                
               // self.MessageLabel2.hidden = YES;
                
                UIButton *RequestButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [RequestButton addTarget:self
                                  action:@selector(CheckInternetMethod)
                        forControlEvents:UIControlEventTouchUpInside];
                [RequestButton setBackgroundColor:[UIColor colorWithRed:41/255.0 green:171/255.0 blue:226/255.0 alpha:1]];
                [RequestButton setTitle:@"Try Again" forState:UIControlStateNormal];
                RequestButton.frame = CGRectMake(300, 600, 330, 65);
                RequestButton.showsTouchWhenHighlighted = YES;
                RequestButton.center = CGPointMake(self.view.frame.size.width  / 2, 600);
                RequestButton.titleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:20];

                [self.InternetConnView addSubview:imageview];
                [self.InternetConnView addSubview:AlertmessageTitle];
                [self.InternetConnView addSubview:Alertmessage];

                [self.InternetConnView addSubview:RequestButton];

                [self.view addSubview: self.InternetConnView];
               // [self.InternetConnView release];
                }
            }
        }
    }
    
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    
    if ([[Def objectForKey:@"AutoRotationLock"] isEqualToString:@"unLocked"]){
        [self changeOrientation];
    }
    
    // Store network ping status in local file
    [self storeNetworkPingStatus];
}

-(void)CheckInternetMethod
{
    //[self.InternetConnView removeFromSuperview];

    if ([self hasInternet])
    {
        [self presentLoginViewController];
        [self.InternetConnView removeFromSuperview];
    }

 }
-(BOOL) shouldAutorotate
{
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    
    if ([[Def objectForKey:@"AutoRotationLock"] isEqualToString:@"Locked"]){
        return NO;
    }
    else{
        return YES;
    }
}



-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

-(void)changeOrientation
{
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"]){
    }
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"AutoRotationLock"]  isEqual: @"unLocked"]){
        
        switch ([UIDevice currentDevice].orientation) {
            case UIDeviceOrientationPortrait:
            case UIDeviceOrientationPortraitUpsideDown:
                
                if IS_IPHONE {
                    
                    self.validatingViewVerticalConstraint.constant = 60.0f;
                    self.validatingViewHorizontalConstraint.constant = 96.0f;
                    self.readyToLaunchVerticalConstraint.constant = 20.0f;
                    self.readyToLaunchHorizontalConstraint.constant = 90.0f;
                    
                    if ([[h platformString] isEqualToString:@"iPhone 7 Plus"])
                        {
                            self.validatingViewVerticalConstraint.constant = 60.0f;
                            self.validatingViewHorizontalConstraint.constant = 145.0f;
                            self.readyToLaunchVerticalConstraint.constant = 20.0f;
                            self.readyToLaunchHorizontalConstraint.constant = 145.0f;
                            self.topDotViewVerticalConstraints.constant = 210.0f;
                            self.bottomDotViewVerticalConstrint.constant = 200.0f;
 
                        }
                        if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"]){
                        self.topDotViewVerticalConstraints.constant = 160.0f;
                        self.bottomDotViewVerticalConstrint.constant = 128.0f;
                    }
                }
                
                else if IS_IPAD {
                    self.validatingViewVerticalConstraint.constant = 10.0f;
                    self.validatingViewHorizontalConstraint.constant = 269.0f;
                    self.readyToLaunchVerticalConstraint.constant = 45.0f;
                    self.readyToLaunchHorizontalConstraint.constant = 269.0f;
                }
                 self.leftProgressDotView.hidden = YES;
                self.rightProgressDotView.hidden = YES;
                self.topProgressDotView.hidden = NO;
                self.bottomProgressDotView.hidden = NO;
                break;
                
            case UIDeviceOrientationLandscapeLeft:
            case UIDeviceOrientationLandscapeRight:
                
                if IS_IPHONE {
                    if ([[h platformString] isEqualToString:@"iPhone 7 Plus"])
                    {
                    self.validatingViewVerticalConstraint.constant = 180.0f;
                    self.validatingViewHorizontalConstraint.constant = 0.0f;
                    self.readyToLaunchVerticalConstraint.constant = 145.0f;
                    self.readyToLaunchHorizontalConstraint.constant = 0.0f;
                    self.leftDotViewVerticalConstraints.constant = 210.0f;
                    self.rightDotViewVerticalConstrint.constant = 200.0f;
                    }
                    else{
                        self.validatingViewVerticalConstraint.constant = 135.0f;
                        self.validatingViewHorizontalConstraint.constant = 0.0f;
                        self.readyToLaunchVerticalConstraint.constant = 100.0f;
                        self.readyToLaunchHorizontalConstraint.constant = 0.0f;
                    }
                }
                else if IS_IPAD {
                    self.validatingViewVerticalConstraint.constant = 200.0f;
                    self.validatingViewHorizontalConstraint.constant = 45.0f;
                    self.readyToLaunchVerticalConstraint.constant = 244.0f;
                    self.readyToLaunchHorizontalConstraint.constant = 45.0f;
                }
                
                self.leftProgressDotView.hidden = NO;
                self.rightProgressDotView.hidden = NO;
                self.topProgressDotView.hidden = YES;
                self.bottomProgressDotView.hidden = YES;
                break;
                
            case UIDeviceOrientationFaceDown:
            case UIDeviceOrientationFaceUp:
            case UIDeviceOrientationUnknown:
                // do nothing
                break;
        }
    }
    else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"AutoRotationLock"]  isEqual: @"Locked"]){
        
        switch ([UIDevice currentDevice].orientation) {
            case UIDeviceOrientationPortrait:
            case UIDeviceOrientationPortraitUpsideDown:
                break;
                
            case UIDeviceOrientationLandscapeLeft:
            case UIDeviceOrientationLandscapeRight:
                break;
                
            case UIDeviceOrientationFaceDown:
            case UIDeviceOrientationFaceUp:
            case UIDeviceOrientationUnknown:
                // do nothing
                break;
        }
        [h release];

    }
    [h release];
}

#pragma mark - Custom Accessors

- (void)setCurrentLoadingStage:(LoadingStage)loadingStage
{
    _currentLoadingStage = loadingStage;
    [self updateLoadingViews];
}

- (void)updateLoadingViews
{
    NSString *currentStoryboard  =[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"];
    
    // this is to validate which storyboard to be used the Black one or the gray
    //You just have to change which storyboard to be used in the general settings
    if ([currentStoryboard isEqualToString:@"Storyboard"] || [currentStoryboard isEqualToString:@"StoryboardiPhone"]) {
        switch (self.currentLoadingStage) {
            case LoadingStageValidatingCredentials:
                
                [self.validatingCredView startAnimatingWithAnimationImages:[self validatingViewAnimationImages]];
                [self.validatingCredView setActive:YES];
                [self.connectingToCloudView reset];
                [self.readyToLaunchView reset];
                [self.connectingToCloudView setHidden:YES];
                [self.readyToLaunchView setHidden:YES];
                [self.validatingCredView setHidden:NO];
                break;
                
            case LoadingStageConnectingToCloud:
                [self.connectingToCloudView startAnimatingWithAnimationImages:[self connectingViewAnimationImages]];
                [self.connectingToCloudView setActive:YES];
                [self.validatingCredView setHidden:YES];
                [self.connectingToCloudView setHidden:NO];
                break;
                
            case LoadingStageReadyToLaunch:
                [self.connectingToCloudView setActive:YES];
                [self.readyToLaunchView startAnimatingWithAnimationImages:[self readyToLaunchViewAnimationImages]];
                [self.readyToLaunchView setActive:YES];
                [self.readyToLaunchView setHidden:NO];
                [self.connectingToCloudView setHidden:YES];
                break;
        }
        
    }
    else if ([currentStoryboard isEqualToString:@"StoryboardiPad"] || [currentStoryboard isEqualToString:@"StoryboardiPhone2"]) {
        switch (self.currentLoadingStage) {
                
            case LoadingStageValidatingCredentials:
                [self.leftProgressDotView stopAnimating];
                [self.topProgressDotView stopAnimating];
                [self.rightProgressDotView reset];
                [self.bottomProgressDotView reset];
                [self.validatingCredView startAnimatingWithAnimationImages:[self validatingViewAnimationImages]];
                [self.validatingCredView setActive:YES];
                [self.connectingToCloudView reset];
                [self.readyToLaunchView reset];
                break;
                
            case LoadingStageConnectingToCloud:
                [self.leftProgressDotView stopAnimating];
                [self.topProgressDotView stopAnimating];
                [self.validatingCredView stopAnimating];
                [self.rightProgressDotView startAnimating];
                [self.bottomProgressDotView startAnimating];
                [self.validatingCredView setActive:NO];
                [self.connectingToCloudView startAnimatingWithAnimationImages:[self connectingViewAnimationImages]];
                [self.connectingToCloudView setActive:YES];
                break;
                
            case LoadingStageReadyToLaunch:
                [self.rightProgressDotView stopAnimating];
                [self.bottomProgressDotView stopAnimating];
                [self.connectingToCloudView stopAnimating];
                [self.connectingToCloudView setActive:YES];
                [self.readyToLaunchView startAnimatingWithAnimationImages:[self readyToLaunchViewAnimationImages]];
                [self.readyToLaunchView setActive:YES];
                break;
        }
    }
}

- (NSArray*)validatingViewAnimationImages
{
    return [self imagesWithImageCount:19 namePattern:@"lock%02d"];
}

- (NSArray*)connectingViewAnimationImages
{
    return [self imagesWithImageCount:33 namePattern:@"cloud%02d"];
}

- (NSArray *)readyToLaunchViewAnimationImages
{
    return [self imagesWithImageCount:35 namePattern:@"check%02d"];
}

- (NSArray *)imagesWithImageCount:(NSInteger)imageCount namePattern:(NSString *)namePattern
{
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:imageCount];
    for (int i = 1; i <= imageCount; ++i) {
        NSString *imageName = [NSString stringWithFormat:namePattern, i];
        UIImage *image = [UIImage imageNamed:imageName];
        [images addObject:image];
    }
    return images;
}

-(void)JsonApplicationViewBeacon
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    NSString* SavedIpaddress = [prefs objectForKey:@"ipaddressaved"];
    NSString* SavedUsername = [prefs objectForKey:@"user1"];
    NSString* SavePassword = [prefs objectForKey:@"pass1"];
    NSString *StringtoURLfordata=[[NSString alloc]
                                  initWithFormat:@"http://beaconoip/client/validate.php?username=%@&password=%@",
                                  SavedUsername,SavePassword];
    NSString *encodedUrl = [StringtoURLfordata stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSURL *FinalUrlWithString = [[NSURL alloc] initWithString:encodedUrl];
    NSData *jsonData= [NSData dataWithContentsOfURL:FinalUrlWithString];
    //NSError *error = nil;
    // NSLog(@"data----%@",str);
    
    NSObject * object = [prefs objectForKey:@"applaunchermessage"];
    if(object != nil)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"applaunchermessage"];
    }
    //NSString *JsonDataBackFromUrl = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    // NSLog(@"data----%@",JsonDataBackFromUrl);
    
    if (jsonData !=(NSData *)nil){
        NSString *ReadyHack = @"readyhack";
        [prefs setObject:ReadyHack forKey:@"readyhack"];
        
        UICollectionViewFlowLayout *CollectionView=[[UICollectionViewFlowLayout alloc] init];
        _appCollectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:CollectionView];
        [_appCollectionView setDataSource:self];
        [_appCollectionView setDelegate:self];
        //[_appCollectionView setBackgroundColor:[UIColor lightGrayColor]];
        UIGraphicsBeginImageContext(self.view.frame.size);
        
            NSString* BackgroundImageSetting = [prefs objectForKey:@"BackgroundImageSetting"];
            
            if (BackgroundImageSetting != nil) {
                _appCollectionView.layer.contents = (id)[UIImage imageNamed:[[NSUserDefaults standardUserDefaults]  objectForKey:@"BackgroundImageSetting"]].CGImage;
                
            }
            [_backgroundImage setNeedsDisplay];
            
            
            if (BackgroundImageSetting == nil) {
                NSString* BackgroundCameraRollimage = [prefs objectForKey:@"BackgroundCameraRollimage"];
                
                if (BackgroundCameraRollimage != nil) {
                    
                    NSURL *url = [NSURL URLWithString:[BackgroundCameraRollimage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                    
                    NSLog(@"camrollurl %@",url);
                    
                    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                    [library assetForURL:url resultBlock:^(ALAsset *asset)
                     {
                         UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0.5 orientation:UIImageOrientationUp];
                         
                         _appCollectionView.layer.contents = (id) copyOfOriginalImage.CGImage;
                     }
                            failureBlock:^(NSError *error)
                     {
                         // error handling
                         NSLog(@"failure-----");
                     }];
                    
                }
            }
        
        UIGraphicsEndImageContext();
        _appCollectionView.alwaysBounceVertical = YES;
        [_appCollectionView setBounces:YES];
        
        // Register cell classes
        [_appCollectionView registerNib:[UINib nibWithNibName:@"AppCells" bundle:nil] forCellWithReuseIdentifier:@"cellIdentifier"];
        
        //TopBar
        CGRect frame = self.view.bounds;
        UIView *TopBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 44)];
        UIColor *TopbarBackgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"topbar"]];
        TopBarView.backgroundColor = TopbarBackgroundColor;
        
        [TopbarBackgroundColor release];
        
        [self.view addSubview:TopBarView];
        
        
        UILabel *AppTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(435, 9, 170, 50)];//Set frame of label in your viewcontroller.
        
        //[label setBackgroundColor:[UIColor blackColor]];//Set background color of label.
        //[label setText:@"Glassware 2.0"];//Set text in label.
        [AppTitleLabel setTextColor:[UIColor colorWithRed:(0/255.f) green:(81/255.f) blue:(146/255.f) alpha:1.0f]];
        [AppTitleLabel setBackgroundColor:[UIColor clearColor]];
        [AppTitleLabel setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
        
        if IS_IPHONE {
            AppTitleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:22.0];
            [AppTitleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
            [AppTitleLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
            [AppTitleLabel setNumberOfLines:1];//Set number of lines in label.
            [AppTitleLabel setText: @"GW Connect"];//Set text in label.
            CGPoint centerLabel = CGPointMake(TopBarView.bounds.size.width/2 +5,
                                              TopBarView.bounds.size.height/2 + 5) ;
            AppTitleLabel.center = centerLabel;
            AppTitleLabel.adjustsFontSizeToFitWidth = YES;
        }
        
        else if IS_IPAD {
            AppTitleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:22.0];
            [AppTitleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
            [AppTitleLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
            [AppTitleLabel setNumberOfLines:1];//Set number of lines in label.
            [AppTitleLabel setText: @"GW Connect"];//Set text in label.
            CGPoint centerLabel = CGPointMake(TopBarView.bounds.size.width/2 +12,
                                              TopBarView.bounds.size.height/2 + 5) ;
            AppTitleLabel.center = centerLabel;
            AppTitleLabel.adjustsFontSizeToFitWidth = YES;
        }
        
        [AppTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
        
        [AppTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
        
        [TopBarView addSubview:AppTitleLabel];
        
        //Exit button
        _exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_exitButton addTarget:self action:@selector(exitMethod)  forControlEvents:UIControlEventTouchUpInside];
        [_exitButton setImage:[UIImage imageNamed:@"icon_logout"] forState:UIControlStateNormal];
        [_exitButton setImage:[UIImage imageNamed:@"icon_logout_selected.png"] forState:UIControlStateHighlighted];
        _exitButton.frame = IS_IPAD ? CGRectMake(4, 0, 65, 55) :  CGRectMake(0, 15, 55, 45) ;
        [TopBarView addSubview:_exitButton];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
        NSString* ServerMic = [prefs objectForKey:@"ServerMicroEnableDisable"];
        
        if (NSNotFound != [ServerMic rangeOfString:@"rue"].location )
        {
            _MicButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [_MicButton addTarget:self action:@selector(MicOnOffMethod:)  forControlEvents:UIControlEventTouchUpInside];
            [_MicButton setImage:[UIImage imageNamed:@"MicrophoneON"] forState:UIControlStateNormal];
            _MicButton.frame =  IS_IPAD ? CGRectMake(70, 10, 29, 29): CGRectMake(60, 24, 25, 25);
            [TopBarView addSubview:_MicButton];
        }
        else
        {
            _MicButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [_MicButton addTarget:self action:@selector(MicOnOffMethod:)  forControlEvents:UIControlEventTouchUpInside];
            [_MicButton setImage:[UIImage imageNamed:@"MicrophoneOFF"] forState:UIControlStateNormal];
            _MicButton.frame = IS_IPAD ? CGRectMake(70, 10, 29, 29): CGRectMake(60, 24, 25, 25);
            [TopBarView addSubview:_MicButton];
        }
        
        [self.view addSubview:_appCollectionView];
        [self.view bringSubviewToFront:TopBarView];
        
        _appCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
        
        TopBarView.translatesAutoresizingMaskIntoConstraints = NO;
        _exitButton.translatesAutoresizingMaskIntoConstraints = NO;
        _MicButton.translatesAutoresizingMaskIntoConstraints = NO;
        AppTitleLabel.translatesAutoresizingMaskIntoConstraints= NO;
        
        [self.view addConstraints:@[
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1
                                                                  constant:0],
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeHeight
                                                                multiplier:1
                                                                  constant:44 + [self toolbarOffset]],
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeLeading
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view attribute:NSLayoutAttributeLeading
                                                                multiplier:1
                                                                  constant:0],
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeTrailing
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeTrailing
                                                                multiplier:1
                                                                  constant:0]
                                    ]];
        
        [self.view addConstraints:@[[NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeBottom
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0f
                                                                  constant:0.0f],
                                    [NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeLeading
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeLeading
                                                                multiplier:1.0f constant:0.0f],
                                    [NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeTrailing
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeTrailing
                                                                multiplier:1.0f constant:0.0f],
                                    [NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:TopBarView
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1.0f
                                                                  constant:0.0f]]];
        
        
        [TopBarView addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:_exitButton
                                                                  attribute:NSLayoutAttributeLeading
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeLeading
                                                                 multiplier:1
                                                                   constant:0],
                                     [NSLayoutConstraint constraintWithItem:_exitButton
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1 constant:0]]];
        
        [TopBarView addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeLeading
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:_exitButton
                                                                  attribute:NSLayoutAttributeTrailing
                                                                 multiplier:1
                                                                   constant:0],
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1
                                                                   constant:-10],
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute: NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1
                                                                   constant:29],
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute: NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1
                                                                   constant:29]]];
        
        [TopBarView addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:TopBarView
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:AppTitleLabel
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1
                                                                   constant:10],
                                     [NSLayoutConstraint constraintWithItem:AppTitleLabel
                                                                  attribute:NSLayoutAttributeCenterX
                                                                  relatedBy:0
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeCenterX
                                                                 multiplier:1
                                                                   constant:0]]];
        
        //Refresh Control Setup
        _refreshControl = [[UIRefreshControl alloc] init];
        //_refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
        
        
        
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];  //title text color :optionala
        
       // NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Pull to Refresh" attributes:attributes];
        
        _refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh" attributes:attributes];

        
        _refreshControl.tintColor = [UIColor whiteColor];
        [_refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
        [_appCollectionView addSubview:_refreshControl];
        _refreshControl.bounds = CGRectMake(_refreshControl.bounds.origin.x,
                                            -50,
                                            _refreshControl.bounds.size.width,
                                            _refreshControl.bounds.size.height);
        
        self.sessionTimer2 = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(refreshData33) userInfo: nil repeats:YES ];
        
        [self getJSONdata];
        
        NSString* SaveUserTypedIpaddress = [prefs objectForKey:@"ipaddressaved"];
        [self ipAddressSavingPlist: SaveUserTypedIpaddress];
    }
    else
    {
        NSString *AppStringDefault = @"empty";
        [self restoreSavedSession:AppStringDefault];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
        NSString* SavedUserEnteredIpaddress = [prefs objectForKey:@"ipaddressaved"];
        [self ipAddressSavingPlist: SavedUserEnteredIpaddress];
        NSString* AppLauncherMessage = @"applauncher";
        [prefs setObject:AppLauncherMessage forKey:@"applaunchermessage"];
        [prefs synchronize];
    }
}


-(void)JsonApplicationView
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    NSString* SavedIpaddress = [prefs objectForKey:@"ipaddressaved"];
    NSString* SavedUsername = [prefs objectForKey:@"user1"];
    NSString* SavePassword = [prefs objectForKey:@"pass1"];
    NSString *StringtoURLfordata=[[NSString alloc]
                                  initWithFormat:@"http://%@/client/validate.php?username=%@&password=%@",
                                  SavedIpaddress,SavedUsername,SavePassword];
    NSString *encodedUrl = [StringtoURLfordata stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSURL *FinalUrlWithString = [[NSURL alloc] initWithString:encodedUrl];
    NSData *jsonData= [NSData dataWithContentsOfURL:FinalUrlWithString];
    //NSError *error = nil;
    // NSLog(@"data----%@",str);
    
    NSObject * object = [prefs objectForKey:@"applaunchermessage"];
    if(object != nil)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"applaunchermessage"];
    }
    //NSString *JsonDataBackFromUrl = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    // NSLog(@"data----%@",JsonDataBackFromUrl);
    
    if (jsonData !=(NSData *)nil){
        NSString *ReadyHack = @"readyhack";
        [prefs setObject:ReadyHack forKey:@"readyhack"];
        
        UICollectionViewFlowLayout *CollectionView=[[UICollectionViewFlowLayout alloc] init];
        _appCollectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:CollectionView];
        [_appCollectionView setDataSource:self];
        [_appCollectionView setDelegate:self];
        //[_appCollectionView setBackgroundColor:[UIColor lightGrayColor]];
        UIGraphicsBeginImageContext(self.view.frame.size);
        NSString* BackgroundImageSetting = [prefs objectForKey:@"BackgroundImageSetting"];
        
        if (BackgroundImageSetting != nil) {
            _appCollectionView.layer.contents = (id)[UIImage imageNamed:[[NSUserDefaults standardUserDefaults]  objectForKey:@"BackgroundImageSetting"]].CGImage;
            
        }
        [_backgroundImage setNeedsDisplay];
        
        
        if (BackgroundImageSetting == nil) {
            NSString* BackgroundCameraRollimage = [prefs objectForKey:@"BackgroundCameraRollimage"];
            
            if (BackgroundCameraRollimage != nil) {
                
                NSURL *url = [NSURL URLWithString:[BackgroundCameraRollimage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                
                NSLog(@"camrollurl %@",url);
                
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                [library assetForURL:url resultBlock:^(ALAsset *asset)
                 {
                     UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0.5 orientation:UIImageOrientationUp];
                     
                     _appCollectionView.layer.contents = (id) copyOfOriginalImage.CGImage;
                 }
                        failureBlock:^(NSError *error)
                 {
                     // error handling
                     NSLog(@"failure-----");
                 }];
                
            }
        }
        
        UIGraphicsEndImageContext();
        _appCollectionView.alwaysBounceVertical = YES;
        [_appCollectionView setBounces:YES];
        
        // Register cell classes
        [_appCollectionView registerNib:[UINib nibWithNibName:@"AppCells" bundle:nil] forCellWithReuseIdentifier:@"cellIdentifier"];
        
        //TopBar
        CGRect frame = self.view.bounds;
        UIView *TopBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 44)];
        UIColor *TopbarBackgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"topbar"]];
        TopBarView.backgroundColor = TopbarBackgroundColor;
        
        [TopbarBackgroundColor release];
        
        [self.view addSubview:TopBarView];
        
        
        UILabel *AppTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(435, 9, 170, 50)];//Set frame of label in your viewcontroller.
        
        //[label setBackgroundColor:[UIColor blackColor]];//Set background color of label.
        //[label setText:@"Glassware 2.0"];//Set text in label.
        [AppTitleLabel setTextColor:[UIColor colorWithRed:(0/255.f) green:(81/255.f) blue:(146/255.f) alpha:1.0f]];
        [AppTitleLabel setBackgroundColor:[UIColor clearColor]];
        [AppTitleLabel setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
        
        if IS_IPHONE {
            AppTitleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:16.0f];
            [AppTitleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
            [AppTitleLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
            [AppTitleLabel setNumberOfLines:1];//Set number of lines in label.
            [AppTitleLabel setText: @"GW Connect"];//Set text in label.
            CGPoint centerLabel = CGPointMake(TopBarView.bounds.size.width/2 +5,
                                              TopBarView.bounds.size.height/2 + 5) ;
            AppTitleLabel.center = centerLabel;
            AppTitleLabel.adjustsFontSizeToFitWidth = YES;
        }
        
        else if IS_IPAD {
            AppTitleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:19.0f];
            [AppTitleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
            [AppTitleLabel setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
            [AppTitleLabel setNumberOfLines:1];//Set number of lines in label.
            [AppTitleLabel setText: @"GW Connect"];//Set text in label.
            CGPoint centerLabel = CGPointMake(TopBarView.bounds.size.width/2 +12,
                                              TopBarView.bounds.size.height/2 + 5) ;
            AppTitleLabel.center = centerLabel;
            AppTitleLabel.adjustsFontSizeToFitWidth = YES;
        }
        
        [AppTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
        
        [AppTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
        
        [TopBarView addSubview:AppTitleLabel];
        
        //Exit button
        _exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_exitButton addTarget:self action:@selector(exitMethod)  forControlEvents:UIControlEventTouchUpInside];
        [_exitButton setImage:[UIImage imageNamed:@"icon_logout"] forState:UIControlStateNormal];
        [_exitButton setImage:[UIImage imageNamed:@"icon_logout_selected.png"] forState:UIControlStateHighlighted];
        _exitButton.frame = IS_IPAD ? CGRectMake(4, 0, 65, 55) :  CGRectMake(0, 15, 55, 45) ;
        [TopBarView addSubview:_exitButton];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
        NSString* ServerMic = [prefs objectForKey:@"ServerMicroEnableDisable"];
        
        if (NSNotFound != [ServerMic rangeOfString:@"rue"].location )
        {
            _MicButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [_MicButton addTarget:self action:@selector(MicOnOffMethod:)  forControlEvents:UIControlEventTouchUpInside];
            [_MicButton setImage:[UIImage imageNamed:@"MicrophoneON"] forState:UIControlStateNormal];
            _MicButton.frame =  IS_IPAD ? CGRectMake(70, 10, 29, 29): CGRectMake(60, 24, 25, 25);
            [TopBarView addSubview:_MicButton];
        }
        else
        {
            _MicButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [_MicButton addTarget:self action:@selector(MicOnOffMethod:)  forControlEvents:UIControlEventTouchUpInside];
            [_MicButton setImage:[UIImage imageNamed:@"MicrophoneOFF"] forState:UIControlStateNormal];
            _MicButton.frame = IS_IPAD ? CGRectMake(70, 10, 29, 29): CGRectMake(60, 24, 25, 25);
            [TopBarView addSubview:_MicButton];
        }
        
        [self.view addSubview:_appCollectionView];
        [self.view bringSubviewToFront:TopBarView];
        
        _appCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
        
        TopBarView.translatesAutoresizingMaskIntoConstraints = NO;
        _exitButton.translatesAutoresizingMaskIntoConstraints = NO;
        _MicButton.translatesAutoresizingMaskIntoConstraints = NO;
        AppTitleLabel.translatesAutoresizingMaskIntoConstraints= NO;
        
        [self.view addConstraints:@[
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1
                                                                  constant:0],
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeHeight
                                                                multiplier:1
                                                                  constant:44 + [self toolbarOffset]],
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeLeading
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view attribute:NSLayoutAttributeLeading
                                                                multiplier:1
                                                                  constant:0],
                                    [NSLayoutConstraint constraintWithItem:TopBarView
                                                                 attribute:NSLayoutAttributeTrailing
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeTrailing
                                                                multiplier:1
                                                                  constant:0]
                                    ]];
        
        [self.view addConstraints:@[[NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeBottom
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0f
                                                                  constant:0.0f],
                                    [NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeLeading
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeLeading
                                                                multiplier:1.0f constant:0.0f],
                                    [NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeTrailing
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeTrailing
                                                                multiplier:1.0f constant:0.0f],
                                    [NSLayoutConstraint constraintWithItem:_appCollectionView
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:TopBarView
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1.0f
                                                                  constant:0.0f]]];
        
        
        [TopBarView addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:_exitButton
                                                                  attribute:NSLayoutAttributeLeading
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeLeading
                                                                 multiplier:1
                                                                   constant:0],
                                     [NSLayoutConstraint constraintWithItem:_exitButton
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1 constant:0]]];
        
        [TopBarView addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeLeading
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:_exitButton
                                                                  attribute:NSLayoutAttributeTrailing
                                                                 multiplier:1
                                                                   constant:0],
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1
                                                                   constant:-10],
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute: NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1
                                                                   constant:29],
                                     [NSLayoutConstraint constraintWithItem:_MicButton
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute: NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1
                                                                   constant:29]]];
        
        [TopBarView addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:TopBarView
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:AppTitleLabel
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1
                                                                   constant:10],
                                     [NSLayoutConstraint constraintWithItem:AppTitleLabel
                                                                  attribute:NSLayoutAttributeCenterX
                                                                  relatedBy:0
                                                                     toItem:TopBarView
                                                                  attribute:NSLayoutAttributeCenterX
                                                                 multiplier:1
                                                                   constant:0]]];
        
        //Refresh Control Setup
        _refreshControl = [[UIRefreshControl alloc] init];
        //_refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
        
        
        
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];  //title text color :optionala
        
        // NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Pull to Refresh" attributes:attributes];
        
        _refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh" attributes:attributes];
        
        _refreshControl.tintColor = [UIColor whiteColor];
        [_refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
        [_appCollectionView addSubview:_refreshControl];
        _refreshControl.bounds = CGRectMake(_refreshControl.bounds.origin.x,
                                            -50,
                                            _refreshControl.bounds.size.width,
                                            _refreshControl.bounds.size.height);
        
        self.sessionTimer2 = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(refreshData33) userInfo: nil repeats:YES ];
        
        [self getJSONdata];
        
        NSString* SaveUserTypedIpaddress = [prefs objectForKey:@"ipaddressaved"];
        [self ipAddressSavingPlist: SaveUserTypedIpaddress];
    }
    else
    {
        NSString *AppStringDefault = @"empty";
        [self restoreSavedSession:AppStringDefault];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
        NSString* SavedUserEnteredIpaddress = [prefs objectForKey:@"ipaddressaved"];
        [self ipAddressSavingPlist: SavedUserEnteredIpaddress];
        NSString* AppLauncherMessage = @"applauncher";
        [prefs setObject:AppLauncherMessage forKey:@"applaunchermessage"];
        [prefs synchronize];
    }
}

- (CGFloat)toolbarOffset {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        return 20;
    } else {
        return 0;
    }
}

-(void)refershControlAction
{
    // NSLog(@"refreshed");
    [self getJSONdata];
    [_appCollectionView reloadData];
    [_refreshControl endRefreshing];
    [self.tumblrHUD showAnimated:NO];
}

-(void) ShowCustomAlert
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                   message:@"Microphone is only compatible for G-series Servers with microphone feature enabled. Would you like to disable/enable the microphone?"
                                                  delegate:self
                                         cancelButtonTitle:@"Enable"
                                         otherButtonTitles:@"Disable", nil];
    
    
    [alert show];
    [alert release];
    
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
        NSString *ServerMicroEnableDisable = @"True";  //or false depending on the app/and whats comming backfrom tcp stream.
        [prefs setObject:ServerMicroEnableDisable forKey:@"ServerMicroEnableDisable"];
        [prefs synchronize];
        [_MicButton setImage:[UIImage imageNamed:@"MicrophoneON.png"] forState:UIControlStateNormal];
        [_MicButton setSelected:YES];
        
        NSLog(@"Mic Enabled");
    } else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
        NSString *ServerMicroEnableDisable = @"False";  //or false depending on the app/and whats comming backfrom tcp stream.
        [prefs setObject:ServerMicroEnableDisable forKey:@"ServerMicroEnableDisable"];
        [prefs synchronize];
        
        [_MicButton setImage:[UIImage imageNamed:@"MicrophoneOFF.png"] forState:UIControlStateNormal];
        [_MicButton setSelected:NO];
        
        NSLog(@"Mic Disabled");
    }
}

-(IBAction) MicOnOffMethod:(UIButton *)sender
{
    [self ShowCustomAlert];
    NSLog([_MicButton isSelected] ? @"Yes" : @"No");
}

-(void) exitMethod
{
    [_appCollectionView removeFromSuperview];
  // [self presentLoginViewController];
    [self stopSessionTimer];
}

-(void) getJSONdata
{
    
    _arrApps = [[NSMutableArray array] init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    NSString* SavedIpaddress = [prefs objectForKey:@"ipaddressaved"];
    NSString* SavedUsername = [prefs objectForKey:@"user1"];
    NSString* SavedPassword = [prefs objectForKey:@"pass1"];
    NSString *UrlEnteredforJson=[[NSString alloc]initWithFormat:@"http://%@/client/validate.php?username=%@&password=%@", SavedIpaddress,SavedUsername,SavedPassword];
    NSString *encodedUrl = [UrlEnteredforJson stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSURL *UrlCreated = [[NSURL alloc] initWithString:encodedUrl];
    NSData *jsonData= [NSData dataWithContentsOfURL:UrlCreated];
    NSError *error = nil;
    // NSLog(@"data----%@",str);
    
    if(jsonData != nil){
        NSDictionary *dataDictionary= [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        if (error != nil) {
            UIAlertView *AlerForNoJsonData = [[UIAlertView alloc] initWithTitle:@"Error loading data from the server" message:[NSString stringWithFormat:@"Error in loading data, error desc.: %@", error] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [AlerForNoJsonData show];
            [AlerForNoJsonData release];
        }
        else {
            [self loadTheApps:dataDictionary];
        }
    }
}

- (void) loadTheApps:(NSDictionary*)jsonData
{
    NSDictionary *JsonDataDictionary = [jsonData objectForKey:@"apps"];
    for (NSDictionary * dataDict in JsonDataDictionary)
    {
        AppsObject *Apps = [[AppsObject alloc] init];
        Apps.connectName = [dataDict objectForKey:@"connectname"];
        Apps.displayName = [dataDict objectForKey:@"displayname"];
        Apps.iconPath = [dataDict objectForKey:@"iconpath"];
        Apps.serverip = [dataDict objectForKey:@"serverip"];
        //NSLog(@"serverip: %@", apps.serverip);
        
        [_arrApps addObject:Apps];
        [_arrApps retain];
    }
}

-(void)refreshData33
{
    CGRect frame = self.view.bounds;
    self.tumblrHUD = [[AMTumblrHud alloc] initWithFrame:CGRectMake(frame.size.width-80, 30, 40, 20)];
    self.tumblrHUD.hudColor = [UIColor colorWithRed:(9/255.f) green:(82/255.f) blue:(144/255.f) alpha:1.0f];
    [self.view addSubview:self.tumblrHUD];
    [self.tumblrHUD showAnimated:YES];
    // NSLog(@"refresh json timer");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self refershControlAction];
    });
}

- (void)stopSessionTimer
{
    // NSLog(@"stop jsontimeer");
    [self.sessionTimer2 invalidate];
    self.sessionTimer2 = nil;
}

-(void)hithit
{
    [contentView removeFromSuperview];
    [self restoreSavedSession];
}

- (void)createRDPSettionViewControllerWithBookmark:(ComputerBookmark*)bookmark
{
    if (bookmark == nil) {
        return;
    }
    // create rdp session
    RDPSession *session = [[[RDPSession alloc] initWithBookmark:bookmark] autorelease];
    // Create the RDP session view controller from XIB file.
    self.sessionViewController = [[[RDPSessionViewController alloc]
                                   initWithNibName:@"RDPSessionView"
                                   bundle:nil
                                   session:session] autorelease];
    self.sessionViewController.delegate = self;
    [self.sessionViewController setHidesBottomBarWhenPushed:YES];
    [self.sessionViewController loadView];
    self.sessionViewController.view.frame = self.view.bounds;
    [self.sessionViewController connectSession];
    self.sessionExpired = NO;
}

- (void)restoreSavedSession:(NSString*) selectedAppName {
    //NSLog(@"this is hit vergood");
    // Set Validating credential animation
    self.currentLoadingStage = LoadingStageValidatingCredentials;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    NSString* SavedUsername = [prefs objectForKey:@"user1"];
    NSString* SavedPassword = [prefs objectForKey:@"pass1"];
    
    // Restore saved session.
    [[Sphere3DAPI sharedInstance] loginWithUsername:SavedUsername
                                        andPassword:SavedPassword
                                withCompletionBlock:^(ComputerBookmark *pBookmark,
                                                      NSError *error)
     {
         if (pBookmark == nil)
         {
             [self.sessionViewController disconnectSession];
         }
         else
         {
             [self createRDPSettionViewControllerWithBookmark:pBookmark];
         }
     } withReadyToLaunchRemoteBlock:^{
         [self readyToLaunchRemoteViewControler];
     } andSelectedAppName:selectedAppName];
}

- (void)presentLoginViewController {
    // Set Validating credential animation
    self.currentLoadingStage = LoadingStageValidatingCredentials;
    LoginViewController *LoginController = [LoginViewController loginViewController];
    LoginController.delegate = self;
    [self presentViewController:LoginController animated:NO completion:nil];
}

- (void)readyToLaunchRemoteViewControler
{
    NSAssert(self.sessionViewController, @"sessionViewController is nil");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(RDCSESSION_VIEWCTRL_DELAY * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showSessionViewCtrl];
    });
}

#pragma mark - LoginViewControllerDelegate
- (void)loginViewController:(id)controller
          loginWithUsername:(NSString*)username
                   password:(NSString*)password
                  HasBeacon:(BOOL) HasBeacon
{
    // Set restored session status to be YES.
    self.hasRestoredSession = YES;
    
    // Dismiss the login view controller.
    [self dismissViewControllerAnimated:NO completion:nil];
    
    // Make login request to the server.
    [[Sphere3DAPI sharedInstance] loginWithUsername:username
                                        andPassword:password
                                withCompletionBlock:^(ComputerBookmark *pBookmark,
                                                      NSError *error)
     {
         NSDictionary * myDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"dictionaryKey"];
         NSString * ClientMode = [myDictionary objectForKey:@"clientmode"];
         NSString * Status = [myDictionary objectForKey:@"status"];
         if([ClientMode intValue] == 1)
         {
             if([Status intValue] == 1)
             {
                 NSLog(@"Success loigin to app screen");
                 
             }
             else{
                 //error message.. if status is 0 {"clientmode":"1","status":0,"message":"Invalid username and\/or password."}
                 NSString * MessageFromJson = [myDictionary objectForKey:@"message"];
                 // NSLog(@"Received: %@", MessageFromJson);
             }
         }
         else
         {
             // use the app app launcher  brandon isnt done this yet...{"clientmode":"0","status":1,"message":"","launchapp":"AppLauncher"}
             if([Status intValue] == 1)
             {
                 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                 NSString *RdpInt = @"1";
                 [prefs setObject:RdpInt forKey:@"rdpint"];
                 [prefs synchronize];
                 
                 NSString * LaunchAppString = [myDictionary objectForKey:@"launchapp"];
                 //NSLog(@"app: %@", launchappstring);
                 [prefs setObject:LaunchAppString forKey:@"applaunch"];
                 [prefs synchronize];
             }
             else{}
         }
         if (error) {
             if ( SPH_ERROR_INVALID_USER == error.code )
             {
                 // Relogin
               
             }
             else if ( SPH_ERROR_INTERNET_CONN == error.code )
             {
                 [self showNetworkErrorAlert];
             }
             else if ( SPH_ERROR_APP_LAUNCH == error.code )
             {
                 NSString * MessageFromJson = [myDictionary objectForKey:@"message"];
                 [self showApplicationLaunchAlertWithMessage:MessageFromJson];
             }
             return;
         }
         // No error
         //[self createRDPSettionViewControllerWithBookmark:pBookmark];
         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
         
         NSString* RdpInt = [prefs objectForKey:@"rdpint"];
         if([RdpInt intValue] == 1 || HasBeacon)
         {
             NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
             NSString* AppLaunch = [prefs objectForKey:@"Beaconappstring2"];
            
             NSString *StartRangeHack = @"StartHack";
             [prefs setObject:StartRangeHack forKey:@"StartRangeHack"];
             [prefs synchronize];
             
             [self restoreSavedSession:AppLaunch];
         }
         else{
             
             NSString* AppLaunch = @"empty";
            
             [self restoreSavedSession:AppLaunch];
         }
     } withReadyToLaunchRemoteBlock:^{
         [self readyToLaunchRemoteViewControler];
     } andSelectedAppName:nil];
}

- (void)ipAddressSavingPlist:(NSString*)ipData {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ipAddress.plist"]; //3
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path]) //4
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"ipAddress" ofType:@"plist"]; //5
        
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    NSMutableArray *arrData = [[NSMutableArray alloc] initWithContentsOfFile: path];
    //here add elements to data file and write data to file
    NSMutableArray *arrChkData = [[NSMutableArray alloc] initWithContentsOfFile:path];
    if (![arrChkData containsObject:ipData]) {
        [arrData addObject:ipData];
        
        [arrData writeToFile: path atomically:YES];
    }
    if (arrData == nil || [arrData count] == 0) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ipaddresshack"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *IpaddressHack = @"iphack";
        [prefs setObject:IpaddressHack forKey:@"ipaddresshack"];
        [prefs synchronize];
    }
}

#pragma mark - Session handling

- (void)sessionDidFail
{
    [self showNetworkErrorAlert];
}

- (void)sessionDidDisconnect
{
    if (!self.sessionExpired) {
        [self clearObjects];
    }
    
    UserSession *lpUserSession = [UserSession loadUserSession];
    // Remove the user session info from local storage.
    [lpUserSession removeUserSession];
    
    if (self.sessionExpired) {
        [[Sphere3DAPI sharedInstance] logout];
        [self showSessionTimeoutAlert];
    }
}

- (void)sessionFailedToConnect:(NSNotification*)notification
{
    [self showNetworkErrorAlert];
}

- (void)sessionDidExpire
{
    self.sessionExpired = YES;
}

- (void)sessionWillConnect
{
    //Load the Connection To Cloud animation.
    self.currentLoadingStage = LoadingStageConnectingToCloud;
}

- (void)sessionDidConnect
{
    // Load the Ready To Launch animation
    self.currentLoadingStage = LoadingStageReadyToLaunch;
}

- (void)clearObjects
{
    self.hasRestoredSession = NO;
    // Release username
    self.username = nil;
    self.password = nil;
    
    // Remove the RDP session view controller from root.
    if (self.sessionViewController) {
        [self dismissViewControllerAnimated:NO completion:nil];
        self.sessionViewController = nil;
        self.sessionExpired = NO;
    }
}

- (void)showSessionViewCtrl
{
    [self presentViewController:self.sessionViewController animated:NO completion:nil];
    
    NSString *object123 = @"text";
    NSDictionary *userInfo =
    [NSDictionary dictionaryWithObject:object123 forKey:@"KeyForReady"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"KeyForReady123" object:nil userInfo:userInfo];
}

#pragma mark - AlertViewControllerDelegate

- (void)clickedOkAlertType:(AlertType)alertType
{
    switch (alertType) {
        case AlertTypeSessionTimeout:
            [self clearObjects];
           // [self presentLoginViewController];
            break;
        default:
            self.hasRestoredSession = YES;
           // [self presentLoginViewController];
    }
}

- (void)storeNetworkPingStatus
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:SPH_SETTING_NETWORK_PING_STATUS];
}
#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_arrApps count];
}

- (AppCells *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AppCells *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    // Fix for null IP address for images path
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* SavedIpAddress = [prefs objectForKey:@"ipaddressaved"];
    AppsObject *appsData = _arrApps[indexPath.row];
    
    cell.lblApp.text = appsData.displayName;
    cell.lblIpAddress.text = appsData.serverip;
    
    NSURL *IconUrl = [NSURL URLWithString:appsData.iconPath];
    if([appsData.iconPath rangeOfString:SavedIpAddress].location == NSNotFound)
    {
        NSString *assignIP = [NSString stringWithFormat:@"http://%@/appicons/glassware.png", SavedIpAddress];
        IconUrl = [NSURL URLWithString:assignIP];
    }
    
    NSData *Data = [NSData dataWithContentsOfURL:IconUrl];
    UIImage *Image = [UIImage imageWithData:Data];
    cell.imgApp.image = Image;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [_exitButton removeFromSuperview];
    [_MicButton removeFromSuperview];
    
    AppCells *cell = (AppCells*)[collectionView cellForItemAtIndexPath:indexPath];
    NSArray *views = [cell.contentView subviews];
    //UILabel *label = [views objectAtIndex:1];
    UILabel *lblIpAddress = [views objectAtIndex:2];
    
    AppsObject *appsData = _arrApps[indexPath.row];
    
    //NSLog(@"Select %@",label.text);
    //restoreSavedSession
    NSString *AppName = appsData.connectName;//label.text;
    NSString *IpaddressJsonSet = lblIpAddress.text;
    
    // NSLog(@"Select %@",AppName);
    // NSLog(@"IpAddress: %@", lblIpAddress.text);
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *AppNameUserDefault = AppName;
    [prefs setObject:AppNameUserDefault forKey:@"appname123"];
    [prefs setObject:IpaddressJsonSet forKey:@"ipaddressjson"];
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [_appCollectionView removeFromSuperview];
    [self restoreSavedSession:AppName];
    [self stopSessionTimer];
}

// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    //set color with animation
    [UIView animateWithDuration:0.0
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         [cell setBackgroundColor:[UIColor grayColor]];
                     }
                     completion:nil];
}

- (void)collectionView:(UICollectionView *)colView  didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    //set color with animation
    [UIView animateWithDuration:0.0
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         [cell setBackgroundColor:[UIColor clearColor]];
                     }
                     completion:nil ];
}
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(127, 127);
}

-(UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(110, 20, 10, 20);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 45.0;
}

- (void)dealloc
{
    [_arrApps release];
    [_appCollectionView release];
    [_bottomDotViewVerticalConstrint release];
    [_topDotViewVerticalConstraints release];
    [_backgroundImage release];
    [super dealloc];
}

@end
