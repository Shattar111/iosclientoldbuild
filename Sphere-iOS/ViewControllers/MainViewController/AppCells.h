//
//  AppCells.h
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2015-05-25.
//
//

#import <UIKit/UIKit.h>

@interface AppCells : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgApp;
@property (strong, nonatomic) IBOutlet UILabel *lblApp;
@property (retain, nonatomic) IBOutlet UILabel *lblIpAddress;
@end
