//
//  SettingsViewController
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  SettingsViewController allows users to modify setting of the app.

//  Things you do in SettingsViewController:
//
//  Show App version
//  View Help guid
//  Change session duration
//  show/hide keyboard tool bar
//  Change size of the mouse pointer


#import <UIKit/UIKit.h>

//for S3MousePointerSize
#import "TouchPointerView.h"

@class UserSettings;
@protocol SettingsViewControllerDelegate;

@interface SettingsViewController : UIViewController

@property (nonatomic, assign) id<SettingsViewControllerDelegate, NSObject> delegate;
@property (nonatomic, retain) UserSettings *userSettings;

+ (instancetype)settingsViewControllerWithUserSettings:(UserSettings *)userSettings;

@end

@protocol SettingsViewControllerDelegate <NSObject>
@required

- (void)settingsViewController:(SettingsViewController*)controller didUpdateSessionDuration:(NSInteger)duration;
- (void)settingsViewController:(SettingsViewController*)controller didUpdateMousePointerSize:(S3MousePointerSize)value;
- (void)settingsViewController:(SettingsViewController*)controller didUpdateKeyboardFunctionEnableSwitch:(BOOL)isOn;
- (void)settingsViewController:(SettingsViewController*)controller didUpdateShowKeyboardToolbar:(BOOL)isOn;

- (void)settingsViewControllerShowHelp:(SettingsViewController*)controller;
- (void)settingsViewControllerShowAccountInfo:(SettingsViewController*)controller;
- (void)settingsViewControllerSetupDropbox:(SettingsViewController*)controller;


@end
