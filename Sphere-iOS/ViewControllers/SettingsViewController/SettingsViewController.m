//
//  SettingsViewController.m
//  IE on Demand
//
//  Created by Xtreme Labs on 2013-10-15.
//
//

#import "SettingsViewController.h"
#import "UserSettings.h"

#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])
//@implementation UINavigationController (Disable_Rotation_iPhone)
//
//- (BOOL)shouldAutorotate {
//        return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations {
//        return UIInterfaceOrientationMaskPortrait;
//}
//// pre-iOS 6 support
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
//        return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
//}
//@end
@interface SettingsViewController ()

@property (nonatomic, assign) IBOutlet UILabel *versionLabel;
//@property (nonatomic, assign) IBOutlet UISlider *sessionDurationSlider;
@property (nonatomic, assign) IBOutlet UISwitch *keyboardToolbarSwitch;
@property (nonatomic, assign) IBOutlet UISwitch *keyboardFunctionEnableSwitch;

@property (nonatomic, assign) IBOutlet UISegmentedControl *mousePointerSizeSegmentedControl;

@end

@implementation SettingsViewController

+ (instancetype)settingsViewControllerWithUserSettings:(UserSettings *)userSettings {
    SettingsViewController *controller = [[[self alloc] init] autorelease];
    controller.userSettings = userSettings;
    return controller;
}

#pragma mark - View life cycle
- (id)init
{
    if (IS_IPAD)
        self = [super initWithNibName:@"SettingsViewController" bundle:nil];
    else
        self = [super initWithNibName:@"SettingsViewControlleriPhone" bundle:nil];
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithTitle:@"Done" style: UIBarButtonItemStyleDone
                                             target:self action:@selector(dismissMyView)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topbar"] forBarMetrics:UIBarMetricsDefault];
    
}
- (void)dismissMyView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupSettingsControlsValues];
    [self tweakViewForIOS7AndUp];
    [self colorSetup];
    
}

- (void) colorSetup {
    
    self.keyboardFunctionEnableSwitch.onTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_blue1"]];
    self.keyboardToolbarSwitch.onTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_blue1"]];
    self.mousePointerSizeSegmentedControl.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_blue1"]];
    
}
- (void)setupSettingsControlsValues {
    self.versionLabel.text = [self appVersion];
    //self.sessionDurationSlider.value = [self.userSettings.sessionDuration intValue];
    self.keyboardToolbarSwitch.on = [self.userSettings.keyboardFunctionKeys boolValue];
    self.keyboardFunctionEnableSwitch.on = [self.userSettings.keyboardFunctionEnableSwitch boolValue];

    self.mousePointerSizeSegmentedControl.selectedSegmentIndex = [self.userSettings.mousePointerSize intValue];
}

- (void)tweakViewForIOS7AndUp {
    if (7.0 <= [[UIDevice currentDevice].systemVersion floatValue]) {
        CGRect newFrame = self.keyboardToolbarSwitch.frame;
        CGRect newFrame1 = self.keyboardFunctionEnableSwitch.frame;

        newFrame.origin.x = 239;
        newFrame1.origin.x = 239;

        self.keyboardToolbarSwitch.frame = newFrame;
        self.keyboardFunctionEnableSwitch.frame = newFrame1;

    }
}

#pragma mark - IBActions

//- (IBAction)updateSessionDuration:(UISlider*)sender {
//    [self.delegate settingsViewController:self didUpdateSessionDuration:(int)sender.value];
//}

-(IBAction)updateMousePointerSize:(UISegmentedControl *)control {
    [self.delegate settingsViewController:self didUpdateMousePointerSize:(S3MousePointerSize)control.selectedSegmentIndex];
}

-(IBAction)updateKeyboardToolbar:(UISwitch *)sender{
    [self.delegate settingsViewController:self didUpdateShowKeyboardToolbar:[sender isOn]];
}
-(IBAction)updatekeyboardFunctionEnableSwitch:(UISwitch *)sender{
    [self.delegate settingsViewController:self didUpdateKeyboardFunctionEnableSwitch:[sender isOn]];
}
- (IBAction)setupDropbox {
    [self.delegate settingsViewControllerSetupDropbox:self];
}

- (IBAction)showHelp
{
    [self.delegate settingsViewControllerShowHelp:self];
    //[self dismissMyView];
}

- (IBAction)showAccountInfo {
    [self.delegate settingsViewControllerShowAccountInfo:self];
}

#pragma mark - Private

- (NSString*)appVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

- (NSString *)buildNumber {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

- (NSString *)versionBuild {
    return [self buildNumber];
}

@end
