//
//  SPH_InAppViewController.h
//  inappdlg
//
//  Created by Soknyra Te on 12/12/13.
//  Copyright (c) 2013 Realwat Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPH_PagingScrollView.h"
#import "SPH_InAppPurchase.h"
#import "SPH_InAppPurchaseView.h"

@interface SPH_InAppViewController : UIViewController <SPH_InAppPurchaseDelegate,
SPH_InAppPurchaseViewDelegate,
SPH_PagingScrollViewDelege>
{
	// VANYUTH : BEGIN : 28/01/2014
    SPH_InAppPurchase *m_inAppPurchase;
    // VANYUTH : END : 28/01/2014
	
	SPH_PurchaseInfo  *m_currentPurchaseInfo;
}

@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnRestore;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (retain, nonatomic) IBOutlet SPH_PagingScrollView *m_pagingScrollView;

@end
