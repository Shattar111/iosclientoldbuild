//
//  SPH_InAppViewController.m
//  inappdlg
//
//  Created by Soknyra Te on 12/12/13.
//  Copyright (c) 2013 Realwat Inc. All rights reserved.
//

#import "SPH_InAppViewController.h"
#import "SPH_Constant.h"

@interface SPH_InAppViewController ()

@end

@implementation SPH_InAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)onClickedRestore:(id)sender
{
	UIAlertView *lpRestoreAlert =
	   [[UIAlertView alloc] initWithTitle :@"Restore Purchases"
	           message:@"Download and install all of your\nprevious purchases."
	           delegate:self cancelButtonTitle:@"Cancel"
	           otherButtonTitles:@"Restore", nil];
	
    lpRestoreAlert.tag = 33;
    
    [lpRestoreAlert show];
    
    [lpRestoreAlert release];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if((33 == alertView.tag) && (1== buttonIndex))
    {
	
		[alertView dismissWithClickedButtonIndex:1 animated:YES];
	
		if ( nil == m_currentPurchaseInfo )
		{
			SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
			return;
		}
	
		[self startIndicatorAnimation];
	
		[self startPurchase:m_currentPurchaseInfo];
	
    }
	else if ( 34 == alertView.tag )
	{
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	m_currentPurchaseInfo        = nil;
	_m_pagingScrollView.delegate = self;
	
	[self stopIndicatorAnimation];
	
	[self createPurchaseViews];
	
}

-(tSPH_ErrorCode) currentView:(UIView *)pCurrentView
{
	tSPH_ErrorCode lReturn = SPH_ERROR;
	
	if ( nil == pCurrentView )
	{
		SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
		return (SPH_ERROR_PARAMETER);
	}
	
	if ( nil != m_currentPurchaseInfo )
	{
		[m_currentPurchaseInfo release];
		m_currentPurchaseInfo = nil;
	}
	
	m_currentPurchaseInfo = [SPH_PurchaseInfo alloc];
	
	if ( TRUE == [pCurrentView isKindOfClass:[SPH_InAppPurchaseView class]])
	{
		SPH_InAppPurchaseView *lpView = (SPH_InAppPurchaseView*)pCurrentView;
	
	
		lReturn = [lpView getPurchaseInfo:&m_currentPurchaseInfo];
		if ( SPH_SUCCESS != lReturn )
		{
			SPH_LOG_CONSOL(lReturn);
			return (lReturn);
		}
	}
	
	return (SPH_SUCCESS);
}

-(tSPH_ErrorCode) createPurchaseViews
{
	if ( nil == _m_pagingScrollView )
	{
		SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
		return (SPH_ERROR_OBJECT_NULL);
	}
	
	for ( int i=0; i < 3; i++ )
	{
		SPH_InAppPurchaseView *lpView = [SPH_InAppPurchaseView getInstanceView];
	
		if ( nil == lpView )
		{
			SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
			return (SPH_ERROR_OBJECT_NULL);
		}
	
		NSString *lpTitle       = @"";
		NSString *lpDescription = @"";
		NSString *lpPrice       = @"";
		NSString *lpImageName   = @"";
		NSString *lpProductId   = @"";
		NSString *lpBGColor     = @"";
	    tSPH_PurchaseType lPurchaseType = SPH_PURCHASE_TYPE_DEMO;
	
		if ( 0 == i )
		{
			lpTitle       = SPH_PURCHASE_TITLE_DEMO;
			lpDescription = SPH_PURCHASE_DESC_DEMO;
			lpPrice       = SPH_PURCHASE_PRICE_DEMO;
			lpImageName   = SPH_PURCHASE_IMG_DEMO;
			lpBGColor     = SPH_PURCAHSE_BGCORLOR_DEMO;
			lPurchaseType = SPH_PURCHASE_TYPE_DEMO;
			lpProductId   = SPH_PURCHASE_PRODUCT_ID_DEMO;
			
		}
		else if ( 1 == i )
		{
			lpTitle       = SPH_PURCHASE_TITLE_MONTHLY;
			lpDescription = SPH_PURCHASE_DESC_MONTHLY;
			lpPrice       = SPH_PURCHASE_PRICE_MONTHLY;
			lpImageName   = SPH_PURCHASE_IMG_MONTHLY;
			lpBGColor     = SPH_PURCAHSE_BGCORLOR_MONTHLY;
			lPurchaseType = SPH_PURCHASE_TYPE_MONTHLY;
			lpProductId   = SPH_PURCHASE_PRODUCT_ID_MONTHLY;
		}
		else
		{
			lpTitle       = SPH_PURCHASE_TITLE_YEARLY;
			lpDescription = SPH_PURCHASE_DESC_YEARLY;
			lpPrice       = SPH_PURCHASE_PRICE_YEARLY;
			lpImageName   = SPH_PURCHASE_IMG_YEARLY;
			lpBGColor     = SPH_PURCAHSE_BGCORLOR_YEARLY;
			lPurchaseType = SPH_PURCHASE_TYPE_YEARLY;
			lpProductId   = SPH_PURCHASE_PRODUCT_ID_YEARLY;
		}
	
		UIGraphicsBeginImageContext(self.view.frame.size);
		[[UIImage imageNamed:lpBGColor] drawInRect:self.view.bounds];
		UIImage *lpImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
	
		SPH_PurchaseInfo *lpPurchaseInfo = [[SPH_PurchaseInfo alloc] init];
	
		lpPurchaseInfo.title        = lpTitle;
	
		lpPurchaseInfo.description  = lpDescription;
	
		lpPurchaseInfo.price        = lpPrice;
	
		lpPurchaseInfo.purchaseType = lPurchaseType;
	
		lpPurchaseInfo.imageName    = lpImageName;
	
		lpPurchaseInfo.productId    = lpProductId;
	
		[lpView setPurchaseInfo:lpPurchaseInfo];
	
		lpView.delegate = self;
	
	    lpView.backgroundColor = [UIColor colorWithPatternImage:lpImage];
	
		[_m_pagingScrollView addSubViewOfScrollView:lpView];
	
		[lpPurchaseInfo release];
	}
	
	return (SPH_SUCCESS);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onClickedBtnDone:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}
- (tSPH_ErrorCode) startPurchase:(SPH_PurchaseInfo*)pPurchaseInfo
{
	
	if ( nil == pPurchaseInfo )
	{
		return (SPH_ERROR_PARAMETER);
	}
	// VANYUTH : BEGIN : 28/01/2014
    if (nil != m_inAppPurchase)
    {
        [m_inAppPurchase release];
        m_inAppPurchase = nil;
    }
    // Allocate m_inAppPurchase object
	m_inAppPurchase = [SPH_InAppPurchase alloc];
    // Verify m_inAppPurchase object
	if ( nil == m_inAppPurchase )
	{
		SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
		return ( SPH_ERROR_OBJECT_NULL );
	}
	// Verify _m_pagingScrollView object
	if ( nil == _indicatorView )
	{
		SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
		return ( SPH_ERROR_OBJECT_NULL );
	}
	// Assign selt to delegate of m_inAppPurchase
	m_inAppPurchase.delegate = self;
	// Start loading animation
	[self startIndicatorAnimation];
	// Start purchase with product id
	[m_inAppPurchase startPurchase:pPurchaseInfo.productId];
    // VANYUTH : END : 28/01/2014
	
	return (SPH_SUCCESS);
}

- (int) onPurchaseFailed
{
	if ( nil == _indicatorView )
	{
		return (0);
	}
	
	[self stopIndicatorAnimation];
	
	UIAlertView *lpFailureAlert = [[UIAlertView alloc]
								   initWithTitle :@"In-App-Purchase Error:"
								   
								   message: @"There was an error "
								   "purchasing this item please try again."
								   
								   delegate : self cancelButtonTitle:@"OK"
								   otherButtonTitles:nil];
	[lpFailureAlert show];
	[lpFailureAlert release];
	
	return (0);
}

- (int) onPurchaseCompleted
{
	if ( nil == _indicatorView )
	{
		return (0);
	}
	
	[self stopIndicatorAnimation];
	
	UIAlertView *lpCompletedAlert = [[UIAlertView alloc]
								   initWithTitle :@"In-App-Purchase"
								   
								   message: @"Subscription is successfully."
								   
								   delegate : self cancelButtonTitle:@"OK"
								   otherButtonTitles:nil];
	
	lpCompletedAlert.tag = 34;
	
	[lpCompletedAlert show];
	[lpCompletedAlert release];
	
	return (0);
}

- (int) onPurchasing
{
	return (0);
}

- (int) onPurchaseNoProductAvailable
{
	if ( nil == _indicatorView )
	{
		return (0);
	}
	
	[self stopIndicatorAnimation];
	
	UIAlertView *lpFailureAlert = [[UIAlertView alloc]
								   initWithTitle :@"No Product:"
								   
								   message: @"No Product Available."
								   
								   delegate : self cancelButtonTitle:@"OK"
								   otherButtonTitles:nil];
	[lpFailureAlert show];
	[lpFailureAlert release];
	
	return (0);
}

-(tSPH_ErrorCode) startIndicatorAnimation
{
	
	if ( nil == _indicatorView )
	{
		SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
		return (SPH_ERROR_OBJECT_NULL);
	}
	
	[_indicatorView startAnimating];
	
	[self.view bringSubviewToFront:_indicatorView];
	
	self.view.userInteractionEnabled = FALSE;
	
	return (SPH_SUCCESS);
}

-(tSPH_ErrorCode) stopIndicatorAnimation
{
	
	if ( nil == _indicatorView )
	{
		SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
		return (SPH_ERROR_OBJECT_NULL);
	}
	
	[_indicatorView stopAnimating];
	
	[self.view sendSubviewToBack:_indicatorView];
	
	self.view.userInteractionEnabled = TRUE;
	
	return (SPH_SUCCESS);
}

- (void)dealloc {
	if ( nil != _m_pagingScrollView )
	{
		[_m_pagingScrollView release];
		_m_pagingScrollView = nil;
	}
    // VANYUTH : BEGIN : 28/01/2014
    // Verify m_inAppPurchase not equal nill, release it.
    if (nil != m_inAppPurchase)
    {
        [m_inAppPurchase release];
        m_inAppPurchase = nil;
    }
    // VANYUTH : END : 28/01/2014
	
	[_indicatorView release];
	[_btnRestore release];
	[super dealloc];
}
@end
