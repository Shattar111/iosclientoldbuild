//
//  BouncingBallsView.m
//  Gravity
//
//  Created by Khaled Murshed on 2014-11-21.
//  Copyright (c) 2014 Sphere3D. All rights reserved.
//

#import "BouncingBallsView.h"

@interface BouncingBallsView(){
    
    UIDynamicAnimator *animator;
    
    IBOutlet UIView *OrangeBall;
    IBOutlet UIView *purpleBall;
    IBOutlet UIView *blueBall;
    IBOutlet UIView *grayBall;
}
@end
@implementation BouncingBallsView




-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if(self){
        
        //1. load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"BouncingBallsView" owner:self options:nil];
        
        //2. Add a s subview
        
        [self addSubview:self.view];
        
        
    }
    return self;
}

-(void)awakeFromNib{
    
        
    OrangeBall.layer.cornerRadius = 20.0;
    
    purpleBall.layer.cornerRadius = 20.0;
    
    blueBall.layer.cornerRadius = 20.0;
    
    grayBall.layer.cornerRadius = 20.0;
    
    // Initialize the animator.
    animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[OrangeBall, purpleBall, blueBall, grayBall]];
    
    [ animator addBehavior:gravityBehavior];
    
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[OrangeBall, purpleBall, blueBall, grayBall]];
    
    
    [collisionBehavior addBoundaryWithIdentifier:@"Top"
                                       fromPoint:CGPointMake(0, 0) toPoint:CGPointMake(300, 0)];
    
    [collisionBehavior addBoundaryWithIdentifier:@"Bottom"
                                       fromPoint:CGPointMake(0, 200) toPoint:CGPointMake(300, 200)];
    
    UIDynamicItemBehavior *ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[OrangeBall,purpleBall, blueBall, grayBall]];
    
    ballBehavior.elasticity = 1.0;
    ballBehavior.resistance = 0.0;
    
    
    [ animator addBehavior:ballBehavior];
    [animator addBehavior:collisionBehavior];
}


@end
