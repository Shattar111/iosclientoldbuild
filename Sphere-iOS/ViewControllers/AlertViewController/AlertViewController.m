//
//  AlertViewController.m
//
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  This is the Alert View Conroller, which is used/called in every part of the Glassware Connect.
//  AlertViewController contains the Delegates/Actions of its Buttons

//  The AlertViewController called from :
//
//  MainViewController.m
//  RDPSessionViewController
//  CustomViewController

#import "AlertViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])
typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);

@interface AlertViewController ()

@property (nonatomic, strong) IBOutlet UIImageView *warningImageView;
@property (nonatomic, strong) IBOutlet UILabel *headerLabel;
@property (nonatomic, strong) IBOutlet UILabel *subLabel;

@property (nonatomic, strong) IBOutlet UIButton *okButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;

@property (retain, nonatomic) IBOutlet UIButton *exitButton;

@property (nonatomic, retain) UIImage *customAlertImage;

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *message;

@property (nonatomic) BOOL hidesCancelButton;
@property (nonatomic) BOOL requiresLeftTextAlignment;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;
@end
@import AssetsLibrary;

@implementation AlertViewController

+ (instancetype)alertViewControllerWithTitle:(NSString*)title alertType:(tSPH_AlertType)type message:(NSString*)message {
    
    AlertViewController *controller;
    
    if (IS_IPHONE)
        controller = [[[self alloc] initWithNibName:@"AlertViewControlleriPhone" bundle:nil] autorelease];
    else
        controller = [[[self alloc] init] autorelease];
 
    controller.title = title;
	controller.message = message;
	
    if (type == SPH_ALERT_BTN_TYPE_OK_CANCEL) {
        controller.hidesCancelButton = NO;
    } else {
        controller.hidesCancelButton = YES;
    }
	
    return controller;
}

#pragma mark - View life cycle
-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* BackgroundImageSetting = [prefs objectForKey:@"BackgroundImageSetting"];
    
    if (BackgroundImageSetting != nil) {
        _backgroundImage.image = [UIImage imageNamed:BackgroundImageSetting];
        
    }
    [_backgroundImage setNeedsDisplay];
    
    
    if (BackgroundImageSetting == nil) {
        NSString* BackgroundCameraRollimage = [prefs objectForKey:@"BackgroundCameraRollimage"];
        
        if (BackgroundCameraRollimage != nil) {
            
            NSURL *url = [NSURL URLWithString:[BackgroundCameraRollimage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@"camrollurl %@",url);
            
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library assetForURL:url resultBlock:^(ALAsset *asset)
             {
                 UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0.5 orientation:UIImageOrientationUp];
                 
                 _backgroundImage.image = copyOfOriginalImage;
             }
                    failureBlock:^(NSError *error)
             {
                 // error handling
                 NSLog(@"failure-----");
             }];
            
        }
    }
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerLabel.text = self.title;
    self.subLabel.text = self.message;
    self.cancelButton.hidden = self.hidesCancelButton;
    if (self.customAlertImage) {
        self.warningImageView.image = self.customAlertImage;
    }
    if (self.requiresLeftTextAlignment) {
        self.subLabel.textAlignment = NSTextAlignmentLeft;
    }
    
    [self ShowHideExitBtn];
}

- (void)setCustomImage:(UIImage *)image {
    self.customAlertImage = image;
    if ([self isViewLoaded]) {
        self.warningImageView.image = image;
    }
}

- (void)setLeftTextAlignment {
    self.requiresLeftTextAlignment = YES;
    if ([self isViewLoaded]) {
        self.subLabel.textAlignment = NSTextAlignmentLeft;
    }
}


- (void) ShowHideExitBtn {
    NSDictionary * myDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"dictionaryKey"];
    
    NSString * clientmode = [myDictionary objectForKey:@"clientmode"];
    if ([clientmode isEqualToString:@"1"] && !self.cancelButton.hidden){
        [_exitButton setHidden:NO];
       
    }
    else{
            [_exitButton setHidden:YES];
        CGRect newFrame = _cancelButton.frame;
        newFrame.origin.y -= 60;
        _cancelButton.frame = newFrame;
    }
    
    
}

    //lock rotation on the iPhone only
- (BOOL)shouldAutorotate {
    if IS_IPHONE
        return NO;
    else
        return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    if IS_IPHONE
    return UIInterfaceOrientationMaskPortrait;
    else
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape;
}

#pragma mark - IBActions

- (IBAction)okClicked {
    [self.delegate alertViewControllerOnClickedOk:self];
}

- (IBAction)cancelClicked {
    [self.delegate alertViewControllerOnClickedCancel:self];
}

- (IBAction)exitClicked:(id)sender {
    [self.delegate alertViewControllerOnClickedExit:self];

}

- (void)dealloc {
    [_exitButton release];
    [_backgroundImage release];
    [super dealloc];
}
@end
