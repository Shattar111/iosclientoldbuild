//
//  AlertViewController.m
//
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  This is the Alert View Conroller, which is used/called in every part of the Glassware Connect.
//  AlertViewController contains the Delegates/Actions of its Buttons

//  The AlertViewController called from :
//
//  MainViewController.m
//  RDPSessionViewController
//  CustomViewController


#import <UIKit/UIKit.h>

#import "SPH_ErrorCode.h"

typedef enum eSPH_AlertType
{
	SPH_ALERT_BTN_TYPE_OK_ONLY = 0,
	SPH_ALERT_BTN_TYPE_OK_CANCEL
	
}tSPH_AlertType;

@protocol AlertViewControllerDelegate;

@interface AlertViewController : UIViewController

@property (nonatomic, strong) id<AlertViewControllerDelegate, NSObject> delegate;

+ (instancetype)alertViewControllerWithTitle:(NSString*)title alertType:(tSPH_AlertType)type message:(NSString*)message;

- (void)setCustomImage:(UIImage *)image;
- (void)setLeftTextAlignment;

@end

@protocol AlertViewControllerDelegate <NSObject>

@required
- (void) alertViewControllerOnClickedOk:(AlertViewController*)controller;
@optional
- (void) alertViewControllerOnClickedCancel:(AlertViewController*)controller;
- (void) alertViewControllerOnClickedExit:(AlertViewController*)controller;

@end
