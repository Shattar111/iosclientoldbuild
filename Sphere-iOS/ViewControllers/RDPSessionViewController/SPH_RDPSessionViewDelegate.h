//
//  SPH_RDPSessionViewDelegate.h
//  FreeRDP
//
//  Created by Soknyra Te on 11/19/13.
//
//

#import <Foundation/Foundation.h>

@protocol SPH_RDPSessionViewDelegate <NSObject>

@optional
- (void)sessionDidFail;
- (void)sessionWillConnect;
- (void)sessionDidConnect;
- (void)sessionWillDisconnect;
- (void)sessionDidDisconnect;
- (void)sessionDidExpire;

@end
