/*
 RDP Session View Controller
 
 Copyright 2013 Thincast Technologies GmbH, Author: Martin Fleisz
 
 This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <math.h>
#import <QuartzCore/QuartzCore.h>
#import "RDPSessionViewController.h"
#import "MyApplication.h"
#import "RDPKeyboard.h"
#import "Utils.h"
#import "Toast+UIView.h"
#import "ConnectionParams.h"
#import "BlockAlertView.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "SPH_HelpViewController.h"
#import "AccountInfoController.h"
#import "Reachability.h"
#import "SPH_Constant.h"
#import "SPH_RememberUser.h"
#import <MobileCoreServices/MobileCoreServices.h>
//#import "SimplePingHelper.h"
#import "Sphere3DAPI.h"
#import "LNNumberpad.h"
#import "LoginViewController.h"
#import "JFMinimalNotification.h"
#import "UIDeviceHardware.h"

#define TOOLBAR_HEIGHT 44
int i = 0;
#define isIOS6 [[[UIDevice currentDevice] systemVersion] floatValue] <= 6.2
#define isIOS8 [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
#define isIOS9 [[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0

#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])
#define UUIDstring @"B9407F30-F5F8-466E-AFF9-25556B57FE6D"
#define iDentifier @"GlasswareApp"
#define bMinor 16515
#define bMajor 48365

BOOL urlink;
int urlpass = 0;
@interface RDPSessionViewController () <UIPopoverControllerDelegate, JFMinimalNotificationDelegate>
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
}
@property (nonatomic, retain) UIPopoverController *shownPopover;
@property (nonatomic) BOOL setHideToolBar;
@property (nonatomic) BOOL logoutStatus;
@property (nonatomic) BOOL disconnectStatus;
@property (nonatomic) BOOL keyboardVisible;
@property (nonatomic) BOOL hasBluetoothKeyboard;
@property (nonatomic) BOOL bluetoothKeyboardTest;
@property (retain, nonatomic) IBOutlet UIScrollView *sessionScrollView;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomSpace;
@property (retain, nonatomic) IBOutlet RDPSessionView *sessionView;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *sessionViewHeight;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *sessionViewWidth;
@property (retain, nonatomic) IBOutlet TouchPointerView *touchPointerView;
@property (retain, nonatomic) IBOutlet UIView *sessionToolbar;
@property (nonatomic) BOOL sessionToolbarVisible;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *sessionToolbarTopSpace;
@property (retain, nonatomic) IBOutlet UITextField *dummyTextField;
@property (retain, nonatomic) IBOutlet UITapGestureRecognizer *singleTapRecognizer;
@property (retain, nonatomic) IBOutlet UITapGestureRecognizer *doubleTapRecognizer;
@property (retain, nonatomic) IBOutlet UITapGestureRecognizer *twoFingersTapRecognizer;
@property (retain, nonatomic) IBOutlet UIPanGestureRecognizer *dragNoDelayRecognizer;
@property (retain, nonatomic) IBOutlet UILongPressGestureRecognizer *dragWithDelayRecognizer;
@property (retain, nonatomic) IBOutlet UILongPressGestureRecognizer *twoFingersLongPressRecognizer;
@property (retain, nonatomic) IBOutlet UIButton *btnSettings;
@property (retain, nonatomic) IBOutlet UIButton *btnMouse;
@property (nonatomic) CGPoint twoFingersLongPressPosition;
@property (retain, nonatomic) RDPSession *session;
@property (nonatomic) BOOL sessionInitialized;
@property (nonatomic) BOOL toggleMouseButtons;
@property (nonatomic, retain) NSTimer *mouseMoveEventTimer;
@property (nonatomic) NSInteger mouseMoveEventsSkipped;
//Hack for issue #4
@property (nonatomic) NSUInteger pasteImageCount;
@property (nonatomic, strong) JFMinimalNotification* minimalNotification;
@property (nonatomic, retain) UINavigationController *iPhoneSettngNavContoller;
@property (nonatomic, strong) CLBeacon         *beacon;
@property (nonatomic, strong) ESTBeaconManager  *beaconManager;
@property (nonatomic, strong) CLBeaconRegion   *beaconRegion;

@end

@implementation RDPSessionViewController

#pragma mark class methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil session:(RDPSession *)session
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.session = session;
        self.session.delegate = self;
        self.sessionInitialized = NO;
        self.sessionToolbarVisible = NO;
        self.toggleMouseButtons = NO;
    }
    return self;
}
#pragma mark - View life cycle
- (id)initWithBeacon:(CLBeacon *)beacon
{
    self = [super init];
    if (self)
    {
        self.beacon = beacon;
    }
    return self;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [self.beaconManager stopRangingBeaconsInRegion:self.beaconRegion];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StartRanging"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StartRangeHack"];

    [super viewDidDisappear:animated];
}
-(void) mySelector:(id)elem
{
 //   [self loggedIn];
}
- (void)beaconManager:(id)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    CLBeacon *firstBeacon = [beacons firstObject];
    
    NSLog(@"UUID %@", firstBeacon);
    if(firstBeacon == nil)
    {
        i++;
    }
    if(i > 4)
    {
        NSLog(@"beaconisnull");
        i = 0;
        [self.beaconManager stopRangingBeaconsInRegion:self.beaconRegion];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Application Connectivity" message:@"Not in beacon range do you wish to exit GW Connect?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        // optional - add more buttons:
        alert.tag = 22;
        [alert addButtonWithTitle:@"Ok"];
        [alert show];
        [alert release];
        
    }
    
}


-(void)EnableBeaconScanning
{
    self.beaconManager = [[ESTBeaconManager alloc] init];
    self.beaconManager.delegate = self;
    
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc]
                                                                       initWithUUIDString:UUIDstring]
                                                                major:bMajor
                                                                minor:bMinor
                                                           identifier:@"GlasswareApp"];
    
    [self.beaconManager startRangingBeaconsInRegion:self.beaconRegion];
  
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StartRanging"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StartRangeHack"];


}
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
  
    
  //[self performSelector:@selector(starttcpstream) withObject:self afterDelay:3 ];

    [self performSelector:@selector(keyboardhackforportrait) withObject:self afterDelay:5.5];

    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    if ([[Def objectForKey:@"StartRanging"] isEqualToString:@"RangeOn"]){
        
        if ([[Def objectForKey:@"StartRangeHack"] isEqualToString:@"StartHack"]){

        [self performSelector:@selector(EnableBeaconScanning) withObject:self afterDelay:6];
        
        }
    }
    
    [super loadView];
    // init keyboard handling vars
    self.setHideToolBar = NO;
    self.keyboardVisible = NO;
    self.disconnectStatus = NO;
    self.logoutStatus = NO;
    [self loadSettings];
    
    self.keyboardToolbarView = [KeyboardToolbarView keyboardToolbar];
    self.keyboardToolbarView.hidden = YES;
    self.keyboardToolbarView.delegate = self;
    
    if ([self.userSettings.keyboardFunctionKeys boolValue])
    {
        [self.view addSubview:self.keyboardToolbarView];
    }
    
    if ([self.userSettings.keyboardFunctionEnableSwitch boolValue])
    {
        self.btnAltKey.hidden = NO;
        self.btnShiftKey.hidden = NO;
        self.btnCtrlKey.hidden = NO;
    }
    else
    {
        self.btnAltKey.hidden = YES;
        self.btnShiftKey.hidden = YES;
        self.btnCtrlKey.hidden = YES;
    }
    // init gesture recognizers
    [self configureGestureRecognizers];
    self.sessionScrollView.scrollsToTop = NO;
    self.sessionScrollView.bounces = NO;
    self.sessionToolbarTopSpace.constant = -TOOLBAR_HEIGHT;
    
    //ScrollView zoom is not working correctly with autolayout
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0) {
        //remove RDPSessionView-related constraints
        self.sessionView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.sessionView removeFromSuperview];
        [self.sessionScrollView addSubview:self.sessionView];
        self.sessionScrollView.contentSize = self.sessionView.bounds.size;
    }
    //Initialize sideBarMenu
    [self SideBarFunctionsInit];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ipaddressjson"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"readyhack"];
    
    //         _minimalNotification = [JFMinimalNotification notificationWithStyle:JFMinimalNotificationStyleDefault
    //                                                                          title:@"This is my awesome title"
    //                                                                       subTitle:@"This is my awesome sub-title"];
    //        UIFont* titleFont = [UIFont fontWithName:@"STHeitiK-Light" size:22];
    //        [_minimalNotification setTitleFont:titleFont];
    //        UIFont* subTitleFont = [UIFont fontWithName:@"STHeitiK-Light" size:16];
    //        [_minimalNotification setSubTitleFont:subTitleFont];
    
  //  self.minimalNotification = [JFMinimalNotification notificationWithStyle:JFMinimalNotificationStyleWarning title:@"App Error" subTitle:@"Unable to pull the full listing of applications, the App Launcher was loaded." dismissalDelay:4.0 touchHandler:^{
     //   [self.minimalNotification dismiss];
   // }];
   // _minimalNotification.delegate = self;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSObject * Object = [prefs objectForKey:@"applaunchermessage"];
    if(Object != nil)
    {
        [self performSelector:@selector(ShowAlertForAppLaucher) withObject:self afterDelay:8.0];
    }
    else
    {
    }
    if (IS_IPHONE)
    {
        UITapGestureRecognizer *SwipeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
        SwipeGesture.numberOfTapsRequired = 3;
        [self.view addGestureRecognizer:SwipeGesture];
        [SwipeGesture release];
    }
    
    NSString* SavedUsernameFromLogin = [prefs objectForKey:@"user1"];
    NSString* SavePasswordFromLogin = [prefs objectForKey:@"pass1"];
    
    if ([SavedUsernameFromLogin rangeOfString:@"-"].location == NSNotFound) {
    } else {
        NSString* replaceduser2 = [SavedUsernameFromLogin stringByReplacingOccurrencesOfString:@"-" withString:@" "];
         NSLog(@"string space3----%@",replaceduser2);

        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:replaceduser2 forKey:@"user1"];
        [prefs synchronize];
    }
    
    if ([SavePasswordFromLogin rangeOfString:@"-"].location == NSNotFound) {
    } else {
        
        NSString* replacedpassword2 = [SavePasswordFromLogin stringByReplacingOccurrencesOfString:@"-" withString:@" "];
        NSLog(@"string space3----%@",replacedpassword2);

        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:replacedpassword2 forKey:@"pass1"];
        [prefs synchronize];
    }
    
    
    //NSRange usernamespaced = [SavedUsernameFromLogin rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
   // NSRange passwordspaced = [SavePasswordFromLogin rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    
   // if(usernamespaced.location != NSNotFound || passwordspaced.location != NSNotFound)
   // {
        
        
   // NSString* replaceduser2 = [SavedUsernameFromLogin stringByReplacingOccurrencesOfString:@"-" withString:@" "];
   // NSString* replacedpassword2 = [SavePasswordFromLogin stringByReplacingOccurrencesOfString:@"-" withString:@" "];
   // NSLog(@"string space3----%@",replaceduser2);
   // NSLog(@"string space3----%@",replacedpassword2);
        
       // NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

       // [prefs setObject:replaceduser2 forKey:@"user1"];
       // [prefs setObject:replacedpassword2 forKey:@"pass1"];
       // [prefs synchronize];
   // }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didfinishLoginReadyToLaunch:)
                                                 name:@"KeyForReady123"
                                               object:nil];
    
    
    
}
-(void)didfinishLoginReadyToLaunch:(NSNotification*)notification
{
    [self starttcpstream];
}

-(void)handleSwipeGesture:(UIPanGestureRecognizer *) sender
{
    if (self.sessionToolbar.alpha == 0.0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.sessionToolbar.alpha = 1.0;
            self.sessionScrollView.frame = CGRectMake(self.sessionScrollView.frame.origin.x, 64, self.sessionScrollView.frame.size.width, self.sessionScrollView.frame.size.height);
            self.setHideToolBar = NO;
        }];
    }
    else
    {
        self.setHideToolBar = YES;
        [self HideToolBar];
    }
}

-(void) HideToolBar
{
    [UIView animateWithDuration:0.3 animations:^{
        self.sessionToolbar.alpha = 0.0;
        self.sessionScrollView.frame = CGRectMake(self.sessionScrollView.frame.origin.x, 18, self.sessionScrollView.frame.size.width, 600);
    }];
}

-(void)ShowAlertForAppLaucher
{
    // Without dismissalDelay and with touchHandler
    [self.view addSubview:_minimalNotification];
    [self.minimalNotification show];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"applaunchermessage"];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self checkPing];
    self.sessionScrollView.bounces = NO;
    

}
-(void)starttcpstream
{
    int port  = 9990;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* IPAdressEnteredSaved = [prefs objectForKey:@"SelectedIpAddress"];
    NSLog(@"ipaddress----%@",IPAdressEnteredSaved);

    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)IPAdressEnteredSaved, port, &readStream, &writeStream);
    if(!CFWriteStreamOpen(writeStream)) {
        NSLog(@"Error, writeStream not open");
        
        return;
    }
    
    [self openstream];
}
-(void)starttcpstream2
{
    int port  = 9990;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* IPAdressEnteredSaved = [prefs objectForKey:@"SelectedIpAddress"];
    NSLog(@"ipaddress----%@",IPAdressEnteredSaved);

    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)IPAdressEnteredSaved, port, &readStream, &writeStream);
    if(!CFWriteStreamOpen(writeStream)) {
        NSLog(@"Error, writeStream not open");
        
        return;
    }
    
    [self openstream2];
}
-(void)openstream
{
    NSLog(@"Opening streams1");
    
    inputStream = (NSInputStream *)readStream;
    outputStream = (NSOutputStream *)writeStream;
    
    [inputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
    [ outputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
    NSDictionary *settings = @{(id)kCFStreamSSLValidatesCertificateChain : @NO};
    CFReadStreamSetProperty(readStream, kCFStreamPropertySSLSettings, settings);
    CFWriteStreamSetProperty(writeStream, kCFStreamPropertySSLSettings, settings);
    
   
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];


    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* UserNameEnteredSaved = [prefs objectForKey:@"TraceFreeUser"];
    NSString* PasswordEnteredSaved = [prefs objectForKey:@"TraceFreePass"];
    
    
    NSString* RequestString = @"authenticate";
    NSDictionary *reauthDictionary = @{@"request" : RequestString, @"username" : UserNameEnteredSaved, @"password" : PasswordEnteredSaved};
    
    [self reauthJSON:reauthDictionary];


}
-(void)openstream2
{
    NSLog(@"Opening streams2.");
    
    inputStream = (NSInputStream *)readStream;
    outputStream = (NSOutputStream *)writeStream;
    
    [inputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
    [ outputStream setProperty:NSStreamSocketSecurityLevelTLSv1 forKey:NSStreamSocketSecurityLevelKey];
    NSDictionary *settings = @{(id)kCFStreamSSLValidatesCertificateChain : @NO};
    CFReadStreamSetProperty(readStream, kCFStreamPropertySSLSettings, settings);
    CFWriteStreamSetProperty(writeStream, kCFStreamPropertySSLSettings, settings);
    
    
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    NSString* UserNameEnteredSaved = [prefs objectForKey:@"TraceFreeUser"];
    NSString* PasswordEnteredSaved = [prefs objectForKey:@"TraceFreePass"];
    
    
    NSString* RequestString = @"authenticate";
    NSDictionary *reauthDictionary = @{@"request" : RequestString, @"username" : UserNameEnteredSaved, @"password" : PasswordEnteredSaved};
    
    [self reauthJSON2:reauthDictionary];
    
    
}
- (void)closestream
{
    NSLog(@"Closing streams.");
    [inputStream close];
    [outputStream close];
    
    [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream setDelegate:nil];
    [outputStream setDelegate:nil];
    
    [inputStream release];
    [outputStream release];
    
    inputStream = nil;
    outputStream = nil;
    
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)event {
    //NSLog(@"Stream triggered.");
    
    switch(event) {
        case NSStreamEventHasSpaceAvailable: {
            if(stream == outputStream) {
              //  NSLog(@"outputStream is ready.");
            }
            break;
        }
        case NSStreamEventHasBytesAvailable: {
            if(stream == inputStream) {
              //  NSLog(@"inputStream is ready.");
                
                uint8_t buf[4096];
                
                NSUInteger length = [inputStream read:buf maxLength:sizeof(buf)];
                NSData *response = [NSData dataWithBytes:buf length:length];
                
               // [self parseRawResponseData:response];
                
                NSString *receivedString = [[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] autorelease];
                NSLog(@"Received: %@", receivedString);
                
            }
            break;
        case NSStreamEventEndEncountered:
          
            //[self closestream];
            break;
        }
        default: {
       //     NSLog(@"Stream is sending an Event: %i", event);
            
            break;
        }
    }
}

- (void)reauthJSON:(NSDictionary *)dict {
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:NULL];
    [self sendreauththroughData:requestData];
}
- (void)reauthJSON2:(NSDictionary *)dict {
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:NULL];
    [self sendreauththroughData2:requestData];
}
- (void)sendreauththroughData:(NSData *)data {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* SavedUrlfromUser = [prefs objectForKey:@"URLCode"];
    NSString* SavedUrlfromUser2 = [prefs objectForKey:@"user1"];
    
    // NSLog(@"codeRDP----%@",SavedUrlfromUser);
    // NSLog(@"urlRDP----%@",SavedUrlfromUser2);
    
    NSMutableData *dataToSend = [NSMutableData dataWithData:data];
    [dataToSend appendData:[self reauthSeparatorData]];
    
    [outputStream write:[dataToSend bytes] maxLength:[dataToSend length]];
    
    NSString * CodeString = @"CODE:";
    
    NSString* combinedString = [CodeString stringByAppendingString: SavedUrlfromUser];
    
    NSDictionary *passthroughurlDictionary = @{@"request" : @"passthrough", @"token" : @"url", @"data" : combinedString};
    
    [self passthroughJSON:passthroughurlDictionary];
    urlink = YES;
    
}
- (void)sendreauththroughData2:(NSData *)data {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* SavedUrlfromUser = [prefs objectForKey:@"user1"];
    
    NSMutableData *dataToSend = [NSMutableData dataWithData:data];
    [dataToSend appendData:[self reauthSeparatorData]];
    
    [outputStream write:[dataToSend bytes] maxLength:[dataToSend length]];
    
    
    NSDictionary *passthroughurlDictionary = @{@"request" : @"passthrough", @"token" : @"url", @"data" : @"youtube.com"};
    
    
    [self passthroughJSON2:passthroughurlDictionary];
    urlink = YES;
    
}
- (NSData *)reauthSeparatorData {
    
    
    return [NSData dataWithBytes:"$" length:1];
}
- (NSData *)passthroughSeparatorData {
    
    
    return [NSData dataWithBytes:"$" length:1];
}

- (void)passthroughJSON:(NSDictionary *)dict {
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:NULL];
    [self sendpassthroughData:requestData];
}
- (void)passthroughJSON2:(NSDictionary *)dict {
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:NULL];
    [self sendpassthroughData2:requestData];
}
- (void)sendpassthroughData2:(NSData *)data {
    
    NSMutableData *dataToSend = [NSMutableData dataWithData:data];
    [dataToSend appendData:[self passthroughSeparatorData]];
    
    [outputStream write:[dataToSend bytes] maxLength:[dataToSend length]];
    
    //[self performSelector:@selector(havetclose) withObject:self afterDelay:0.2 ];
    [self havetoclose];
}
- (void)sendpassthroughData:(NSData *)data {
    
    NSMutableData *dataToSend = [NSMutableData dataWithData:data];
    [dataToSend appendData:[self passthroughSeparatorData]];
    
    [outputStream write:[dataToSend bytes] maxLength:[dataToSend length]];

     //[self performSelector:@selector(havetclose) withObject:self afterDelay:0.2 ];
    [self havetoclose];
}
-(void)havetoclose
{
    [self closestream];
}

-(void)keyboardhackforportrait
{
    UIInterfaceOrientation Orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(Orientation))
    {
        if (!self.keyboardVisible)
        {
            if (IS_IPAD)
            {
                [self showKeyboard];
            }
            [_minimalNotification removeFromSuperview];
        }
    }
    [self updateScrollEnabled];
    
}
- (void)viewDidLayoutSubviews
{
    [self updateScrollEnabled];
}

- (void)checkPing
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString* reachabilityCheckIP = [Sphere3DAPI sharedInstance].connectedIP;
        NSTimeInterval timeBefore = [[NSDate date] timeIntervalSince1970];
        const char *host = [reachabilityCheckIP cStringUsingEncoding:NSASCIIStringEncoding];
        SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host);
        SCNetworkReachabilityFlags flags;
        Boolean success = SCNetworkReachabilityGetFlags(reachability, &flags);
        Boolean available = success && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
        
        if (available)
        {
            NSTimeInterval timeAfter = [[NSDate date] timeIntervalSince1970];
            NSTimeInterval delay = timeAfter - timeBefore;
            //NSLog(@"ping = %f", delay);
            if (delay > 0.05)
            { //50ms
                [self performSelector:@selector(showHighPingAlert) withObject:nil afterDelay:18]; //ping alert to user
            }
        }
        else
        {
            NSLog(@"Host is unreachable");
        }
    });
}

-(void)showHighPingAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Please be aware that your network connection is slow. This may affect the performace and login possibilities of the application." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
    // optional - add more buttons:
    [alert show];
    [alert release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    
    if ([[Def objectForKey:@"AutoRotationLock"] isEqualToString:@"Locked"])
        return NO;
    else
        return YES;
}
-(BOOL) shouldAutorotate
{
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    
    if ([[Def objectForKey:@"AutoRotationLock"] isEqualToString:@"Locked"]){
        return NO;
    }
    else{
        return YES;
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (self.setHideToolBar==YES)
    {
        [self HideToolBar];
    }
    [self fixPortraitOrientation];
    if (![self.touchPointerView isHidden])
    {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        [self.touchPointerView ensurePointerIsVisibleLandscape];
    }
    else {
        [self.touchPointerView ensurePointerIsVisiblePortrait];
    }
}
}

- (void)fixPortraitOrientation
{
    UIInterfaceOrientation Orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(Orientation))
    {
        CGFloat offsetX = (self.sessionView.frame.size.width - self.sessionScrollView.frame.size.width) / 2;
        [self.sessionScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
        if (!self.keyboardVisible)
        {
            if (IS_IPAD)
            {
                [self showKeyboard];
            }
            [_minimalNotification removeFromSuperview];
        }
    }
    else
    {
        if (self.keyboardVisible)
        {
            [self hideKeyboard];
        }
    }
    
    // Functions keys: Ctr, Alt, Shift will be hidden in Portrait orientation on iPhone
    if IS_IPHONE {
        if (UIInterfaceOrientationIsPortrait(Orientation)) {
            self.btnAltKey.hidden = YES;
            self.btnShiftKey.hidden = YES;
            self.btnCtrlKey.hidden = YES;
        } else {
            if ([self.userSettings.keyboardFunctionEnableSwitch boolValue])
            {
                self.btnAltKey.hidden = NO;
                self.btnShiftKey.hidden = NO;
                self.btnCtrlKey.hidden = NO;
            }
            else
            {
                self.btnAltKey.hidden = YES;
                self.btnShiftKey.hidden = YES;
                self.btnCtrlKey.hidden = YES;
            }
        }
    }
    
    [self updateScrollEnabled];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:animated];
    // if sesssion is suspended - notify that we got a new bitmap context
    if ([self.session isSuspended])
        [self sessionBitmapContextWillChange:self.session];
    // init keyboard
    [[RDPKeyboard getSharedRDPKeyboard] initWithSession:_session delegate:self];
    //[self startSessionTimer];  commented out... not needed anymore.. but still implemented to work anytime.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self connectSession];
    self.hasBluetoothKeyboard = YES;
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = YES;
    self.bluetoothKeyboardTest = YES;
    //[self showKeyboard];
    /*
    if(self.hasBluetoothKeyboard)
    {
        [self performSelector:@selector(BluetoothKeyboardHack)
                   withObject:nil
                   afterDelay:3];
       // [self BluetoothKeyboardHack];
    }
     */
    if (isIOS8)
    {
        [self showKeyboard];
        [self hideKeyboard];
    }
    else
    {
        [self showKeyboard];
    }
}

/*
-(void)BluetoothKeyboardHack
{
    [self showKeyboard];
    [self hideKeyboard];
}
 */
- (void)connectSession
{
    if (!self.sessionInitialized)
    {
        if ([self.session isSuspended])
        {
            [self.session resume];
            [self sessionBitmapContextDidChange:self.session];
            [self.sessionView setNeedsDisplay];
        }
        else
            [self.session connect];
        
        self.sessionInitialized = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // reset all modifier keys on rdp keyboard
    [[RDPKeyboard getSharedRDPKeyboard] reset];
    // hide toolbar and keyboard
    
    //[self showSessionToolbar:NO];
    [self hideKeyboard];
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = NO;
}

- (void)dealloc
{
    // remove any observers
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // the session lives on longer so set the delegate to nil
    [_session setDelegate:nil];
    
    [_session release];
    [_userSettings release];
    [_sessionToolbarTopSpace release];
    [_singleTapRecognizer release];
    [_doubleTapRecognizer release];
    [_sideBarFunctions release];
    [_minimalNotification release];
    [_btnSettings release];
    [super dealloc];
}

#pragma mark -
#pragma mark ScrollView delegate methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.sessionView;
}

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    [self updateScrollEnabled];
    [self.sessionView setNeedsDisplay];
}

- (void)updateScrollEnabled {
    CGFloat Scale = self.sessionScrollView.zoomScale;
    UIInterfaceOrientation Orientation = [UIApplication sharedApplication].statusBarOrientation;
    BOOL isOrientationLandscape = UIInterfaceOrientationIsLandscape(Orientation);
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    
    if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM+CDMA)"] || [[h platformString]  isEqualToString:@"iPhone 5c (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5c (Global)"] || [[h platformString]  isEqualToString:@"iPhone 5s (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5s (Global)"] || [[h platformString]  isEqualToString:@"iPhone 6+"] || [[h platformString]  isEqualToString:@"iPhone 6"] || [[h platformString]  isEqualToString:@"iPod Touch 4G"] || [[h platformString]  isEqualToString:@"iPod Touch 5G"] || [[h platformString]  isEqualToString:@"iPhone 7 Plus"])
    {
        if (Scale == 1 && isOrientationLandscape && (!self.keyboardVisible || self.hasBluetoothKeyboard)) {
            self.dragNoDelayRecognizer.enabled = NO;
            self.dragWithDelayRecognizer.enabled = YES;
        } else {
            self.dragNoDelayRecognizer.enabled = NO;
            self.dragWithDelayRecognizer.enabled = YES;
        }
    }
    else
    {
        if (Scale == 1 && isOrientationLandscape && (!self.keyboardVisible || self.hasBluetoothKeyboard)) {
            self.dragNoDelayRecognizer.enabled = YES;
            self.dragWithDelayRecognizer.enabled = NO;
        } else {
            self.dragNoDelayRecognizer.enabled = NO;
            self.dragWithDelayRecognizer.enabled = YES;
        }
    }
    [h release];
}

#pragma mark -
#pragma mark TextField delegate methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.keyboardVisible = YES;
    [self updateScrollEnabled];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.keyboardVisible = NO;
    [self updateScrollEnabled];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self extendSessionTimeout];
    if([string length] > 0)	{
        for(int i = 0 ; i < [string length]; i++) {
            unichar curChar = [string characterAtIndex:i];
            // special handling for return/enter key
            if(curChar == '\n')
                [[RDPKeyboard getSharedRDPKeyboard] sendEnterKeyStroke];
            else
                [[RDPKeyboard getSharedRDPKeyboard] sendUnicode:curChar];
        }
    }
    else
        [[RDPKeyboard getSharedRDPKeyboard] sendBackspaceKeyStroke];
    
    [[textField undoManager] registerUndoWithTarget:self selector:@selector(revertTextView:) object:textField.attributedText];
    return NO;
}

-(void) revertTextView: (NSAttributedString *) textViewAttString
{
    [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyUndo];
}

#pragma mark -
#pragma mark RDPSessionDelegate functions
- (void)session:(RDPSession*)session didFailToConnect:(int)reason
{
    if ([self.delegate respondsToSelector:@selector(sessionDidFail)])
    {
        [self.delegate sessionDidFail];
    }
}

- (void)sessionWillConnect:(RDPSession*)session
{
    if ([self.delegate respondsToSelector:@selector(sessionWillConnect)])
    {
        [self.delegate sessionWillConnect];
    }
}

- (void)sessionDidConnect:(RDPSession*)session
{
    // register keyboard notification handlers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name: UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name: UIKeyboardDidHideNotification object:nil];
    
    if ([self.delegate respondsToSelector:@selector(sessionDidConnect)])
    {
        [self.delegate sessionDidConnect];
    }
}

- (void)sessionWillDisconnect:(RDPSession*)session
{
    if ([self.delegate respondsToSelector:@selector(sessionWillDisconnect)])
    {
        [self.delegate sessionWillDisconnect];
    }
}

- (void)sessionDidDisconnect:(RDPSession*)session
{
    if (!self.logoutStatus)
    {
        if ([self.delegate respondsToSelector:@selector(sessionDidExpire)])
        {
            [self.delegate sessionDidExpire];
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(sessionDidDisconnect)] && !self.disconnectStatus)
    {
        self.disconnectStatus = YES;
        [self.delegate sessionDidDisconnect];
    }
    [self stopSessionTimer];
}

- (void)sessionBitmapContextWillChange:(RDPSession*)session
{
    rdpSettings *sessionSettings = [session getSessionParams];
    [self setSessionViewSize:CGSizeMake(sessionSettings->DesktopWidth, sessionSettings->DesktopHeight)];
    // show/hide toolbar
    [self.session setToolbarVisible:![[NSUserDefaults standardUserDefaults] boolForKey:@"ui.hide_tool_bar"]];
    [self showSessionToolbar:[self.session toolbarVisible]];
}

- (void)setSessionViewSize:(CGSize)size {
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0) {
        self.sessionViewWidth.constant = size.width;
        self.sessionViewHeight.constant = size.height;
    } else {
        self.sessionView.frame = CGRectMake(0, 0, size.width, size.height);
        self.sessionScrollView.contentSize = size;
    }
    [self.sessionScrollView setZoomScale:1.0];
}

- (void)sessionBitmapContextDidChange:(RDPSession*)session
{
    // associate view with session
    [self.sessionView setSession:session];
    // issue an update (this might be needed in case we had a resize for instance)
    [self.sessionView setNeedsDisplay];
}

- (void)session:(RDPSession*)session needsRedrawInRect:(CGRect)rect
{
    [self.sessionView setNeedsDisplayInRect:rect];
}

- (void)session:(RDPSession *)session requestsAuthenticationWithParams:(NSMutableDictionary *)params
{
    
}

- (void)session:(RDPSession *)session verifyCertificateWithParams:(NSMutableDictionary *)params
{
    
}

- (CGSize)sizeForFitScreenForSession:(RDPSession*)session
{
    if (IsPad())
        return self.view.bounds.size;
    else
    {
        // on phones make a resolution that has a 16:10 ratio with the phone's height
        CGSize Size = self.view.bounds.size;
        CGFloat maxSize = (Size.width > Size.height) ? Size.width : Size.height;
        return CGSizeMake(maxSize * 1.6f, maxSize);
    }
}


-(void)sessionDidStartUploadingImage:(RDPSession*)session
{
    //Hack for issue #4
    self.pasteImageCount++;
    if ( YES == self.progressView.hidden )
    {
        self.progressView.hidden = NO;
        [self.progressDotView startAnimating];
    }
}

-(void)sessionDidFinishedUploadingImage:(RDPSession*)session
{
    //Hack for issue #4
    if (![[[Sphere3DAPI sharedInstance] launchedApp] isEqualToString:@"WordPerfect7"] || self.pasteImageCount != 1)
    {
        [self.progressDotView stopAnimating];
        self.progressView.hidden = YES;
    }
}

-(void)sessionDidUploadExceedImage:(RDPSession *)session
                           message:(NSString *)lpMsg
{
    [self.progressDotView stopAnimating];
    self.progressView.hidden = YES;
    [self showImageUploadAlertWithMessage:lpMsg];
}

#pragma mark -
#pragma mark event handlers

- (IBAction)toggleKeyboard
{

    //Hide if side bar menu function is active
    if ([self.view.subviews containsObject:_functionsKeysViewController])
    {
        [_btnSideBarFuntions setSelected:NO];
        [_functionsKeysViewController removeFromSuperview];
    }
    [self extendSessionTimeout];
    
    if(self.keyboardVisible)
    {
        [self hideKeyboard];
    }
    else
    {
        [self showKeyboard];
    }
}

- (void)showKeyboard
{
    self.hasBluetoothKeyboard = YES;
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = YES;
    [self.dummyTextField becomeFirstResponder];
    
    if (self.setHideToolBar==YES)
    {
        [self HideToolBar];
    }
    
    if(isIOS9)
    {
        if ([self.dummyTextField respondsToSelector:@selector(inputAssistantItem)])
        {
            UITextInputAssistantItem *inputAssistantItem = [self.dummyTextField inputAssistantItem];
            inputAssistantItem.leadingBarButtonGroups = @[];
            inputAssistantItem.trailingBarButtonGroups = @[];
        }
    }
}

- (void)hideKeyboard
{
    if (!self.hasBluetoothKeyboard)
    {
        [self.dummyTextField resignFirstResponder];
    }
}

- (IBAction)toggleTouchPointer
{
    [self extendSessionTimeout];
    BOOL toggle_visibilty = ![self.touchPointerView isHidden];
    [self.touchPointerView setHidden:toggle_visibilty];
    
    if (toggle_visibilty) {
        [_btnMouse setSelected:NO];
    }
    else
    {
        [_btnMouse setSelected:YES];
    }
    
}

- (IBAction)disconnectSession:(id)sender
{
    [self hideKeyboard];
    //[self showLogoutAlert];
    
    [self ShowAlertLogout];
}

- (IBAction)showSettingsView:(UIButton *)sender
{
    //Hide if side bar menu function is active
    if ([self.view.subviews containsObject:_functionsKeysViewController])
    {
        [_btnSideBarFuntions setSelected:NO];
        [_functionsKeysViewController removeFromSuperview];
    }
    [self extendSessionTimeout];
    
    if( !self.settingsViewController )
    {
        self.settingsViewController = [SettingsViewController settingsViewControllerWithUserSettings:self.userSettings];
        self.settingsViewController.delegate = self;
    }
    
    if (IS_IPAD)
    {
        if (!self.shownPopover)
        {
            [_btnSettings setSelected:YES];
            self.shownPopover = [[[UIPopoverController alloc] initWithContentViewController:self.settingsViewController]
                                 autorelease];
        }
        
        if (isIOS6)   /// we are here try to compare ios 7 and under
        {
            CGRect sourceRect = sender.frame;
            [self.shownPopover setPopoverContentSize:self.settingsViewController.view.frame.size];
            [self.shownPopover presentPopoverFromRect:sourceRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            self.shownPopover.delegate = self;
        }
        else
        {
            CGRect sourceRect = CGRectMake( 65, 35, 40, 30 );
            [self.shownPopover setPopoverContentSize:self.settingsViewController.view.frame.size];
            [self.shownPopover presentPopoverFromRect:sourceRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            
            self.shownPopover.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"topbar"]];
            self.shownPopover.delegate = self;
        }
        
        if (self.keyboardVisible)
        {
            [self hideKeyboard];
        }
        
    }
    else if (IS_IPHONE)
    {
        _iPhoneSettngNavContoller = [[UINavigationController alloc] initWithRootViewController:self.settingsViewController];
        [self presentViewController:_iPhoneSettngNavContoller
                           animated:YES
                         completion:nil];
        
        //_iPhoneSettngNavContoller.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.shownPopover = nil;
    [_btnSettings setSelected:NO];
}

- (IBAction)switchWindows
{
 //  [self starttcpstream2];

    [self extendSessionTimeout];
    [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyAltShiftTab];
}


- (void)ShowAlertLogout{
    NSString *alertTitle = @"Logout";
    NSString *alertMessage = @"Are you sure you want to logout?";
    NSString *alertAction1 = @"Logout";
    NSString *alertAction2 = @"Exit to Workspace";
    NSString *alertAction3 = @"Cancel";
    
    
    if ([UIAlertController class]) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:alertTitle
                                                                       message:alertMessage
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:alertAction1 style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [self clickedOkAlertType:AlertTypeLogout];
                                                              }];
        UIAlertAction* ExitAction = [UIAlertAction actionWithTitle:alertAction2 style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [self clickedOkAlertType:AlertTypeExit];
                                                           }];
        UIAlertAction* CancelAction = [UIAlertAction actionWithTitle:alertAction3 style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 
                                                             }];
        [alert addAction:defaultAction];
       // [alert addAction:ExitAction];  //no applauncher straight to the rdp session
        [alert addAction:CancelAction];
        
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        UIAlertView *alertV=[[UIAlertView alloc]initWithTitle:alertTitle
                                                      message:alertMessage
                                                     delegate:self
                                            cancelButtonTitle:alertAction3
                                            otherButtonTitles:alertAction1,nil];
        alertV.tag = 99;
        [alertV show];
    }
}

#pragma mark ----------------------
#pragma mark JFMinimalNotificationDelegate
#pragma mark ----------------------

- (void)minimalNotificationWillShowNotification:(JFMinimalNotification*)notification
{
    
}

- (void)minimalNotificationDidShowNotification:(JFMinimalNotification*)notification
{
    
}

- (void)minimalNotificationWillDisimissNotification:(JFMinimalNotification*)notification
{
    
}

- (void)minimalNotificationDidDismissNotification:(JFMinimalNotification*)notification
{
    [_minimalNotification removeFromSuperview];
}


#pragma mark -
#pragma mark iOS Keyboard Notification Handlers
- (void)keyboardWillShow:(NSNotification *)notification
{
    self.hasBluetoothKeyboard = NO;
    [MyApplication sharedMyApplication].hasBluetoothKeyboard = NO;
    
    if (self.bluetoothKeyboardTest)
    {
        [UIView setAnimationsEnabled:NO];
    }
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardFrame = [[self view] convertRect:keyboardEndFrame toView:nil];
    NSTimeInterval animationDuration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    self.scrollViewBottomSpace.constant = keyboardFrame.size.height;
    //    self.scrollViewBottomSpace.constant = isIOS8 ? keyboardFrame.size.height-216.0f : keyboardFrame.size.height;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    if (self.bluetoothKeyboardTest)
    {
        [self hideKeyboard];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    self.scrollViewBottomSpace.constant = 0;
    if (self.bluetoothKeyboardTest)
    {
        [self.view layoutIfNeeded];
    }
    else
    {
        [UIView animateWithDuration:animationDuration animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)keyboardDidHide:(NSNotification*)notification
{
    if (self.bluetoothKeyboardTest)
    {
        self.bluetoothKeyboardTest = NO;
        [UIView setAnimationsEnabled:YES];
        [self fixPortraitOrientation];
    }
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"])
    {
        [self.keyboardToolbarView setHidden:YES];
    }
    [h release];
}

#pragma mark -
#pragma mark Gesture handlers

- (IBAction)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self extendSessionTimeout];
    CGPoint Pos = [recognizer locationInView:self.sessionView];
    if (self.toggleMouseButtons) {
        [self sendRightMouseButtonClickAtPosition:Pos];
    }
    else
    {
        [self sendLeftMouseButtonClickAtPosition:Pos];
    }
    self.toggleMouseButtons = NO;
}

- (IBAction)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    [self extendSessionTimeout];
    CGPoint Pos = [recognizer locationInView:self.sessionView];
    [self sendLeftMouseButtonDoubleClickAtPosition:Pos];
    self.toggleMouseButtons = NO;
}

- (IBAction)handleTwoFingersTap:(UITapGestureRecognizer *)recognizer
{
    [self extendSessionTimeout];
    self.toggleMouseButtons = !self.toggleMouseButtons;
}

- (IBAction)handleDrag:(UIGestureRecognizer *)recognizer
{
    [self extendSessionTimeout];
    CGPoint
    Pos = [recognizer locationInView:self.sessionView];
    
    if([recognizer state] == UIGestureRecognizerStateBegan)
        [self sendLeftMouseButtonDownAtPosition:Pos];
    else if([recognizer state] == UIGestureRecognizerStateChanged)
        [self sendMouseMoveForPosition:Pos];
    else if([recognizer state] == UIGestureRecognizerStateEnded)
        [self sendLeftMouseButtonUpAtPosition:Pos];
}

- (IBAction)handleDoubleLongPress:(UILongPressGestureRecognizer *)recognizer
{
    [self extendSessionTimeout];
    // this point is mapped against the scroll view because we want to have relative movement to the screen/scrollview
    CGPoint Pos = [recognizer locationInView:self.sessionScrollView];
    
    if([recognizer state] == UIGestureRecognizerStateBegan)
        self.twoFingersLongPressPosition = Pos;
    else if([recognizer state] == UIGestureRecognizerStateChanged)
    {
        CGFloat Delta = self.twoFingersLongPressPosition.y - Pos.y;
        if(Delta > GetScrollGestureDelta())
        {
            [self.session sendInputEvent:[self eventDescriptorForMouseEvent:GetMouseWheelEvent(YES) position:Pos]];
            self.twoFingersLongPressPosition = Pos;
        }
        else if(Delta < -GetScrollGestureDelta())
        {
            [self.session sendInputEvent:[self eventDescriptorForMouseEvent:GetMouseWheelEvent(NO) position:Pos]];
            self.twoFingersLongPressPosition = Pos;
        }
    }
}

#pragma mark -
#pragma mark Touch Pointer delegates

- (void)touchPointerClose
{
    [self extendSessionTimeout];
    [self toggleTouchPointer];
    [_btnMouse setSelected:NO];
}

- (void)touchPointerLeftClick:(CGPoint)pos down:(BOOL)down
{
    [self extendSessionTimeout];
    CGPoint convertedPosition = [self.touchPointerView convertPoint:pos toView:self.sessionView];
    [self sendLeftMouseButtonDown:down atPosition:convertedPosition];
}

- (void)touchPointerRightClick:(CGPoint)pos down:(BOOL)down
{
    [self extendSessionTimeout];
    CGPoint convertedPosition = [self.touchPointerView convertPoint:pos toView:self.sessionView];
    [self sendRightMouseButtonDown:down atPosition:convertedPosition];
}

- (void)touchPointerMove:(CGPoint)pos
{
    [self extendSessionTimeout];
    CGPoint convertedPosition = [self.touchPointerView convertPoint:pos toView:self.sessionView];
    [self sendMouseMoveForPosition:convertedPosition];
}

- (void)touchPointerScrollDown:(BOOL)down
{
    [self extendSessionTimeout];
    [self.session sendInputEvent:[self eventDescriptorForMouseEvent:GetMouseWheelEvent(down) position:CGPointZero]];
}

- (void)touchPointerToggleKeyboard
{
    [self extendSessionTimeout];
    // [self toggleKeyboard];
    //    self.dummyTextField.inputView = [[[NSBundle mainBundle] loadNibNamed:@"LNHexNumberpad" owner:self options:nil] objectAtIndex:0];
    //
    if(self.keyboardVisible)
    {
        self.dummyTextField.inputView = nil;
        [self hideKeyboard];
    }
    else
    {
        [self showKeyboard];
    }
}

- (void)touchPointerCopy
{
    [self extendSessionTimeout];
    [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCopy];
}

- (void)touchPointerPaste
{
    [self extendSessionTimeout];
    [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyPaste];
}


#pragma mark - SettingsViewControllerDelegate

-(void)settingsViewController:(SettingsViewController *)controller
     didUpdateSessionDuration:(NSInteger)duration
{
    if (duration <= 0) return;
    if (self.sessionTimer)
    {
        [self.sessionTimer invalidate];
        self.sessionTimer = nil;
    }
    self.sessionTimer = [NSTimer scheduledTimerWithTimeInterval:(duration * 60) target:self selector:@selector(sessionExpired) userInfo:nil repeats:NO];   //timer for the rating uialertview rating the app after 20 mintues
    [self setSessionDuration:duration];
    [self saveSettings];
}

- (void)settingsViewController:(SettingsViewController*)controller didUpdateMousePointerSize:(S3MousePointerSize)value
{
    [self setMousePointerSize:value];
    [self saveSettings];
}

- (void)settingsViewController:(SettingsViewController*)controller didUpdateShowKeyboardToolbar:(BOOL)isOn
{
    [self setKeyboardFunctionKeys:isOn];
    [self saveSettings];
    if (isOn && [self.keyboardToolbarView superview] == nil)
    {
        [self.view addSubview:self.keyboardToolbarView];
        [self.keyboardToolbarView setHidden:YES];
    }
    else if (!isOn && [self.keyboardToolbarView superview] != nil)
    {
        [self.keyboardToolbarView removeFromSuperview];
    }
}
- (void)settingsViewController:(SettingsViewController*)controller didUpdateKeyboardFunctionEnableSwitch:(BOOL)isOn
{
    [self setKeyboardFunctionSwitchKeys:isOn];
    [self saveSettings];
    if (isOn)
    {
        self.btnAltKey.hidden = NO;
        self.btnShiftKey.hidden = NO;
        self.btnCtrlKey.hidden = NO;
    }
    else if (!isOn )
    {
        self.btnCtrlKey.hidden = YES;
        self.btnAltKey.hidden = YES;
        self.btnShiftKey.hidden = YES;
    }
}

- (void)settingsViewControllerShowHelp:(SettingsViewController *)controller
{
    
    [_btnSettings setSelected:NO];
    if (IS_IPAD)
    {
        [self.shownPopover dismissPopoverAnimated:YES];
        SPH_HelpViewController *helpController = [[[SPH_HelpViewController alloc] init] autorelease];
        [self presentFloatingPopupController:helpController];
    }
    else if (IS_IPHONE)
    {
        SPH_HelpViewController *helpController = [[SPH_HelpViewController alloc] initWithNibName:@"SPH_HelpViewControlleriPhone" bundle:nil];
        [_iPhoneSettngNavContoller pushViewController:helpController animated:YES];
    }
}

- (void)settingsViewControllerShowAccountInfo:(SettingsViewController *)controller
{
    [self.shownPopover dismissPopoverAnimated:YES];
    AccountInfoController *accountInfoController = [[[AccountInfoController alloc] init] autorelease];
    accountInfoController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:accountInfoController animated:YES completion:nil];
}

- (void)settingsViewControllerSetupDropbox:(SettingsViewController*)controller
{
    [self.shownPopover dismissPopoverAnimated:YES];
    UIAlertView *AlertDropBox = [[UIAlertView alloc] initWithTitle:@"File Storage" message:@"Would you like to install/uninstall Dropbox?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
    [AlertDropBox addButtonWithTitle:@"Yes"];
    [AlertDropBox show];
}

- (void)disconnectSession
{
    [self.session disconnect];
}

#pragma mark -
#pragma mark Helper functions
-(void)showSessionToolbar:(BOOL)show
{
    // already shown or hidden?
    if (self.sessionToolbarVisible == show)
        return;
    if(show)
    {
        self.sessionToolbarTopSpace.constant = 0;
    }
    else
    {
        self.sessionToolbarTopSpace.constant = -TOOLBAR_HEIGHT;
    }
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
    self.sessionToolbarVisible = show;
}

- (void)configureGestureRecognizers
{
    [self.singleTapRecognizer requireGestureRecognizerToFail:self.doubleTapRecognizer];
}

- (void)suspendSession
{
    // suspend session and pop navigation controller
    [self.session suspend];
    // pop current view controller
    [[self navigationController] popViewControllerAnimated:YES];
}

- (NSDictionary*)eventDescriptorForMouseEvent:(int)event position:(CGPoint)position
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"mouse", @"type",
            [NSNumber numberWithUnsignedShort:event], @"flags",
            [NSNumber numberWithUnsignedShort:lrintf(position.x)], @"coord_x",
            [NSNumber numberWithUnsignedShort:lrintf(position.y)], @"coord_y",
            nil];
}

- (void)sendLeftMouseButtonDown:(BOOL)down atPosition:(CGPoint)pos
{
    [self.session sendInputEvent:[self eventDescriptorForMouseEvent:GetLeftMouseButtonClickEvent(down) position:pos]];
}

- (void)sendLeftMouseButtonDownAtPosition:(CGPoint)pos
{
    [self sendLeftMouseButtonDown:YES atPosition:pos];
}

- (void)sendLeftMouseButtonUpAtPosition:(CGPoint)pos
{
    [self sendLeftMouseButtonDown:NO atPosition:pos];
}

- (void)sendLeftMouseButtonClickAtPosition:(CGPoint)pos
{
    [self sendLeftMouseButtonDownAtPosition:pos];
    [self sendLeftMouseButtonUpAtPosition:pos];
}

- (void)sendLeftMouseButtonDoubleClickAtPosition:(CGPoint)pos
{
    [self sendLeftMouseButtonClickAtPosition:pos];
    [self sendLeftMouseButtonClickAtPosition:pos];
}

- (void)sendRightMouseButtonDown:(BOOL)down atPosition:(CGPoint)pos
{
    [self.session sendInputEvent:[self eventDescriptorForMouseEvent:GetRightMouseButtonClickEvent(down) position:pos]];
}

- (void)sendRightMouseButtonDownAtPosition:(CGPoint)pos
{
    [self sendRightMouseButtonDown:YES atPosition:pos];
}

- (void)sendRightMouseButtonUpAtPosition:(CGPoint)pos
{
    [self sendRightMouseButtonDown:NO atPosition:pos];
}

- (void)sendRightMouseButtonClickAtPosition:(CGPoint)position
{
    [self sendRightMouseButtonDownAtPosition:position];
    [self sendRightMouseButtonUpAtPosition:position];
}

- (void)sendDelayedMouseEventWithTimer:(NSTimer*)timer
{
    self.mouseMoveEventTimer = nil;
    NSDictionary* Event = [timer userInfo];
    [self.session sendInputEvent:Event];
}

- (void)sendMouseMoveForPosition:(CGPoint)position
{
    NSDictionary* Event = [self eventDescriptorForMouseEvent:PTR_FLAGS_MOVE position:position];
    // cancel pending mouse move events
    [self.mouseMoveEventTimer invalidate];
    self.mouseMoveEventsSkipped++;
    if (self.mouseMoveEventsSkipped >= 5)
    {
        [self.session sendInputEvent:Event];
        self.mouseMoveEventsSkipped = 0;
    }
    else
    {
        self.mouseMoveEventTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self
                                                                  selector:@selector(sendDelayedMouseEventWithTimer:) userInfo:Event repeats:NO];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 99) //This tag is used by logoff UIAlertView
    {
        switch (buttonIndex) {
            case 1:
                [self clickedOkAlertType:AlertTypeLogout];
                break;
            case 2:
                [self clickedOkAlertType:AlertTypeExit];
                break;
            default:
                break;
        }
    }
    if (alertView.tag == 22) //This tag is used by logoff UIAlertView
    {
        if (buttonIndex == 1) {
            
            [self.beaconManager stopRangingBeaconsInRegion:self.beaconRegion];
            
            exit(0);
        }
        else{
            [self.beaconManager startRangingBeaconsInRegion:self.beaconRegion];
            
        }
    }
}

#pragma mark - ToolBarButtons
- (IBAction)CtrlAlrShiftKeysClicked:(id)sender
{
    switch ([sender tag])
    {
        case 1:
            [self.keyboardToolbarView shiftKeyClicked:sender];
            
            break;
            
        case 2:
            [self.keyboardToolbarView altKeyClicked:sender];
            
            break;
            
        case 3:
            [self.keyboardToolbarView controlKeyClicked:sender];
            
            break;
        default:
            break;
    }
}

#pragma mark - KeyboardToolbarViewDelegate

- (void)keyboardToolbarViewDidClickEscButton:(KeyboardToolbarView*)keyboardToolbar
{
    [[RDPKeyboard getSharedRDPKeyboard] sendEscapeKeyStroke];
}

- (void)keyboardToolbarViewDidClickTabButton:(KeyboardToolbarView*)keyboardToolbar
{
    [[RDPKeyboard getSharedRDPKeyboard] toggleTabKey];
}

- (void)keyboardToolbarView:(KeyboardToolbarView*)keyboardToolbar
        didToggleCtrlButton:(BOOL)isOn{
    [[RDPKeyboard getSharedRDPKeyboard] toggleCtrlKey];
    if (isOn)
    {
        [_btnCtrlKey setAlpha:1.0];
        [_btnCtrlKey setSelected:YES];
    }
    else
    {
        [_btnCtrlKey setAlpha:0.55];
        [_btnCtrlKey setSelected:NO];
    }
}

- (void)keyboardToolbarView:(KeyboardToolbarView*)keyboardToolbar
         didToggleAltButton:(BOOL)isOn{
    [[RDPKeyboard getSharedRDPKeyboard] toggleAltKey];
    if (isOn)
    {
        [_btnAltKey setAlpha:1.0];
        [_btnAltKey setSelected:YES];
    }
    else
    {
        [_btnAltKey setAlpha:0.55];
        [_btnAltKey setSelected:NO];
    }
}

- (void)keyboardToolbarView:(KeyboardToolbarView*)keyboardToolbar
       didToggleShiftButton:(BOOL)isOn
{
    [[RDPKeyboard getSharedRDPKeyboard] toggleShiftKey];
    if (isOn)
    {
        [_btnShiftKey setAlpha:1.0];
        [_btnShiftKey setSelected:YES];
    }
    else
    {
        [_btnShiftKey setAlpha:0.55];
        [_btnShiftKey setSelected:NO];
    }
}

- (void)keyboardToolbarViewDidClickDelButton:(KeyboardToolbarView*)keyboardToolbar
{
    [[RDPKeyboard getSharedRDPKeyboard] toggleDelKey];
}

- (void)keyboardToolbarViewDidClickFunctionsKeyButton:(KeyboardToolbarView *)keyboardToolbar{
    self.dummyTextField.inputView = [[[NSBundle mainBundle] loadNibNamed:@"FunctionsKeyBoard" owner:self options:nil] objectAtIndex:0];
    
    if(self.keyboardVisible) {
        [self hideKeyboard];
        //[self showKeyboard];
        [self performSelector:@selector(showKeyboard) withObject:self afterDelay:0.3];
    } else {
        //[self showKeyboard];
        [self performSelector:@selector(showKeyboard) withObject:self afterDelay:0.2];
    }
}

- (void)keyboardToolbarViewDidClickNumberKeyButton:(KeyboardToolbarView *)keyboardToolbar
{
    self.dummyTextField.inputView = [[[NSBundle mainBundle] loadNibNamed:@"LNHexNumberpad" owner:self options:nil] objectAtIndex:0];
    if(self.keyboardVisible)
    {
        [self hideKeyboard];
        //[self showKeyboard];
        [self performSelector:@selector(showKeyboard) withObject:self afterDelay:0.3];
    }
    else
    {
        //[self showKeyboard];
        [self performSelector:@selector(showKeyboard) withObject:self afterDelay:0.2];
    }
}

-(void)keyboardToolbarViewDidClickStandardKeyButton:(KeyboardToolbarView *)keyboardToolbar
{
    if(self.keyboardVisible)
    {
        self.dummyTextField.inputView = nil;
        [self hideKeyboard];
        // [self showKeyboard];
        [self performSelector:@selector(showKeyboard) withObject:self afterDelay:0.3];
    }
    else
    {
        //[self showKeyboard];
        [self performSelector:@selector(showKeyboard) withObject:self afterDelay:0.2];
    }
}

#pragma mark - Settings properties accessors

- (void)setSessionDuration:(int)minutes
{
    self.userSettings.sessionDuration = @(minutes);
    //    sessionDuration = minutes;
}

- (int)getSessionDuration
{
    return [self.userSettings.sessionDuration intValue];
}

- (S3MousePointerSize)getMousePointerSize
{
    return [self.userSettings.mousePointerSize intValue];
}

- (void)setMousePointerSize:(S3MousePointerSize)pointerSize
{
    self.userSettings.mousePointerSize = @(pointerSize);
    //    mousePointerSize = pointerSize;
    if (self.touchPointerView) {
        [self.touchPointerView setPointerSize:pointerSize
                                     atCenter:[self.touchPointerView
                                               getMouseCenterPoint]];
        [self mouseMoveToClientLocation];
    }
}

- (void)setKeyboardFunctionKeys:(BOOL)enabled
{
    self.userSettings.keyboardFunctionKeys = @(enabled);
}

- (BOOL)keyboardFunctionKeys
{
    return [self.userSettings.keyboardFunctionKeys boolValue];
}

- (void)setKeyboardFunctionSwitchKeys:(BOOL)enabled
{
    self.userSettings.keyboardFunctionEnableSwitch = @(enabled);
}

- (BOOL)keyboardFunctionSwitchKeys
{
    return [self.userSettings.keyboardFunctionEnableSwitch boolValue];
}

- (void)mouseMoveToClientLocation
{
    BOOL toggle_visibilty = ![self.touchPointerView isHidden];
    if (toggle_visibilty)
    {
        [self.touchPointerView performMouseLeftClickAtPoint:[self.touchPointerView arrowLocation]];
    }
}

#pragma mark - User settings persistance

- (void)loadSettings
{
    self.userSettings = [UserSettings loadUserSettings];
    if (self.touchPointerView)
    {
        [self.touchPointerView setPointerSize:[self.userSettings.mousePointerSize intValue]
                                     atCenter:[self view].center];
        [self mouseMoveToClientLocation];
    }
}

- (void)saveSettings
{
    [self.userSettings save];
}

#pragma mark - RDC Session Time
- (void)startSessionTimer
{
    [self stopSessionTimer];
    //  NSLog(@"start session");
    NSInteger sessionTimeoutInSec = [self.userSettings.sessionDuration intValue] * 60;
    self.sessionTimer = [NSTimer scheduledTimerWithTimeInterval:sessionTimeoutInSec target:self selector:@selector(sessionExpired) userInfo:nil repeats:NO];
}

- (void)stopSessionTimer
{
    if(![self.sessionTimer isValid]) return;
    //  NSLog(@"stop session");
    [self.sessionTimer invalidate];
    self.sessionTimer = nil;
}

- (void)extendSessionTimeout
{
    if(nil == self.sessionTimer || ![self.sessionTimer isValid]) return;
    // [self startSessionTimer];
}

-(void)sessionExpired
{
    [self stopSessionTimer];
    [self disconnectSession];
}

#pragma mark - CustomAlertViewDelegate

- (void)clickedOkAlertType:(AlertType)alertType
{
    switch (alertType)
    {
        case AlertTypeLogout:
            if (self.session)
            {
                self.logoutStatus = YES;
                [self.session disconnect];
            }
            break;
        case AlertTypeExit:
            if (self.session)
            {
                self.logoutStatus = YES;
                [self.session disconnect];
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *rdpint = @"4";
                [prefs setObject:rdpint forKey:@"loginexit"];
                [prefs synchronize];
            }
            break;
        default:
            break;
    }
}

- (void)willShowCustomAlert
{
    [self.shownPopover dismissPopoverAnimated:NO];
}

#pragma mark SideBar Function Menu
- (void) SideBarFunctionsInit
{
    CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    int Yaxis;
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] < 7.0)
        Yaxis = TOOLBAR_HEIGHT;
    else
        Yaxis = TOOLBAR_HEIGHT + statusBarHeight;
    
    _functionsKeysViewController = [[FunctionsKeysSideMenu alloc] initWithFrame:CGRectMake(0, Yaxis, 170, 705)];
    _functionsKeysViewController.delegate = self;
    _functionsKeysViewController.alpha=0.5f;
}
-(void)pressedKeysFunctions:(UIButton*) sender
{
    NSString *numberPressed  = sender.titleLabel.text;
    ((void (^)())@{
                   @"F1" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F1];},
                   @"F2" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F2];},
                   @"F3" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F3];},
                   @"F4" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F4];},
                   @"F5" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F5];},
                   @"F6" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F6];},
                   @"F7" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F7];},
                   @"F8" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F8];},
                   @"F9" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F9];},
                   @"F10" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F10];},
                   @"F11" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F11];},
                   @"F12" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F12];
        //[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyF1Plus];
    },
                   }[numberPressed] ?: ^{
                       NSLog(@"default");
                   })();
}

- (IBAction)SideBarMenuAction:(id)sender
{
    CATransition *Transition = [CATransition animation];
    Transition.duration = 1.0;
    
    if ([self.view.subviews containsObject:_functionsKeysViewController])
    {
        Transition.type = kCATransitionFade; //choose your animation
        [_functionsKeysViewController.layer addAnimation:Transition forKey:nil];
        [_functionsKeysViewController removeFromSuperview];
        [_btnSideBarFuntions setSelected:NO];
    }
    else
    {
        Transition.type = kCATransitionFade;
        [_sideBarFunctions.view.layer addAnimation:Transition forKey:nil];
        [self.view addSubview:_functionsKeysViewController];
        [_btnSideBarFuntions setSelected:YES];
    }
}
@end
