//  RDPSessionViewController
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  RDPSessionViewController is the main part of Glassware connect. The contain the main session of the RDP.

//  The RDPSessionViewController mostly consists of:
//
//  Toolbar
//  RDP Container
//  User Session control
//  Keyboards layout
//  Mouse On/Off

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "RDPSession.h"
#import "RDPKeyboard.h"
#import "RDPSessionView.h"
#import "TouchPointerView.h"
#import "SPH_RDPSessionViewDelegate.h"
#import "AlertViewController.h"
#import "SettingsViewController.h"
#import "KeyboardToolbarView.h"
#import "UserSettings.h"
#import "FunctionsKeysSideMenu.h"
#import "ProgressDotsView.h"
#import <CoreLocation/CoreLocation.h>
#import <EstimoteSDK/ESTBeaconManager.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import <Foundation/Foundation.h>

@interface RDPSessionViewController : CustomViewController <RDPSessionDelegate, TouchPointerDelegate, RDPKeyboardDelegate, UIScrollViewDelegate, UITextFieldDelegate
,SettingsViewControllerDelegate
,UIAlertViewDelegate, FunctionsKeysViewDelegate
,KeyboardToolbarViewDelegate,ESTBeaconManagerDelegate,NSStreamDelegate
>

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil session:(RDPSession*)session;

@property (nonatomic, assign) id<SPH_RDPSessionViewDelegate> delegate;
@property (nonatomic, strong) NSTimer *sessionTimer;
@property (nonatomic, strong) UserSettings *userSettings;
@property (nonatomic, strong) UIPopoverController *addImagePopoverController;
@property (nonatomic, strong) SettingsViewController *settingsViewController;
@property (nonatomic, strong) KeyboardToolbarView *keyboardToolbarView;
@property (strong, nonatomic) IBOutlet ProgressDotsView *progressDotView;
@property (strong, nonatomic) IBOutlet UIView           *progressView;
@property (retain, nonatomic) IBOutlet UIButton *btnShiftKey;
@property (retain, nonatomic) IBOutlet UIButton *btnAltKey;
@property (retain, nonatomic) IBOutlet UIButton *btnCtrlKey;
@property (retain, nonatomic) IBOutlet UIButton *btnSideBarFuntions;
@property (nonatomic, retain) UIViewController *sideBarFunctions;
@property (nonatomic, strong) FunctionsKeysSideMenu *functionsKeysViewController;
- (id)initWithBeacon:(CLBeacon *)beacon;
- (void)disconnectSession;
- (void)connectSession;
@end
