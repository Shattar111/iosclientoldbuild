#import <UIKit/UIKit.h>

typedef enum : NSInteger {
    AlertTypeCustom = -1,
    AlertTypeNone = 0,
    AlertTypeLogout,
    AlertTypeNetworkError,
    AlertTypeApplicationLaunch,
    AlertTypeSessionTimeout,
    AlertTypeImageUpload,
    AlertTypeExit
} AlertType;

@protocol CustomAlertDelegate <NSObject>

- (void)willShowCustomAlert;
- (void)clickedOkAlertType:(AlertType)alertType;
- (void)clickedCancelAlertType:(AlertType)alertType;

@end

@interface CustomViewController : UIViewController <CustomAlertDelegate>

// static property
@property (nonatomic, assign) CustomViewController *topViewController;

@property (nonatomic) AlertType shownAlertType;

- (void)presentFloatingPopupController:(UIViewController *)controller;

- (void)showNetworkErrorAlert;
- (void)showServerErrorAlert;
- (void)showApplicationLaunchAlertWithMessage:(NSString *)message;
- (void)showSessionTimeoutAlert;
- (void)showUserNotAllowaccessAlert;

- (void)showLogoutAlert;
- (void)showImageUploadAlertWithMessage:(NSString *)message;

@end
