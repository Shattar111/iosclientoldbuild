#import "CustomViewController.h"

#import "SPH_Constant.h"

#import "AlertViewController.h"

#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])

@interface CustomViewController () <AlertViewControllerDelegate>

@property (retain, nonatomic) IBOutlet NSLayoutConstraint *customToolbarHeight;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *customToolbarContentCentering;

#pragma mark - Popup
@property (retain, nonatomic) IBOutlet UIView *popupView;
@property (retain, nonatomic) IBOutlet UIView *popupContainerView;
@property (retain, nonatomic) IBOutlet UILabel *popupTitleLabel;


@property (retain, nonatomic) UIViewController *floatingPopupController;

@end

static CustomViewController *_topViewController = nil;

@implementation CustomViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.topViewController = self;
    //Run on next runloop iteration
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateStatusBar];
    });
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.topViewController = nil;
}

- (void)updateStatusBar {
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0) {
        self.customToolbarHeight.constant = 64;
        self.customToolbarContentCentering.constant = -10;
    }
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Static property accessors

@dynamic topViewController;

- (void)setTopViewController:(CustomViewController *)topViewController {
    _topViewController = topViewController;
}

- (CustomViewController *)topViewController {
    return _topViewController;
}

#pragma mark - External Interface

- (void)showAlertWithTitle:(NSString *)title type:(tSPH_AlertType)type message:(NSString *)message
{
    AlertViewController *controller = [AlertViewController alertViewControllerWithTitle:title alertType:type
                                                           message:message];
    controller.delegate = self;
	controller.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.topViewController willShowCustomAlert];
    [self.topViewController presentViewController:controller animated:NO completion:nil];
}

- (void)showSheetAlertWithTitle:(NSString *)title type:(tSPH_AlertType)type message:(NSString *)message image:(UIImage *)image {
    AlertViewController *controller = [AlertViewController alertViewControllerWithTitle:title
                                        alertType:type message:message];
    controller.delegate = self;
	controller.modalPresentationStyle = UIModalPresentationFormSheet;
    if (image) {
        [controller setCustomImage:image];
    }
    [self.topViewController willShowCustomAlert];
    [self.topViewController presentViewController:controller animated:NO completion:nil];
}

- (void)showSheetAlertWithTitle:(NSString *)title type:(tSPH_AlertType)type message:(NSString *)message {
    [self showSheetAlertWithTitle:title type:type message:message image:nil];
}

- (void)showNetworkErrorAlert {
    self.shownAlertType = AlertTypeNetworkError;
    [self showAlertWithTitle:SPH_STR_TITLE_NETWORK_ERROR type:SPH_ALERT_BTN_TYPE_OK_ONLY
                               message:SPH_STR_MSG_NETWORK_ERROR];
}
- (void)showServerErrorAlert {
    self.shownAlertType = AlertTypeNetworkError;
    [self showAlertWithTitle:SPH_STR_TITLE_SERVER1_ERROR type:SPH_ALERT_BTN_TYPE_OK_ONLY
                     message:SPH_STR_MSG_SERVER_ERROR];
}


- (void)showApplicationLaunchAlertWithMessage:(NSString *)message {
    self.shownAlertType = AlertTypeApplicationLaunch;
    [self showAlertWithTitle:SPH_STR_TITLE_SERVER_ERROR type:SPH_ALERT_BTN_TYPE_OK_ONLY
                               message:message];
}

- (void)showSessionTimeoutAlert {
    self.shownAlertType = AlertTypeSessionTimeout;
    [self showAlertWithTitle:SPH_STR_TITLE_SESSION_TIMEOUT type:SPH_ALERT_BTN_TYPE_OK_ONLY
                               message:SPH_STR_MSG_LOGIN_AGAIN];
}

- (void)showUserNotAllowaccessAlert {
    self.shownAlertType = AlertTypeNetworkError;
    [self showAlertWithTitle:SPH_STR_TITLE_NO_USERNAME_ACCESS type:SPH_ALERT_BTN_TYPE_OK_ONLY
                     message:SPH_STR_MSG_NO_USERNAME_ACCESS];
}


- (void)showLogoutAlert {
    self.shownAlertType = AlertTypeLogout;
    [self showSheetAlertWithTitle:@"" type:SPH_ALERT_BTN_TYPE_OK_CANCEL message:@"Are you sure you want to logout?"
                            image:[UIImage imageNamed:@"model_logout_icon"]];
}

- (void)showImageUploadAlertWithMessage:(NSString *)message {
    self.shownAlertType = AlertTypeImageUpload;
    [self showSheetAlertWithTitle:SPH_UPLOAD_ALERT_TITLE type:SPH_ALERT_BTN_TYPE_OK_ONLY message:message];
}

#pragma mark - Floating Popup

- (void)presentFloatingPopupController:(UIViewController *)controller {
    
    self.floatingPopupController = controller;
    
    if (!self.popupView) {
        [self loadPopup];
    }
    
    [self.popupContainerView addSubview:controller.view];
    controller.view.frame = self.popupContainerView.bounds;
    self.popupTitleLabel.text = controller.title;
    [self addChildViewController:controller];
    
    [self.view addSubview:self.popupView];
    //self.popupView.center = self.view.center;
    
    if (IS_IPAD)
        self.popupView.center = self.view.center;
    else
        
        self.popupView.center = CGPointMake(160, 300);
    
}

- (void)dismissFloatingPopup {
    [self.floatingPopupController removeFromParentViewController];
    [self.floatingPopupController.view removeFromSuperview];
    self.floatingPopupController = nil;
    
    [self.popupView removeFromSuperview];
}

- (IBAction)popupDoneTouch {
    [self dismissFloatingPopup];
}

- (IBAction)handlePopupDrag:(UIPanGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint center = self.popupView.center;
        CGPoint translation = [recognizer translationInView:self.popupView];
        self.popupView.center = CGPointMake(center.x + translation.x, center.y + translation.y);
        [recognizer setTranslation:CGPointZero inView:self.popupView];
    }
}

- (void)loadPopup {
    
    if (IS_IPAD)
        
        [[NSBundle mainBundle] loadNibNamed:@"FloatingPopup" owner:self options:nil] ;
    else
        
        [[NSBundle mainBundle] loadNibNamed:@"FloatingPopupiPhone" owner:self options:nil];
    
}

#pragma mark - AlertViewControllerDelegate

- (void)alertViewControllerOnClickedOk:(AlertViewController *)controller {
    [controller.presentingViewController dismissViewControllerAnimated:NO completion:^{
        [self clickedOkAlertType:self.shownAlertType];
        self.shownAlertType = AlertTypeNone;
    }];
}

- (void)alertViewControllerOnClickedCancel:(AlertViewController *)controller {
    [controller.presentingViewController dismissViewControllerAnimated:NO completion:^{
        [self clickedCancelAlertType:self.shownAlertType];
        self.shownAlertType = AlertTypeNone;
    }];
}

- (void) alertViewControllerOnClickedExit:(AlertViewController*)controller;{
    [controller.presentingViewController dismissViewControllerAnimated:NO completion:^{
        self.shownAlertType = AlertTypeExit;
        [self clickedOkAlertType:self.shownAlertType];
    }];
    
}
#pragma mark - CustomAlertDelegate

- (void)willShowCustomAlert {
    NSLog(@"You probably need to override %s", __PRETTY_FUNCTION__);
}

- (void)clickedOkAlertType:(AlertType)alertType {
    NSLog(@"You probably need to override %s", __PRETTY_FUNCTION__);
}

- (void)clickedCancelAlertType:(AlertType)alertType {
    NSLog(@"You probably need to override %s", __PRETTY_FUNCTION__);
}

- (void)clickedExitAlertType:(AlertType)alertType {
    NSLog(@"You probably need to override %s", __PRETTY_FUNCTION__);
}
- (void)dealloc {
    [_customToolbarHeight release];
    [_customToolbarContentCentering release];
    [_popupView release];
    [_popupContainerView release];
    [_popupTitleLabel release];
    [super dealloc];
}
@end
