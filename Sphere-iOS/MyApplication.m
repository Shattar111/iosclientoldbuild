#import "MyApplication.h"

#import "RDPKeyboard.h"
#import <objc/message.h>
#import <objc/runtime.h>

typedef enum : NSUInteger {
    EventTypeKeyDown = 0x0A,
    EventTypeKeyUp = 0x0B,
    EventTypeStatusChanged = 0x0C,
} EventType;

typedef enum : NSUInteger {
    KeyCodeArrowUp = 0x52,
    KeyCodeArrowDown = 0x51,
    KeyCodeArrowLeft = 0x50,
    KeyCodeArrowRight = 0x4f,
    KeyCodeDelete = 0x4c,
    KeyCodeTab = 0x2b,
    KeyCodeCtrl = 0xE0,
    KeyCodeAlt = 0xE6
} KeyCode;

typedef struct GSEventRecord {
    union {
        UInt8 bytes[0x38];
        struct {
            UInt32 r0; //isa?
            UInt32 r1;
            UInt32 type;
            UInt32 subtype;
            CGPoint location;
            CGPoint windowLocation;
            UInt32 context;
            UInt64 timestamp;
            UInt32 window;
            UInt32 flags;
            UInt32 sender;
            UInt32 infoSize;
            UInt32 keyCode;
        };
    };
} GSEventRecord;

@interface MyApplication ()

@end

@implementation MyApplication

+ (MyApplication *)sharedMyApplication {
    return (MyApplication *)[self sharedApplication];
}

static void handleKeyEvent(MyApplication *self, SEL _cmd, UIEvent *event) {
    struct objc_super s = {.receiver = self, .super_class = [UIApplication class]};
    
    objc_msgSendSuper(&s, _cmd, event);
    
    if (self.hasBluetoothKeyboard) {
        NSMethodSignature *s1 = [event methodSignatureForSelector:[self keyCodeSelector]];
        NSInvocation *inv1 = [NSInvocation invocationWithMethodSignature:s1];
        [inv1 setTarget:event];
        [inv1 setSelector:[self keyCodeSelector]];
        [inv1 invoke];
        long keyCode = 0;
        [inv1 getReturnValue:&keyCode];
           //NSLog(@"Code = %ld", keyCode);

        
        NSMethodSignature *s2 = [event methodSignatureForSelector:[self isKeyDownSelector]];
        NSInvocation *inv2 = [NSInvocation invocationWithMethodSignature:s2];
        [inv2 setTarget:event];
        [inv2 setSelector:[self isKeyDownSelector]];
        [inv2 invoke];
        BOOL isKeyDown;
        [inv2 getReturnValue:&isKeyDown];
         //NSLog(@"Key down = %d", isKeyDown);

        [self sendKeyCode:keyCode up:!isKeyDown];
    }
}

+ (void)initialize {
    [super initialize];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        Method description = class_getInstanceMethod([UIApplication class], [self handleKeyEventSelector]);
        const char *types = method_getTypeEncoding(description);
        class_addMethod([self class], [self handleKeyEventSelector], (IMP)handleKeyEvent, types);
    }
}

- (SEL)keyCodeSelector {
    return [self selectorFromComponents:@[@"_k", @"eyC", @"ode"]];
}

- (SEL)isKeyDownSelector {
    return [self selectorFromComponents:@[@"_", @"isK", @"eyD", @"own"]];
}

- (SEL)gsEventSelector {
    return [self selectorFromComponents:@[@"_gsE", @"vent"]];
}

+ (SEL)handleKeyEventSelector {
    return [self selectorFromComponents:@[@"handle", @"", @"KeyU", @"IEvent", @":"]];
}

- (SEL)selectorFromComponents:(NSArray *)components {
    return [[self class] selectorFromComponents:components];
}

+ (SEL)selectorFromComponents:(NSArray *)components {
    NSMutableString *string = [NSMutableString string];
    for (NSString *component in components) {
        [string appendString:component];
    }
    return NSSelectorFromString(string);
}

- (void)sendEvent:(UIEvent *)event {
    [super sendEvent:event];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        if ([event respondsToSelector:[self gsEventSelector]]) {
            
            GSEventRecord *eventMem = (GSEventRecord *)[event performSelector:[self gsEventSelector]];

            if (eventMem) {
                if (eventMem->type == EventTypeKeyDown || eventMem->type == EventTypeKeyUp) {
                    BOOL up = eventMem->type == EventTypeKeyUp;
                    [self sendKeyCode:eventMem->keyCode up:up];
                }
            }
            
        }
    }
    
}

- (void)sendKeyCode:(long)keyCode up:(BOOL)up {
    switch (keyCode) {
        case KeyCodeArrowUp:
            return [[RDPKeyboard getSharedRDPKeyboard] sendUpArrowKeyUp:up];
        case KeyCodeArrowDown:
            return [[RDPKeyboard getSharedRDPKeyboard] sendDownArrowKeyUp:up];
        case KeyCodeArrowLeft:
            return [[RDPKeyboard getSharedRDPKeyboard] sendLeftArrowKeyUp:up];
        case KeyCodeArrowRight:
            return [[RDPKeyboard getSharedRDPKeyboard] sendRightArrowKeyUp:up];
        case KeyCodeDelete:
            return [[RDPKeyboard getSharedRDPKeyboard] sendDeleteKeyUp:up];
        case KeyCodeTab:
            return [[RDPKeyboard getSharedRDPKeyboard] sendTabKeyUp:up];
        case KeyCodeCtrl:
            return [[RDPKeyboard getSharedRDPKeyboard] sendCtrlKeyUp:up];
        case KeyCodeAlt:
            return [[RDPKeyboard getSharedRDPKeyboard] sendAltKeyUp:up];

        default:
            break;
    }
}


@end
