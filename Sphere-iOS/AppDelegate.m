#import "AppDelegate.h"

#import "UIColor+Sphere3d.h"

#define iOS8andAbove ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//#define UUIDstring @"B9407F30-F5F8-466E-AFF9-25556B57FE6D"
//#define iDentifier @"GlasswareApp"
//#define bMinor 33333
//#define bMajor 55555
#define noteMessage @"You entered Sphere3D office. \nIts time to open Glassware App."
@implementation AppDelegate

NSString* jsonUsername;
NSString* jsonPassword;
NSString* jsonIPAddress;


- (BOOL)application:(UIApplication *)__unused application didFinishLaunchingWithOptions:(NSDictionary *)__unused launchOptions {
    [self.window makeKeyAndVisible];
    [self setupApplicationAppearance];
    [NSThread sleepForTimeInterval:1.5];
    
    NSUserDefaults *Def = [NSUserDefaults standardUserDefaults];
    NSString *Ver = [Def stringForKey:@"Version"];
    NSString *CurVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
    if(Ver == nil || [Ver compare:CurVer] != 0)
    {
        if(Ver == nil)
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
            
            
            
            NSString *ServerMicroEnableDisable = @"False";  //or false depending on the app/and whats comming backfrom tcp stream.
            
            [prefs setObject:ServerMicroEnableDisable forKey:@"ServerMicroEnableDisable"];
            [prefs synchronize];
            NSLog(@"hitonce");
            if ([Def objectForKey:@"MontioringLock"] == nil) {
                [Def setObject:@"Locked" forKey:@"MontioringLock"];
                if ([Def objectForKey:@"BackgroundImageSetting"] == nil) {
                    [Def setObject:@"Background8" forKey:@"BackgroundImageSetting"];
                    
                }
            }
        }
        //Run once-per-upgrade code, if any
        [Def setObject:CurVer forKey:@"Version"];
    }
    

    
    if ([Def objectForKey:@"AutoRotationLock"] == nil) {
        [Def setObject:@"unLocked" forKey:@"AutoRotationLock"];
        
    }
    
    
    
    
    if ([[Def objectForKey:@"MontioringLock"] isEqualToString:@"Locked"]){
        [self initializeBeacon];
    }
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application{
    
    //This issue should be solved in near future: Reseting the orientation lock to unlocked if the app is terminated,
    //or there will be an issue where authentication view controller objects will be missed up
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"unLocked" forKey:@"AutoRotationLock"];
}

- (void)setupApplicationAppearance {
    UIColor *tintColor = [UIColor sphere3dControlsTintColor];
    [[UISlider appearance] setMinimumTrackTintColor:tintColor];
    [[UISwitch appearance] setOnTintColor:tintColor];
    [[UISegmentedControl appearance] setTintColor:tintColor];
}

- (void)dealloc {
    [_window release];
    [super dealloc];
}


-(void) initializeBeacon {
    
    self.beaconManager = [ESTBeaconManager new];
    self.beaconManager.delegate = self;
    [self.beaconManager requestAlwaysAuthorization];
    
//    [self.beaconManager startMonitoringForRegion:[[CLBeaconRegion alloc]
//                                                  initWithProximityUUID:[[NSUUID alloc]
//                                                                         initWithUUIDString:UUIDstring]
//                                                  major:bMajor minor:bMinor identifier:iDentifier]];
    NSString* path = [self GetPlistFilePath];
    NSMutableArray *plist = [NSMutableArray arrayWithContentsOfFile: path];
    
    
    
    int i = 0;
    //Loop through all the beacons in plist to find the match
    for (NSDictionary *item in plist) {
        
        _region  = [[CLBeaconRegion alloc]
                                   initWithProximityUUID:[[NSUUID alloc]
                                                          initWithUUIDString:[item objectForKey:@"UUID"]]
                                   major:[[item objectForKey:@"Major"] intValue]
                                   minor:[[item objectForKey:@"Minor"] intValue]
                                   identifier:[[item objectForKey:@"UUID"] stringByAppendingFormat:@"%@ ",[item objectForKey:@"Major"]]];
        i++;
        
        _region.notifyOnEntry = YES;
        _region.notifyEntryStateOnDisplay = YES;
        
        [self.beaconManager startMonitoringForRegion:_region];
        
    }
    

    if iOS8andAbove
        [[UIApplication sharedApplication]
         registerUserNotificationSettings:[UIUserNotificationSettings
                                           settingsForTypes:UIUserNotificationTypeAlert | UIRemoteNotificationTypeSound
                                           categories:nil]];
    else
        
        [[UIApplication sharedApplication]
         registerForRemoteNotificationTypes: (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    
    
}

-(void) stopBeaconRegioning{
    
    [self.beaconManager stopMonitoringForRegion:_region];
    
}

- (void)beaconManager:(id)manager didEnterRegion:(CLBeaconRegion *)region {
    UILocalNotification *notification = [UILocalNotification new];
    notification.alertBody = noteMessage;
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    CLBeaconRegion *r = (CLBeaconRegion *) region;
    NSLog(@"UUID %@, major %@, minor %@", r.proximityUUID.UUIDString, r.major, r.minor);
    notification.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:r.proximityUUID.UUIDString, @"UUID",
                             r.major.stringValue, @"Major",
                             r.minor.stringValue, @"Minor", nil];
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *RDPSessionEnableRangingString = @"RangeOn";
    [prefs setObject:RDPSessionEnableRangingString forKey:@"StartRanging"];
    [prefs synchronize];
 
    
    NSDictionary *dict = [notification userInfo]; //contains slided notification data
    
    if ([self CompareJSONBeconData:dict[@"UUID"] major:dict[@"Major"] minor:dict[@"Minor"]]){
        
        NSDictionary *beaconDataDict = [NSDictionary dictionaryWithObjectsAndKeys:jsonUsername, @"Username",
                                        jsonPassword, @"Password",
                                        jsonIPAddress, @"IPAddress", nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconNotification"
                                                            object:self
                                                          userInfo:beaconDataDict];
    }
}

-(NSString*)GetPlistFilePath{ //Get the path of the plist file in the document structure of the device
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"BeaconList.plist"]; //3
    
    return path;
}

- (BOOL)CompareJSONBeconData:(NSString*)uuid major:(NSString*)major minor:(NSString*)minor
{
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"beaconJSON" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filepath];
    NSError *error = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data
                                                             options:kNilOptions
                                                               error:&error];
    if( !error )
    {
        NSDictionary *JsonDataDictionary = [jsonData objectForKey:@"BeaconsList"];
        for (NSDictionary * dataDict in JsonDataDictionary)
        {
            if ([dataDict[@"UUID"] isEqualToString:uuid] && [dataDict[@"Major"] isEqualToString:major] && [dataDict[@"Minor"] isEqualToString:minor]){
                
                jsonUsername = dataDict[@"Username"];
                jsonPassword = dataDict[@"Password"];
                jsonIPAddress = dataDict[@"IPAddress"];
                
                return YES;
            }
        }
    }
    else {
        NSLog(@"%@", [error description]);
    }
    return NO;
}
@end
