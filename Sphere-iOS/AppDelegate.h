#import <EstimoteSDK/EstimoteSDK.h>

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate, ESTBeaconManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString *beaconnotification;

@property (nonatomic, assign) ESTBeaconManager *beaconManager;
@property (nonatomic, assign) CLBeaconRegion *region;

-(void) initializeBeacon;
-(void) stopBeaconRegioning;
-(NSString*)GetPlistFilePath;

@end
