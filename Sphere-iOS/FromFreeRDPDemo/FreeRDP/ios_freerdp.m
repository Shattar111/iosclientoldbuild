/*
 RDP run-loop
 
 Copyright 2013 Thincast Technologies GmbH, Authors: Martin Fleisz, Dorian Johnson
 
 This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#import <freerdp/utils/event.h>
#import <freerdp/gdi/gdi.h>
#import <freerdp/channels/channels.h>
#import <freerdp/client/channels.h>
#import <freerdp/client/cmdline.h>

#import "ios_freerdp.h"
#import "ios_freerdp_ui.h"
#import "ios_freerdp_events.h"
#import "ios_plugin_loader.h"

#import "RDPSession.h"
#import "Utils.h"

#import "freerdp/client/cliprdr.h"
#import "SPH_ErrorCode.h"
#import "SPH_Constant.h"

extern int freerdp_client_add_dynamic_channel(rdpSettings* settings, int count, char** params);


void cliprdr_process_cb_data_request_event(freerdp* instance);
void process_cliprdr_event(freerdp* instance, wMessage* event);
void cliprdr_process_cb_format_list_event(freerdp* instance,
                                          RDP_CB_FORMAT_LIST_EVENT *event);  //not implemented
void cliprdr_send_data_request(freerdp* instance, UINT32 format);
void cliprdr_process_cb_monitor_ready_event(freerdp* inst);
void cliprdr_process_cb_data_response_event(freerdp* instance,
                                            RDP_CB_DATA_RESPONSE_EVENT *event); //not implemented
void cliprdr_process_text(freerdp* instance, BYTE* data, int len); //not implemented

//__REALWAT__: END


#pragma mark Connection helpers

static BOOL
ios_pre_connect(freerdp * instance)
{
    rdpSettings* settings = instance->settings;
    
    settings->AutoLogonEnabled = settings->Password && (strlen(settings->Password) > 0);
    
    // Verify screen width/height are sane
    if ((settings->DesktopWidth < 64) || (settings->DesktopHeight < 64) || (settings->DesktopWidth > 4096) || (settings->DesktopHeight > 4096))
    {
        //NSLog(@"%s: invalid dimensions %d %d", __func__, settings->DesktopWidth, settings->DesktopHeight);
        return FALSE;
    }
    
   // BOOL bitmap_cache = settings->BitmapCacheEnabled;
    
    settings->OrderSupport[NEG_DSTBLT_INDEX] = TRUE;
    settings->OrderSupport[NEG_PATBLT_INDEX] = TRUE;
    settings->OrderSupport[NEG_SCRBLT_INDEX] = TRUE;
    settings->OrderSupport[NEG_OPAQUE_RECT_INDEX] = TRUE;
    settings->OrderSupport[NEG_DRAWNINEGRID_INDEX] = FALSE;
    settings->OrderSupport[NEG_MULTIDSTBLT_INDEX] = FALSE;
    settings->OrderSupport[NEG_MULTIPATBLT_INDEX] = FALSE;
    settings->OrderSupport[NEG_MULTISCRBLT_INDEX] = FALSE;
    settings->OrderSupport[NEG_MULTIOPAQUERECT_INDEX] = TRUE;
    settings->OrderSupport[NEG_MULTI_DRAWNINEGRID_INDEX] = FALSE;
    settings->OrderSupport[NEG_LINETO_INDEX] = TRUE;
    settings->OrderSupport[NEG_POLYLINE_INDEX] = TRUE;
   // settings->OrderSupport[NEG_MEMBLT_INDEX] = bitmap_cache;
    settings->OrderSupport[NEG_MEM3BLT_INDEX] = TRUE;
   // settings->OrderSupport[NEG_MEMBLT_V2_INDEX] = bitmap_cache;
    settings->OrderSupport[NEG_MEM3BLT_V2_INDEX] = FALSE;
    settings->OrderSupport[NEG_SAVEBITMAP_INDEX] = FALSE;
    settings->OrderSupport[NEG_GLYPH_INDEX_INDEX] = TRUE;
    settings->OrderSupport[NEG_FAST_INDEX_INDEX] = TRUE;
    settings->OrderSupport[NEG_FAST_GLYPH_INDEX] = TRUE;
    settings->OrderSupport[NEG_POLYGON_SC_INDEX] = FALSE;
    settings->OrderSupport[NEG_POLYGON_CB_INDEX] = FALSE;
    settings->OrderSupport[NEG_ELLIPSE_SC_INDEX] = FALSE;
    settings->OrderSupport[NEG_ELLIPSE_CB_INDEX] = FALSE;
    
    settings->FrameAcknowledge = 10;
    
    /******************************************************************/
    /* code to enable and disable sound before rdp is connected */
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //saving username in the memory if the connection is successfull
    
    
    char *p[1] = {"rdpsnd"};
    freerdp_client_add_static_channel(instance->settings, 1, p);
    
    
    NSString* servermicro = [prefs objectForKey:@"ServerMicroEnableDisable"];
    //NSLog(@"Server micro string: %@", servermicro);         //printing what user has typed in
    //microphone redirection  //audio in
    if (NSNotFound != [servermicro rangeOfString:@"rue"].location )
    {
        char *p[1] = {"rdpsnd"};
        p[0] = "audin";
        freerdp_client_add_dynamic_channel(instance->settings, 1, p);
        
    }
    
    /******************************************************************/
    
    
    freerdp_client_load_addins(instance->context->channels, instance->settings);
    
    freerdp_channels_pre_connect(instance->context->channels, instance);
    
    return TRUE;
}

static BOOL ios_post_connect(freerdp* instance)
{
    mfInfo* mfi = MFI_FROM_INSTANCE(instance);
    
    instance->context->cache = cache_new(instance->settings);
    
    // Graphics callbacks
    ios_allocate_display_buffer(mfi);
    instance->update->BeginPaint = ios_ui_begin_paint;
    instance->update->EndPaint = ios_ui_end_paint;
    instance->update->DesktopResize = ios_ui_resize_window;
    
    // Channel allocation
    freerdp_channels_post_connect(instance->context->channels, instance);
    
    [mfi->session performSelectorOnMainThread:@selector(sessionDidConnect) withObject:nil waitUntilDone:YES];
    return TRUE;
}

static int ios_receive_channel_data(freerdp* instance, int channelId, UINT8* data, int size, int flags, int total_size)
{
    return freerdp_channels_data(instance, channelId, data, size, flags, total_size);
}

void ios_process_channel_event(rdpChannels* channels, freerdp* instance)
{
    wMessage* event = freerdp_channels_pop_event(channels);
    
    //__REALWAT__: BEGIN: 13/12/2013: PTS: GC-45
    if (event)
    {
       // fprintf(stderr, "channel_activity_cb: message %d\n", event->id);
        
        switch (GetMessageClass(event->id))
        {
            case CliprdrChannel_Class:
                process_cliprdr_event(instance, event);
                break;
        }
        
        freerdp_event_free(event);
    }
    else
    {
        mfInfo* lpMfi = MFI_FROM_INSTANCE(instance);
        
        if ( NULL == lpMfi )
        {
            return;
        }
        
        // stop progress animation
        [lpMfi->session performSelectorOnMainThread:
         @selector(stopProgressUploadingImage)
                                         withObject:nil
                                      waitUntilDone:YES];
    }
    
    //if (event)
    //  freerdp_event_free(event);
    
    //__REALWAT__ : END
    
}

#pragma mark -
#pragma mark Running the connection

int
ios_run_freerdp(freerdp * instance)
{
    mfContext* context = (mfContext*)instance->context;
    mfInfo* mfi = context->mfi;
    rdpChannels* channels = instance->context->channels;
    
    mfi->connection_state = TSXConnectionConnecting;
    
    if (!freerdp_connect(instance))
    {
        //NSLog(@"%s: inst->rdp_connect failed", __func__);
        return mfi->unwanted ? MF_EXIT_CONN_CANCELED : MF_EXIT_CONN_FAILED;
    }
    
    if (mfi->unwanted)
        return MF_EXIT_CONN_CANCELED;
    
    mfi->connection_state = TSXConnectionConnected;
    
    // Connection main loop
    NSAutoreleasePool* pool;
    int i;
    int fds;
    int max_fds;
    int rcount;
    int wcount;
    void* rfds[32];
    void* wfds[32];
    fd_set rfds_set;
    fd_set wfds_set;
    struct timeval timeout;
    int select_status;
    
    memset(rfds, 0, sizeof(rfds));
    memset(wfds, 0, sizeof(wfds));
    
    while (!freerdp_shall_disconnect(instance))
    {
        rcount = wcount = 0;
        
        pool = [[NSAutoreleasePool alloc] init];
        
        if (freerdp_get_fds(instance, rfds, &rcount, wfds, &wcount) != TRUE)
        {
            //NSLog(@"%s: inst->rdp_get_fds failed", __func__);
            break;
        }
        
        if (freerdp_channels_get_fds(channels, instance, rfds, &rcount, wfds, &wcount) != TRUE)
        {
            //NSLog(@"%s: freerdp_chanman_get_fds failed", __func__);
            break;
        }
        
        if (ios_events_get_fds(mfi, rfds, &rcount, wfds, &wcount) != TRUE)
        {
            //NSLog(@"%s: ios_events_get_fds", __func__);
            break;
        }
        
        max_fds = 0;
        FD_ZERO(&rfds_set);
        FD_ZERO(&wfds_set);
        
        for (i = 0; i < rcount; i++)
        {
            fds = (int)(long)(rfds[i]);
            
            if (fds > max_fds)
                max_fds = fds;
            
            FD_SET(fds, &rfds_set);
        }
        
        if (max_fds == 0)
            break;
        
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;
        
        select_status = select(max_fds + 1, &rfds_set, NULL, NULL, &timeout);
        
        // timeout?
        if (select_status == 0)
        {
            // __REALWAT__: BEGIN: 04/12/2013 : Fix leak memory
            [pool release];
            pool = nil;
            // __REALWAT__: END
            
            continue;
        }
        else if (select_status == -1)
        {
            /* these are not really errors */
            if (!((errno == EAGAIN) ||
                  (errno == EWOULDBLOCK) ||
                  (errno == EINPROGRESS) ||
                  (errno == EINTR))) /* signal occurred */
            {
                //NSLog(@"%s: select failed!", __func__);
                break;
            }
        }
        
        // Check the libfreerdp fds
        if (freerdp_check_fds(instance) != true)
        {
            //NSLog(@"%s: inst->rdp_check_fds failed.", __func__);
            break;
        }
        
        // Check input event fds
        if (ios_events_check_fds(mfi, &rfds_set) != TRUE)
        {
            // This event will fail when the app asks for a disconnect.
            //NSLog(@"%s: ios_events_check_fds failed: terminating connection.", __func__);
            break;
        }
        
        // Check channel fds
        if (freerdp_channels_check_fds(channels, instance) != TRUE)
        {
            //NSLog(@"%s: freerdp_chanman_check_fds failed", __func__);
            break;
        }
        ios_process_channel_event(channels, instance);
        
        [pool release]; pool = nil;
    }
    
    CGContextRelease(mfi->bitmap_context);
    mfi->bitmap_context = NULL;
    mfi->connection_state = TSXConnectionDisconnected;
    
    // Cleanup
    freerdp_channels_close(channels, instance);
    freerdp_disconnect(instance);
    gdi_free(instance);
    cache_free(instance->context->cache);
    
    [pool release]; pool = nil;
    return MF_EXIT_SUCCESS;
}

#pragma mark -
#pragma mark Context callbacks

int ios_context_new(freerdp* instance, rdpContext* context)
{
    mfInfo* mfi = (mfInfo*)calloc(1, sizeof(mfInfo));
    ((mfContext*) context)->mfi = mfi;
    context->channels = freerdp_channels_new();
    ios_events_create_pipe(mfi);
    
    mfi->_context = context;
    mfi->context = (mfContext*)context;
    mfi->context->settings = instance->settings;
    mfi->instance = instance;
    return 0;
}

void ios_context_free(freerdp* instance, rdpContext* context)
{
    mfInfo* mfi = ((mfContext*) context)->mfi;
    freerdp_channels_free(context->channels);
    ios_events_free_pipe(mfi);
    free(mfi);
}

#pragma mark -
#pragma mark Initialization and cleanup

freerdp* ios_freerdp_new()
{
    freerdp* inst = freerdp_new();
    
    inst->PreConnect = ios_pre_connect;
    inst->PostConnect = ios_post_connect;
    inst->Authenticate = ios_ui_authenticate;
    inst->VerifyCertificate = ios_ui_check_certificate;
    inst->VerifyChangedCertificate = ios_ui_check_changed_certificate;
    inst->ReceiveChannelData = ios_receive_channel_data;
    
    inst->ContextSize = sizeof(mfContext);
    inst->ContextNew = ios_context_new;
    inst->ContextFree = ios_context_free;
    freerdp_context_new(inst);
    
    // determine new home path
    NSString* home_path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    free(inst->settings->HomePath);
    free(inst->settings->ConfigPath);
    inst->settings->HomePath = strdup([home_path UTF8String]);
    inst->settings->ConfigPath = strdup([[home_path stringByAppendingPathComponent:@".freerdp"] UTF8String]);
    
    return inst;
}

void ios_freerdp_free(freerdp* instance)
{
    freerdp_context_free(instance);
    freerdp_free(instance);
}

void ios_init_freerdp()
{
    signal(SIGPIPE, SIG_IGN);
    freerdp_channels_global_init();
    freerdp_register_addin_provider(sphere_load_channel_addin_entry, 0);
}

void ios_uninit_freerdp()
{
    freerdp_channels_global_uninit();
}




//__REALWAT__: BEGIN: 13/12/2013: PTS: GC-45

#pragma mark - Freerdp clipboard code / shattar sihra

void process_cliprdr_event(freerdp* instance, wMessage* event)
{
    int lMessageType;
    
    if (event)
    {
        lMessageType = GetMessageType(event->id);
        
        switch (lMessageType)
        {
                /*
                 * Monitor Ready PDU is sent by server to indicate that it has been
                 * initialized and is ready. This PDU is transmitted by the server after it has sent
                 * Clipboard Capabilities PDU
                 */
            case CliprdrChannel_MonitorReady:
                cliprdr_process_cb_monitor_ready_event(instance);
                break;
                
                /*
                 * The Format List PDU is sent either by the client or the server when its
                 * local system clipboard is updated with new clipboard data. This PDU
                 * contains the Clipboard Format ID and name pairs of the new Clipboard
                 * Formats on the clipboard
                 */
            case CliprdrChannel_FormatList:
                cliprdr_process_cb_format_list_event(instance,
                                                     (RDP_CB_FORMAT_LIST_EVENT*) event);
                break;
                
                /*
                 * The Format Data Request PDU is sent by the receipient of the Format List PDU.
                 * It is used to request the data for one of the formats that was listed in the
                 * Format List PDU
                 */
            case CliprdrChannel_DataRequest:
                cliprdr_process_cb_data_request_event(instance);
                break;
                
                /*
                 * The Format Data Response PDU is sent as a reply to the Format Data Request PDU.
                 * It is used to indicate whether processing of the Format Data Request PDU
                 * was successful. If the processing was successful, the Format Data Response PDU
                 * includes the contents of the requested clipboard data
                 */
            case CliprdrChannel_DataResponse:
                cliprdr_process_cb_data_response_event(instance,
                                                       (RDP_CB_DATA_RESPONSE_EVENT*) event);
                break;
                
            default:
              //  printf("process_cliprdr_event: unknown event type %d\n",
                     //  GetMessageType(event->id));
                break;
        }
    }
}

void cliprdr_process_cb_data_request_event(freerdp* instance)
{
    
    int                           lLen          = 0;
    RDP_CB_DATA_RESPONSE_EVENT  * lpEvent       = nil;
    mfInfo                      * lpMfi         = MFI_FROM_INSTANCE(instance);
    NSString                    * lpData        = @"";
    UIImage                     * lpImage       = nil;
    NSData                      * lpImageData   = nil;
    NSArray                     * lpImages      = nil;
    NSArray                     * lpPasteStrings= nil;
    tSPH_ErrorCode                lRetun;
    BOOL                          lStatus       = NO;
    BOOL                          lError        = NO;
    NSString                    * lpMsg         = nil;
    //__REALWAT__: BEGIN: 05/02/2014
    id                            lImg          = nil;
    //__REALWAT__: BEGIN: 06/02/2014
    UIImage                     *lpNewImage     = nil;
    // create data response event
    lpEvent = (RDP_CB_DATA_RESPONSE_EVENT*) freerdp_event_new(CliprdrChannel_Class, CliprdrChannel_DataResponse, NULL, NULL);
    
    // get pasteboard's images
    lpImages = [lpMfi->session.pasteboard_rd images];
    
    if (0 < lpImages)
    {
        lImg = [lpImages lastObject];
        if (YES == [lImg isKindOfClass:[NSData class]])
        {
            lpImage = [UIImage imageWithData:lImg];
            // NSLog(@"IImage type = %@",[lImg class]);
        }
        else if(YES == [lImg isKindOfClass:[UIImage class]])
        {
            lpImage = lImg;
            // NSLog(@"IImage type 1 = %@",[lImg class]);
        }
        else
        {
            SPH_LOG_CONSOL(SPH_ERROR_INVALID_TYPE);
            //  NSLog(@"IImage type 2 = %@",[lImg class]);
            return;
        }
        if (nil == lpImage)
        {
            SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
            return;
        }
        
        // check image resolution
        lRetun = [lpMfi->session checkExceedImageResolution:lpImage
                                                     output:&lpNewImage];
        if (SPH_SUCCESS != lRetun)
        {
            SPH_LOG_CONSOL(lRetun);
            return;
        }
        if (nil == lpNewImage)
        {
            SPH_LOG_CONSOL(SPH_ERROR_OBJECT_NULL);
            return;
        }
        
        lRetun = [lpMfi->session checkExceedImageSize:lpNewImage
                                               output:&lStatus];
        
        if ( ( SPH_SUCCESS != lRetun ) || ( YES == lStatus) )
        {
            lError = YES;
            lpMsg  = SPH_UPLOAD_ALERT_MSG_SIZE;
        }
        
        if ( YES == lError )
        {
            lpEvent->data = NULL;
            lpEvent->size = 0;
            
            // send pasteboard data event to server
            freerdp_channels_send_event(instance->context->channels,
                                        (wMessage*) lpEvent);
            
            [lpMfi->session performSelectorOnMainThread:@selector(showExceedImageSizeAlert:)
                                             withObject:lpMsg
                                          waitUntilDone:NO];
            
            return;
        }
        
        lpImageData = [lpMfi->session imageAsBMP32:lpNewImage includeHeader:NO];
        
        if ( 0 >= lpImageData.length )
        {
            return;
        }
        
        lLen = (int) ([lpImageData length]);
        
        // set event data
        lpEvent->data = malloc(lLen);
        
        memcpy(lpEvent->data, [lpImageData bytes], lLen);
        
        // start progress animation
        [lpMfi->session performSelectorOnMainThread:@selector(startProgressUploadingImage)
                                         withObject:nil
                                      waitUntilDone:NO];
        
    }
    else
    {
        // get pasteboard strings
        lpPasteStrings = [lpMfi->session.pasteboard_rd strings];
        
        if ( 0 < lpPasteStrings.count )
        {
            lpData = [lpPasteStrings lastObject];
        }
        
        if( 0 >= lpData.length )
        {
            return;
        }
        
        lLen = (int) ([lpData length] * 2 + 2);
        
        // set event data
        lpEvent->data = malloc(lLen);
        [lpData getCString:(char *) lpEvent->data
                 maxLength:lLen
                  encoding:NSUnicodeStringEncoding];
    }
    //__REALWAT__: END: 06/02/2014
    // set event data size
    lpEvent->size = lLen;
    //__REALWAT__: END: 05/02/2014
    
    // send pasteboard data event to server
    freerdp_channels_send_event(instance->context->channels,
                                (wMessage*) lpEvent);
    
    
}


/** *********************************************************************
 * called when channel data is available
 ***********************************************************************/


int process_plugin_args(rdpSettings* settings, const char* name, RDP_PLUGIN_DATA* plugin_data, void* user_data)
{
    rdpChannels* channels = (rdpChannels*) user_data;
    
    freerdp_channels_load_plugin(channels, settings, name, plugin_data);
    
    return 1;
}

void cliprdr_send_supported_format_list(freerdp* instance, UINT32 formatType)
{
    RDP_CB_FORMAT_LIST_EVENT* event;
    
    event = (RDP_CB_FORMAT_LIST_EVENT*) freerdp_event_new(CliprdrChannel_Class, CliprdrChannel_FormatList, NULL, NULL);
    
    // set paste data format list
    event->formats = (UINT32*) malloc(sizeof(UINT32) * 1);
    event->num_formats = 1;
    event->formats[0] = formatType;
    
    // set cliprdr format list to server
    freerdp_channels_send_event(instance->context->channels, (wMessage*) event);
}

void cliprdr_process_cb_monitor_ready_event(freerdp* instance)
{
    wMessage* event;
    RDP_CB_FORMAT_LIST_EVENT* format_list_event;
    
    event = freerdp_event_new(CliprdrChannel_Class, CliprdrChannel_FormatList, NULL, NULL);
    
    format_list_event = (RDP_CB_FORMAT_LIST_EVENT*) event;
    format_list_event->num_formats = 0;
    
    freerdp_channels_send_event(instance->context->channels, event);
}
/*
 * stuff related to clipboard redirection
 */

/**
 * remote system has requested clipboard data from local system
 */

void cliprdr_send_data_request(freerdp* instance, UINT32 format)
{
    RDP_CB_DATA_REQUEST_EVENT* event;
    
    event = (RDP_CB_DATA_REQUEST_EVENT*) freerdp_event_new(CliprdrChannel_Class, CliprdrChannel_DataRequest, NULL, NULL);
    
    event->format = format;
    freerdp_channels_send_event(instance->context->channels, (wMessage*) event);
}

/**
 * list of supported clipboard formats; currently only the following are supported
 *    CB_FORMAT_TEXT
 *    CB_FORMAT_UNICODETEXT
 */

void cliprdr_process_cb_format_list_event(freerdp* instance,
                                          RDP_CB_FORMAT_LIST_EVENT* event)
{
    int        i;
    mfInfo     *mfi     = MFI_FROM_INSTANCE(instance);
    RDPSession *session = mfi->session;
    
    if (event->num_formats == 0)
        return;
    
    for (i = 0; i < event->num_formats; i++)
    {
        switch (event->formats[i])
        {
            case CB_FORMAT_RAW:
                printf("CB_FORMAT_RAW: not yet supported\n");
                break;
                
            case CB_FORMAT_TEXT:
            case CB_FORMAT_UNICODETEXT:
                session.pasteboard_format = CB_FORMAT_UNICODETEXT;
                cliprdr_send_data_request(instance, CB_FORMAT_UNICODETEXT);
                return;
                break;
                
            case CB_FORMAT_DIB:
                printf("CB_FORMAT_DIB: not yet supported\n");
                break;
                
            case CB_FORMAT_HTML:
                printf("CB_FORMAT_HTML\n");
                break;
                
            case CB_FORMAT_PNG:
                printf("CB_FORMAT_PNG: not yet supported\n");
                break;
                
            case CB_FORMAT_JPEG:
                printf("CB_FORMAT_JPEG: not yet supported\n");
                break;
                
            case CB_FORMAT_GIF:
                printf("CB_FORMAT_GIF: not yet supported\n");
                break;
        }
    }
}

/**
 * at the moment, only the following formats are supported
 *    CB_FORMAT_TEXT
 *    CB_FORMAT_UNICODETEXT
 */

void cliprdr_process_cb_data_response_event(freerdp* instance,
                                            RDP_CB_DATA_RESPONSE_EVENT* event)
{
    NSString    *lpPasteStr;
    mfInfo      *lpMfi      = MFI_FROM_INSTANCE(instance);
    RDPSession  *lpSession = lpMfi->session;
    
    if (event->size == 0)
        return;
    
    if ( (CB_FORMAT_TEXT        == lpSession.pasteboard_format) ||
        (CB_FORMAT_UNICODETEXT == lpSession.pasteboard_format))
    {
        lpPasteStr = [[NSString alloc] initWithCharacters:(unichar *) event->data
                                                   length:event->size / 2];
        
        [lpMfi->session.pasteboard_wr setString:lpPasteStr];
    }
    
}

//__REALWAT__: END
