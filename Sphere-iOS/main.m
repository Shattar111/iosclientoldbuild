//
//  main.m
//  Sphere-iOS
//
//  Created by Dima on 3/6/14.
//
//

#import <UIKit/UIKit.h>

#import "MyApplication.h"
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([MyApplication class]), NSStringFromClass([AppDelegate class]));
    }
}
