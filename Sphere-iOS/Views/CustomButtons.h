//
//  CustomButtons.h
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2015-10-29.
//
//

#import <UIKit/UIKit.h>

@interface CustomButtons : UIButton
@property  (nonatomic, assign) CGFloat hue;
@property  (nonatomic, assign) CGFloat saturation;
@property  (nonatomic, assign) CGFloat brightness;
@end
