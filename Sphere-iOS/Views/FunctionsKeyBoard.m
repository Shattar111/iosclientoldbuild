/******************************************************************************
 * v. 0.9.5  09 MAY 2013
 * Filename  LNNumberpad.m
 * Project:  LNNumberpad
 * Purpose:  Class to display a custom LNNumberpad on an iPad and properly handle
 *           the text input.
 * Author:   Louis Nafziger
 *
 * Copyright 2012 - 2013 Louis Nafziger
 ******************************************************************************
 *
 * This file is part of LNNumberpad.
 *
 * COPYRIGHT 2012 - 2013 Louis Nafziger
 *
 * LNNumberpad is free software: you can redistribute it and/or modify
 * it under the terms of the The MIT License (MIT).
 *
 * LNNumberpad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * The MIT License for more details.
 *
 * You should have received a copy of the The MIT License (MIT)
 * along with LNNumberpad.  If not, see <http://opensource.org/licenses/MIT>.
 *
 *****************************************************************************/

#import "FunctionsKeyBoard.h"

#pragma mark - Private methods

@interface FunctionsKeyBoard ()

@property (nonatomic, strong) UIResponder <UITextInput> *targetTextInput;

@end

#pragma mark - LNNumberpad Implementation

@implementation FunctionsKeyBoard

@synthesize targetTextInput;

#pragma mark - Shared LNNumberpad method

+ (FunctionsKeyBoard *)defaultLNNumberpad {
    static FunctionsKeyBoard *defaultLNNumberpad = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        defaultLNNumberpad = [[[NSBundle mainBundle] loadNibNamed:@"FunctionsKeyBoard" owner:self options:nil] objectAtIndex:0];
    });
    
    return defaultLNNumberpad;
}

#pragma mark - view lifecycle

- (void)awakeFromNib
{
    [self orientationChanged];
    
    
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addObservers];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self addObservers];
    }
    return self;
}

- (void)addObservers {
    //Detect Device rotation
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    // Keep track of the textView/Field that we are editing
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(editingDidBegin:)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(editingDidBegin:)
                                                 name:UITextViewTextDidBeginEditingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(editingDidEnd:)
                                                 name:UITextFieldTextDidEndEditingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(editingDidEnd:)
                                                 name:UITextViewTextDidEndEditingNotification
                                               object:nil];
}

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIDeviceOrientationDidChangeNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidBeginEditingNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidBeginEditingNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidEndEditingNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidEndEditingNotification
                                                  object:nil];
    self.targetTextInput = nil;
    
    [super dealloc];
    
}

- (void) orientationChanged
{
    UIInterfaceOrientation iOrientation = [UIApplication sharedApplication].statusBarOrientation;
    
    switch (iOrientation)
    {
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            
            [self HideButtons:NO];
            break;
            
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            
            [self HideButtons:YES];
            break;
            
        default:
            break;
    };
}

-(void)HideButtons:(BOOL)hide{
    for (UIButton *button in self.subviews)
    {
        if ([button isKindOfClass:[UIButton class]])
        {
            if (button.tag >= 80) //Buttons tagged from 80 to 103 for combinations and arrows
            {
                [button setHidden:hide];
            }
        }
    }
    
}
#pragma mark - editingDidBegin/End

// Editing just began, store a reference to the object that just became the firstResponder
- (void)editingDidBegin:(NSNotification *)notification {
    if ([notification.object isKindOfClass:[UIResponder class]])
    {
        if ([notification.object conformsToProtocol:@protocol(UITextInput)]) {
            self.targetTextInput = notification.object;
            return;
        }
    }
    
    // Not a valid target for us to worry about.
    self.targetTextInput = nil;
}

// Editing just ended.
- (void)editingDidEnd:(NSNotification *)notification {
    self.targetTextInput = nil;
}

#pragma mark - Keypad IBAction's

// A number (0-9) was just pressed on the number pad
// Note that this would work just as well with letters or any other character and is not limited to numbers.
- (IBAction)numberpadNumberPressed:(UIButton *)sender {
    if (self.targetTextInput) {
        NSString *numberPressed  = sender.titleLabel.text;
        if ([numberPressed length] > 0) {
            UITextRange *selectedTextRange = self.targetTextInput.selectedTextRange;
            if (selectedTextRange) {
                [self textInput:self.targetTextInput replaceTextAtTextRange:selectedTextRange withString:numberPressed];
            }
        }
        
        else if (numberPressed == nil){ //For Space bar
            UITextRange *selectedTextRange = self.targetTextInput.selectedTextRange;
            [self textInput:self.targetTextInput replaceTextAtTextRange:selectedTextRange withString:@" "];
        }
    }
}

// The delete button was just pressed on the number pad
- (IBAction)numberpadDeletePressed:(UIButton *)sender {
    if (self.targetTextInput) {
        UITextRange *selectedTextRange = self.targetTextInput.selectedTextRange;
        if (selectedTextRange) {
            // Calculate the selected text to delete
            UITextPosition  *startPosition  = [self.targetTextInput positionFromPosition:selectedTextRange.start offset:-1];
            if (!startPosition) {
                return;
            }
            UITextPosition  *endPosition    = selectedTextRange.end;
            if (!endPosition) {
                return;
            }
            UITextRange     *rangeToDelete  = [self.targetTextInput textRangeFromPosition:startPosition
                                                                               toPosition:endPosition];
            
            [self textInput:self.targetTextInput replaceTextAtTextRange:rangeToDelete withString:@""];
        }
    }
}
- (IBAction)functionKeysPressed:(UIButton*)sender {
    if (self.targetTextInput){
        NSString *numberPressed  = sender.titleLabel.text;
        
        ((void (^)())@{
                       @"F1" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F1];},
                       @"F2" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F2];},
                       @"F3" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F3];},
                       @"F4" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F4];},
                       @"F5" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F5];},
                       @"F6" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F6];},
                       @"F7" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F7];},
                       @"F8" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F8];},
                       @"F9" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F9];},
                       @"F10" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F10];},
                       @"F11" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F11];},
                       @"F12" : ^{[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:VK_F12];
            //[[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyF1Plus];
        },
                       
                       }[numberPressed] ?: ^{
                           NSLog(@"default");
                       })();
    }
}
- (IBAction)keysHomePressed:(id)sender {
    UIButton *btn = sender;
    
    if (btn.tag>=1 && btn.tag<=18)
    {
        switch (btn.tag) {
            case 1:
                [[RDPKeyboard getSharedRDPKeyboard] Directories];
                break;
            case 2:
                [[RDPKeyboard getSharedRDPKeyboard] Headset];

                break;
            case 3:
                [[RDPKeyboard getSharedRDPKeyboard] QuickSearch];

                break;
            case 4:
                [[RDPKeyboard getSharedRDPKeyboard] Settings];

                break;
            case 5:
                [[RDPKeyboard getSharedRDPKeyboard] SpeakerPhone];

                break;
            case 6:
                [[RDPKeyboard getSharedRDPKeyboard] OpenMenu];
                break;
            case 7:
                [[RDPKeyboard getSharedRDPKeyboard] Services];
                break;
            case 8:
                [[RDPKeyboard getSharedRDPKeyboard] ToggleMute];
                break;
            case 9:
                [[RDPKeyboard getSharedRDPKeyboard] OnlineHelp];

                break;
            case 10:
                [[RDPKeyboard getSharedRDPKeyboard] VoiceMessageSystem];
                break;
            case 11:
                [[RDPKeyboard getSharedRDPKeyboard] Preferences];
                break;
            case 12:
                [[RDPKeyboard getSharedRDPKeyboard] CloseCommunicator];

                break;
            case 13:
                [[RDPKeyboard getSharedRDPKeyboard] ExitCommunicator];
                break;
            case 14:
                [[RDPKeyboard getSharedRDPKeyboard] HangsUpCall];
                break;
            case 15:
                [[RDPKeyboard getSharedRDPKeyboard] AnswerACall];
                break;
            case 16:
                [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:(VK_RETURN| KBDEXT)];
                break;
            case 17:
                [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:(VK_PRIOR | KBDEXT)];

                break;
            case 18:
                [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:(VK_NEXT| KBDEXT)];
                break;
            default:
                break;
        }
    }
}

- (IBAction)keysArrowsPressed:(id)sender {
    UIButton *btn = sender;
    
    if (btn.tag>=80 && btn.tag<=83)
    {
        switch (btn.tag) {
            case 80:
                [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:(VK_UP | KBDEXT)];
                break;
            case 81:
                [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:(VK_LEFT | KBDEXT)];
                break;
            case 82:
                [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:(VK_DOWN | KBDEXT)];
                break;
            case 83:
                [[RDPKeyboard getSharedRDPKeyboard] sendVirtualKeyCode:(VK_RIGHT| KBDEXT)];
                break;
                
            default:
                break;
        }
    }
}


// The Enter button was just pressed on the number pad
- (IBAction)numberpadEnterPressed:(UIButton *)sender {
    [[RDPKeyboard getSharedRDPKeyboard] sendEnterKeyStroke];
}

// The done button was just pressed on the number pad
- (IBAction)numberpadDonePressed:(UIButton *)sender {
    if (self.targetTextInput) {
        [self.targetTextInput resignFirstResponder];
    }
}

#pragma mark - text replacement routines

// Check delegate methods to see if we should change the characters in range
- (BOOL)textInput:(id <UITextInput>)textInput shouldChangeCharactersInRange:(NSRange)range withString:(NSString *)string {
    if (textInput) {
        if ([textInput isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField *)textInput;
            if ([textField.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
                if ([textField.delegate textField:textField
                    shouldChangeCharactersInRange:range
                                replacementString:string]) {
                    return YES;
                }
            } else {
                // Delegate does not respond, so default to YES
                return YES;
            }
        } else if ([textInput isKindOfClass:[UITextView class]]) {
            UITextView *textView = (UITextView *)textInput;
            if ([textView.delegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
                if ([textView.delegate textView:textView
                        shouldChangeTextInRange:range
                                replacementText:string]) {
                    return YES;
                }
            } else {
                // Delegate does not respond, so default to YES
                return YES;
            }
        }
    }
    return NO;
}

// Replace the text of the textInput in textRange with string if the delegate approves
- (void)textInput:(id <UITextInput>)textInput replaceTextAtTextRange:(UITextRange *)textRange withString:(NSString *)string {
    if (textInput) {
        if (textRange) {
            // Calculate the NSRange for the textInput text in the UITextRange textRange:
            int startPos                    = [textInput offsetFromPosition:textInput.beginningOfDocument
                                                                 toPosition:textRange.start];
            int length                      = [textInput offsetFromPosition:textRange.start
                                                                 toPosition:textRange.end];
            NSRange selectedRange           = NSMakeRange(startPos, length);
            
            if ([self textInput:textInput shouldChangeCharactersInRange:selectedRange withString:string]) {
                // Make the replacement:
                [textInput replaceRange:textRange withText:string];
            }
        }
    }
}

@end
