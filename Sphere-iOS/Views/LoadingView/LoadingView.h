//
//  LoadingView.h
//
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  LoadingView sets Active/inActive of the current loading stage.




@import UIKit;

@interface LoadingView : UIView

@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel*label;

@property (nonatomic, strong) NSString *animatedImageName;

-(void)startAnimatingWithAnimationImages:(NSArray*)images;
-(void)stopAnimating;

- (void)reset;

- (void)setActive:(BOOL)active;

@end
