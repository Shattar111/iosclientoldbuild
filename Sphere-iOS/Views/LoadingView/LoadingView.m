//
//  LoadingView.m
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-17.
//
//

#import "LoadingView.h"

@interface LoadingView ()
@property (nonatomic, strong) UIImage *inactiveImage;
@end

@implementation LoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.inactiveImage = self.imageView.image;
}

-(void)startAnimatingWithAnimationImages:(NSArray*)images {
    self.imageView.animationImages = images;
    [self.imageView startAnimating];
    
    self.label.highlighted = YES;
}

-(void)stopAnimating {
    [self.imageView stopAnimating];
    self.imageView.image = [self.imageView.animationImages lastObject];
}

- (void)reset {
    [self stopAnimating];
    self.imageView.image = self.inactiveImage;
    self.label.highlighted = NO;
    [self setActive:NO];
}

- (void)setActive:(BOOL)active {
    if (active) {
        self.label.font = [UIFont fontWithName:@"Avenir-Medium" size:22.0f];
    } else {
        self.label.font = [UIFont fontWithName:@"Avenir-Roman" size:22.0f];
    }
}


@end
