//
//  ProgressDotsView.m
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-17.
//
//

#import "ProgressDotsView.h"

@interface ProgressDotsView ()
@property (nonatomic, retain) NSArray *dots;
@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, assign) NSUInteger nextIndex;
@end

@implementation ProgressDotsView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    NSMutableArray *dotsArray = [NSMutableArray array];
    dotsArray[0] = self.dot1ImageView;
    dotsArray[1] = self.dot2ImageView;
    dotsArray[2] = self.dot3ImageView;
    if (nil != self.dot4ImageView) {
        dotsArray[3] = self.dot4ImageView;
    }
    self.dots = dotsArray;
    self.nextIndex = 0;
}

-(void)startAnimating
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(updateDot) userInfo:nil repeats:YES];
}

-(void)stopAnimating
{
    [self.timer invalidate];
    self.timer = nil;
    for (UIImageView *v in self.dots) {
        v.highlighted = YES;
    }
}

- (void)reset {
    for (UIImageView *v in self.dots) {
        v.highlighted = NO;
    }
}

-(void)updateDot
{
    NSUInteger dotsCount = [self.dots count];
    if (dotsCount <= self.nextIndex) {
        for (UIImageView *v in self.dots) {
            v.highlighted = NO;
        }
        self.nextIndex = 0;
    } else {
        UIImageView *dot = (UIImageView *) self.dots[self.nextIndex];
        dot.highlighted = YES;
        self.nextIndex++;
    }
}
@end
