//
//  ProgressDotsView.h
//
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  ProgressDotsView sets up the animation of loading/progress dots of the Main Story board .




@import UIKit;

@interface ProgressDotsView : UIView

@property (nonatomic, strong) IBOutlet UIImageView *dot1ImageView;
@property (nonatomic, strong) IBOutlet UIImageView *dot2ImageView;
@property (nonatomic, strong) IBOutlet UIImageView *dot3ImageView;
@property (nonatomic, strong) IBOutlet UIImageView *dot4ImageView;

- (void)startAnimating;
- (void)stopAnimating;
- (void)reset;

@end
