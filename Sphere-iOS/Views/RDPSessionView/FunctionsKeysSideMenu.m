//
//  FunctionsKeysSideMenu.m
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2015-03-23.
//
//

#import "FunctionsKeysSideMenu.h"

@interface FunctionsKeysSideMenu ()

@end

@implementation FunctionsKeysSideMenu
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 1. Load the .xib file .xib file must match classname
        NSString *className = NSStringFromClass([self class]);
        _customView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        
        // 2. Set the bounds if not set by programmer (i.e. init called)
//        if(CGRectIsEmpty(frame)) {
//            self.bounds = _customView.bounds;
//        }
        
        // 3. Add as a subview
        _customView.frame = CGRectMake(0, 0, 170, 705);
        [self addSubview:_customView];
    }
    return self;
}

//- (id)initWithCoder:(NSCoder *)aDecoder {
//    self = [super initWithCoder:aDecoder];
//    if(self) {
//        
//        // 1. Load .xib file
//        NSString *className = NSStringFromClass([self class]);
//        _customView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
//        
//        // 2. Add as a subview
//        [self addSubview:_customView];
//        
//    }
//    return self;
//}


- (IBAction)btnFunctionsKeys:(id)sender {
    if ([_delegate respondsToSelector:@selector(pressedKeysFunctions:)])
        [_delegate pressedKeysFunctions:sender];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
