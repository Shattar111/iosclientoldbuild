//
//  FunctionsKeysSideMenu.h
//  Sphere-iOS
//
//  Created by Khaled Murshed on 2015-03-23.
//
//  The purpose of this call is to display the side menu functions F1-F12

#import <UIKit/UIKit.h>

@protocol FunctionsKeysViewDelegate;

@interface FunctionsKeysSideMenu : UIView

@property (assign, nonatomic) id<FunctionsKeysViewDelegate, NSObject> delegate;
@property (nonatomic, strong) FunctionsKeysSideMenu *customView;

@property (nonatomic, strong) IBOutlet UIView *view;

// delegate

@end

@protocol FunctionsKeysViewDelegate <NSObject>
-(void) pressedKeysFunctions:(UIButton*) sender;
@end