/*
  KeyboardViewController.h

  Glassware Connect

  Created by Sphere 3D 2015

  KeyboardToolbarView handles the Keyboard tool bar in the RDP session.
  Contains following button:
  
  - ESC
  - Control
  - Alt
  - Shift
  
  - 
 */




#import <UIKit/UIKit.h>

@protocol KeyboardToolbarViewDelegate;

@interface KeyboardToolbarView : UIView

@property (assign, nonatomic) id<KeyboardToolbarViewDelegate, NSObject> delegate;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *ctrlButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *altButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *shiftButtons;

@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *landscapeRightView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UIView *portraitRightView;
@property (strong, nonatomic) IBOutlet UIView *splitView;
@property (strong, nonatomic) IBOutlet UIView *splitRightView;


@property (retain, nonatomic) IBOutlet UISegmentedControl *KeyB1;
@property (retain, nonatomic) IBOutlet UISegmentedControl *KeyB2;
@property (retain, nonatomic) IBOutlet UISegmentedControl *KeyB3;

@property (nonatomic) BOOL escapeSelected;
@property (nonatomic) BOOL tabSelected;
@property (nonatomic) BOOL ctrlSelected;
@property (nonatomic) BOOL altSelected;
@property (nonatomic) BOOL shiftSelected;


+ (instancetype)keyboardToolbar;

-(IBAction)escapeKeyClicked:(id)sender;
-(IBAction)tabKeyClicked:(id)sender;
-(IBAction)controlKeyClicked:(id)sender;
-(IBAction)altKeyClicked:(id)sender;
-(IBAction)shiftKeyClicked:(id)sender;


@end

@protocol KeyboardToolbarViewDelegate <NSObject>
@optional
- (void)keyboardToolbarViewDidClickEscButton:(KeyboardToolbarView*)keyboardToolbar;
- (void)keyboardToolbarViewDidClickTabButton:(KeyboardToolbarView*)keyboardToolbar;
- (void)keyboardToolbarView:(KeyboardToolbarView*)keyboardToolbar didToggleCtrlButton:(BOOL)isOn;
- (void)keyboardToolbarView:(KeyboardToolbarView*)keyboardToolbar didToggleAltButton:(BOOL)isOn;
- (void)keyboardToolbarView:(KeyboardToolbarView*)keyboardToolbar didToggleShiftButton:(BOOL)isOn;


- (void)keyboardToolbarViewDidClickDelButton:(KeyboardToolbarView*)keyboardToolbar;

//Custom Keyboards
- (void)keyboardToolbarViewDidClickNumberKeyButton:(KeyboardToolbarView*)keyboardToolbar;
- (void)keyboardToolbarViewDidClickStandardKeyButton:(KeyboardToolbarView*)keyboardToolbar;

@end