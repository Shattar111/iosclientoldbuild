//
//  LeftKeyboardViewController.m
//  IE on Demand
//
//  Created by DX114-XL on 2013-10-22.
//
//

#import "KeyboardToolbarView.h"
#import "UIDeviceHardware.h"

#define iOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define iOS10 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)

#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])


@interface KeyboardToolbarView ()
@end

@implementation KeyboardToolbarView

+ (instancetype)keyboardToolbar {
    UINib *nib;
   /* if (7.0 <= [[UIDevice currentDevice].systemVersion floatValue]) {
        nib = [UINib nibWithNibName:@"KeyboardToolbarView-iOS7" bundle:nil];
    } else {
        nib = [UINib nibWithNibName:@"KeyboardToolbarView-iOS6" bundle:nil];
    }*/
    
    //nib = IS_IPAD? [UINib nibWithNibName:@"KeyboardToolbarView-iOS6" bundle:nil] : [UINib nibWithNibName:@"KeyboardToolbarView-iPhone" bundle:nil];
    
    nib = IS_IPAD? [UINib nibWithNibName:@"KeyboardToolbarView-New" bundle:nil] : [UINib nibWithNibName:@"KeyboardToolbarView-iPhone" bundle:nil];
    
    NSArray *views = [nib instantiateWithOwner:nil options:nil];
    for (id v in views) {
        if ([v isKindOfClass:[KeyboardToolbarView class]]) {
            KeyboardToolbarView *keyboardToolbarView = (KeyboardToolbarView*)v;
            [keyboardToolbarView addSubview:keyboardToolbarView.landscapeView];
            [keyboardToolbarView addSubview:keyboardToolbarView.landscapeRightView];
            [keyboardToolbarView addSubview:keyboardToolbarView.portraitView];
            [keyboardToolbarView addSubview:keyboardToolbarView.portraitRightView];
            [keyboardToolbarView addSubview:keyboardToolbarView.splitView];
            [keyboardToolbarView addSubview:keyboardToolbarView.splitRightView];
            keyboardToolbarView.landscapeView.hidden = YES;
            keyboardToolbarView.landscapeRightView.hidden = YES;
            keyboardToolbarView.portraitView.hidden = YES;
            keyboardToolbarView.portraitRightView.hidden = YES;
            keyboardToolbarView.splitView.hidden = YES;
            keyboardToolbarView.splitRightView.hidden = NO;
            
            if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
                CGRect frame1= keyboardToolbarView.KeyB1.frame;
                CGRect frame2= keyboardToolbarView.KeyB2.frame;
                CGRect frame3= keyboardToolbarView.KeyB3.frame;
                [keyboardToolbarView.KeyB1 setFrame:CGRectMake(frame1.origin.x, frame1.origin.y, frame1.size.width, 39)];
                [keyboardToolbarView.KeyB2 setFrame:CGRectMake(frame2.origin.x, frame2.origin.y, frame2.size.width, 39)];
                [keyboardToolbarView.KeyB2 setFrame:CGRectMake(frame3.origin.x, frame3.origin.y, frame3.size.width, 39)];
            }
            
            return v;
        }
    }
    NSAssert(false, @"Should always return valid instance");
    return nil;
}

#pragma mark - view management

- (void)willMoveToSuperview:(UIView *)newSuperview {
    if (nil != newSuperview) {
        [self subscribeToKeyboardNotifications];
    }
    [super willMoveToSuperview:newSuperview];
}

- (void)removeFromSuperview {
    [self unsubscribeToKeyboardNotifications];
    [super removeFromSuperview];
}


#pragma mark - IBActions

-(IBAction)escapeKeyClicked:(id)sender {
    if (nil != self.delegate && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickEscButton:)]) {
        [self.delegate keyboardToolbarViewDidClickEscButton:self];
    }
}

-(IBAction)tabKeyClicked:(id)sender {
    if (nil != self.delegate && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickTabButton:)]) {
        [self.delegate keyboardToolbarViewDidClickTabButton:self];
    }
}

-(IBAction)delKeyClicked:(id)sender {
    // notify delegate didClickedControlKey
    
    if(self.ctrlSelected && self.altSelected)
    {
    //not aloud to hit control alt delete....
    }
    else
    {
        if (nil != self.delegate && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickDelButton:)]) {
            [self.delegate keyboardToolbarViewDidClickDelButton:self];
        }
    }

}

-(IBAction)controlKeyClicked:(id)sender {
    // notify delegate didClickedControlKey

    self.ctrlSelected = !self.ctrlSelected;
    for(UIButton *ctrlBtn in self.ctrlButtons)
    {
        ctrlBtn.selected = self.ctrlSelected;
    }
    
    if (nil != self.delegate && [self.delegate respondsToSelector:@selector(keyboardToolbarView:didToggleCtrlButton:)]) {
        [self.delegate keyboardToolbarView:self didToggleCtrlButton:self.ctrlSelected];
    }
}

-(IBAction)altKeyClicked:(id)sender
{

    
    self.altSelected = !self.altSelected;
    for(UIButton *altBtn in self.altButtons)
    {
        altBtn.selected = self.altSelected;
    }
    
    if (nil != self.delegate && [self.delegate respondsToSelector:@selector(keyboardToolbarView:didToggleAltButton:)]) {
        [self.delegate keyboardToolbarView:self didToggleAltButton:self.altSelected];
    }
    
    
}


-(IBAction)shiftKeyClicked:(id)sender;
{

    
    self.shiftSelected = !self.shiftSelected;
    for(UIButton *shifttBtn in self.shiftButtons)
    {
        shifttBtn.selected = self.shiftSelected;
    }
    
    if (nil != self.delegate && [self.delegate respondsToSelector:@selector(keyboardToolbarView:didToggleShiftButton:)]) {
        [self.delegate keyboardToolbarView:self didToggleShiftButton:self.shiftSelected];
    }
}


- (IBAction)segKeyboardSelected1:(id)sender {
    if(_KeyB1.selectedSegmentIndex == 0){
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickStandardKeyButton:)]) {
            
            [self.delegate keyboardToolbarViewDidClickStandardKeyButton:self];
            _KeyB2.selectedSegmentIndex = 0;
            _KeyB3.selectedSegmentIndex = 0;
        }
        
    }
    if(_KeyB1.selectedSegmentIndex == 1){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickNumberKeyButton:)]) {
            
            [self.delegate keyboardToolbarViewDidClickNumberKeyButton:self];
            _KeyB2.selectedSegmentIndex = 1;
            _KeyB3.selectedSegmentIndex = 1;
        }
    }
}

- (IBAction)segKeyboardSelected2:(id)sender {
    if(_KeyB2.selectedSegmentIndex == 0){
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickStandardKeyButton:)]) {
            
            [self.delegate keyboardToolbarViewDidClickStandardKeyButton:self];
            _KeyB1.selectedSegmentIndex = 0;
            _KeyB3.selectedSegmentIndex = 0;
        }
        
    }
    if(_KeyB2.selectedSegmentIndex == 1){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickNumberKeyButton:)]) {
            
            [self.delegate keyboardToolbarViewDidClickNumberKeyButton:self];
            _KeyB1.selectedSegmentIndex = 1;
            _KeyB3.selectedSegmentIndex = 1;
        }
    }
}

- (IBAction)segKeyboardSelected3:(id)sender {
    if(_KeyB3.selectedSegmentIndex == 0){
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickStandardKeyButton:)]) {
            
            [self.delegate keyboardToolbarViewDidClickStandardKeyButton:self];
            _KeyB2.selectedSegmentIndex = 0;
            _KeyB1.selectedSegmentIndex = 0;
        }
        
    }
    if(_KeyB3.selectedSegmentIndex == 1){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(keyboardToolbarViewDidClickNumberKeyButton:)]) {
            
            [self.delegate keyboardToolbarViewDidClickNumberKeyButton:self];
            _KeyB2.selectedSegmentIndex = 1;
            _KeyB1.selectedSegmentIndex = 1;
        }
    }
}

#pragma mark - Keyboard notification

- (void)subscribeToKeyboardNotifications {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardToolbarKeyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardToolbarKeyboardDidChangeFrame:) name: UIKeyboardDidChangeFrameNotification object:nil];
    
}

- (void)unsubscribeToKeyboardNotifications {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [nc removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
}

#pragma mark - NSNotification Keyboard

- (void)keyboardToolbarKeyboardWillChangeFrame:(NSNotification *)notification    //kayboard will hide
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];

   // NSLog(@"keyboardToolbarKeyboardWillChangeFrame");
    self.hidden = YES;
    
   
    if(self.ctrlSelected == TRUE)
    {
        self.ctrlSelected = FALSE;
        
        for(UIButton *ctrlBtn in self.ctrlButtons)
        {
            ctrlBtn.selected = self.ctrlSelected;
        }
        
        [self.delegate keyboardToolbarView:self didToggleCtrlButton:self.ctrlSelected];
       
        }
 /*
    if(self.altSelected == TRUE)
    {
        self.altSelected = FALSE;
        
        for(UIButton *altBtn in self.altButtons)
        {
            altBtn.selected = self.altSelected;
        }
        
        [self.delegate keyboardToolbarView:self didToggleAltButton:self.altSelected];
       
          }

    if(self.shiftSelected == TRUE)
    {
        self.shiftSelected = FALSE;
        
        for(UIButton *shiftBtn in self.shiftButtons)
        {
            shiftBtn.selected = self.shiftSelected;
        }
        
        [self.delegate keyboardToolbarView:self didToggleShiftButton:self.shiftSelected];
        
    }
*/
}
-(void)onKeyboardHide:(NSNotification *)notification
{
    
    if(self.ctrlSelected == TRUE)
    {
        self.ctrlSelected = FALSE;
        
        for(UIButton *ctrlBtn in self.ctrlButtons)
        {
            ctrlBtn.selected = self.ctrlSelected;
        }
        
        [self.delegate keyboardToolbarView:self didToggleCtrlButton:self.ctrlSelected];

    }
     /*
    if(self.altSelected == TRUE)
    {
        self.altSelected = FALSE;
        
        for(UIButton *altBtn in self.altButtons)
        {
            altBtn.selected = self.altSelected;
        }
        
        [self.delegate keyboardToolbarView:self didToggleAltButton:self.altSelected];
        
    }
   
    if(self.shiftSelected == TRUE)
    {
        self.shiftSelected = FALSE;
        
        for(UIButton *shiftBtn in self.shiftButtons)
        {
            shiftBtn.selected = self.shiftSelected;
        }
        
        [self.delegate keyboardToolbarView:self didToggleShiftButton:self.shiftSelected];
        
    }
*/
}

-(void)onKeyboardShow:(NSNotification *)notification
{
    self.hidden = NO;
    
    
}
- (void)keyboardToolbarKeyboardDidChangeFrame:(NSNotification *)notification    //kayboard will hide
{
      [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
   // NSLog(@"keyboardToolbarKeyboardDidChangeFrame");
    BOOL isSplit = [self isKeyboardSplitWithEndRect:[[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue]];
    
    CGRect rect = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    NSLog(@"x %f y %f width %f height %f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (!iOS8) {
        if([self shouldHideKeyboardWithRect:rect orientation:orientation]) {
            self.hidden = YES;
            return;
        }
    } else
    {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardDidShowNotification object:nil];
    }
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    switch (orientation) {
        case UIInterfaceOrientationLandscapeLeft:
            
            if(iOS8){
                
                UIDeviceHardware *h=[[UIDeviceHardware alloc] init];

                if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM+CDMA)"] || [[h platformString]  isEqualToString:@"iPhone 5c (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5c (Global)"] || [[h platformString]  isEqualToString:@"iPhone 5s (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5s (Global)"] || [[h platformString]  isEqualToString:@"iPhone 6+"] || [[h platformString]  isEqualToString:@"iPhone 6"] || [[h platformString]  isEqualToString:@"iPod Touch 4G"] || [[h platformString]  isEqualToString:@"iPod Touch 5G"] || [[h platformString]  isEqualToString:@"iPhone 7 Plus"])
                {
                    
                    
                    self.frame = CGRectMake(0,
                                            rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                            screenSize.width,
                                            self.frame.size.height);
                }
                else
                {
                    self.frame = CGRectMake(0,
                                            
                                            768 - rect.origin.x - rect.size.height - self.frame.size.height - [self toolbarOffset],
                                            
                                            screenSize.width,
                                            
                                            self.frame.size.height);
                }
                
                [h release];
                

                } else {
     
                    
                    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
                    
                    
                    if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"])
                    {
                        self.frame = CGRectMake(0,158 - self.frame.size.height - [self toolbarOffset],screenSize.height,self.frame.size.height);
                        
                    }
                    
                    else if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM+CDMA)"] || [[h platformString]  isEqualToString:@"iPhone 5c (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5c (Global)"] || [[h platformString]  isEqualToString:@"iPhone 5s (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5s (Global)"] || [[h platformString]  isEqualToString:@"iPhone 6+"] || [[h platformString]  isEqualToString:@"iPhone 6"] || [[h platformString]  isEqualToString:@"iPod Touch 4G"] || [[h platformString]  isEqualToString:@"iPod Touch 5G"] || [[h platformString]  isEqualToString:@"iPhone 7 Plus"])
                    {
                        
                        
                        self.frame = CGRectMake(0,
                                                rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                                screenSize.width,
                                                self.frame.size.height);
                    }
                    else
                    {
                        self.frame = CGRectMake(0,
                                                rect.origin.x - self.frame.size.height - [self toolbarOffset],
                                                screenSize.height,
                                                self.frame.size.height);
                    }
                    [h release];
    
                    
                    
                    
                    
                    
            }
            
            break;
            
        case UIInterfaceOrientationLandscapeRight:
            if (iOS8) {
                
                
                UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
                
                if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM+CDMA)"] || [[h platformString]  isEqualToString:@"iPhone 5c (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5c (Global)"] || [[h platformString]  isEqualToString:@"iPhone 5s (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5s (Global)"] || [[h platformString]  isEqualToString:@"iPhone 6+"] || [[h platformString]  isEqualToString:@"iPhone 6"] || [[h platformString]  isEqualToString:@"iPod Touch 4G"] || [[h platformString]  isEqualToString:@"iPod Touch 5G"] || [[h platformString]  isEqualToString:@"iPhone 7 Plus"])
                {
                    
                    
                    self.frame = CGRectMake(0,
                                            rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                            screenSize.width,
                                            self.frame.size.height);
                }
                else
                {
                    self.frame = CGRectMake(0,
                                            
                                            768 - rect.origin.x - rect.size.height - self.frame.size.height - [self toolbarOffset],
                                            
                                            screenSize.width,
                                            
                                            self.frame.size.height);
                }
                
                [h release];

                }
            else
            {
                UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
                
               
                if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"])
                {
                    self.frame = CGRectMake(0,158 - self.frame.size.height - [self toolbarOffset],screenSize.height,self.frame.size.height);

                }
                
               else if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM+CDMA)"] || [[h platformString]  isEqualToString:@"iPhone 5c (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5c (Global)"] || [[h platformString]  isEqualToString:@"iPhone 5s (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5s (Global)"] || [[h platformString]  isEqualToString:@"iPhone 6+"] || [[h platformString]  isEqualToString:@"iPhone 6"] || [[h platformString]  isEqualToString:@"iPod Touch 4G"] || [[h platformString]  isEqualToString:@"iPod Touch 5G"] || [[h platformString]  isEqualToString:@"iPhone 7 Plus"])
                {
                    
                    
                    self.frame = CGRectMake(0,
                                            rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                            screenSize.width,
                                            self.frame.size.height);
                }
                else
                {
                    self.frame = CGRectMake(0,
                                            
                                            768 - rect.origin.x - rect.size.width - self.frame.size.height - [self toolbarOffset],
                                            
                                            screenSize.height,
                                            
                                            self.frame.size.height);
                }
                [h release];


            }
            break;
            
        case UIInterfaceOrientationPortrait:
            if(IS_IPAD)
            {
                if(iOS10)
                {
            self.frame = CGRectMake(0,
                                    759 - self.frame.size.height - [self toolbarOffset],
                                    screenSize.width,
                                    self.frame.size.height);
                }
                else
                {
                    self.frame = CGRectMake(0,
                                            rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                            screenSize.width,
                                            self.frame.size.height);

                }
            }
            else
            {
                UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
                
                if ([[h platformString] isEqualToString:@"iPhone 7 Plus"])
                {
                self.frame = CGRectMake(0,
                                        462 - [self toolbarOffset],
                                        screenSize.width,
                                        self.frame.size.height);
                }
                else
                {
                
                self.frame = CGRectMake(0,
                                        rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                        screenSize.width,
                                        self.frame.size.height);
                }
            }
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            if (isSplit) {
                self.frame = CGRectMake(0,
                                        760 - rect.origin.y,
                                        screenSize.width,
                                        self.frame.size.height);
            } else {
                if (!iOS8){
                    self.frame = CGRectMake(0,
                                            760 - rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                            screenSize.width,
                                            self.frame.size.height);
                } else {
                    self.frame = CGRectMake(0,
                                            rect.origin.y - self.frame.size.height - [self toolbarOffset],
                                            screenSize.width,
                                            self.frame.size.height);
                }
            }
            break;
        default:
            NSAssert(NO, @"Should Never Reach");
            
    }
    
    self.landscapeRightView.frame = CGRectMake(self.frame.size.width - self.landscapeRightView.frame.size.width,
                                           0,
                                           self.landscapeRightView.frame.size.width,
                                           self.landscapeRightView.frame.size.height);
    
    self.portraitRightView.frame = CGRectMake(self.frame.size.width - self.portraitRightView.frame.size.width,
                                           0,
                                           self.portraitRightView.frame.size.width,
                                           self.portraitRightView.frame.size.height);
    
    self.splitRightView.frame = CGRectMake(self.frame.size.width - self.splitRightView.frame.size.width,
                                           0,
                                           self.splitRightView.frame.size.width,
                                           self.splitRightView.frame.size.height);
    
    [self showToolbarForSplitKeyboard:isSplit];
}

- (CGFloat)toolbarOffset {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
        return 20;
    } else {
        return 0;
    }
}

- (BOOL)shouldHideKeyboardWithRect:(CGRect)rect orientation:(UIInterfaceOrientation)orientation
{
    //NSLog(@"%@", NSStringFromCGRect(rect));
    switch (orientation) {
        case UIInterfaceOrientationLandscapeLeft:       return 768.0f <= rect.origin.x;
            
        case UIInterfaceOrientationLandscapeRight:      return rect.origin.x <= -216.0f;
            
        case UIInterfaceOrientationPortrait:            return 1024.0f <= rect.origin.y;
            
        case UIInterfaceOrientationPortraitUpsideDown:  return rect.origin.y < 0;
            
        default:
            NSAssert(NO, @"Should Never Reach");
    }
    return NO;
}

- (void) showToolbarForSplitKeyboard:(BOOL)isSplit
{
    if (!iOS8) self.hidden = NO;
    
    if(isSplit)
    {
        self.landscapeView.hidden = YES;
        self.portraitView.hidden = YES;
        self.splitView.hidden = NO;
        
        self.landscapeRightView.hidden = YES;
        self.portraitRightView.hidden = YES;
        self.splitRightView.hidden = NO;
        
        return;
    }

    switch ([[UIApplication sharedApplication] statusBarOrientation]) {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            
            
            if IS_IPHONE {
                self.landscapeView.hidden = YES;
                self.portraitView.hidden = NO;
                self.splitView.hidden = YES;
                
                self.landscapeRightView.hidden = YES;
                self.portraitRightView.hidden = NO;
                self.splitRightView.hidden = YES;

            }
            else
            {
                self.landscapeView.hidden = NO;
                self.portraitView.hidden = YES;
                self.splitView.hidden = YES;
                
                self.landscapeRightView.hidden = NO;
                self.portraitRightView.hidden = YES;
                self.splitRightView.hidden = YES;
 
            }
            
            
            break;
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
                self.landscapeView.hidden = YES;
                self.portraitView.hidden = NO;
                self.splitView.hidden = YES;
            
                self.landscapeRightView.hidden = YES;
                self.portraitRightView.hidden = NO;
                self.splitRightView.hidden = YES;
            break;
        default:
            NSAssert(NO, @"Should Never Reach");
    }
}

- (BOOL)isKeyboardSplitWithEndRect:(CGRect)endRect {
    
    switch ([[UIApplication sharedApplication] statusBarOrientation]) {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            return (iOS8 ? 216.0f == endRect.size.height : 216.0f == endRect.size.width);
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            return (iOS8 ? 216.0f == endRect.size.width : 216.0f == endRect.size.height);
        default:
            break;
    }
    return NO;
}


- (void)dealloc {
    [_KeyB1 release];
    [_KeyB2 release];
    [super dealloc];
}
@end
