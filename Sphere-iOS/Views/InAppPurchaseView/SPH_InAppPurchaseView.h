/*******************************************************************************
 * $FileName: SPH_InAppPurchaseView.h$
 * $Date    : 2014-Jan-24$
 * $Revision: 1.0$
 * $Author  : RealWat Inc $
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D $
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  Clase declaration for In-App purchase view.
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: 2014-Jan-24       RealWat Inc             Creation$
 *
 ******************************************************************************/

#import <UIKit/UIKit.h>
#import "SPH_PurchaseInfo.h"
#import "SPH_ErrorCode.h"

@protocol SPH_InAppPurchaseViewDelegate <NSObject>

@required
-(tSPH_ErrorCode) startPurchase:(SPH_PurchaseInfo*)purchaseInfo;

@end

@interface SPH_InAppPurchaseView : UIView
{
	SPH_PurchaseInfo *m_purcahseInfo;
}

// Declare all property for In App purchase view.
@property (retain, nonatomic) id<SPH_InAppPurchaseViewDelegate> delegate;
@property (retain, nonatomic) IBOutlet UIImageView              *imageView;
@property (retain, nonatomic) IBOutlet UILabel                  *lblTitle;
@property (retain, nonatomic) IBOutlet UITextView               *lblDescription;
@property (retain, nonatomic) IBOutlet UIButton                 *btnPurchase;

-(tSPH_ErrorCode) setPurchaseInfo:(SPH_PurchaseInfo*)pPurchaseInfo;
-(tSPH_ErrorCode) getPurchaseInfo:(SPH_PurchaseInfo**)pCurchaseInfo;
+(instancetype)   getInstanceView;
@end
