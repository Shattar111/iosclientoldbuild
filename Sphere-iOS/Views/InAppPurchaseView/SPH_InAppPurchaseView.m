/*******************************************************************************
 * $FileName: SPH_InAppPurchaseView.m$
 * $Date    : 2014-Jan-24$
 * $Revision: 1.0$
 * $Author  : RealWat Inc $
 * $Archive : $
 * $Logfile : $
 * Project  : Sphere3D $
 *******************************************************************************
 * Copyright RealWat Inc.
 * This document contains CONFIDENTIAL information, which is the property of
 * REALWAT INC. Reproduction of this document,
 * utilization of its contents or disclosure to third parties (even in part)
 * without the prior written permission of REALWAT is prohibited.
 *******************************************************************************
 * MODULE:
 *  Clase implementation for In-App purchase view.
 *
 * HISTORY:
 *       __DATE__       : __AUTHOR__          : __DESCRIPTION__
 * $Log: 2014-Jan-24       RealWat Inc             Creation$
 *
 ******************************************************************************/

#import "SPH_InAppPurchaseView.h"

@implementation SPH_InAppPurchaseView

+(instancetype) getInstanceView
{
	
	SPH_InAppPurchaseView *lpInAppView = nil;
	
    NSArray *lpArrayOfViews =
	      [[NSBundle mainBundle] loadNibNamed:@"SPH_InAppPurchaseView"
										owner:self
									  options:nil];
    if ( 1 > lpArrayOfViews.count )
	{
        return nil;
    }
	
	for (id lpView in lpArrayOfViews)
	{
		if ( TRUE == [lpView isKindOfClass:[SPH_InAppPurchaseView class]])
		{
			lpInAppView = (SPH_InAppPurchaseView*) lpView;
			break;
		}
		
	}
    return (lpInAppView);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.backgroundColor = [UIColor redColor];
		
    }
    return self;
}

-(tSPH_ErrorCode) setPurchaseInfo:(SPH_PurchaseInfo *)pPurchaseInfo
{
	if ( nil == pPurchaseInfo )
	{
		SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
		return (SPH_ERROR_PARAMETER);
	}
	
	m_purcahseInfo = [pPurchaseInfo retain];
	
	[_imageView setImage:[UIImage imageNamed:m_purcahseInfo.imageName]];
	[_lblTitle setText:m_purcahseInfo.title];
	[_lblDescription setText:m_purcahseInfo.description];
	[_btnPurchase setTitle:pPurchaseInfo.price forState:UIControlStateNormal];

	return (SPH_SUCCESS);
}

-(tSPH_ErrorCode) getPurchaseInfo:(SPH_PurchaseInfo**)pPurchaseInfo
{
	if ( nil == pPurchaseInfo )
	{
		SPH_LOG_CONSOL(SPH_ERROR_PARAMETER);
		return (SPH_ERROR_PARAMETER);
	}
	
	*pPurchaseInfo = [m_purcahseInfo retain];
	
	return (SPH_SUCCESS);

}

- (IBAction)onClickedPurchase:(id)sender
{
    // VANYUTH : BEGIN : 28/01/2014
    BOOL lStatus = NO;
    // Verify delegate object, if not nil call startPurchase delegate function
    if ( nil != _delegate )
    {
        // Call respondsToSelector function with startPurchase function
        lStatus = [_delegate respondsToSelector:@selector(startPurchase:)];
        // Verify status, if yes call startPurchase delegate function
        if (YES == lStatus)
        {
            [_delegate startPurchase:m_purcahseInfo];
        }
    }
	// VANYUTH : END : 28/01/2014
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    // VANYUTH : BEGIN : 28/01/2014
    if (nil != _imageView)
    {
        [_imageView      release];
        _imageView = nil;
    }
    if (nil != _lblTitle)
    {
        [_lblTitle      release];
        _lblTitle = nil;
    }
	if (nil != _lblDescription)
    {
        [_lblDescription      release];
        _lblDescription = nil;
    }
    if (nil != _btnPurchase)
    {
        [_btnPurchase      release];
        _btnPurchase = nil;
    }
    if (nil != m_purcahseInfo)
    {
        [m_purcahseInfo      release];
        m_purcahseInfo = nil;
    }
    // VANYUTH : END : 28/01/2014
	[super dealloc];
}
@end
