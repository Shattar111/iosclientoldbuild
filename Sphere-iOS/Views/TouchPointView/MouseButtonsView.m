#import "MouseButtonsView.h"

#import "ScrollArrowsView.h"

@interface MouseButtonsView ()

@property (retain, nonatomic) NSArray *arrows;

@property (assign, nonatomic) IBOutlet ScrollArrowsView *scrollArrowsView;

@end

@implementation MouseButtonsView

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"scrolling"];
    [_arrows release];
    [_scrollArrowsView release];
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.arrows = @[[UIImage imageNamed:@"MouseArrow_0.png"], [UIImage imageNamed:@"MouseArrow_1.png"],
                    [UIImage imageNamed:@"MouseArrow_2.png"], [UIImage imageNamed:@"MouseArrow_3.png"]];
    [self addObserver:self forKeyPath:@"scrolling" options:0 context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self && [keyPath isEqualToString:@"scrolling"]) {
        self.scrollArrowsView.scrolling = self.scrolling;
        [self.scrollArrowsView setNeedsDisplay];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)drawRect:(CGRect)rect {
    // Display Arrow
    CGPoint arrowTip;
    UIImage *arrowImage = self.arrows[self.arrowQuadrant];
    switch (self.arrowQuadrant)
    {
        case S3MouseArrowTopRight:
            self.arrowHotSpot = CGPointMake(self.bounds.size.width, 0);
            arrowTip = CGPointMake(self.bounds.size.width - arrowImage.size.width, 0);
            break;
        case S3MouseArrowBottomRight:
            self.arrowHotSpot = CGPointMake(self.bounds.size.width, self.bounds.size.height);
            arrowTip = CGPointMake(self.bounds.size.width - arrowImage.size.width,
                                   self.bounds.size.height - arrowImage.size.height);
            break;
        case S3MouseArrowBottomLeft:
            self.arrowHotSpot = CGPointMake(0, self.bounds.size.height);
            arrowTip = CGPointMake(0, self.bounds.size.height - arrowImage.size.height);
            break;
        default:
            self.arrowHotSpot = CGPointMake(0, 0);
            arrowTip = CGPointMake(0, 0);
            break;
    }
    CGRect arrowRect;
    arrowRect.origin = arrowTip;
    arrowRect.size = CGSizeMake(arrowImage.size.width, arrowImage.size.height);
    [arrowImage drawInRect:arrowRect];
//    [arrowImage drawAtPoint:arrowTip];
}

@end
