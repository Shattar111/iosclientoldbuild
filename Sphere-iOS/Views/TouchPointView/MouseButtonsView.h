//
//  MouseButtonsView
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  MouseButtonsView responsible to setting up the mouse buttons to be used in TouchPointerView.




#import <UIKit/UIKit.h>

//temporary, for constants
#import "S3MouseView.h"

@interface MouseButtonsView : UIView

@property (nonatomic) CGPoint arrowHotSpot;
@property (nonatomic) S3MouseArrow arrowQuadrant;
@property (nonatomic) BOOL scrolling;


@end
