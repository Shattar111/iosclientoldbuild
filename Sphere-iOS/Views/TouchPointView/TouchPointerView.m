/*
 RDP Touch Pointer View 
 
 Copyright 2013 Thincast Technologies GmbH, Author: Martin Fleisz
 
 This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. 
 If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#import "TouchPointerView.h"
#import "Utils.h"

#import "MouseButtonsView.h"
#import "UIDeviceHardware.h"

#import "S3MouseView.h"

#define RESET_DEFAULT_POINTER_IMAGE_DELAY 0.15

#define POINTER_ACTION_CURSOR   0
#define POINTER_ACTION_CLOSE    3
#define POINTER_ACTION_RCLICK   2
#define POINTER_ACTION_LCLICK   4
#define POINTER_ACTION_MOVE     4
#define POINTER_ACTION_SCROLL   5
#define POINTER_ACTION_KEYBOARD 7
#define POINTER_ACTION_EXTKEYBOARD 8
#define POINTER_ACTION_RESET 6

#define IS_IPAD ([[[UIDevice currentDevice] model] isEqualToString:@"iPad"])
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])

@interface TouchPointerView ()

@property (retain, nonatomic) IBOutlet UIPanGestureRecognizer *moveRecognizer;
@property (retain, nonatomic) IBOutlet UIPanGestureRecognizer *dragRecognizer;
@property (retain, nonatomic) IBOutlet UILongPressGestureRecognizer *scrollRecognizer;

@property (retain, nonatomic) IBOutlet MouseButtonsView *mouseButtonsView;

@property (assign, nonatomic) IBOutlet UIButton *buttonLeftClick;
@property (assign, nonatomic) IBOutlet UIButton *buttonRightClick;

@property (assign, nonatomic) IBOutlet UIButton *buttonShowKeyboard;
@property (assign, nonatomic) IBOutlet UIButton *buttonChangeCursor;
@property (assign, nonatomic) IBOutlet UIButton *buttonScroll;
@property (assign, nonatomic) IBOutlet UIButton *buttonClose;

@property (assign, nonatomic) IBOutlet UIButton *buttonCopy;
@property (assign, nonatomic) IBOutlet UIButton *buttonPaste;

@property (nonatomic) CGPoint prevTouchPoint;

@end

@implementation TouchPointerView {
    // transformation and image currently drawn
    CGAffineTransform _pointer_transformation;
    UIImage* _cur_pointer_img;

    UIImage* _default_pointer_img;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // set content mode when rotating (keep aspect ratio)
    [self setContentMode:UIViewContentModeTopLeft];
    
   
    if (IS_IPAD)
    {
        
    [[NSBundle mainBundle] loadNibNamed:@"MouseButtons" owner:self options:nil];
    
    }
    else
    {
        
    [[NSBundle mainBundle] loadNibNamed:@"MouseButtonsiphone" owner:self options:nil];
    
    }
    
    self.mouseButtonsView.center = CGPointMake(200, 200);
    [self configureButtonImages];
    [self addSubview:self.mouseButtonsView];
    [self.moveRecognizer requireGestureRecognizerToFail:self.dragRecognizer];

}

- (void)configureButtonImages {
    
    if (IS_IPAD)
    {
    [self.buttonLeftClick setImage:[self leftButtonHighlightedImage] forState:UIControlStateHighlighted];
    [self.buttonShowKeyboard setImage:[UIImage imageNamed:@"button_bgh1"] forState:UIControlStateHighlighted];
    [self.buttonRightClick setImage:[UIImage imageNamed:@"button_bgh2"] forState:UIControlStateHighlighted];
    [self.buttonChangeCursor setImage:[UIImage imageNamed:@"button_bgh3"] forState:UIControlStateHighlighted];
    [self.buttonScroll setImage:[UIImage imageNamed:@"button_bgh4"] forState:UIControlStateHighlighted];
    [self.buttonClose setImage:[UIImage imageNamed:@"button_bgh5"] forState:UIControlStateHighlighted];
    [self.buttonPaste setImage:[UIImage imageNamed:@"button_bgh6"] forState:UIControlStateHighlighted];
    [self.buttonCopy setImage:[UIImage imageNamed:@"button_bgh7"] forState:UIControlStateHighlighted];
    }
    else{
        
    [self.buttonLeftClick setImage:[self leftButtonHighlightedImage] forState:UIControlStateHighlighted];
    [self.buttonRightClick setImage:[UIImage imageNamed:@"button_bghrightblue"] forState:UIControlStateHighlighted];
    [self.buttonChangeCursor setImage:[UIImage imageNamed:@"button_bgh3pointerblue"] forState:UIControlStateHighlighted];
    [self.buttonScroll setImage:[UIImage imageNamed:@"button_bghscrollblue"] forState:UIControlStateHighlighted];

    }
    
    }

- (UIImage *)leftButtonNormalImage {
    return [UIImage imageNamed:@"button"];
}

- (UIImage *)leftButtonHighlightedImage {
    return [UIImage imageNamed:@"button_drag"];
}

- (UIImage *)leftButtonDragImage {
    return [UIImage imageNamed:@"button_selected"];
}

#pragma mark - Mouse buttons outlets

- (IBAction)showKeyboard {
    [self.delegate touchPointerToggleKeyboard];
}

- (IBAction)leftButtonClick {
    [self.delegate touchPointerLeftClick:[self arrowLocation] down:YES];
    [self.delegate touchPointerLeftClick:[self arrowLocation] down:NO];
}

- (IBAction)rightButtonClick {
    [self.delegate touchPointerRightClick:[self arrowLocation] down:YES];
    [self.delegate touchPointerRightClick:[self arrowLocation] down:NO];
}

- (IBAction)clipboardPaste {
    [self.delegate touchPointerPaste];
}

- (IBAction)clipboardCopy {
    [self.delegate touchPointerCopy];
}

- (IBAction)close {
    [self.delegate touchPointerClose];
}

- (IBAction)changeCursor {
    self.mouseButtonsView.arrowQuadrant++;
    self.mouseButtonsView.arrowQuadrant %= MOUSE_ARROW_QUADRANT_MAX;
    [self.mouseButtonsView setNeedsDisplay];
}


- (void)dealloc
{
    [_mouseButtonsView release];
    [super dealloc];
}

#pragma mark - Public interface

// positions the pointer on screen if it got offscreen after an orentation change
-(void)ensurePointerIsVisibleLandscape
{
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    
    if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM+CDMA)"] || [[h platformString]  isEqualToString:@"iPhone 5c (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5c (Global)"] || [[h platformString]  isEqualToString:@"iPhone 5s (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5s (Global)"] || [[h platformString]  isEqualToString:@"iPhone 6+"] || [[h platformString]  isEqualToString:@"iPhone 6"] || [[h platformString]  isEqualToString:@"iPod Touch 4G"] || [[h platformString]  isEqualToString:@"iPod Touch 5G"] || [[h platformString]  isEqualToString:@"iPhone 7 Plus"])
    {
        
        self.mouseButtonsView.center = CGPointMake(200, 200);
    }
    else{
        self.mouseButtonsView.center = self.center;

    }
    
    [h release];

   /* CGRect bounds = [self bounds];
    if(_pointer_transformation.tx > (bounds.size.width - _cur_pointer_img.size.width))
        _pointer_transformation.tx = bounds.size.width - _cur_pointer_img.size.width;
    if(_pointer_transformation.ty > (bounds.size.height - _cur_pointer_img.size.height))
        _pointer_transformation.ty = bounds.size.height - _cur_pointer_img.size.height;
   // [self setNeedsDisplay];
   // [_mouse_view setNeedsDisplay];
    */
}

-(void)ensurePointerIsVisiblePortrait
{
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    
    if ([[h platformString] isEqualToString:@"iPhone 4"] || [[h platformString] isEqualToString:@"iPhone 4S"] || [[h platformString] isEqualToString:@"Verizon iPhone 4"] || [[h platformString]  isEqualToString:@"iPhone 3GS"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5 (GSM+CDMA)"] || [[h platformString]  isEqualToString:@"iPhone 5c (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5c (Global)"] || [[h platformString]  isEqualToString:@"iPhone 5s (GSM)"] || [[h platformString]  isEqualToString:@"iPhone 5s (Global)"] || [[h platformString]  isEqualToString:@"iPhone 6+"] || [[h platformString]  isEqualToString:@"iPhone 6"] || [[h platformString]  isEqualToString:@"iPod Touch 4G"] || [[h platformString]  isEqualToString:@"iPod Touch 5G"] || [[h platformString]  isEqualToString:@"iPhone 7 Plus"])
    {
        
        self.mouseButtonsView.center = CGPointMake(200, 200);
    }
    else{
        self.mouseButtonsView.center = CGPointMake(400, 500);
        
    }
    
    [h release];
    
    /* CGRect bounds = [self bounds];
     if(_pointer_transformation.tx > (bounds.size.width - _cur_pointer_img.size.width))
     _pointer_transformation.tx = bounds.size.width - _cur_pointer_img.size.width;
     if(_pointer_transformation.ty > (bounds.size.height - _cur_pointer_img.size.height))
     _pointer_transformation.ty = bounds.size.height - _cur_pointer_img.size.height;
     // [self setNeedsDisplay];
     // [_mouse_view setNeedsDisplay];
     */
}
// show/hides the touch pointer
-(void)setHidden:(BOOL)hidden
{  
    [super setHidden:hidden];

#if(0)
    // if shown center pointer in view
    if(!hidden)
    {
        _pointer_transformation = CGAffineTransformMakeTranslation(([self bounds].size.width - [_cur_pointer_img size].width) / 2, 
                                                                  ([self bounds].size.height - [_cur_pointer_img size].height) / 2);
       [self setNeedsDisplay];
    } 
#endif   
}

-(UIEdgeInsets)getEdgeInsets
{
    return UIEdgeInsetsMake(0, 0, [_cur_pointer_img size].width, [_cur_pointer_img size].height);
}

- (CGPoint)getPointerPosition
{
    return CGPointMake(_pointer_transformation.tx, _pointer_transformation.ty);        
}

- (int)getPointerWidth
{
    return [_cur_pointer_img size].width;    
}

- (int)getPointerHeight
{
    return [_cur_pointer_img size].height;
}

//__REALWAT__: BEGIN
-(void)setPointerSize:(S3MousePointerSize)pointerSize
             atCenter:(CGPoint)center
{
    CGFloat scale = 0;
    switch (pointerSize) {
        case S3MousePointerSizeSmall:
            scale = 1;
            break;
        case S3MousePointerSizeNormal:
            scale = 1.3;
            break;
        default:
            scale = 1;
    }
    self.mouseButtonsView.transform = CGAffineTransformMakeScale(scale, scale);
    [self.mouseButtonsView setNeedsDisplay];
}

- (CGPoint)arrowLocation
{
    return [self.mouseButtonsView convertPoint:self.mouseButtonsView.arrowHotSpot toView:self];
}

-(CGPoint)getMouseCenterPoint
{
    return self.mouseButtonsView.center;
}
- (void)performMouseLeftClickAtPoint:(CGPoint)point
{
    [[self delegate] touchPointerLeftClick:point down:YES];
    [[self delegate] touchPointerLeftClick:point down:NO];
}
//__REALWAT__: END

- (void)setCurrentPointerImage:(UIImage*)image
{
    _cur_pointer_img = image;
    [self setNeedsDisplay];
}

- (void)displayPointerActionImage:(UIImage*)image
{
    [self setCurrentPointerImage:image];
    [self performSelector:@selector(setCurrentPointerImage:) withObject:_default_pointer_img afterDelay:RESET_DEFAULT_POINTER_IMAGE_DELAY];
}

// helper that returns YES if the given point is within the pointer
-(BOOL) pointInsidePointer:(CGPoint)point
{
    return CGRectContainsPoint(self.mouseButtonsView.frame, point);
}

// this filters events - if the pointer was clicked the scrollview won't get any events
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    return [self pointInsidePointer:point];
}

#pragma mark - Action handlers

- (BOOL)isMoveDragRecognizer:(UIGestureRecognizer *)recognizer {
    return recognizer == self.moveRecognizer || recognizer == self.dragRecognizer;
}

- (void)handleGesture:(UIGestureRecognizer*)recognizer sendClick:(BOOL)sendClick
{
    if([recognizer state] == UIGestureRecognizerStateBegan)
    {
        CGPoint touchPos = [recognizer locationInView:self];
        
        //__REALWAT__: BEGIN: 21/Nov/2013
        
        self.prevTouchPoint = touchPos;
        
        if (recognizer == self.moveRecognizer) {
            [self.buttonLeftClick setImage:[self leftButtonHighlightedImage] forState:UIControlStateNormal];
        } else if (recognizer == self.dragRecognizer) {
            [self.buttonLeftClick setImage:[self leftButtonDragImage] forState:UIControlStateNormal];
            [self.delegate touchPointerLeftClick:[self arrowLocation] down:YES];
        } else if (recognizer == self.scrollRecognizer) {
            self.mouseButtonsView.scrolling = YES;
        }
    }
    else if([recognizer state] == UIGestureRecognizerStateChanged)
    {
        if([self isMoveDragRecognizer:recognizer])
        {
            CGPoint touchPos = [recognizer locationInView:self];
            
            self.mouseButtonsView.center = touchPos;
            
           // _pointer_transformation = CGAffineTransformTranslate(_pointer_transformation, touchPos.x - _prev_touch_location.x, touchPos.y - _prev_touch_location.y);
            [self.delegate touchPointerMove:[self arrowLocation]];
            self.prevTouchPoint = touchPos;
            //[self setNeedsDisplay];
        } if (recognizer == self.scrollRecognizer) {
            
            CGPoint touchPos = [recognizer locationInView:self];
            float delta = touchPos.y - self.prevTouchPoint.y;
            if(delta > GetScrollGestureDelta())
            {
                [self.delegate touchPointerScrollDown:YES];
                self.prevTouchPoint = touchPos;
            }
            else if(delta < -GetScrollGestureDelta())
            {
                [self.delegate touchPointerScrollDown:NO];
                self.prevTouchPoint = touchPos;
            }
        }
    }
    else if(recognizer.state == UIGestureRecognizerStateEnded)
    {
        if(recognizer == self.dragRecognizer)
        {
            if(sendClick)
                [self.delegate touchPointerLeftClick:[self arrowLocation] down:NO];
        }
        [self.buttonLeftClick setImage:[self leftButtonNormalImage] forState:UIControlStateNormal];
        self.mouseButtonsView.scrolling = NO;
    }
}

- (IBAction)handlePointerMove:(UIPanGestureRecognizer *)recognizer {
    [self handleGesture:recognizer sendClick:NO];
}

- (IBAction)handleDrag:(UILongPressGestureRecognizer *)recognizer {
    [self handleGesture:recognizer sendClick:YES];
}

- (IBAction)handleScroll:(UILongPressGestureRecognizer *)recognizer {
    [self handleGesture:recognizer sendClick:NO];
}
@end
