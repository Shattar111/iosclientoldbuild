#import "ScrollArrowsView.h"

@implementation ScrollArrowsView

-(void)drawRect:(CGRect)rect {
    if (self.scrolling)
    {
        CGContextRef context = UIGraphicsGetCurrentContext();

        UIColor *colour = [UIColor colorWithRed:245.0 / 255.0 green: 45.0 / 255.0 blue: 12.0 / 255.0 alpha:1];
        CGFloat x;
        CGFloat y;
        CGPoint center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
        CGContextSaveGState(context);
        
        CGContextSetStrokeColorWithColor(context, colour.CGColor);
        CGContextSetFillColorWithColor(context, colour.CGColor);
        CGContextSetLineWidth(context, 2.0);
        CGContextSetLineCap(context, kCGLineCapRound);
        
        x = center.x - 8;
        y = center.y - 5;
        CGContextMoveToPoint(context, x, y);
        CGContextAddLineToPoint(context, x + 8, 0);
        CGContextAddLineToPoint(context, x + 16, y);
        CGContextAddLineToPoint(context, x, y);
        CGContextFillPath(context);
        
        y = center.y + 5;
        CGContextMoveToPoint(context, x, y);
        CGContextAddLineToPoint(context, x + 8, self.frame.size.height);
        CGContextAddLineToPoint(context, x + 16, y);
        CGContextAddLineToPoint(context, x, y);
        CGContextFillPath(context);
        
        CGContextRestoreGState(context);
    }

}

@end
