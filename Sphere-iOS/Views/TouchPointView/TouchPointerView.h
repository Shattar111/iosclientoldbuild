//
//  TouchPointerView
//  Glassware Connect
//
//  Created by Sphere 3D 2015
//
//  This class responsible for handling all the functionality of the mouse buttons.
//  There are 2 mouse interfaces to be loaded, either "MouseButtons" for iPad or "MouseButtonsiPhone" for iPhone devices.
//  TouchPointerView does almost all the functionality of the ordinary mouse:

//  Show/Hide Keyboard
//  Right/Left clicks
//  Copy/Paste
//  Drag/Scroll
//  Change Cursor position
//  Move/Relocate mouse position




#import <UIKit/UIKit.h>
#import "S3MouseView.h"

typedef NS_ENUM(NSInteger, S3MousePointerSize) {
    S3MousePointerSizeLarge = 2,
    S3MousePointerSizeNormal = 1,
    S3MousePointerSizeSmall = 0,
};

// protocol for touch pointer callbacks
@protocol TouchPointerDelegate
// callback if touch pointer should be closed
-(void)touchPointerClose;
// callback for a left click action
-(void)touchPointerLeftClick:(CGPoint)pos down:(BOOL)down;
// callback for a right click action
-(void)touchPointerRightClick:(CGPoint)pos down:(BOOL)down;
// callback for pointer move action
-(void)touchPointerMove:(CGPoint)pos;
// callback if scrolling is performed
-(void)touchPointerScrollDown:(BOOL)down;
// callback for toggling the standard keyboard
-(void)touchPointerToggleKeyboard;

-(void)touchPointerCopy;
-(void)touchPointerPaste;

@end


@interface TouchPointerView : UIView

@property(assign) CGPoint centerPoint;
@property(assign) S3MousePointerSize pointerSize;

@property (assign) IBOutlet NSObject<TouchPointerDelegate>* delegate;

// positions the pointer on screen if it got offscreen after an orentation change or after displaying the keyboard
-(void)ensurePointerIsVisibleLandscape;
-(void)ensurePointerIsVisiblePortrait;

// returns the extent required for the scrollview to use the touch pointer near the edges of the session view
-(UIEdgeInsets)getEdgeInsets;

// return pointer dimension and position information
- (CGPoint)getPointerPosition;
- (int)getPointerWidth;
- (int)getPointerHeight;

//__REALWAT__: BEGIN
- (void)performMouseLeftClickAtPoint:(CGPoint)point;
- (CGPoint)arrowLocation;
- (void)setPointerSize:(S3MousePointerSize)pointerSize
              atCenter:(CGPoint)center;
-(CGPoint)getMouseCenterPoint;
//__REALWAT__: END

@end
