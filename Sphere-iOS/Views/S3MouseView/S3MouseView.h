//
//  S3MouseView.h
//
//  Created by Jon Newlands on 2013-02-22.
//  Based on previous work by Antonio Cabezuelo Vivo on 2008-11-19
//

#import <UIKit/UIKit.h>

#define MOUSE_ARROW_MAX                     3
#define MOUSE_ARROW_QUADRANT_MAX            4
#define MOUSE_BUTTON_ACTION_ARROW_FLIP      3
#define MOUSE_BUTTON_ACTION_COPY            7
#define MOUSE_BUTTON_ACTION_HIDE            5
#define MOUSE_BUTTON_ACTION_KEYBOARD        1
#define MOUSE_BUTTON_ACTION_LEFT_CLICK      0
#define MOUSE_BUTTON_ACTION_NONE           -1
#define MOUSE_BUTTON_ACTION_PASTE           6
#define MOUSE_BUTTON_ACTION_RIGHT_CLICK     2
#define MOUSE_BUTTON_ACTION_WHEEL_SCROLLER  4
#define MOUSE_BUTTON_CIRCULAR_INDEX_START   1
#define MOUSE_BUTTON_MAX                    7

typedef NS_ENUM(NSInteger, S3MouseArrow) {
    S3MouseArrowTopLeft = 0,
    S3MouseArrowTopRight = 1,
    S3MouseArrowBottomRight = 2,
    S3MouseArrowBottomLeft = 3
};

@interface S3MouseView : UIView
{
  @private
    UIImage *arrows[MOUSE_ARROW_MAX + 1];
	UIImage *buttons[MOUSE_BUTTON_MAX + 1];
    CGFloat endAngle;
    CGGradientRef gradientBackground;
    CGGradientRef gradientDrag;
    CGGradientRef gradientSelected;
	CGFloat initAngle;
}

@property (nonatomic) CGPoint arrowHotSpot;
@property (nonatomic) S3MouseArrow arrowQuadrant;
@property (nonatomic) NSInteger buttonSelected;
@property (nonatomic) BOOL isDragMode;
@property (nonatomic) NSInteger scaleFactor;

- (CGFloat)minimumSize;
- (void)reset;
- (int)testForPoint:(CGPoint)point;

@end
