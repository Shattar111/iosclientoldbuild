//
//  S3MouseView.h
//
//  Created by Jon Newlands on 2013-02-22.
//  Based on previous work by Antonio Cabezuelo Vivo on 2008-11-19
//

#import "S3MouseView.h"

#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)
#define RADIANS_TO_DEGREE(__ANGLE__) ((__ANGLE__) / M_PI * 180.0)

#define kStartAngle				0.0

#define kEndAngle				0
#define kTotalAngle				(360.0 - endAngle + initAngle)

#define kInnerCircleRadius		(28.0 + 10.0 * _scaleFactor)
#define kOuterCircleRadius		(kInnerCircleRadius + 50.0)
#define kBackItemRadius         (kOuterCircleRadius - 5.0)
#define kParentItemRadius       (kOuterCircleRadius + 5.0)
#define kCornerRadius           5.0
#define kRoundedRectRadius      10.0

#define kAlphaValue             0.55

@interface S3MouseView (Private)

- (void) initGradients;
- (void) itemSelected:(NSInteger)index;
- (void) testForPoint:(CGPoint)point;

@end

@implementation S3MouseView

@synthesize isDragMode = _isDragMode;

/**
 */
- (void)dealloc
{
    CGGradientRelease(gradientBackground);
    CGGradientRelease(gradientDrag);
	CGGradientRelease(gradientSelected);
    for (int i = 0; i <= MOUSE_ARROW_MAX; i++) {
        [arrows[i] release];
    }
    for (int i = 0; i <= MOUSE_BUTTON_MAX; i++) {
        [buttons[i] release];
    }
    [super dealloc];
}

/**
 */
- (void)drawRect:(CGRect)rect
{
	CGRect bounds = self.bounds;
    UIImage *button;
	CGPoint center = CGPointMake(bounds.size.width / 2.0, bounds.size.height / 2.0);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(context, 153.0/255.0, 153.0/255.0, 153.0/255.0, 1.0);
	CGContextSetRGBFillColor(context, 100.0/255.0, 100.0/255.0, 100.0/255.0, kAlphaValue);
	CGContextSetLineWidth(context, 0.6);
	CGFloat startAngle = initAngle;
    CGFloat pieceAngle = kTotalAngle / MOUSE_BUTTON_MAX;
    
    // Configure Colour Data
	float myColorValues[] = {255.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0, 1.0};
	CGColorRef myColor;
	float myColorValues2[] = {51.0 / 255.0, 51.0 / 255.0, 51.0 / 255.0, 1.0};
	CGColorRef myColor2;
    CGColorSpaceRef myColorSpace;
	myColorSpace = CGColorSpaceCreateDeviceRGB ();
    myColor = CGColorCreate (myColorSpace, myColorValues);
	myColor2 = CGColorCreate (myColorSpace, myColorValues2);
    
    // Display Arrow
    CGPoint arrowTip;
    switch (_arrowQuadrant)
    {
        case S3MouseArrowTopRight:
            _arrowHotSpot = CGPointMake(self.frame.size.width, 0);
            arrowTip = CGPointMake(self.frame.size.width - arrows[_arrowQuadrant].size.width, 0);
            break;
        case S3MouseArrowBottomRight:
            _arrowHotSpot = CGPointMake(self.frame.size.width, self.frame.size.height);
            arrowTip = CGPointMake(self.frame.size.width - arrows[_arrowQuadrant].size.width, self.frame.size.height - arrows[_arrowQuadrant].size.height);
            break;
        case S3MouseArrowBottomLeft:
            _arrowHotSpot = CGPointMake(0, self.frame.size.height);
            arrowTip = CGPointMake(0, self.frame.size.height - arrows[_arrowQuadrant].size.height);
            break;
        default:
            _arrowHotSpot = CGPointMake(0, 0);
            arrowTip = CGPointMake(0, 0);
            break;
    }
    [arrows[_arrowQuadrant] drawAtPoint:arrowTip];
    
    // Loop through each button and display centered in corresponding wedge.
	for (int i = MOUSE_BUTTON_CIRCULAR_INDEX_START; i <= MOUSE_BUTTON_MAX; i++) {
        CGFloat radius = kOuterCircleRadius;
        
		CGFloat alpha = RADIANS_TO_DEGREE(atanf(kCornerRadius/radius));
		CGFloat cradius = cosf(DEGREES_TO_RADIANS(alpha)) * radius;
		CGFloat angle = startAngle - pieceAngle;
		CGPoint b = CGPointMake(center.x + (radius - kCornerRadius) * cosf(DEGREES_TO_RADIANS(startAngle)), center.y + (radius - kCornerRadius) * sinf(DEGREES_TO_RADIANS(startAngle)));
		CGPoint c = CGPointMake(center.x + cradius * cosf(DEGREES_TO_RADIANS(startAngle)), center.y + cradius * sinf(DEGREES_TO_RADIANS(startAngle)));
		CGPoint d = CGPointMake( center.x + radius * cosf(DEGREES_TO_RADIANS(startAngle - alpha)), center.y + radius * sinf(DEGREES_TO_RADIANS(startAngle - alpha)));
		CGPoint f = CGPointMake(center.x + cradius * cosf(DEGREES_TO_RADIANS(angle)), center.y + cradius * sinf(DEGREES_TO_RADIANS(angle)));
		CGPoint g = CGPointMake(center.x + (radius - kCornerRadius) * cosf(DEGREES_TO_RADIANS(angle)), center.y + (radius - kCornerRadius) * sinf(DEGREES_TO_RADIANS(angle)));
        
        CGMutablePathRef path = CGPathCreateMutable();
		CGPathMoveToPoint(path, NULL, b.x, b.y);
		CGPathAddArcToPoint(path, NULL, c.x, c.y, d.x, d.y, kCornerRadius);
		CGPathAddArc(path, NULL, center.x, center.y, radius, DEGREES_TO_RADIANS(startAngle-alpha), DEGREES_TO_RADIANS(startAngle - pieceAngle + alpha), 1);
		CGPathAddArcToPoint(path, NULL, f.x, f.y, g.x, g.y, kCornerRadius);
		CGPathAddArc(path, NULL, center.x, center.y, kInnerCircleRadius, DEGREES_TO_RADIANS( startAngle - pieceAngle), DEGREES_TO_RADIANS(startAngle), 0);
		CGPathCloseSubpath(path);
        
		CGContextSaveGState(context);
		CGContextSetShadow(context, CGSizeMake(0, 0), 5.0);
		CGContextAddPath(context, path);
		CGContextDrawPath(context, kCGPathFill);
		CGContextRestoreGState(context);
        
		CGContextSaveGState(context);
		CGContextAddPath(context, path);
		CGContextClip(context);
		if (i == _buttonSelected) {
			CGContextDrawRadialGradient(context, gradientSelected, center, radius * 0.3, center, radius, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
		}
        else {
			CGContextDrawRadialGradient(context, gradientBackground, center, kInnerCircleRadius, center, radius - 2.0, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
		}
		CGContextRestoreGState(context);
		CGPathRelease(path);
        
		CGContextSaveGState(context);
		CGFloat pieceCenterAngle = startAngle - pieceAngle / 2.0;
		CGFloat pieceRadius = kInnerCircleRadius + (radius - kInnerCircleRadius) / 2.0;
        
        CGPoint labelCenter = CGPointMake(center.x + pieceRadius * cosf(DEGREES_TO_RADIANS(pieceCenterAngle)), center.y + pieceRadius * sinf(DEGREES_TO_RADIANS(pieceCenterAngle)));
        
        button = buttons[i];
        if (button) {
            [button drawAtPoint:CGPointMake(labelCenter.x - button.size.width / 2.0, labelCenter.y - button.size.height / 2.5 ) ];
		}
        
        CGContextRestoreGState(context);
		startAngle -= pieceAngle;
	}
    
	CGColorRelease (myColor);
	CGColorRelease (myColor2);
    CGColorSpaceRelease (myColorSpace);
	
	CGContextSaveGState(context);
	CGContextSetShadow(context, CGSizeMake(0, 0), 4.0);
	CGContextAddEllipseInRect(context, CGRectMake(center.x - kInnerCircleRadius, center.y - kInnerCircleRadius, kInnerCircleRadius * 2.0, kInnerCircleRadius * 2.0));
	CGContextDrawPath(context, kCGPathFill);
	CGContextRestoreGState(context);
    
	// Draw the inner circle.
	CGContextSaveGState(context);
	CGContextAddEllipseInRect(context, CGRectMake(center.x - kInnerCircleRadius, center.y - kInnerCircleRadius, kInnerCircleRadius * 2.0, kInnerCircleRadius * 2.0));
	CGContextClip(context);
    
    // Apply proper shading if the center area is selected.
    CGGradientRef gradient = gradientBackground;
    if (_buttonSelected == MOUSE_BUTTON_ACTION_LEFT_CLICK) {
        if (_isDragMode) {
            gradient = gradientDrag;
        }
        else {
            gradient = gradientSelected;
        }
    }
    CGContextDrawRadialGradient(context, gradient, center, 0.0, center, kInnerCircleRadius + 20, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
	CGContextRestoreGState(context);
    
    // If mouse wheel is selected, draw quide arrows.
    if (_buttonSelected == MOUSE_BUTTON_ACTION_WHEEL_SCROLLER)
    {
        UIColor *colour = [UIColor colorWithRed:245.0 / 255.0 green: 45.0 / 255.0 blue: 12.0 / 255.0 alpha:kAlphaValue];
        CGFloat x;
        CGFloat y;
        
        CGContextSaveGState(context);
       
        CGContextSetStrokeColorWithColor(context, colour.CGColor);
        CGContextSetFillColorWithColor(context, colour.CGColor);
        CGContextSetLineWidth(context, 2.0);
        CGContextSetLineCap(context, kCGLineCapRound);

        x = center.x - 8;
        y = center.y - 5;
        CGContextMoveToPoint(context, x, y);
        CGContextAddLineToPoint(context, x + 8, 0);
        CGContextAddLineToPoint(context, x + 16, y);
        CGContextAddLineToPoint(context, x, y);
        CGContextFillPath(context);
        
        y = center.y + 5;
        CGContextMoveToPoint(context, x, y);
        CGContextAddLineToPoint(context, x + 8, self.frame.size.height);
        CGContextAddLineToPoint(context, x + 16, y);
        CGContextAddLineToPoint(context, x, y);
        CGContextFillPath(context);

        CGContextRestoreGState(context);
    }
    
	// Draw center button.
    button = buttons[MOUSE_BUTTON_ACTION_LEFT_CLICK];
    [button drawAtPoint:CGPointMake(center.x - button.size.width / 2.0, center.y - button.size.height / 2.0 ) ];
}

/**
 */
- (void)initGradients {
    
	CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
	
	CGFloat col1[] =
	{
		184.0 / 255.0, 185.0 / 255.0, 186.0 /255.0, kAlphaValue,
		203.0 / 255.0, 204.0 / 255.0, 206.0 /255.0, kAlphaValue,
		243.0 / 255.0, 243.0 / 255.0, 243.0 /255.0, kAlphaValue,
	};
	gradientBackground = CGGradientCreateWithColorComponents(rgb, col1, NULL, sizeof(col1) /( sizeof(col1[0]) * 4));
	
	CGFloat col2[] =
	{
		 39.0 / 255.0, 119.0 / 255.0, 252.0 / 255.0, kAlphaValue,
		114.0 / 255.0, 173.0 / 255.0, 255.0 / 255.0, kAlphaValue,
	};
	gradientSelected = CGGradientCreateWithColorComponents(rgb, col2, NULL, sizeof(col2) / (sizeof(col2[0]) * 4));
    
    CGFloat col3[] =
	{
        245.0 / 255.0, 45.0 / 255.0, 12.0 / 255.0, kAlphaValue,
		184 / 255.0, 45.0 / 255.0, 12.0 / 255.0, kAlphaValue,
	};
	gradientDrag = CGGradientCreateWithColorComponents(rgb, col3, NULL, sizeof(col3) / (sizeof(col3[0]) * 4));

	CGColorSpaceRelease(rgb);
}

/**
 */
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _arrowQuadrant = S3MouseArrowTopLeft;
        _buttonSelected = MOUSE_BUTTON_ACTION_NONE;
        self.backgroundColor = [UIColor clearColor];
        [self initGradients];
        
        arrows[S3MouseArrowTopLeft] = [[UIImage imageNamed:@"MouseArrow_0.png"] retain];
        arrows[S3MouseArrowTopRight] = [[UIImage imageNamed:@"MouseArrow_1.png"] retain];
        arrows[S3MouseArrowBottomRight] = [[UIImage imageNamed:@"MouseArrow_2.png"] retain];
        arrows[S3MouseArrowBottomLeft] = [[UIImage imageNamed:@"MouseArrow_3.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_LEFT_CLICK] = [[UIImage imageNamed:@"MouseLeftClick.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_KEYBOARD] = [[UIImage imageNamed:@"MouseKeyboard.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_RIGHT_CLICK] = [[UIImage imageNamed:@"MouseRightClick.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_ARROW_FLIP] = [[UIImage imageNamed:@"MouseFlipArrow.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_WHEEL_SCROLLER] = [[UIImage imageNamed:@"MouseScroller.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_HIDE] = [[UIImage imageNamed:@"MouseHide.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_PASTE] = [[UIImage imageNamed:@"MousePaste.png"] retain];
        buttons[MOUSE_BUTTON_ACTION_COPY] = [[UIImage imageNamed:@"MouseCopy.png"] retain];

        initAngle = kStartAngle;
		endAngle = kEndAngle;
    }
    return self;
}

/**
 */
- (CGFloat)minimumSize
{
	return (kParentItemRadius + 5.0 ) * 2.0;
}

/**
 */
- (void)reset
{
  	_buttonSelected = MOUSE_BUTTON_ACTION_NONE;
    _isDragMode = NO;
	[self setNeedsDisplay];
}

/**
 */
- (void)setIsDragMode:(BOOL)isDragMode
{
    self->_isDragMode = isDragMode;
    [self setNeedsDisplay];
}

/**
 */
- (int)testForPoint:(CGPoint)point
{
    CGPoint center = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.height / 2.0);
    CGFloat pieceAngle = kTotalAngle / MOUSE_BUTTON_MAX;
	CGPoint rp = CGPointMake(point.x - center.x, point.y - center.y);
    CGFloat radius = sqrtf(rp.x * rp.x + rp.y * rp.y);
    CGFloat startAngle = initAngle;
    
	CGFloat angle = RADIANS_TO_DEGREE(acosf(rp.x / radius));
	if (rp.x < 0.0 && rp.y > 0.0) {
		angle = angle - 360.0;
    } else if (rp.x > 0.0 && rp.y > 0.0) {
        angle = -360 + angle;
	} else if (rp.y < 0.0 || rp.x < 0.0) {
		angle = -angle;
	}
    
    _buttonSelected = MOUSE_BUTTON_ACTION_NONE;
    if (radius > kInnerCircleRadius)
    {
        for (int i = MOUSE_BUTTON_CIRCULAR_INDEX_START; i <= MOUSE_BUTTON_MAX; i++) {
            if (angle < startAngle && angle > (startAngle - pieceAngle) && radius > kInnerCircleRadius) {
                _buttonSelected = i;
                break;
            }
            startAngle -= pieceAngle;
        }
    }
    else {
        _buttonSelected = MOUSE_BUTTON_ACTION_LEFT_CLICK;
    }
	[self setNeedsDisplay];
    return _buttonSelected;
}

@end
