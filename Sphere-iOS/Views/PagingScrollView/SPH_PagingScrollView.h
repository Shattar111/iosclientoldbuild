//
//  SPH_PagingScrollView.h
//  inappdlg
//
//  Created by Soknyra Te on 12/12/13.
//  Copyright (c) 2013 Realwat Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPH_ErrorCode.h"

@protocol SPH_PagingScrollViewDelege <NSObject>

-(tSPH_ErrorCode) currentView:(UIView*)currentView;

@end

@interface SPH_PagingScrollView : UIView <UIScrollViewDelegate>
{
	UIScrollView  * m_scrollView;
}

@property (nonatomic, retain) id<SPH_PagingScrollViewDelege> delegate;
@property (nonatomic, retain) IBOutlet UIPageControl         *pageControl;

- (void) addSubViewOfScrollView:(UIView*)view;

@end
