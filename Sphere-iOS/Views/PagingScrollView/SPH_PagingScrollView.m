//
//  SPH_PagingScrollView.m
//  inappdlg
//
//  Created by Soknyra Te on 12/12/13.
//  Copyright (c) 2013 Realwat Inc. All rights reserved.
//

#import "SPH_PagingScrollView.h"


#import <QuartzCore/QuartzCore.h>

@implementation SPH_PagingScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		
		[self createScrollView:self.bounds];
		
    }
    return self;
}

- (void) awakeFromNib
{
	[super awakeFromNib];
	
	[self setNeedsLayout];
	
	[self layoutIfNeeded];
	
	[self setAutoresizesSubviews:YES];
	
	[self initWithFrame:self.frame];
}

- (void) createScrollView:(CGRect)frame
{

	m_scrollView = [[UIScrollView alloc] initWithFrame:frame];
	
	if ( nil == m_scrollView )
	{
		return;
	}
	
	m_scrollView.delegate = self;
	m_scrollView.pagingEnabled = YES;
	m_scrollView.showsHorizontalScrollIndicator = NO;
	m_scrollView.showsVerticalScrollIndicator = NO;
	m_scrollView.canCancelContentTouches = YES;
	m_scrollView.backgroundColor = [UIColor clearColor];
	
	[self addSubview:m_scrollView];
	
	// border radius
	[self.layer setCornerRadius:3.0f];
	
	// border
	[self.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[self.layer setBorderWidth:1.5f];
	
	// drop shadow
	[self.layer setShadowColor:[UIColor blackColor].CGColor];
	[self.layer setShadowOpacity:0.8];
	[self.layer setShadowRadius:3.0];
	[self.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
    
	
	CGRect lPageCtrlFrame = frame;
	
	lPageCtrlFrame.origin.x = 0;
	lPageCtrlFrame.origin.y = frame.size.height - 50;
	lPageCtrlFrame.size.height = 50;
	
	//_pageControl.backgroundColor = [UIColor redColor];
	_pageControl.numberOfPages   = 0;
	_pageControl.currentPage     = 0;
	
	
	[_pageControl addTarget:self
					  action:@selector(onPageChanged:)
			forControlEvents:UIControlEventValueChanged];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
	
	CGFloat lPageWidth = m_scrollView.frame.size.width;
    
	float fractionalPage = m_scrollView.contentOffset.x / lPageWidth;
    
	NSInteger lPage = lround(fractionalPage);
    
	_pageControl.currentPage = lPage;
	
	//NSLog(@"Page Controll : %d", _pageControl.currentPage);
	
	if ( nil != self.delegate )
	{
		[self.delegate currentView:[[scrollView subviews] objectAtIndex:lPage]];
	}
	
}


- (IBAction) onPageChanged:(id)sender
{
	//NSLog(@"Page Controll : %d", _pageControl.currentPage);
	
	CGFloat x = _pageControl.currentPage * m_scrollView.frame.size.width;
	
    [m_scrollView setContentOffset:CGPointMake(x, 0) animated:YES];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) addSubViewOfScrollView:(UIView *)view
{
	if ( nil == view )
	{
		return;
	}
	
	if ( nil == m_scrollView )
	{
		return;
	}
	
	NSInteger lNumOfSubViews = m_scrollView.subviews.count;
	
	CGRect lViewFrame = CGRectMake(lNumOfSubViews * self.frame.size.width,
							       0,
							       self.frame.size.width,
							       self.frame.size.height);
	
	view.frame = lViewFrame;
	
	[m_scrollView addSubview:view];
	
	_pageControl.numberOfPages = lNumOfSubViews + 1;
	
	[self bringSubviewToFront:_pageControl];
	
	m_scrollView.contentSize = CGSizeMake( (lNumOfSubViews + 1) * self.frame.size.width,
										  self.frame.size.height);
	
	if ( nil != self.delegate )
	{
		NSArray *lpViews = [m_scrollView subviews];
		if ( 1 == [lpViews count] )
		{
			[self.delegate currentView:[[m_scrollView subviews] objectAtIndex:0]];
		}
	}
}

- (void) dealloc
{
	if ( nil != m_scrollView )
	{
		[m_scrollView release];
		m_scrollView = nil;
	}
    // VANYUTH : BEGIN : 28/01/2014
    // Verify m_pageControl object, if not nil release it. 
    if (nil != _pageControl)
    {
        [_pageControl release];
        _pageControl = nil;
    }
	// VANYUTH : END : 28/01/2014
	[super dealloc];
}

@end
