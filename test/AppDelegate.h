//
//  AppDelegate.h
//  test
//
//  Created by Khaled on 2014-09-08.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

